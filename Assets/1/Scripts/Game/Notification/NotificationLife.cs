﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class NotificationLife : MonoBehaviour {

	private float time = 2.5f;

	public Text notificationText;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		time -= Time.deltaTime;
		if(time <= 0)
			Destroy(gameObject);
	}

	public void SetNotificationText(string text)
	{
		notificationText.text = text;
	}

	public void SetNotificationText(string text, int fontSize)
	{
		notificationText.text = text;
		notificationText.fontSize = fontSize;
	}

    public class Factory : PlaceholderFactory<NotificationLife>
    {
    }
}
