﻿using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class ProfileMenuController : Menu {

    private IDBConnection dbConnection;
    private UserDataHolder userDataHolder;

    //ui
    public Button btnBack;
    public Button btnAddEnergy;

    public Text txtFirstName;
    public Text txtLastName;
    public Text txtEnergy;
    public Text txtPopularity;
    public Text txtPrestige;
    public Text txtRanking;
    public Text txtMoney;
    //

    [Inject]
    public void Construct(IDBConnection dbConnection)
    {
        this.dbConnection = dbConnection;
    }

    // Use this for initialization
    void Start () {
        userDataHolder = GameObject.FindGameObjectWithTag("UserDataHolder").GetComponent<UserDataHolder>();
        SetUIData();
        btnBack.onClick.AddListener(BtnBack);
    }
	
	private void BtnBack()
    {
        MenuController.DestroyMenu(gameObject);
    }

    private void SetUIData()
    {
        txtFirstName.text = userDataHolder.FirstName;
        txtLastName.text = userDataHolder.LastName;
        txtEnergy.text = userDataHolder.Energy.ToString();
        txtPopularity.text = userDataHolder.Popularity.ToString();
        txtPrestige.text = userDataHolder.Respectability.ToString();
        txtRanking.text = GetPlayerRank().ToString() + "#";
        txtMoney.text = Money.GetMoneyString(userDataHolder.Wealth);
    }

    private int GetPlayerRank()
    {
        dbConnection.OpenConnection();

        var query = "SELECT COUNT(*) FROM agents WHERE Wealth >= (SELECT Wealth FROM agents WHERE ID = 1)";
        var reader = dbConnection.DownloadQuery(query);
        reader.Read();
        var rankOfPlayer = reader.GetInt32(0);

        reader.Close();
        reader = null;

        dbConnection.CloseConnection();

        return rankOfPlayer;
    }

    public class Factory : PlaceholderFactory<ProfileMenuController>
    {
    }
}
