﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPlayerFinanceQueryMaker {
    string GetInsertQuery(IncomeExpenseObject incomeExpenseObject);
    string GetIncomeOrExpenseQuery(int incomeOrExpenceID);
    
    string GetAllIncomesAndExpensesQuery();
    string GetAllIncomesQuery();
    string GetAllExpensesQuery();
}
