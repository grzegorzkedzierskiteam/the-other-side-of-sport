﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPlayerFinanceManager {
    void AddIncomeOrExpense(IncomeExpenseObject incomeOrExpence);
    IncomeExpenseObject GetIncomeOrExpense(int incomeOrExpenceID);
    List<IncomeExpenseObject> GetAllIncomesAndExpenses();
    List<IncomeExpenseObject> GetAllIncomes();
    List<IncomeExpenseObject> GetAllExpenses();
}
