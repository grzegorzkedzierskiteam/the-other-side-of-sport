﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IncomeExpenseObject {

    public int ID { get; set; }
    public MyDateTime Date { get; set; }
    public int Value { get; set; }
    public int PersonID { get; set; }
    public string PersonName { get; set; }
    public string Info { get; set; }

    public IncomeExpenseObject(int id, int year, int week, int value, int personID, string personName, string info)
    {
        ID = id;
        Date = new MyDateTime(week, year);
        Value = value;
        PersonID = personID;
        PersonName = personName;
        Info = info;
    }
}
