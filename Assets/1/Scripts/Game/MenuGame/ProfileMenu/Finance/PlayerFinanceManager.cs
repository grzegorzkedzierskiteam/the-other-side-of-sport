﻿using Mono.Data.Sqlite;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFinanceManager : IPlayerFinanceManager
{
    private IDBConnection dbConnection;
    private IPlayerFinanceQueryMaker queryMaker;

    public PlayerFinanceManager(IDBConnection dbConnection, IPlayerFinanceQueryMaker queryMaker)
    {
        this.dbConnection = dbConnection;
        this.queryMaker = queryMaker;
    }

    public void AddIncomeOrExpense(IncomeExpenseObject incomeOrExpence)
    {
        string query = queryMaker.GetInsertQuery(incomeOrExpence);
        dbConnection.ModifyQuery(query);
    }

    public List<IncomeExpenseObject> GetAllExpenses()
    {
        string query = queryMaker.GetAllExpensesQuery();
        SqliteDataReader reader = dbConnection.DownloadQuery(query);
        return GetList(reader);
    }

    public List<IncomeExpenseObject> GetAllIncomes()
    {
        string query = queryMaker.GetAllIncomesQuery();
        SqliteDataReader reader = dbConnection.DownloadQuery(query);
        return GetList(reader);
    }

    public List<IncomeExpenseObject> GetAllIncomesAndExpenses()
    {
        string query = queryMaker.GetAllIncomesAndExpensesQuery();
        SqliteDataReader reader = dbConnection.DownloadQuery(query);
        return GetList(reader);
    }

    public IncomeExpenseObject GetIncomeOrExpense(int incomeOrExpenceID)
    {
        string query = queryMaker.GetIncomeOrExpenseQuery(incomeOrExpenceID);
        SqliteDataReader reader = dbConnection.DownloadQuery(query);
        return GetElement(reader);
    }

    private IncomeExpenseObject GetElement(SqliteDataReader reader)
    {
        if (reader.HasRows)
        {
            reader.Read();
            IncomeExpenseObject element = new IncomeExpenseObject(reader.GetInt32(0), reader.GetInt32(1), reader.GetInt32(2), reader.GetInt32(3), reader.GetInt32(4), reader.GetString(5), reader.GetString(6));
            return element;
        }
        else
        {
            reader.Close();
            reader = null;
            return null;
        }
    }

    private List<IncomeExpenseObject> GetList(SqliteDataReader reader)
    {
        if (reader.HasRows)
        {
            List<IncomeExpenseObject> list = new List<IncomeExpenseObject>();
            while (reader.Read())
            {
                IncomeExpenseObject element = new IncomeExpenseObject(reader.GetInt32(0), reader.GetInt32(1), reader.GetInt32(2), reader.GetInt32(3), reader.GetInt32(4), reader.GetString(5), reader.GetString(6));
                list.Add(element);
            }

            return list;
        }
        else
        {
            reader.Close();
            reader = null;
            return null;
        }
    }
}
