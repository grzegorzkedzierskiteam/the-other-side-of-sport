﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFinanceQueryMaker : IPlayerFinanceQueryMaker {

    private const string columns = "ID,Week,Year,Value,PersonID,PersonName,Info";
    private const string columnsWithoutID = "Week,Year,Value,PersonID,PersonName,Info";

    public string GetAllExpensesQuery()
    {
        string query = "SELECT " + columns + " FROM finance WHERE Value < 0;";

        return query;
    }

    public string GetAllIncomesAndExpensesQuery()
    {
        string query = "SELECT " + columns + " FROM finance;";

        return query;
    }

    public string GetAllIncomesQuery()
    {
        string query = "SELECT " + columns + " FROM finance WHERE Value >= 0;";

        return query;
    }

    public string GetIncomeOrExpenseQuery(int incomeOrExpenceID)
    {
        string query = "SELECT " + columns + " FROM finance WHERE ID = " + incomeOrExpenceID + ";";
        return query;
    }

    public string GetInsertQuery(IncomeExpenseObject incomeExpenseObject)
    {
        string query = "INSERT INTO finance(" + columnsWithoutID + ") VALUES(" +
                        incomeExpenseObject.Date.Week + "," +
                        incomeExpenseObject.Date.Year + "," +
                        incomeExpenseObject.Value + "," +
                        incomeExpenseObject.PersonID + "," +
                        "'" + incomeExpenseObject.PersonName + "'," +
                        "'" + incomeExpenseObject.Info + "');";
        return query;
    }
}
