﻿using Messages;
using UnityEngine;

namespace Messages
{
    public class MessageEffects : IMessageEffects
    {
        private readonly MenuController menuController;
        private readonly IMessageManagerDB messageManagerDb;
        private readonly IPlayerEnergy playerEnergy;

        public MessageEffects(MenuController menuController, IMessageManagerDB messageManagerDb,
            IPlayerEnergy playerEnergy)
        {
            this.menuController = menuController;
            this.messageManagerDb = messageManagerDb;
            this.playerEnergy = playerEnergy;
        }

        public bool FiresAthlete(IDBConnection dbConnection, ClientObject clientObject, int chance)
        {
            if (!clientObject.HasTeam) return false;
            var willHeBeFired = Random.Range(0, chance);

            if (willHeBeFired < 10)
            {
                MessageDeletingEffects.DeletePlayerFromTeam(dbConnection, clientObject.ID);
                var textOfNotification = "Your client " + clientObject.FullName + " has been fired from " +
                                         clientObject.Team + " for violating his agreement with them.";
                AddNotification(dbConnection, textOfNotification, clientObject.ID, clientObject.FullName);
                return true;
            }

            return false;
        }

        public bool GoesToPrison(IDBConnection dbConnection, ClientObject clientObject, int chance)
        {
            var hasBeenAcquitted = Random.Range(0, chance);

            if (hasBeenAcquitted < 5)
            {
                var textOfNotification =
                    "Your client " + clientObject.FullName + " has been acquitted of domestic violence.";
                AddNotification(dbConnection, textOfNotification, clientObject.ID, clientObject.FullName);
            }
            else
            {
                var isHeGoingToPrison = Random.Range(0, chance);

                if (isHeGoingToPrison < 5)
                {
                    var textOfNotification = "Your client " + clientObject.FullName +
                                             " has been arrested for domestic violence. He's going to a prison.";
                    AddNotification(dbConnection, textOfNotification, clientObject.ID, clientObject.FullName);
                    ClientData.DeletePlayer(dbConnection, clientObject.ID);
                }

                return true;
            }

            return false;
        }

        public bool ChangeOverallSkill(IDBConnection dbConnection, ClientObject clientObject, int maxDifference)
        {
            int difference;
            if (maxDifference > 0)
                difference = Random.Range(0, maxDifference);
            else if (maxDifference < 0)
                difference = Random.Range(maxDifference, 1);
            else
                return false;

            AthleteStatUpdateManager.ChangeOverallSkill(dbConnection, clientObject, difference);

            return difference != 0;
        }

        public void Help(IDBConnection dbConnection, IMessage message)
        {
            var energyCost = message.MessageResponses.Find(x => x.TypeOfResponse == TypeOfResponse.Help)
                .ListOfCost.Find(x => x.Property == PropertyEnum.Energy).Value;

            IClientObject clientObject = ClientData.GetAllDetails(dbConnection, message.ParticipantID);

            var relationDifference = message.MessageResponses.Find(x => x.TypeOfResponse == TypeOfResponse.Help)
                .ListOfCost.Find(x => x.Property == PropertyEnum.Relation).Value;

            RelationManager.ChangeRelationWithClient(dbConnection, clientObject, relationDifference);

            playerEnergy.ChangePlayerEnergy(dbConnection, energyCost);
        }

        public void Payment(IDBConnection dbConnection, IMessage message)
        {
            var moneyCost = message.MessageResponses.Find(x => x.TypeOfResponse == TypeOfResponse.Payment)
                                .ListOfCost.Find(x => x.Property == PropertyEnum.Money).Value * -1;

            var energyCost = message.MessageResponses.Find(x => x.TypeOfResponse == TypeOfResponse.Payment)
                .ListOfCost.Find(x => x.Property == PropertyEnum.Energy).Value;

            var relationDifference = message.MessageResponses.Find(x => x.TypeOfResponse == TypeOfResponse.Payment)
                .ListOfCost.Find(x => x.Property == PropertyEnum.Relation).Value;

            IClientObject clientObject = ClientData.GetAllDetails(dbConnection, message.ParticipantID);
            Money.AthleteWastesMoney(dbConnection, moneyCost, clientObject);

            RelationManager.ChangeRelationWithClient(dbConnection, clientObject, relationDifference);

            playerEnergy.ChangePlayerEnergy(dbConnection, energyCost);
        }

        public void Talk(IDBConnection dbConnection, IMessage message)
        {
            var energyCost = message.MessageResponses.Find(x => x.TypeOfResponse == TypeOfResponse.Talk)
                .ListOfCost.Find(x => x.Property == PropertyEnum.Energy).Value;

            var relationDifference = message.MessageResponses.Find(x => x.TypeOfResponse == TypeOfResponse.Talk)
                .ListOfCost.Find(x => x.Property == PropertyEnum.Relation).Value;

            IClientObject clientObject = ClientData.GetAllDetails(dbConnection, message.ParticipantID);
            RelationManager.ChangeRelationWithClient(dbConnection, clientObject, relationDifference);

            playerEnergy.ChangePlayerEnergy(dbConnection, energyCost);
        }

        private void AddNotification(IDBConnection dbConnection, string textOfNotification, int participantID, string participantName)
        {
            var query = "SELECT Week, Year FROM config;";
            var reader = dbConnection.DownloadQuery(query);
            reader.Read();
            var currentTime = new MyDateTime(reader.GetInt32(0), reader.GetInt32(1));

            reader.Close();
            reader = null;

            var notification = new Notification(0, textOfNotification, 500, participantID, participantName,
                PersonType.Athlete, currentTime);
            messageManagerDb.SetUpDate(currentTime);
            messageManagerDb.AddMessageToDB(dbConnection, notification);
        }
    }
}