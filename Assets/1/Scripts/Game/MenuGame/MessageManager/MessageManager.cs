﻿using System;
using System.Collections;
using System.Collections.Generic;
using Messages;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Messages
{
    public class MessageManager : IMessageManager
    {
        private readonly IMessageEffects messageEffects;

        public MessageManager(IMessageEffects messageEffects)
        {
            this.messageEffects = messageEffects;
        }

        public void Respond(IDBConnection dbConnection, IMessage message, TypeOfResponse typeOfResponse)
        {
            switch (typeOfResponse)
            {
                case TypeOfResponse.Talk:
                    messageEffects.Talk(dbConnection, message);
                    break;

                case TypeOfResponse.Payment:
                    messageEffects.Payment(dbConnection, message);
                    break;

                case TypeOfResponse.Help:
                    messageEffects.Help(dbConnection, message);
                    break;
            }

            Delete(dbConnection, message.ID);
        }

        public void DeleteMessage(IDBConnection dbConnection, IMessage message)
        {
            var client = ClientData.GetAllDetails(dbConnection, message.ParticipantID);
            int penalty;

            switch (message.TypeOfMessage)
            {
                case TypeOfMessage.AccusationOfDoping:
                    penalty = Random.Range(100000, 500000);
                    MessageDeletingEffects.PayPenalty(dbConnection, client.ID, penalty);
                    MessageDeletingEffects.DecreasePopularity(dbConnection, client.ID, PersonType.Athlete, 1, 10);
                    messageEffects.FiresAthlete(dbConnection, client, 20);
                    break;

                case TypeOfMessage.AccusationOfDomesticViolence:
                    penalty = Random.Range(10000, 50000);
                    MessageDeletingEffects.PayPenalty(dbConnection, client.ID, penalty);
                    MessageDeletingEffects.DecreasePopularity(dbConnection, client.ID, PersonType.Athlete, 1, 10);
                    messageEffects.FiresAthlete(dbConnection, client, 20);
                    messageEffects.GoesToPrison(dbConnection, client, 20);
                    break;

                case TypeOfMessage.AccusationOfHavingDrugs:
                    penalty = Random.Range(50000, 100000);
                    MessageDeletingEffects.PayPenalty(dbConnection, client.ID, penalty);
                    MessageDeletingEffects.DecreasePopularity(dbConnection, client.ID, PersonType.Athlete, 1, 10);
                    messageEffects.FiresAthlete(dbConnection, client, 20);
                    messageEffects.GoesToPrison(dbConnection, client, 20);
                    break;

                case TypeOfMessage.AccusationOfSexualAssault:
                    penalty = Random.Range(100000, 500000);
                    MessageDeletingEffects.PayPenalty(dbConnection, client.ID, penalty);
                    MessageDeletingEffects.DecreasePopularity(dbConnection, client.ID, PersonType.Athlete, 1, 10);
                    messageEffects.FiresAthlete(dbConnection, client, 20);
                    messageEffects.GoesToPrison(dbConnection, client, 20);
                    break;

                case TypeOfMessage.Assault:
                    penalty = Random.Range(10000, 50000);
                    MessageDeletingEffects.PayPenalty(dbConnection, client.ID, penalty);
                    MessageDeletingEffects.DecreasePopularity(dbConnection, client.ID, PersonType.Athlete, 1, 10);
                    messageEffects.FiresAthlete(dbConnection, client, 20);
                    messageEffects.GoesToPrison(dbConnection, client, 20);
                    break;

                case TypeOfMessage.HealthProblemsByStupidity:
                    messageEffects.FiresAthlete(dbConnection, client, 12);
                    break;

                case TypeOfMessage.PersonalProblems:
                    messageEffects.ChangeOverallSkill(dbConnection, client, -6);
                    break;
            }


            var relationDifference = Random.Range(-8, 0);
            RelationManager.ChangeRelationWithClient(dbConnection, client, relationDifference);

            Delete(dbConnection, message.ID);
        }

        private void Delete(IDBConnection dBConnection, int messageID)
        {
            string query = "DELETE FROM messages WHERE ID =" + messageID + ";" +
                           "DELETE FROM messages_responses WHERE Message_ID = " + messageID + ";";
            dBConnection.ModifyQuery(query);
        }
    }
}