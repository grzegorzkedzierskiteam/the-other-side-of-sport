﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMessageManager
{
    void Respond(IDBConnection dbConnection, IMessage message, TypeOfResponse typeOfResponse);
    void DeleteMessage(IDBConnection dbConnection, IMessage message);
}
