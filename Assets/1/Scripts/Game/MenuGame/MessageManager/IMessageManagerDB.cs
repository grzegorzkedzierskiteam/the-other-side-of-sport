﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMessageManagerDB
{
    void SetUpDate(MyDateTime currentDateTime);
    void AddMessageToDB(IDBConnection dbConnection, IMessage message);
    List<IMessage> GetAllMessagesFromDB(IDBConnection dbConnection);
}
