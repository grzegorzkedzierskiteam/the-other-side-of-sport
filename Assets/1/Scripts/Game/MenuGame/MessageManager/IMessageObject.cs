﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMessageObject { 
    int ID { get; set; }
    string Description { get; set; }
    int ExpiringTime { get; set; }
    bool DoesHaveEffect { get; set; }
    int ParticipantID { get; set; }
    string ParticipantName { get; set; }
    PersonType ParticipantType { get; set; }
    TypeOfMessage TypeOfMessage { get; set; }
    MyDateTime DateTime { get; set; }
    List<MessageResponse> MessageResponses { get; set; }
}
