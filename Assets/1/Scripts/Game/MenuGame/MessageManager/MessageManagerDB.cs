﻿using System;
using System.Collections.Generic;
using System.Linq;
using Debug = UnityEngine.Debug;

public class MessageManagerDB : IMessageManagerDB
{
    private MenuController menuController;
    private MyDateTime currentDateTime;

    public MessageManagerDB(MenuController menuController)
    {
        this.menuController = menuController;   
    }

    public void SetUpDate(MyDateTime currentDateTime)
    {
        this.currentDateTime = currentDateTime;
    }

    public void AddMessageToDB(IDBConnection dbConnection, IMessage message)
    {
        var query = "INSERT INTO messages (Description, ExpiringTime, DoesHaveEffect, ParticipantID, ParticipantName, ParticipantType, TypeOfMessage, Week, Year) " +
                        "VALUES(\"" + message.Description + "\", "
                                    + message.ExpiringTime + ", "
                                    + Convert.ToInt32(message.DoesHaveEffect) + ", "
                                    + message.ParticipantID + ", \""
                                    + message.ParticipantName + "\", \""
                                    + Enum.GetName(typeof(PersonType), message.ParticipantType) + "\", \""
                                    + Enum.GetName(typeof(TypeOfMessage), message.TypeOfMessage) + "\"," +
                                    +message.DateTime.Week + "," +
                                    +message.DateTime.Year + ");";

        dbConnection.ModifyQuery(query);


        if (message.TypeOfMessage == TypeOfMessage.Notification) return;

        query = "SELECT ID FROM messages ORDER BY ID DESC LIMIT 1;";
        var reader = dbConnection.DownloadQuery(query);

        reader.Read();
        var messageID = reader.GetInt32(0);
        reader.Close();

        query = "";

        foreach (var messageResponse in message.MessageResponses)
        {
            foreach (var cost in messageResponse.ListOfCost)
            {
                query += "INSERT INTO messages_responses (Message_ID, PropertyString, Value, TypeOfResponse)" +
                         "VALUES(" + messageID + "," +
                         "\"" + cost.Property.ToString() + "\"," +
                         cost.Value + "," +
                         "\"" + messageResponse.TypeOfResponse.ToString() + "\");";
            }
        }

        if (query != "")
            dbConnection.ModifyQuery(query);
    }

    public List<IMessage> GetAllMessagesFromDB(IDBConnection dbConnection)
    {
        var query = "SELECT ID, Description, ExpiringTime, DoesHaveEffect, ParticipantID, ParticipantName, ParticipantType, TypeOfMessage, Week, Year FROM messages ORDER BY Year, Week;";
        var reader = dbConnection.DownloadQuery(query);

        var listOfMessages = new List<IMessage>();

        if (!reader.HasRows) return listOfMessages;

        while (reader.Read())
        {
            var args = new object[]
            {
                reader.GetInt32(0),
                reader.GetString(1),
                reader.GetInt32(2),
                reader.GetInt32(4),
                reader.GetString(5),
                (PersonType)Enum.Parse(typeof(PersonType), reader.GetString(6)),
                new MyDateTime(reader.GetInt32(8), reader.GetInt32(9))
            };

            if ((TypeOfMessage) Enum.Parse(typeof(TypeOfMessage), reader.GetString(7)) !=
                TypeOfMessage.Notification)
            {
                Array.Resize(ref args, 8);
                args[7] = GetMessageResponseList(dbConnection, reader.GetInt32(0));
            }

            var typeString = $"Messages.{reader.GetString(7)}, Assembly-CSharp";
            var typeOfMessage = Type.GetType(typeString);

            var obj = (IMessage)Activator.CreateInstance(typeOfMessage,args);

            listOfMessages.Add(obj);
        }

        reader.Close();

        return listOfMessages;

    }

    private List<MessageResponse> GetMessageResponseList(IDBConnection dbConnection, int messageID)
    {
        var listOfResponses = new List<MessageResponse>();

        var query = "SELECT PropertyString, Value, TypeOfResponse FROM messages_responses WHERE Message_ID =" + messageID + ";";
        var reader = dbConnection.DownloadQuery(query);
        while (reader.Read())
        {
            var typeOfResponse = (TypeOfResponse)Enum.Parse(typeof(TypeOfResponse), reader.GetString(2));

            if (listOfResponses.Any(x => x.TypeOfResponse == typeOfResponse))
            {
                var responseCost = new ResponseCost(reader.GetInt32(1), (PropertyEnum)Enum.Parse(typeof(PropertyEnum), reader.GetString(0)));

                listOfResponses.Find(x => x.TypeOfResponse == typeOfResponse).ListOfCost.Add(responseCost);
            }
            else
            {
                listOfResponses.Add(new MessageResponse(typeOfResponse, new List<ResponseCost>()));

                var responseCost = new ResponseCost(reader.GetInt32(1), (PropertyEnum)Enum.Parse(typeof(PropertyEnum), reader.GetString(0)));

                listOfResponses.Find(x => x.TypeOfResponse == typeOfResponse).ListOfCost.Add(responseCost);
            }
        }

        reader.Close();

        return listOfResponses;
    }
}
