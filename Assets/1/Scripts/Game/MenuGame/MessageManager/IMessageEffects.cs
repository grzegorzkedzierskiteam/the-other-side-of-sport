﻿public interface IMessageEffects
{
    bool FiresAthlete(IDBConnection dbConnection, ClientObject clientObject, int chance);
    bool GoesToPrison(IDBConnection dbConnection, ClientObject clientObject, int chance);
    bool ChangeOverallSkill(IDBConnection dbConnection, ClientObject clientObject, int maxDifference);
    void Talk(IDBConnection dbConnection, IMessage message);
    void Payment(IDBConnection dbConnection, IMessage message);
    void Help(IDBConnection dbConnection, IMessage message);
}
