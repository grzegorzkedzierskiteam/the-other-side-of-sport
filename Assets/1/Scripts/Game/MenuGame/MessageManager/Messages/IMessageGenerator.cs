﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMessageGenerator {
    IMessage GetMessage(IPerson person, TypeOfMessage typeOfMessage, MyDateTime dateTime);
}
