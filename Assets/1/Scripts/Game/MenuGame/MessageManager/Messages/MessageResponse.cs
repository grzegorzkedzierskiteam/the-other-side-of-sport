﻿using System.Collections.Generic;
using UnityEngine;

public class MessageResponse : IMessageResponse
{
    public TypeOfResponse TypeOfResponse { get; set; }
    public List<ResponseCost> ListOfCost { get; set; }

    public MessageResponse(TypeOfResponse typeOfResponse, List<ResponseCost> listOfResponse)
    {
        TypeOfResponse = typeOfResponse;
        ListOfCost = listOfResponse;
    }
}
