﻿public interface IMessage : IMessageObject
{
    void GenerateNewMessage(IDBConnection dbConnection, InjuriesController injuriesController);
}
