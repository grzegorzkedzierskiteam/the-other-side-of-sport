﻿using Mono.Data.Sqlite;
using UnityEngine;

namespace Messages
{
    public static class MessageDeletingEffects
    {
        public static void DecreasePopularity(IDBConnection dBConnection, int participantID, PersonType personType, int min, int max)
        {
            var table = GetNameOfTable(personType);

            var query = "SELECT Popularity FROM " + table + " WHERE ID = " + participantID + ";";
            var reader = dBConnection.DownloadQuery(query);
            reader.Read();
            var popularity = reader.GetInt32(0);
            reader.Close();
            reader = null;

            var decreasingValue = Random.Range(min, max);
            popularity = GetDecreasedPopularity(popularity, decreasingValue);

            query = "UPDATE " + table + " SET Popularity = " + popularity + " WHERE ID = " + participantID + ";";
            dBConnection.ModifyQuery(query);
        }

        public static void DeletePlayerFromTeam(IDBConnection dBConnection, int participantID)
        {
            var query = "UPDATE players SET Team = 'None' WHERE ID = " + participantID + ";";
            dBConnection.ModifyQuery(query);
        }

        public static void PayPenalty(IDBConnection dBConnection, int participantID, int penalty)
        {
            var clientObject = ClientData.GetAllDetails(dBConnection, participantID);
            Money.AthleteWastesMoney(dBConnection, penalty, clientObject);
        }

        private static int GetDecreasedPopularity(int popularity, int decreasingValue)
        {
            popularity -= decreasingValue;

            if (popularity < 0)
                popularity = 0;

            return popularity;
        }

        private static string GetNameOfTable(PersonType personType)
        {
            if (personType == PersonType.Agent)
                return "agents";
            else
                return "players";
        }
    }
}

