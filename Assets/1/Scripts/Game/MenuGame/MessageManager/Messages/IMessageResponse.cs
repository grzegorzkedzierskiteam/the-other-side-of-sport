﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMessageResponse {

	TypeOfResponse TypeOfResponse { get; set; }
    List<ResponseCost> ListOfCost { get; set; }
}
