﻿using Zenject;

namespace Messages
{
    public abstract class Message 
    {
        public Message()
        {
            
        }

        protected virtual void Delete(IDBConnection dBConnection)
        {

        }

        protected virtual void AddNotification(IDBConnection dBConnection, string textOfNotification, int participantID, string participantName)
        {

        }
    }
}
