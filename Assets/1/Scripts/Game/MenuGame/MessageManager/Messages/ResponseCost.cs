﻿using System.Text;
using UnityEngine;

public class ResponseCost
{
    public int Value { get; set; }
    public string ValueString { get; set; }
    public PropertyEnum Property { get; set; }
    public Object PrefabOfResponseCost { get; set; }

    public ResponseCost(int value, PropertyEnum property)
    {
        Value = value;
        ValueString = SetValueString(value, property);
        PrefabOfResponseCost = SetPrefabOfResponseCost(property);
        Property = property;
    }

    private string SetValueString(int value, PropertyEnum property)
    {
        switch (property)
        {
            case PropertyEnum.Energy:
                return new StringBuilder(value.ToString()).Append("%").ToString();

            case PropertyEnum.Health:
                return new StringBuilder(value.ToString()).Append("%").ToString();

            case PropertyEnum.Money:
                return Money.GetShortMoneyString(value);

            case PropertyEnum.Relation:
                return new StringBuilder(value.ToString()).Append("%").ToString();

            default:
                throw new System.Exception("Prefab of this property doesn't exixt.");
        }
    }

    private Object SetPrefabOfResponseCost(PropertyEnum propertyEnum)
    {
        switch (propertyEnum)
        {
            case PropertyEnum.Energy:
                return Resources.Load("ResponseCostsPrefabs/EnergyCost");
            case PropertyEnum.Money:
                return Resources.Load("ResponseCostsPrefabs/MoneyCost");
            case PropertyEnum.Relation:
                return Resources.Load("ResponseCostsPrefabs/RelationCost");

            default:
                throw new System.Exception("Prefab of this property doesn't exixt.");
        }
    }
}
