﻿using System;
using System.Collections.Generic;
using Random = UnityEngine.Random;

namespace Messages
{
    public class MessageGenerator : IMessageGenerator
    {
        private readonly IDBConnection dBConnection;
        private readonly InjuriesController injuriesController;

        public MessageGenerator(IDBConnection dBConnection, InjuriesController injuriesController)
        {
            this.dBConnection = dBConnection;
            this.injuriesController = injuriesController;
        }

        public IMessage GetMessage(IPerson person, TypeOfMessage typeOfMessage, MyDateTime dateTime)
        {
            var args = new object[]
            {
                0,
                null,
                0,
                person.ID,
                person.FullName,
                PersonType.Athlete,
                dateTime,
                null
            };

            var typeString = $"Messages.{typeOfMessage.ToString()}, Assembly-CSharp";
            var type = Type.GetType(typeString);

            var obj = (IMessage)Activator.CreateInstance(type, args);
            obj.GenerateNewMessage(dBConnection, injuriesController);

            return obj;
        }
    }
}
