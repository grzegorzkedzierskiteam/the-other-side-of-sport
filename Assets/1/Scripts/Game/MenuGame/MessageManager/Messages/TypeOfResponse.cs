﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TypeOfResponse {
    Payment, Talk, Promise, Help
}
