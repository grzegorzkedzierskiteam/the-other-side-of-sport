﻿using System.Collections.Generic;
using UnityEngine;

namespace Messages
{
    public class HealthProblemsByStupidity : IMessage
    {
        public int ID { get; set; }
        public string Description { get; set; }
        public int ExpiringTime { get; set; }
        public bool DoesHaveEffect { get; set; }
        public int ParticipantID { get; set; }
        public string ParticipantName { get; set; }
        public PersonType ParticipantType { get; set; }
        public TypeOfMessage TypeOfMessage { get; set; }
        public MyDateTime DateTime { get; set; }
        public List<MessageResponse> MessageResponses { get; set; }

        public HealthProblemsByStupidity(int messageID, string description, int expiringTime, int participantID, string participantName, PersonType participantType, MyDateTime dateTime, List<MessageResponse> messageResponses)
        {
            ID = messageID;
            Description = description;
            ExpiringTime = expiringTime;
            DoesHaveEffect = true;
            ParticipantID = participantID;
            ParticipantName = participantName;
            ParticipantType = participantType;
            TypeOfMessage = TypeOfMessage.HealthProblemsByStupidity;
            DateTime = dateTime;
            MessageResponses = messageResponses;
        }

        public void GenerateNewMessage(IDBConnection dbConnection, InjuriesController injuriesController)
        {
            var descriptions = new List<string>()
            {
                "Your Client " + ParticipantName + " took part in a paintball event and got injured.",
                "'Hey I have a problem. While I was skydiving I got injured.'",
                "'Man I had an accident while motorcycle riding. I'm injured.'"
            };

            Description = descriptions[Random.Range(0, descriptions.Count)];

            MessageResponses = new List<MessageResponse>();

            var talk = new MessageResponse(TypeOfResponse.Talk, new List<ResponseCost>()
            {
                new ResponseCost(Random.Range(-60, -20), PropertyEnum.Energy),
                new ResponseCost(Random.Range(5, 10), PropertyEnum.Relation)
            });

            MessageResponses.Add(talk);

            ExpiringTime = Random.Range(1, 3);

            var popularityDifference = Random.Range(-10, -3);
            var overallSkillDifference = Random.Range(-10, -1);

            var clientObject = ClientData.GetAllDetails(dbConnection, ParticipantID);

            AthleteStatUpdateManager.ChangePopularity(dbConnection, clientObject, popularityDifference);
            AthleteStatUpdateManager.ChangeOverallSkill(dbConnection, clientObject, overallSkillDifference);

            var injury = injuriesController.GenerateInjury();

            var query = "UPDATE players SET InjuryName = '" + injury.InjuryName + "', InjuryTime = " + injury.InjuryTime + " WHERE ID = " + ID + " AND InjuryTime < " + injury.InjuryTime + ";";
            dbConnection.ModifyQuery(query);
        }
    }
}

