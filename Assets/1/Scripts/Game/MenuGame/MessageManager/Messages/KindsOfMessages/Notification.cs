﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Messages
{
    public class Notification : IMessage
    {
        public int ID { get; set; }
        public string Description { get; set; }
        public int ExpiringTime { get; set; }
        public bool DoesHaveEffect { get; set; }
        public int ParticipantID { get; set; }
        public string ParticipantName { get; set; }
        public PersonType ParticipantType { get; set; }
        public TypeOfMessage TypeOfMessage { get; set; }
        public MyDateTime DateTime { get; set; }
        public List<MessageResponse> MessageResponses { get; set; }

        public Notification(int messageID, string description, int expiringTime, int participantID, string participantName, PersonType participantType, MyDateTime dateTime) 
        {
            ID = messageID;
            Description = description;
            ExpiringTime = expiringTime;
            DoesHaveEffect = false;
            ParticipantID = participantID;
            ParticipantName = participantName;
            ParticipantType = participantType;
            TypeOfMessage = TypeOfMessage.Notification;
            DateTime = dateTime;
        }

        public void GenerateNewMessage(IDBConnection dbConnection, InjuriesController injuriesController)
        {
            throw new Exception("You cannot generate a notification just like other messages.");
        }
    }
}
