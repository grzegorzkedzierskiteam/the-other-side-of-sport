﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Messages
{
    public class PersonalProblems : IMessage {

        public int ID { get; set; }
        public string Description { get; set; }
        public int ExpiringTime { get; set; }
        public bool DoesHaveEffect { get; set; }
        public int ParticipantID { get; set; }
        public string ParticipantName { get; set; }
        public PersonType ParticipantType { get; set; }
        public TypeOfMessage TypeOfMessage { get; set; }
        public MyDateTime DateTime { get; set; }
        public List<MessageResponse> MessageResponses { get; set; }

        public PersonalProblems(int messageID, string description, int expiringTime, int participantID, string participantName, PersonType participantType, MyDateTime dateTime, List<MessageResponse> messageResponses)
        {
            ID = messageID;
            Description = description;
            ExpiringTime = expiringTime;
            DoesHaveEffect = true;
            ParticipantID = participantID;
            ParticipantName = participantName;
            ParticipantType = participantType;
            TypeOfMessage = TypeOfMessage.PersonalProblems;
            DateTime = dateTime;
            MessageResponses = messageResponses;
        }

        public void GenerateNewMessage(IDBConnection dbConnection, InjuriesController injuriesController)
        {
            var descriptions = new List<string>()
            {
                "Personal problems",
                "'Personal problems'",
                "'Personal problems'"
            };

            var selectedDescription = Random.Range(0, descriptions.Count);

            var messageResponses = new List<MessageResponse>();

            var help = new MessageResponse(TypeOfResponse.Help, new List<ResponseCost>()
            {
                new ResponseCost(Random.Range(-70, -30), PropertyEnum.Energy),
                new ResponseCost(Random.Range(5, 11), PropertyEnum.Relation)
            });
            messageResponses.Add(help);
            

            Description = descriptions[selectedDescription];
            ExpiringTime = Random.Range(1, 3);
            MessageResponses = messageResponses;
        }
    }
}
