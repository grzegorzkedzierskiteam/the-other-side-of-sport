﻿using Messages;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Messages
{
    public class HealthProblems : IMessage
    {
        public int ID { get; set; }
        public string Description { get; set; }
        public int ExpiringTime { get; set; }
        public bool DoesHaveEffect { get; set; }
        public int ParticipantID { get; set; }
        public string ParticipantName { get; set; }
        public PersonType ParticipantType { get; set; }
        public TypeOfMessage TypeOfMessage { get; set; }
        public MyDateTime DateTime { get; set; }
        public List<MessageResponse> MessageResponses { get; set; }

        public HealthProblems(int messageID, string description, int expiringTime, int participantID, string participantName, PersonType participantType, MyDateTime dateTime, List<MessageResponse> messageResponses)
        {
            ID = messageID;
            Description = description;
            ExpiringTime = expiringTime;
            DoesHaveEffect = false;
            ParticipantID = participantID;
            ParticipantName = participantName;
            ParticipantType = participantType;
            TypeOfMessage = TypeOfMessage.HealthProblems;
            DateTime = dateTime;
            MessageResponses = messageResponses;
        }

        public void GenerateNewMessage(IDBConnection dbConnection, InjuriesController injuriesController)
        {
            var descriptions = new List<string>()
            {
                "'Man I have bad news. I've had an accident and got injured.'",
                "'I've had an accident at training and I've got injured.'",
                "Your client " + ParticipantName + " was hit by a car last night. He's injured."
            };

            var selectedDescription = Random.Range(0, descriptions.Count);

            var injury = injuriesController.GenerateInjury();

            var popularityDifference = Random.Range(-10, -3);
            var overallSkillDifference = Random.Range(-10, -1);

            IClientObject clientObject = ClientData.GetAllDetails(dbConnection, ParticipantID);

            AthleteStatUpdateManager.ChangePopularity(dbConnection, clientObject, popularityDifference);
            AthleteStatUpdateManager.ChangeOverallSkill(dbConnection, clientObject, overallSkillDifference);

            var query = "UPDATE players SET InjuryName = '" + injury.InjuryName + "', InjuryTime = " + injury.InjuryTime + " WHERE ID = " + ParticipantID + " AND InjuryTime < " + injury.InjuryTime + ";";
            dbConnection.ModifyQuery(query);

            Description = descriptions[selectedDescription];
            ExpiringTime = Random.Range(1,3);
            MessageResponses = new List<MessageResponse>();
        }
    }
}

