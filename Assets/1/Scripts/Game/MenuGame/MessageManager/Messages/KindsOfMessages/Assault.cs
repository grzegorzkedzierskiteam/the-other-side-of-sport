﻿using System.Collections.Generic;
using UnityEngine;

namespace Messages
{
    public class Assault : IMessage
    {
        public int ID { get; set; }
        public string Description { get; set; }
        public int ExpiringTime { get; set; }
        public bool DoesHaveEffect { get; set; }
        public int ParticipantID { get; set; }
        public string ParticipantName { get; set; }
        public PersonType ParticipantType { get; set; }
        public TypeOfMessage TypeOfMessage { get; set; }
        public MyDateTime DateTime { get; set; }
        public List<MessageResponse> MessageResponses { get; set; }

        public Assault(int messageID, string description, int expiringTime, int participantID, string participantName, PersonType participantType, MyDateTime dateTime, List<MessageResponse> messageResponses)
        {
            ID = messageID;
            Description = description;
            ExpiringTime = expiringTime;
            DoesHaveEffect = true;
            ParticipantID = participantID;
            ParticipantName = participantName;
            ParticipantType = participantType;
            TypeOfMessage = TypeOfMessage.Assault;
            DateTime = dateTime;
            MessageResponses = messageResponses;
        }

        public void GenerateNewMessage(IDBConnection dbConnection, InjuriesController injuriesController)
        {
            var descriptions = new List<string>()
            {
                "'Man i beat someone up in a club. I need your help.'",
                "'I need your help. I`m so fucking stupid. I hit little motherfucker in club last night.'",
                "Your client " + ParticipantName + " is accused of assault. He beat a man up in a club last night."
            };

            var selectedDescription = Random.Range(0, descriptions.Count);

            var messageResponses = new List<MessageResponse>();
            var payment = new MessageResponse(TypeOfResponse.Payment, new List<ResponseCost>()
            {
                new ResponseCost(Random.Range(-500000, -10000), PropertyEnum.Money),
                new ResponseCost(Random.Range(-20, -5), PropertyEnum.Energy),
                new ResponseCost(Random.Range(-10, -5), PropertyEnum.Relation)
            });

            var talk = new MessageResponse(TypeOfResponse.Talk, new List<ResponseCost>()
            {
                new ResponseCost(Random.Range(-40, -10), PropertyEnum.Energy),
                new ResponseCost(Random.Range(2, 5), PropertyEnum.Relation)
            });

            messageResponses.Add(payment);
            messageResponses.Add(talk);

            Description = descriptions[selectedDescription];
            ExpiringTime = Random.Range(2, 9);
            MessageResponses = messageResponses;
        }
    }
}

