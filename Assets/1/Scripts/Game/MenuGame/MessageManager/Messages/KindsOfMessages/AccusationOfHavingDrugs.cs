﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Messages
{
    class AccusationOfHavingDrugs : IMessage
    {
        public int ID { get; set; }
        public string Description { get; set; }
        public int ExpiringTime { get; set; }
        public bool DoesHaveEffect { get; set; }
        public int ParticipantID { get; set; }
        public string ParticipantName { get; set; }
        public PersonType ParticipantType { get; set; }
        public TypeOfMessage TypeOfMessage { get; set; }
        public MyDateTime DateTime { get; set; }
        public List<MessageResponse> MessageResponses { get; set; }

        public AccusationOfHavingDrugs(int messageID, string description, int expiringTime, int participantID, string participantName, PersonType participantType, MyDateTime dateTime, List<MessageResponse> messageResponses)
        {
            ID = messageID;
            Description = description;
            ExpiringTime = expiringTime;
            DoesHaveEffect = true;
            ParticipantID = participantID;
            ParticipantName = participantName;
            ParticipantType = participantType;
            TypeOfMessage = TypeOfMessage.AccusationOfHavingDrugs;
            DateTime = dateTime;
            MessageResponses = messageResponses;
        }

        public void GenerateNewMessage(IDBConnection dbConnection, InjuriesController injuriesController)
        {
            MessageDeletingEffects.DecreasePopularity(dbConnection, ParticipantID, PersonType.Athlete, 4, 12);

            var descriptions = new List<string>()
            {
                "'Drugs'",
                "'Drugs'",
                "Drugs."
            };

            var selectedDescription = Random.Range(0, descriptions.Count);

            var messageResponses = new List<MessageResponse>();
            var payment = new MessageResponse(TypeOfResponse.Payment, new List<ResponseCost>()
            {
                new ResponseCost(Random.Range(-100000, -50000), PropertyEnum.Money),
                new ResponseCost(Random.Range(-30, -15), PropertyEnum.Energy),
                new ResponseCost(Random.Range(-16, -10), PropertyEnum.Relation)
            });

            var talk = new MessageResponse(TypeOfResponse.Talk, new List<ResponseCost>()
            {
                new ResponseCost(Random.Range(-60, -50), PropertyEnum.Energy),
                new ResponseCost(Random.Range(7, 13), PropertyEnum.Relation)
            });

            messageResponses.Add(payment);
            messageResponses.Add(talk);

            Description = descriptions[selectedDescription];
            ExpiringTime = Random.Range(2, 9);
            MessageResponses = messageResponses;
        }
    }
}
