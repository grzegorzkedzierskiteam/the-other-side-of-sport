﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TypeOfMessage {
    Assault,
    AccusationOfDomesticViolence,
    AccusationOfSexualAssault,
    HealthProblemsByStupidity,
    HealthProblems,
    PersonalProblems,
    Notification,
    AccusationOfHavingDrugs,
    AccusationOfDoping
}
