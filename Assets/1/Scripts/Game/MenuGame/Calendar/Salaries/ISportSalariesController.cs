﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISportSalariesController {
    void ExecutePayments(List<AgentObject> listOfAllAgents, MyDateTime dateTime);
}
