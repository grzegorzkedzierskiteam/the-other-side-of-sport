﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SportsWeeksManager {

	public static int GetSeasonWeekFootball(int realWeek)
    {
        if (realWeek > 5 && realWeek < 37)
            return -1;
        else
        {
            if (realWeek < 6)
                return 0;
            else
            {
                return realWeek - 36;
            }
        }
    }
}
