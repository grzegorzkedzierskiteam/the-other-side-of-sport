﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISalariesController {
    void ExecuteGettingSalaries(List<AgentObject> listOfAllAgents, MyDateTime dateTime);
}
