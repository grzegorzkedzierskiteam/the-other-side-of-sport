﻿using System.Collections.Generic;
using UnityEngine;

public class FootballSalariesController : ISportSalariesController
{
    private readonly IDBConnection dbConnection;
    private readonly IPlayerFinanceManager playerFinanceManager;

    private const int DIVISOR = 20;
    private const int AGENT_WAGE = 4;

    public FootballSalariesController(IDBConnection dbConnection, IPlayerFinanceManager playerFinanceManager)
    {
        this.dbConnection = dbConnection;
        this.playerFinanceManager = playerFinanceManager;
    }

    public void ExecutePayments(List<AgentObject> listOfAllAgents, MyDateTime dateTime)
    {
        if (IsSeason(dateTime))
        {
            var player = listOfAllAgents.Find(x => x.ID == 1);
            if(player.Clients != "")
                SeasonPaymentsForPlayer(player, dateTime);
            SeasonPaymentsAgentsWithoutPlayer(listOfAllAgents);
            SeasonPaymentsAthletesWithoutAgents();
            SeasonPaymentsAthletesWithAgents();
            SeasonPaymentsAthletesWithPlayerAsAgent();

            SetPlayersGotMoney();
        }
    }

    private void SeasonPaymentsForPlayer(AgentObject playerObject, MyDateTime currentDate)
    {
        var allClientsOfPlayer = AgentData.GetAllClientsOfAgent(dbConnection, playerObject.ID);

        var wages = 0;
        
        foreach (ClientObject client in allClientsOfPlayer)
        {
            if(client.Sport == "Football" && client.Contract.Value != 0)
            {
                var wage = GetAgentWageFromClientContract(client, currentDate);
                wages += wage;
                var income = new IncomeExpenseObject(0, currentDate.Year, currentDate.Week, wage, client.ID, client.FirstName + " " + client.LastName, "Player got a paid.");
                playerFinanceManager.AddIncomeOrExpense(income);
            }
        }

        UpdatePlayerWealth(wages);
    }

    private void UpdatePlayerWealth(int wages)
    {
        var query = "UPDATE agents SET Wealth = Wealth + " + wages + " WHERE ID = 1;";
        dbConnection.ModifyQuery(query);
    }

    private int GetAgentWageFromClientContract(ClientObject client, MyDateTime dateTime)
    {
        var wage = (client.Contract.Value / client.Contract.GetFullLengthOfContract(dateTime.Year));
        wage = Mathf.RoundToInt(wage / DIVISOR * (client.ContractWithAgent.Wage / 100f));
        return wage;
    }

    private void SeasonPaymentsAgentsWithoutPlayer(List<AgentObject> listOfAllAgents)
    {
        var query = "";

        foreach (var agent in listOfAllAgents)
            if(agent.ID != 1)
                query += "UPDATE agents SET Wealth = Wealth + ROUND((" +
                            "SELECT IFNULL((SUM(p.Contract / (c.Year + p.ContractLenght - p.YearOfSign) / " + DIVISOR + " * (" + AGENT_WAGE + " / 100.0))), 0)" +
                            " FROM players p, config c WHERE p.Sport = 'Football' AND p.Agent = " + agent.ID + "))" +
                         " WHERE ID = " + agent.ID + " AND Clients != '';";

        dbConnection.ModifyQuery(query);
    }

    private void SeasonPaymentsAthletesWithoutAgents()
    {
        var query = "UPDATE players SET Wealth = Wealth + " +
            "ROUND(" +
            "(SELECT ((p.Contract / (c.Year + p.ContractLenght - p.YearOfSign)) / " + DIVISOR + ") FROM players p, config c WHERE p.ID = players.ID)" +
            ")" +
            " WHERE Contract > 0 AND Agent = -1;";

        dbConnection.ModifyQuery(query);
    }

    private void SeasonPaymentsAthletesWithAgents()
    {
        var query = "UPDATE players SET Wealth = Wealth + " +
            "ROUND(" +
            "(SELECT ((p.Contract / (c.Year + p.ContractLenght - p.YearOfSign)) / " + DIVISOR + ") * (" + AGENT_WAGE + " / 100.0) FROM players p, config c WHERE p.ID = players.ID)" +
            ")" +
            " WHERE Contract > 0 AND Agent > 1;";

        dbConnection.ModifyQuery(query);
    }

    private void SeasonPaymentsAthletesWithPlayerAsAgent()
    {
        var query = "UPDATE players SET Wealth = Wealth + " +
            "ROUND(" +
            "(SELECT ((p.Contract / (c.Year + p.ContractLenght - p.YearOfSign)) / " + DIVISOR + ") * ((100.00 - AgentWage) / 100.00) FROM players p, config c WHERE p.ID = players.ID)" +
            ")" +
            " WHERE Contract > 0 AND Agent = 1;";

        dbConnection.ModifyQuery(query);
    }

    private void SetPlayersGotMoney()
    {
        var query = "UPDATE players SET GotMoney = GotMoney + " +
            "ROUND(" +
            "(SELECT ((p.Contract / (c.Year + p.ContractLenght - p.YearOfSign)) / " + DIVISOR + ") FROM players p, config c WHERE p.ID = players.ID)" +
            ")" +
            " WHERE Contract > 0;";

        dbConnection.ModifyQuery(query);
    }

    private bool IsSeason(MyDateTime currentDate)
    {
        if (SportsWeeksManager.GetSeasonWeekFootball(currentDate.Week) != -1)
            return true;
        else
            return false;
    }
}
