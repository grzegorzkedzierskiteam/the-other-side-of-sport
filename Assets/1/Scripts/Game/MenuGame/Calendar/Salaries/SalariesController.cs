﻿using System.Collections.Generic;

public class SalariesController : ISalariesController{

    private readonly List<ISportSalariesController> listOfSalariesControllersPerSport;

    public SalariesController(ISportSalariesController football)
    {
        listOfSalariesControllersPerSport = new List<ISportSalariesController>
        {
            football
        };
    }

    public void ExecuteGettingSalaries(List<AgentObject> listOfAllAgents, MyDateTime dateTime)
    {
        foreach(var controller in listOfSalariesControllersPerSport)
        {
            controller.ExecutePayments(listOfAllAgents, dateTime);
        }
    }
}
