﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventObject {

    private int id;
    private int year;
    private int week;
    private int seasonWeek;
    private string sport;
    private string team1;
    private string team2;
    private string eventType;
    private string info;
    private string matchType;

    public int ID
    {
        get { return id; }
        set { id = value; }
    }

    public int Year
    {
        get { return year; }
        set { year = value; }
    }

    public int Week
    {
        get { return week; }
        set { week = value; }
    }

    public int SeasonWeek
    {
        get { return seasonWeek; }
        set { seasonWeek = value; }
    }

    public string Sport
    {
        get { return sport; }
        set { sport = value; }
    }

    public string Team1
    {
        get { return team1; }
        set { team1 = value; }
    }

    public string Team2
    {
        get { return team2; }
        set { team2 = value; }
    }

    public string EventType
    {
        get { return eventType; }
        set { eventType = value; }
    }

    public string Info
    {
        get { return info; }
        set { info = value; }
    }

    public string MatchType
    {
        get { return matchType; }
        set { matchType = value; }
    }
}
