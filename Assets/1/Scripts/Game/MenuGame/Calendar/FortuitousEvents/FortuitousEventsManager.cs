﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FortuitousEventsManager : IFortuitousEventsManager
{
    private readonly IDBConnection dBConnection;
    private readonly IFortuitousEventsGenerator fortuitousEventsGenerator;

    public FortuitousEventsManager(IDBConnection dBConnection, IFortuitousEventsGenerator fortuitousEventsGenerator)
    {
        this.dBConnection = dBConnection;
        this.fortuitousEventsGenerator = fortuitousEventsGenerator;
    }

    public void RandomFortuitousEventsForClients(MyDateTime dateTime)
    {
        var listOfClients = AgentData.GetAllClientsOfAgent(dBConnection, 1);

        foreach (var client in listOfClients)
        {
            var didAnythingHappen = Random.Range(0, 100);
            if(didAnythingHappen < 10)
                fortuitousEventsGenerator.GenerateRandomFortuitousEvent(client, dateTime);
        }
    }

    public void RandomFortuitousEventsForPlayer()
    {
        //throw new System.NotImplementedException();
    }
}
