﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IFortuitousEventsGenerator {

    IMessage GenerateRandomFortuitousEvent(IPerson person, MyDateTime dateTime);
}
