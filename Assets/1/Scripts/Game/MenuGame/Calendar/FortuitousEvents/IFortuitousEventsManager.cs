﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IFortuitousEventsManager {

    void RandomFortuitousEventsForClients(MyDateTime dateTime);
    void RandomFortuitousEventsForPlayer();
}
