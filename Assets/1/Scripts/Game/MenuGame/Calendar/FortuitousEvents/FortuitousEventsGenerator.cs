﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FortuitousEventsGenerator : IFortuitousEventsGenerator
{
    private IDBConnection dBConnection;
    private IMessageGenerator messageGenerator;

    #region limits
    private int assault = 5;
    private int healthProblemsByStupidity = 13;
    private int healthProblems = 22;
    private int accusationOfDomesticViolence = 26;
    private int accusationOfSexualAssault = 32;
    private int PersonalProblems = 40;
    #endregion

    public FortuitousEventsGenerator(IDBConnection dBConnection, IMessageGenerator messageGenerator)
    {
        this.dBConnection = dBConnection;
        this.messageGenerator = messageGenerator;
    }

    public IMessage GenerateRandomFortuitousEvent(IPerson person, MyDateTime dateTime)
    {
        var whatHasHappen = Random.Range(0,100);

        switch(person.Character.NameOfCharacter)
        {
            case "Born leader":
                SetLimits(person.Character);
                return GetMessage(person, whatHasHappen, dateTime);

            case "Melancholic":
                SetLimits(person.Character);
                return GetMessage(person, whatHasHappen, dateTime);

            case "Phlegmatic":
                SetLimits(person.Character);
                return GetMessage(person, whatHasHappen, dateTime);

            case "Sanguine":
                SetLimits(person.Character);
                return GetMessage(person, whatHasHappen, dateTime);
        }

        return null;
    }

    private IMessage GetMessage(IPerson person, int whatHasHappend, MyDateTime dateTime)
    {
        if (whatHasHappend < assault)
            return messageGenerator.GetMessage(person, TypeOfMessage.Assault, dateTime);
        else if (whatHasHappend < healthProblemsByStupidity)
            return messageGenerator.GetMessage(person, TypeOfMessage.HealthProblemsByStupidity, dateTime);
        else
            return null;
    }

    private void SetLimits(ICharacter character)
    {
        switch (character.NameOfCharacter)
        {
            case "Born leader":
                assault = 6;
                break;
            case "Melancholic":
                assault = 4;
                break;
            case "Phlegmatic":
                assault = 3;
                break;
            case "Sanguine":
                assault = 8;
                break;
        }
    }
}
