﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IRandomFortuitousEventsGenerator {

    IMessageObject GenerateRandomCapriceForClient();
    IMessageObject GenerateRandomFortuitousEventForClient();
    IMessageObject GenerateRandomFortuitousEventForPlayer();
}
