﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IAthletesActivities {

    void ExecuteActivities(IDBConnection dBConnection, IClientObject clientObject);
}
