﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AthletesActivitiesController : IAthletesActivitiesController
{
    private IDBConnection dBConnection;

    public AthletesActivitiesController(IDBConnection dBConnection)
    {
        this.dBConnection = dBConnection;
    }

    public void ExecuteAthletesActivites()
    {
        List<ClientObject> listOfAllAthletes = ClientData.GetAllAthletesDetails(dBConnection);

        if (listOfAllAthletes.Any(x => x.Wealth < 0))
            Debug.Log(listOfAllAthletes.Find(x => x.Wealth < 0).Wealth);

        foreach (ClientObject athlete in listOfAllAthletes)
            athlete.Activities.ExecuteActivities(dBConnection, athlete);
    }
}
