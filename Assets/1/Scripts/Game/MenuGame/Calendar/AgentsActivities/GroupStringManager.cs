﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class GroupStringManager {

    public static List<int> GetAlliesEnemiesClientsIDs(string groupIDs)
    {
        List<int> ids = new List<int>();

        if (groupIDs != "")
        {
            List<string> idsString = new List<string>(groupIDs.Split('+'));

            idsString.RemoveAt(0);
            foreach (string id in idsString)
            {
                ids.Add(Int32.Parse(id));
            }
        }

        return ids;
    }

    public static string AddPersonToGroup(string group, string person)
    {
        if (person != "")
        {
            string newGroup = group;
            newGroup += "+" + person;

            return newGroup;
        }
        else
            return group;
    }

    public static string DeletePersonFromGroup(string group, string person)
    {
        if (person != "")
        {
            string newGroup = "";
            List<int> groupElements = GetAlliesEnemiesClientsIDs(group);
            int elementToDelete = 0;
            foreach (int id in groupElements)
            {
                if (id.ToString() == person)
                    elementToDelete = id;
            }

            groupElements.Remove(elementToDelete);

            foreach (int id in groupElements)
            {
                newGroup += "+" + id.ToString();
            }

            return newGroup;
        }
        else
            return group;
    }

    public static bool IsPersonInGroup(string group, string person)
    {
        if (person != "")
        {
            List<int> listGroup = GetAlliesEnemiesClientsIDs(group);

            int personID = Int32.Parse(person);

            return listGroup.Any(item => item == personID);
        }
        else
            return false;
    }

    public static string SetStringGroupFromList(List<int> list)
    {
        string stringGroup = "";

        foreach (int id in list)
            stringGroup += "+" + id;

        return stringGroup;
    }

    public static List<TypeOfResponse> GetListAvailableResponses(string stringGroup)
    {
        List<TypeOfResponse> list = new List<TypeOfResponse>();

        if (stringGroup != "")
        {
            List<string> responses = new List<string>(stringGroup.Split('+'));

            responses.RemoveAt(0);
            foreach (string response in responses)
            {
                list.Add((TypeOfResponse)Enum.Parse(typeof(TypeOfResponse), response));
            }
        }

        return list;
    }

    public static string GetStringAvailableResponses(List<TypeOfResponse> list)
    {
        string stringGroup = "";

        foreach (TypeOfResponse type in list)
            stringGroup += "+" + Enum.GetName(typeof(TypeOfResponse), type);

        return stringGroup;
    }
}
