﻿public class AgentsContractsManager : IAgentsContractsManager {

    private IDBConnection dbConnection;
    private IContractMaker contractMaker;

    public AgentsContractsManager(IDBConnection dbConnection, IContractMaker contractMaker)
    {
        this.dbConnection = dbConnection;
        this.contractMaker = contractMaker;
    }

    public void ProposeClientToAnyTeam(ClientObject client, Agent agent, int yearOfSign, int negotiatingStatus = 50)
    {
        var selectedTeams = Teams.GetNumberRandomTeams(5, Teams.GetFootballTeams());

        foreach (var selectedTeam in selectedTeams)
        {
            if (DoesTeamHaveAnyFreeSlotsInDepthChartAtPosition(dbConnection, selectedTeam.Name, selectedTeam.Sport, client.Position) == false)
                break;

            negotiatingStatus += GetBasicNegotiatingEffects(client, agent, selectedTeam.Name);

            var contract = contractMaker.GetRandomFootballContractFromCurrentYear(yearOfSign, client.Position, client.OverallSkill, client.Prospectiveness);
            var doesHeWantTo = UnityEngine.Random.Range(0, 2);

            if (negotiatingStatus > 50 && doesHeWantTo == 0)
            {
                AgentsSimpleMethods.SignContract(dbConnection, client, agent, contract, selectedTeam.Name, selectedTeam.Sport);
                return;
            }
            else
            {
                doesHeWantTo = UnityEngine.Random.Range(0, 2);
                contract = contractMaker.GetNewLowerContract(contract, yearOfSign);
                negotiatingStatus += 5;

                if (negotiatingStatus > 50 && doesHeWantTo == 0)
                {
                    AgentsSimpleMethods.SignContract(dbConnection, client, agent, contract, selectedTeam.Name, selectedTeam.Sport);
                    return;
                }
            }
        }
    }

    public void ReSigningContract(ClientObject client, Agent agent, int yearOfSign, int negotiatingStatus = 50)
    {
        if (DoesTeamHaveAnyFreeSlotsInDepthChartAtPosition(dbConnection, client.Team, client.Sport, client.Position) == false)
            return;

        negotiatingStatus += GetBasicNegotiatingEffects(client, agent, client.Team);

        var contract = contractMaker.GetNewGreaterContract(client.Contract,yearOfSign);
        var doesHeWantTo = UnityEngine.Random.Range(0, 2);

        if (negotiatingStatus > 50 && doesHeWantTo == 0)
        {
            AgentsSimpleMethods.SignContract(dbConnection, client, agent, contract, client.Team, client.Sport);
            return;
        }
        else
        {
            contract = client.Contract;
            doesHeWantTo = UnityEngine.Random.Range(0, 2);
            negotiatingStatus += 5;

            if (negotiatingStatus > 50 && doesHeWantTo == 0)
            {
                AgentsSimpleMethods.SignContract(dbConnection, client, agent, contract, client.Team, client.Sport);
                return;
            }
            else
            {
                contract = contractMaker.GetNewLowerContract(contract, yearOfSign);
                doesHeWantTo = UnityEngine.Random.Range(0, 3);
                negotiatingStatus += 5;

                if (negotiatingStatus > 50 && doesHeWantTo == 0)
                {
                    AgentsSimpleMethods.SignContract(dbConnection, client, agent, contract, client.Team, client.Sport);
                    return;
                }
            }
        }
    }

    private int GetBasicNegotiatingEffects(ClientObject client, Agent agent, string selectedTeamName)
    {
        var negotiatingStatus = 0;

        negotiatingStatus += NegotiatingWithTeamEffects.GetPositionInDepthChartEffect(dbConnection, selectedTeamName, client);
        negotiatingStatus += NegotiatingWithTeamEffects.GetPlayerAgeEffect(client.Age);
        negotiatingStatus += NegotiatingWithTeamEffects.GetPlayerInjuryEffect(client.InjuryTime);
        negotiatingStatus += NegotiatingWithTeamEffects.GetAgentSkillEffect(agent.Respectability, agent.Popularity);
        negotiatingStatus += NegotiatingWithTeamEffects.GetLuckyEffect();
        negotiatingStatus += NegotiatingWithTeamEffects.GetTeamMoodEffect();

        return negotiatingStatus;
    }

    private bool DoesTeamHaveAnyFreeSlotsInDepthChartAtPosition(IDBConnection dbConnection, string teamName, string sport, string position)
    {
        switch(sport)
        {
            case "Football":
                var teamRosterStatus = GetFootballTeamRosterStatus(dbConnection, teamName);
                if (teamRosterStatus.AreThereAnyFreeSlotsAtPosition(position) == false)
                    return false;
                break;

            default:
                return false;
        }

        return true;
    }

    private FootballRosterStatus GetFootballTeamRosterStatus(IDBConnection dbConnection, string teamName)
    {
        var teamRosterStatus = new FootballRosterStatus(dbConnection, teamName);

        return teamRosterStatus;
    }
}
