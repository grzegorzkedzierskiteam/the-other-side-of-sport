﻿using Mono.Data.Sqlite;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class AgentsSimpleMethods {

    public static void SetAthleteAsClientOfPlayerInDB(IDBConnection dbConnection, IUserDataHolder userData, IClientObject clientObject, IOffer offer)
    {
        string query = "UPDATE players SET Agent = 1,AgentContractDateEndWeek = " + userData.DateTime.Week + ", AgentContractDateEndYear = " + (offer.ContractLength + userData.DateTime.Year) + ", AgentWage = " + offer.ContractValue + ", IsReady = 0 WHERE ID = " + clientObject.ID + ";";

        if(!GroupStringManager.IsPersonInGroup(userData.Clients,clientObject.ID.ToString()))
            query += "UPDATE agents SET Clients = Clients || '+" + clientObject.ID + "' WHERE ID = 1;";

        dbConnection.ModifyQuery(query);
    }

    public static void AgentLosesClient(IDBConnection dbConnection, int clientID, int agentID)
    {
        string clients = GetClientsOfAgentIfAgentHasTheClient(dbConnection, clientID, agentID);
        clients = GroupStringManager.DeletePersonFromGroup(clients, clientID.ToString());

        AgentData.UpdateStringGroupDB(dbConnection, agentID, clients, AgentGroupEnum.Clients);
    }

    private static string GetClientsOfAgentIfAgentHasTheClient(IDBConnection dbConnection, int clientID, int agentID)
    {
        string query = "SELECT Clients FROM agents WHERE (Clients LIKE '%+" + clientID + "' OR Clients LIKE '%+" + clientID + "+%') AND ID = " + agentID;
        SqliteDataReader reader = dbConnection.DownloadQuery(query);
        if (reader.HasRows)
        {
            reader.Read();
            string clients = reader.GetString(0);
            reader.Close();
            reader = null;

            return clients;
        }
        else
            throw new Exception("Searched agent does not have wanted client or the agent does not exist.");
    }

    public static void DecreaseAttempt(IDBConnection dbConnection, IClientObject clientObject)
    {
        int newReadyStatus = 0;
        if (clientObject.ReadyStatus == 1)
            newReadyStatus = 2;
        else if (clientObject.ReadyStatus == 2)
            newReadyStatus = 3;
        else 
            newReadyStatus = 0;

        string query = "UPDATE players SET IsReady =" + newReadyStatus + " WHERE ID = " + clientObject.ID;
        dbConnection.ModifyQuery(query);
    }    

    public static void SignContract(IDBConnection dbConnection, ClientObject client, AgentObject agent, Contract contract ,string team, string sport)
    {
        string query = "UPDATE players SET Contract = " + contract.Value + ", ContractGuaranteedMoney = " + contract.GuaranteedMoney + ", ContractLenght = " + contract.Length + ", YearOfSign = " + contract.YearOfSign + ", Team = '" + team + "', Sport = '" + sport + "', IsReady = 0 WHERE ID = " + client.ID + ";";
        query += "UPDATE agents SET Popularity = Popularity + 3 WHERE ID = " + agent.ID + " AND (Popularity + 3) < 100;";
        query += "UPDATE agents SET Respectability = Respectability + 3 WHERE ID = " + agent.ID + " AND (Respectability + 3) < 100;";
        dbConnection.ModifyQuery(query);
    }
}
