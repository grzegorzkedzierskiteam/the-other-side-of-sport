﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IAgentsContractsManager {
    void ReSigningContract(ClientObject client, Agent agent, int yearOfSign, int negotiatingStatus = 50);
    void ProposeClientToAnyTeam(ClientObject client, Agent agent, int yearOfSign, int negotiatingStatus = 50);
}
