﻿using System;
using Mono.Data.Sqlite;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgentsActivitiesController {

    private readonly IDBConnection dbConnection;
    private readonly IAgentsContractsManager agentsContractsManager;

    public AgentsActivitiesController(IDBConnection _dbConnection, IAgentsContractsManager _agentsContractsManager)
    {
        dbConnection = _dbConnection;
        agentsContractsManager = _agentsContractsManager;
    }

    public void ExecuteAgentsActivities()
    {
        var userDataHolder = UserData.GetUserDataHolder();
        var query = "SELECT ID,FirstName,LastName,Wealth,Popularity,Respectability,Allies,Enemies,Energy,Clients,Character,Relation,IsFavorite,Gender FROM agents WHERE ID != 1 ORDER BY RANDOM()";
        var reader = dbConnection.DownloadQuery(query);
        var agents = new List<IAgent>();
        while(reader.Read())
        {
            agents.Add(new Agent(dbConnection, agentsContractsManager, userDataHolder, reader.GetInt32(0),
                reader.GetString(1), reader.GetString(2), (Gender)Enum.Parse(typeof(Gender), reader.GetString(13)), reader.GetInt32(3), 
                reader.GetInt32(4),reader.GetInt32(5), reader.GetString(6), 
                reader.GetString(7), reader.GetInt32(8), reader.GetString(9),
                reader.GetInt32(11), reader.GetString(10), reader.GetInt32(12)));
        }

        reader.Close();
        reader = null;

        foreach(var agent in agents)
        {
            agent.DeleteClients();
            agent.SearchNewClients();
            agent.ReSigningContracts();
            agent.ProposeClientToAnyTeam();
            agent.SetNewAllyOrEnemy(agents);
            agent.LoseAllyOrEnemy(agents);
            agent.ChangeHisMindForRumorReason();
        }
    }
}
