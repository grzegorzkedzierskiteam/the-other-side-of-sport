﻿using Mono.Data.Sqlite;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Agent : AgentObject, IAgent{

    private IDBConnection dbConnection;
    private List<int> alliesIDs;
    private List<int> enemiesIDs;
    private List<int> clientsIDs;
    private int wealthLevel;

    private UserDataHolder userDataHolder;
    private IAgentsContractsManager agentsContractsManager;

    public Agent(IDBConnection _dbConnection, IAgentsContractsManager _agentsContractsManager, UserDataHolder _userDataHolder, int _iD, string _name, string _surname, Gender _gender, int _wealth, int _popularity, int _respectability, string _allies, string _enemies, int _energy, string _clients, int _relation, string _character, int _isFavorite) : base(_iD, _name, _surname, _gender, _wealth, _popularity, _respectability, _allies, _enemies, _energy, _clients, _relation, _character, _isFavorite)
    {
        dbConnection = _dbConnection;
        alliesIDs = GroupStringManager.GetAlliesEnemiesClientsIDs(Allies);
        enemiesIDs = GroupStringManager.GetAlliesEnemiesClientsIDs(Enemies);
        clientsIDs = GroupStringManager.GetAlliesEnemiesClientsIDs(Clients);
        wealthLevel = GetWealthLevel();
        agentsContractsManager = _agentsContractsManager;
        userDataHolder = _userDataHolder;
    }

    public List<int> AlliesIDs
    {
        set { alliesIDs = value; }
        get { return alliesIDs; }
    }

    public List<int> EnemiesIDs
    {
        set { enemiesIDs = value; }
        get { return enemiesIDs; }
    }

    public List<int> ClientsIDs
    {
        set { clientsIDs = value; }
        get { return clientsIDs; }
    }

    #region wealth
    // do zmiany
    public void GetSalaries()
    {
        if(clientsIDs.Any())
        {
            string query = "SELECT Contract FROM players WHERE Agent=" + ID;
            SqliteDataReader reader = dbConnection.DownloadQuery(query);
            List<int> contractsList = new List<int>();
            while (reader.Read())
            {
                contractsList.Add(reader.GetInt32(0));
            }

            reader.Close();
            reader = null;

            int income = 0;
            foreach(int salary in contractsList)
            {
                income += Mathf.RoundToInt((salary/12) * 2.5f);
            }

            income = Mathf.RoundToInt(income * 0.6f);

            query = "UPDATE agents SET Wealth = Wealth + " + income + " - " + GetExpenses() + " WHERE ID = " + ID;
            dbConnection.ModifyQuery(query);
        }
    }

    private int GetExpenses()
    {
        int expenses = 0;
        //regular expenses
        expenses = 500 * wealthLevel;

        //buy a car
        if (UnityEngine.Random.Range(1, 13) == 1 && wealthLevel > 3)
            expenses += UnityEngine.Random.Range(50000, 3000000);

        //buy a plane
        if (UnityEngine.Random.Range(1, 20) == 1 && wealthLevel > 40)
            expenses += UnityEngine.Random.Range(500000, 5000000);

        //buy a house
        if (UnityEngine.Random.Range(1, 25) == 1 && wealthLevel > 100)
            expenses += UnityEngine.Random.Range(100000, 250000000);
       
        //buy a yacht
        if (UnityEngine.Random.Range(1, 35) == 1 && wealthLevel > 100)
            expenses += UnityEngine.Random.Range(10000000, 200000000);

        return expenses;
    }

    private int GetWealthLevel()
    {
        if (Wealth < 100000)
            return 1;
        if (Wealth < 500000 && Wealth >= 100000)
            return 3;
        if (Wealth < 5000000 && Wealth >= 500000)
            return 20;
        if (Wealth < 20000000 && Wealth >= 5000000)
            return 40;
        if (Wealth < 100000000 && Wealth >= 20000000)
            return 100;
        if (Wealth < 1000000000 && Wealth >= 100000000)
            return 150;
        if (Wealth >= 1000000000)
            return 200;
        else
            return 50;
    }
    #endregion
    #region clients
    public void DeleteClients()
    {
        if(UnityEngine.Random.Range(1,200) == 1 && wealthLevel > 3 && clientsIDs.Any())
        {
            string query = "SELECT ID FROM players WHERE Agent = " + ID + " ORDER BY Contract";
            SqliteDataReader reader = dbConnection.DownloadQuery(query);
            if(reader.HasRows)
            {
                reader.Read();
                int client = reader.GetInt32(0);

                Clients = GroupStringManager.DeletePersonFromGroup(Clients, client.ToString());
                clientsIDs = GroupStringManager.GetAlliesEnemiesClientsIDs(Clients);

                query = "UPDATE players SET Agent = -1, IsReady = 0 WHERE ID = " + client + ";";
                query += "UPDATE agents SET Clients = '" + Clients + "' WHERE ID = " + ID + ";";
                dbConnection.ModifyQuery(query);
            }

            reader.Close();
            reader = null;

        }
    }

    public void SearchNewClients()
    {
        int random = UnityEngine.Random.Range(0, 25);
        if(random == 0)
        {
            int agentReach = GetAgentReach(Popularity, Respectability);
            string query = "SELECT ID,Popularity,OverallSkill FROM players WHERE Agent = -2 AND OverallSkill <= " + agentReach + " AND IsReady = 1 ORDER BY OverallSkill DESC, Popularity DESC LIMIT 1";
            SqliteDataReader reader = dbConnection.DownloadQuery(query);

            if(reader.HasRows)
            {
                reader.Read();
                int playerID = reader.GetInt32(0);
                int playerNegotiatingSkill = (reader.GetInt32(1) + reader.GetInt32(2) + reader.GetInt32(2)) / 3;
                int agentNegotiatingSkill = (Popularity + Respectability) / 2;

                int negotiating = agentNegotiatingSkill - playerNegotiatingSkill;

                int lucky = UnityEngine.Random.Range(0, 30);

                if (negotiating + lucky > 10)
                {
                    Clients = GroupStringManager.AddPersonToGroup(Clients, playerID.ToString());
                    query = "UPDATE agents SET Clients = '" + Clients + "' WHERE ID =" + ID + ";";
                    query += "UPDATE players SET Agent = " + ID + ", IsReady = 0 WHERE ID = " + playerID + ";";
                    dbConnection.ModifyQuery(query);
                }
            }

            reader.Close();
            reader = null;
        }
    }

    public void ReSigningContracts()
    {
        int random = UnityEngine.Random.Range(0, 20);
        if(random == 0)
        {
            string query = "SELECT ID,Popularity,OverallSkill,Prospectiveness,Age,Character,College,Position,Team,Contract,Sport,InjuryTime,Wealth,Dept FROM players WHERE Team !='Retirement' AND ContractLenght = 1 AND Agent = " + ID + " LIMIT 5";
            SqliteDataReader reader = dbConnection.DownloadQuery(query);
            if (reader.HasRows)
            {
                while(reader.Read())
                {
                    ClientObject newClient = new ClientObject(reader.GetInt32(0), "", "", reader.GetString(10), reader.GetString(7), reader.GetString(6), reader.GetString(8), reader.GetInt32(4), reader.GetInt32(12), reader.GetInt32(13), reader.GetInt32(1), CharacterManager.GetCharacterFromNameOfCharacter(reader.GetString(5)), 0, reader.GetInt32(9),0, 1, 0, 0, reader.GetInt32(2), reader.GetInt32(3), ID, reader.GetInt32(11),"",0);
                    agentsContractsManager.ReSigningContract(newClient, this, userDataHolder.DateTime.Year, 60);
                }
                reader.Close();
                reader = null;
            }
        }
    }

    public void ProposeClientToAnyTeam()
    {
        int random = UnityEngine.Random.Range(0, 20);
        if (random == 0)
        {
            string query = "SELECT ID,Popularity,OverallSkill,Prospectiveness,Age,Character,College,Position,Team,Contract,Sport,InjuryTime,Wealth,Dept FROM players WHERE Team !='Retirement' AND ContractLenght = 0 AND Agent = " + ID + " AND IsReady = 1 LIMIT 5";
            SqliteDataReader reader = dbConnection.DownloadQuery(query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    ClientObject newClient = new ClientObject(reader.GetInt32(0), "", "", reader.GetString(10), reader.GetString(7), reader.GetString(6), reader.GetString(8), reader.GetInt32(4), reader.GetInt32(12), reader.GetInt32(13), reader.GetInt32(1), CharacterManager.GetCharacterFromNameOfCharacter(reader.GetString(5)), 0, reader.GetInt32(9), 0, 0, 0, 0, reader.GetInt32(2), reader.GetInt32(3), ID, reader.GetInt32(11),"",0);
                    agentsContractsManager.ProposeClientToAnyTeam(newClient, this, userDataHolder.DateTime.Year);
                }
                reader.Close();
                reader = null;
            }
        }
    }

    public void SetNewAllyOrEnemy(List<IAgent> listOfAllAgents)
    {
        int decision = UnityEngine.Random.Range(-50, 51);
        if(decision >= 44)
        {
            if(AlliesAndEnemiesManager.CanAgentHaveMoreAlliesOrEnemies(listOfAllAgents,this))
            {
                IAgent newAlly = AlliesAndEnemiesManager.GetIDOfProbablyNewAllyOrEnemy(listOfAllAgents, this);
                string query = AlliesAndEnemiesManager.GetSetAgentAsAllyQuery(listOfAllAgents.Find(x => x.ID == ID), listOfAllAgents.Find(x => x.ID == newAlly.ID));

                listOfAllAgents.Find(x => x.ID == ID).AlliesIDs.Add(newAlly.ID);
                listOfAllAgents.Find(x => x.ID == newAlly.ID).AlliesIDs.Add(ID);

                dbConnection.ModifyQuery(query);
            }
        }
        else if(decision <= -44)
        {
            if (AlliesAndEnemiesManager.CanAgentHaveMoreAlliesOrEnemies(listOfAllAgents, this))
            {
                IAgent newEnemy = AlliesAndEnemiesManager.GetIDOfProbablyNewAllyOrEnemy(listOfAllAgents, this);
                string query = AlliesAndEnemiesManager.GetSetAgentAsEnemyQuery(listOfAllAgents.Find(x => x.ID == ID), listOfAllAgents.Find(x => x.ID == newEnemy.ID));

                listOfAllAgents.Find(x => x.ID == ID).EnemiesIDs.Add(newEnemy.ID);
                listOfAllAgents.Find(x => x.ID == newEnemy.ID).EnemiesIDs.Add(ID);
                
                dbConnection.ModifyQuery(query);
            }
        }
    }

    public void LoseAllyOrEnemy(List<IAgent> listOfAllAgents)
    {
        int decision = UnityEngine.Random.Range(-50, 51);
        if (decision >= 44)
        {
            if(AlliesAndEnemiesManager.DoesAgentHaveAllyOrEnemy(this,AlliesAndEnemiesManager.GroupType.Allies))
            {
                IAgent agentToDeleteFromAllies = AlliesAndEnemiesManager.GetAgentToDeleteFromAlliesOrEnemy(listOfAllAgents, this, AlliesAndEnemiesManager.GroupType.Allies);

                string query = AlliesAndEnemiesManager.GetDeleteAgentFromAlliesQuery(listOfAllAgents.Find(x => x.ID == ID), listOfAllAgents.Find(x => x.ID == agentToDeleteFromAllies.ID));

                listOfAllAgents.Find(x => x.ID == ID).AlliesIDs.Remove(agentToDeleteFromAllies.ID);
                listOfAllAgents.Find(x => x.ID == agentToDeleteFromAllies.ID).AlliesIDs.Remove(ID);

                dbConnection.ModifyQuery(query);
            }
        }
        else if (decision <= -44)
        {
            if (AlliesAndEnemiesManager.DoesAgentHaveAllyOrEnemy(this, AlliesAndEnemiesManager.GroupType.Enemies))
            {
                IAgent agentToDeleteFromEnemies = AlliesAndEnemiesManager.GetAgentToDeleteFromAlliesOrEnemy(listOfAllAgents, this, AlliesAndEnemiesManager.GroupType.Enemies);

                string query = AlliesAndEnemiesManager.GetDeleteAgentFromEnemiesQuery(listOfAllAgents.Find(x => x.ID == ID), listOfAllAgents.Find(x => x.ID == agentToDeleteFromEnemies.ID));

                listOfAllAgents.Find(x => x.ID == ID).EnemiesIDs.Remove(agentToDeleteFromEnemies.ID);
                listOfAllAgents.Find(x => x.ID == agentToDeleteFromEnemies.ID).EnemiesIDs.Remove(ID);

                dbConnection.ModifyQuery(query);
            }
        }
    }

    private int GetAgentReach(int popularity, int respectability)
    {
        int reach = 0;

        int negotiatingSkill = (popularity + respectability) / 2;

        if (negotiatingSkill > 85)
            reach = 99;
        if (negotiatingSkill <= 85 && negotiatingSkill > 77)
            reach = 90;
        if (negotiatingSkill <= 77 && negotiatingSkill > 66)
            reach = 84;
        if (negotiatingSkill <= 66)
            reach = 65;

        return reach;
    }
    #endregion
    #region player
    public void ChangeHisMindForRumorReason()
    {
        if(Relation != 50 && Relation > 10 && Relation < 90)
        {
            int isAnyRumor = UnityEngine.Random.Range(0, 20);
            if(isAnyRumor == 0)
            {
                int result = UnityEngine.Random.Range(-2, 3);
                string query = "UPDATE agents SET Relation = Relation + " + result + " WHERE ID = " + ID + ";";
                dbConnection.ModifyQuery(query);
            }
        }
    }
    #endregion
}
