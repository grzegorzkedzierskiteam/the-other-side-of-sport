﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class RelationManager {
    public static void ChangeRelationWithOtherAgent(IDBConnection dbConnection, IAgentObject agentObject, int difference)
    {
        agentObject.Relation = ChangeRelationNumber(agentObject.Relation, difference);
        var query = "UPDATE agents SET Relation =" + agentObject.Relation + " WHERE ID =" + agentObject.ID +";";

        if (agentObject.Relation < 30)
        {
            var userDataHolder = UserData.GetUserDataHolder();
            var user = new AgentObject(1, "", "", Gender.Male,0, 0, 0, userDataHolder.Allies, userDataHolder.Enemies, 0, userDataHolder.Clients, 0, "", 0);

            if (!AlliesAndEnemiesManager.AreTheyEnemies(user,agentObject))
                query += AlliesAndEnemiesManager.GetSetAgentAsEnemyQuery(user, agentObject);
        }
        else if (agentObject.Relation > 60)
        {
            var userDataHolder = UserData.GetUserDataHolder();
            var user = new AgentObject(1, "", "", Gender.Male,0, 0, 0, userDataHolder.Allies, userDataHolder.Enemies, 0, userDataHolder.Clients, 0, "", 0);

            if (!AlliesAndEnemiesManager.AreTheyAllis(user, agentObject))
                query += AlliesAndEnemiesManager.GetSetAgentAsAllyQuery(user, agentObject);
        }
        else
        {
            if(GroupStringManager.IsPersonInGroup(agentObject.Allies, "1"))
            {
                var userDataHolder = UserData.GetUserDataHolder();
                var user = new AgentObject(1, "", "", Gender.Male, 0, 0, 0, userDataHolder.Allies, userDataHolder.Enemies, 0, userDataHolder.Clients, 0, "", 0);
                query += AlliesAndEnemiesManager.GetDeleteAgentFromAlliesQuery(user, agentObject);
            }
            else if(GroupStringManager.IsPersonInGroup(agentObject.Enemies, "1"))
            {
                var userDataHolder = UserData.GetUserDataHolder();
                var user = new AgentObject(1, "", "", Gender.Male,0, 0, 0, userDataHolder.Allies, userDataHolder.Enemies, 0, userDataHolder.Clients, 0, "", 0);
                query += AlliesAndEnemiesManager.GetDeleteAgentFromEnemiesQuery(user, agentObject);
            }
        }

        dbConnection.ModifyQuery(query);
    }

    public static void ChangeRelationWithClient(IDBConnection dbConnection, IClientObject clientObject, int difference)
    {
        clientObject.Relation = ChangeRelationNumber(clientObject.Relation, difference);

        var query = "UPDATE players SET Relation =" + clientObject.Relation + " WHERE ID =" + clientObject.ID + ";";
    
        dbConnection.ModifyQuery(query);
    }

    public static int ChangeRelationNumber(int personRelation, int difference)
    {
        personRelation += difference;
        if (personRelation > 100)
            personRelation = 100;
        else if (personRelation < 0)
            personRelation = 0;

        return personRelation;
    }
}
