﻿using Mono.Data.Sqlite;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class AlliesAndEnemiesManager {
    public enum GroupType { Allies, Enemies }

    public static IAgent GetIDOfProbablyNewAllyOrEnemy(List<IAgent> listOfAllAgents, IAgent agent)
    {
        int random = UnityEngine.Random.Range(-12, 13);

        var probalbyNewAllyOrEnemy = listOfAllAgents.Where(x => (!agent.AlliesIDs.Contains(x.ID)) && (!agent.EnemiesIDs.Contains(x.ID)) && (x.ID != agent.ID))
                                                        .OrderBy(x => Mathf.Abs(random + agent.Popularity + agent.Respectability - x.Popularity - x.Respectability)).ThenByDescending(x => x.Wealth);

        if (probalbyNewAllyOrEnemy.Any())
            return probalbyNewAllyOrEnemy.First();
        else
            throw new Exception("Agent with ID: " + agent.ID + " has all agent as allies or enemies.");
    }

    public static IAgent GetAgentToDeleteFromAlliesOrEnemy(List<IAgent> listOfAllAgents, IAgent agent, GroupType groupType)
    {
        if(groupType == GroupType.Allies)
        {
            List<int> allies = agent.AlliesIDs;
            allies.Remove(1);
            if (DoesAgentHaveAllyOrEnemy(agent, GroupType.Allies))
            {
                int random = UnityEngine.Random.Range(0, allies.Count);
                return listOfAllAgents.Find(x => x.ID == allies[random]);
            }
            else
                throw new Exception("Agent has no allies. Deleting is impossible");
        }
        else if(groupType == GroupType.Enemies)
        {
            List<int> enemies = agent.EnemiesIDs;
            enemies.Remove(1);
            if (DoesAgentHaveAllyOrEnemy(agent, GroupType.Enemies))
            {
                int random = UnityEngine.Random.Range(0, enemies.Count);
                return listOfAllAgents.Find(x => x.ID == enemies[random]);
            }
            else
                throw new Exception("Agent has no enemies. Deleting is impossible");
        }
        else
            throw new Exception("Group type did not select.");
    }

    public static bool DoesAgentHaveAllyOrEnemy(IAgent agent, GroupType groupType)
    {
        if(groupType == GroupType.Allies)
        {
            if (agent.AlliesIDs.Any(x => x != 1))
                return true;
            else
                return false;
        }
        else
        {
            if (agent.EnemiesIDs.Any(x => x != 1))
                return true;
            else
                return false;
        }
    }

    public static bool CanAgentHaveMoreAlliesOrEnemies(List<IAgent> listOfAllAgents, IAgent agent)
    {
        if (listOfAllAgents.Any(x => ((!agent.AlliesIDs.Contains(x.ID)) && (!agent.EnemiesIDs.Contains(x.ID)) && (x.ID != agent.ID))))
            return true;
        else
            return false;
    }

    public static string GetSetAgentAsEnemyQuery(IAgentObject firstAgent, IAgentObject secondAgent)
    {
        firstAgent.Enemies = GroupStringManager.AddPersonToGroup(firstAgent.Enemies, secondAgent.ID.ToString());
        secondAgent.Enemies = GroupStringManager.AddPersonToGroup(secondAgent.Enemies, firstAgent.ID.ToString());

        string query = "UPDATE agents SET Enemies = '" + firstAgent.Enemies + "' WHERE ID =" + firstAgent.ID + ";" +
                        "UPDATE agents SET Enemies = '" + secondAgent.Enemies + "' WHERE ID =" + secondAgent.ID + ";";

        if (AreTheyAllis(firstAgent, secondAgent))
            query += GetDeleteAgentFromAlliesQuery(firstAgent, secondAgent);

        return query;
    }

    public static string GetSetAgentAsAllyQuery(IAgentObject firstAgent, IAgentObject secondAgent)
    {
        firstAgent.Allies = GroupStringManager.AddPersonToGroup(firstAgent.Allies, secondAgent.ID.ToString());
        secondAgent.Allies = GroupStringManager.AddPersonToGroup(secondAgent.Allies, firstAgent.ID.ToString());

        string query = "UPDATE agents SET Allies = '" + firstAgent.Allies + "' WHERE ID =" + firstAgent.ID + ";" +
                        "UPDATE agents SET Allies = '" + secondAgent.Allies + "' WHERE ID =" + secondAgent.ID + ";";

        if (AreTheyEnemies(firstAgent, secondAgent))
            query += GetDeleteAgentFromEnemiesQuery(firstAgent, secondAgent);

        return query;
    }

    public static string GetDeleteAgentFromAlliesQuery(IAgentObject firstAgent, IAgentObject secondAgent)
    {
        firstAgent.Allies = GroupStringManager.DeletePersonFromGroup(firstAgent.Allies, secondAgent.ID.ToString());
        secondAgent.Allies = GroupStringManager.DeletePersonFromGroup(secondAgent.Allies, firstAgent.ID.ToString());

        string query = "UPDATE agents SET Allies = '" + firstAgent.Allies + "' WHERE ID =" + firstAgent.ID + ";" +
                        "UPDATE agents SET Allies = '" + secondAgent.Allies + "' WHERE ID =" + secondAgent.ID + ";";

        return query;
    }

    public static string GetDeleteAgentFromEnemiesQuery(IAgentObject firstAgent, IAgentObject secondAgent)
    {
        firstAgent.Enemies = GroupStringManager.DeletePersonFromGroup(firstAgent.Enemies, secondAgent.ID.ToString());
        secondAgent.Enemies = GroupStringManager.DeletePersonFromGroup(secondAgent.Enemies, firstAgent.ID.ToString());

        string query = "UPDATE agents SET Enemies = '" + firstAgent.Enemies + "' WHERE ID =" + firstAgent.ID + ";" +
                        "UPDATE agents SET Enemies = '" + secondAgent.Enemies + "' WHERE ID =" + secondAgent.ID + ";";

        return query;
    }

    public static bool AreTheyAllis(IAgentObject firstAgent, IAgentObject secondAgent)
    {
        if (GroupStringManager.IsPersonInGroup(secondAgent.Allies, firstAgent.ID.ToString()) && GroupStringManager.IsPersonInGroup(firstAgent.Allies, secondAgent.ID.ToString()))
            return true;
        else
            return false;
    }

    public static bool AreTheyEnemies(IAgentObject firstAgent, IAgentObject secondAgent)
    {
        if (GroupStringManager.IsPersonInGroup(secondAgent.Enemies, firstAgent.ID.ToString()) && GroupStringManager.IsPersonInGroup(firstAgent.Enemies, secondAgent.ID.ToString()))
            return true;
        else
            return false;
    }
}
