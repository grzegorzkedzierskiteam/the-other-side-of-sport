﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IAgent : IAgentObject {
    List<int> AlliesIDs { get; set; }
    List<int> EnemiesIDs { get; set; }
    List<int> ClientsIDs { get; set; }
    void DeleteClients();
    void SearchNewClients();
    void ReSigningContracts();
    void ProposeClientToAnyTeam();
    void SetNewAllyOrEnemy(List<IAgent> listOfAllAgents);
    void LoseAllyOrEnemy(List<IAgent> listOfAllAgents);
    void GetSalaries();
    void ChangeHisMindForRumorReason();
}
