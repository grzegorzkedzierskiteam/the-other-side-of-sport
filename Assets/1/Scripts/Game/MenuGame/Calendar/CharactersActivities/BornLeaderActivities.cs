﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BornLeaderActivities : IAthletesActivities {

    public void ExecuteActivities(IDBConnection dBConnection, IClientObject clientObject)
    {
        EarningMoney(dBConnection, clientObject);
        WastingMoney(dBConnection, clientObject);

        PayOffDept(dBConnection, clientObject);
        BorrowingMoney(dBConnection, clientObject);
        BorrowingMoneyFromAgent(dBConnection, clientObject);
    } 

    private void EarningMoney(IDBConnection dBConnection, IClientObject clientObject)
    {
        AdvertisementPayment(dBConnection, clientObject);
        Investing(dBConnection, clientObject);
        Merchandising(dBConnection, clientObject);
        Working(dBConnection, clientObject);
    }

    private void WastingMoney(IDBConnection dBConnection, IClientObject clientObject)
    {
        DailyExpenses(dBConnection, clientObject);
        FinancialPenalties(dBConnection, clientObject);

        if (clientObject.Wealth > 0)
        {
            BuyExpensiveThing(dBConnection, clientObject);
            Charity(dBConnection, clientObject);
            Gambling(dBConnection, clientObject);
            Shopping(dBConnection, clientObject);
        }
         
        Partying(dBConnection, clientObject);   
    }

    #region EarningMoney
    private void AdvertisementPayment(IDBConnection dBConnection, IClientObject clientObject)
    {
        if(clientObject.AgentID != 1)
        {
            int didHeDoCommercial = Random.Range(0, 100);
            if(didHeDoCommercial < 4)
            {
                int value = Random.Range(100, 5000 * clientObject.Popularity);
                Money.AthleteEarnsMoney(dBConnection, value, clientObject);
            }
        }
    }

    private void BorrowingMoney(IDBConnection dBConnection, IClientObject clientObject)
    {
        if (clientObject.Wealth < 0)
        {
            int isHeBorrowing = Random.Range(0, 50);
            if (isHeBorrowing < 4)
                Money.AthleteInDeptBorrowsMoney(dBConnection, clientObject);
        }
    }

    private void BorrowingMoneyFromAgent(IDBConnection dBConnection, IClientObject clientObject)
    {
        if (clientObject.Wealth < 0 && clientObject.HasAgent)
        {
            int isHeBorrowing = Random.Range(0, 50);
            if (isHeBorrowing < 4)
                Money.AthleteInDeptBorrowsMoneyFromAgent(dBConnection, clientObject);
        }
    }

    private void Merchandising(IDBConnection dBConnection, IClientObject clientObject)
    {
        if(clientObject.Popularity >= 80)
        {
            int value = Random.Range(4000, 3000 * clientObject.Popularity);

            Money.AthleteEarnsMoney(dBConnection, value, clientObject);
        }
    }
    
    private void Investing(IDBConnection dBConnection, IClientObject clientObject)
    {
        int doesHeInvest = Random.Range(0, 20);

        if(doesHeInvest < 5)
        {
            int isPositive = Random.Range(0, 10);

            int value = Random.Range(500, 25000);

            if (isPositive < 6)
                Money.AthleteEarnsMoney(dBConnection, value, clientObject);
            else
                Money.AthleteWastesMoney(dBConnection, value, clientObject);
        }
    }

    private void Working(IDBConnection dBConnection, IClientObject clientObject)
    {
        if(clientObject.Team == "None" || clientObject.Team == "Retirement")
        {
            int isWorking = Random.Range(0, 20);
            if(isWorking < 16)
            {
                int value = Random.Range(290, 3000);
                Money.AthleteEarnsMoney(dBConnection, value, clientObject);
            }
        }
    }
    #endregion

    #region Wasting Money
    private void DailyExpenses(IDBConnection dBConnection, IClientObject clientObject)
    {
        int value = Random.Range(100, 5000);

        Money.AthleteWastesMoney(dBConnection, value, clientObject);
    }

    private void Partying(IDBConnection dBConnection, IClientObject clientObject)
    {
        int didHeGoPartying = Random.Range(0, 40);

        if(didHeGoPartying < 8)
        {
            int value = Random.Range(500, 10000);
            Money.AthleteWastesMoney(dBConnection, value, clientObject);

            int wasPopularityDecreased = Random.Range(0,100);
            if(wasPopularityDecreased < 8)
            {
                int decreasedValue = Random.Range(-2, 0);
                AthleteStatUpdateManager.ChangePopularity(dBConnection, clientObject, decreasedValue);
            }
        }
    }

    private void Shopping(IDBConnection dBConnection, IClientObject clientObject)
    {
        int didHeGoShopping = Random.Range(0, 40);

        if (didHeGoShopping < 6)
        {
            int value = Random.Range(500, 10000);
            Money.AthleteWastesMoney(dBConnection, value, clientObject);
        }
    }

    private void BuyExpensiveThing(IDBConnection dBConnection, IClientObject clientObject)
    {
        if(clientObject.Wealth > 0)
        {
            int didHeBuySomething = Random.Range(0, 100);

            if (didHeBuySomething < 8)
            {
                int value = Random.Range(clientObject.Wealth / 5, clientObject.Wealth / 2);
                Money.AthleteWastesMoney(dBConnection, value, clientObject);
            }
        }
    }

    private void Gambling(IDBConnection dBConnection, IClientObject clientObject)
    {
        int didHeGoGambling = Random.Range(0, 100);

        if(didHeGoGambling < 6)
        {
            int value = (int)(Random.Range(clientObject.Wealth / 8, clientObject.Wealth / 1.2f));
            Money.AthleteWastesMoney(dBConnection, value, clientObject);
        }
    }

    private void FinancialPenalties(IDBConnection dBConnection, IClientObject clientObject)
    {
        int doesHeHaveToPayPenalty = Random.Range(0, 100);

        if (doesHeHaveToPayPenalty < 3)
        {
            int value = Random.Range(50, 10000);
            Money.AthleteWastesMoney(dBConnection, value, clientObject);

            int popularityPenalty = Random.Range(-5, 0);
            AthleteStatUpdateManager.ChangePopularity(dBConnection, clientObject, popularityPenalty);
        }
    }

    private void Charity(IDBConnection dBConnection, IClientObject clientObject)
    {
        if(clientObject.Wealth > 1000000 && clientObject.AgentID != 1)
        {
            int doesHeWorkCharity = Random.Range(0, 110);
            if(doesHeWorkCharity < 5)
            {
                int popularityValue = 0;
                int moneyValue = Random.Range(5000, clientObject.Wealth / 10);

                if (moneyValue > 5000000)
                    popularityValue = 3;
                else if (moneyValue > 1000000)
                    popularityValue = 2;
                else
                    popularityValue = 1;

                Money.AthleteWastesMoney(dBConnection, moneyValue, clientObject);
                AthleteStatUpdateManager.ChangePopularity(dBConnection, clientObject, popularityValue);
            }
        }
    }

    private void PayOffDept(IDBConnection dBConnection, IClientObject clientObject)
    {
        if(clientObject.Wealth > 20000 && clientObject.Dept > 0)
        {
            int doesHePayOffDept = Random.Range(0, 20);
            if(doesHePayOffDept < 8)
            {
                int buffor = 10000;
                int max = 0;
                if (clientObject.Wealth > clientObject.Dept + buffor)
                    max = clientObject.Dept;
                else
                    max = clientObject.Wealth - buffor;

                int howBigPartOfDept = Random.Range(0, 10);
                int money = 0;

                if (howBigPartOfDept > 5)
                    money = max;
                else if (howBigPartOfDept > 3)
                    money = Random.Range(max / 2, max);
                else
                    money = Random.Range(max / 6, max / 2);

                Money.AthletePayOffDept(dBConnection, money, clientObject);
            }
        }
    }
    #endregion
}
