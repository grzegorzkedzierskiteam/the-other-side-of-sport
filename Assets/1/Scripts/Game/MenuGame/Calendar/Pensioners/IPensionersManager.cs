﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPensionersManager {
    void DeletePlayers();
    void RetirePlayers();
}
