﻿using Mono.Data.Sqlite;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PensionersManager : IPensionersManager
{
    private IDBConnection dbConnection;

    public PensionersManager(IDBConnection dbConnection)
    {
        this.dbConnection = dbConnection;
    }

    public void DeletePlayers()
    {
        DeleteClientsOfAgents(GetPlayersToDeleting());

        DeletePlayersFromDB();
    }

    public void RetirePlayers()
    {
        string query = "UPDATE players SET Team = 'Retirement' WHERE Age > 42 AND ContractLenght = 0;";
        dbConnection.ModifyQuery(query);
    }

    private void DeletePlayersFromDB()
    {
        string query = "DELETE FROM players WHERE Age > 50 AND ContractLenght = 0;";
        dbConnection.ModifyQuery(query);
    }

    private void DeleteClientsOfAgents(List<ClientObject> clients)
    {
        if(clients.Count != 0)
        {
            foreach (ClientObject client in clients)
            {
                AgentsSimpleMethods.AgentLosesClient(dbConnection, client.ID, client.AgentID);
            }              
        }
    }

    private List<ClientObject> GetPlayersToDeleting()
    {
        List<ClientObject> list = new List<ClientObject>();
        string query = "SELECT ID, Agent FROM players WHERE Age > 50 AND ContractLenght = 0 AND Agent != -1";
        SqliteDataReader reader = dbConnection.DownloadQuery(query);
        if (reader.HasRows)
        {
            while (reader.Read())
                list.Add(new ClientObject(reader.GetInt32(0), "", "", "", "", "", "", 0, 0, 0, 0, new BornLeader()
                                            , 0, 0, 0, 0, 0, 0, 0, 0, reader.GetInt32(1), 0, "", 0));

            return list;
        }
        else
            return new List<ClientObject>();
    }
}
