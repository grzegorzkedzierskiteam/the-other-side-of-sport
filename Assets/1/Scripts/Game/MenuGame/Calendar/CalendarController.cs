﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CalendarController {

    private readonly IDBConnection dbConnection;

    public CalendarController(IDBConnection _dbConnection)
    {
        dbConnection = _dbConnection;
    }

    public void AdvanceToNextWeek()
    {
        UserDataHolder userDataHolder = GameObject.FindGameObjectWithTag("UserDataHolder").GetComponent<UserDataHolder>();
        userDataHolder.DateTime.AdvanceToNextWeek();

        string query = "UPDATE config SET Week = " + userDataHolder.DateTime.Week + ", Year = " + userDataHolder.DateTime.Year + " WHERE ID = 1;";
        dbConnection.ModifyQuery(query);
    }
	
}
