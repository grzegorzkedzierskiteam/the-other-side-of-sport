﻿using Mono.Data.Sqlite;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayersStatsProgress : IPlayersStatsProgress {

    private readonly IDBConnection dbConnection;

    public PlayersStatsProgress(IDBConnection dbConnection)
    {
        this.dbConnection = dbConnection;
    }

    public void PlayersAgeUp(MyDateTime dateTime)
    {
        if (dateTime.Week == 52)
        {
            var query = "UPDATE players SET Age = Age + 1";
            dbConnection.ModifyQuery(query);
        }
    }

    public void PlayersProgress(MyDateTime dateTime)
    {
        if (dateTime.IsLastWeekOfMonth)
        {
            var playersList = new List<ClientObject>();
            var query = "SELECT ID,Popularity,OverallSkill,Prospectiveness FROM players";
            var reader = dbConnection.DownloadQuery(query);
            while (reader.Read())
                playersList.Add(new ClientObject(reader.GetInt32(0), "", "", "", "", "", "", 0, 0, 0,reader.GetInt32(1), null, 0, 0, 0, 0, 0, 0, reader.GetInt32(2), reader.GetInt32(3), 0, 0, "", 0));

            reader.Close();
            reader = null;

            var overallSkillProgressList = new List<PlayerProgress>();
            var popularityProgressList = new List<PlayerProgress>();

            for (var i = 50; i < 100; i++)
            {
                overallSkillProgressList.Add(new PlayerProgress(i));
                popularityProgressList.Add(new PlayerProgress(i));
            }

            foreach (var player in playersList)
            {
                player.OverallSkill += GetSkillProgress(player.Prospectiveness);

                if (player.OverallSkill > 99)
                    player.OverallSkill = 99;

                if (player.OverallSkill < 50)
                    player.OverallSkill = 50;

                player.Popularity += GetPopularityProgress(player.Popularity);

                if (player.Popularity > 99)
                    player.Popularity = 99;

                if (player.Popularity < 50)
                    player.Popularity = 50;

                overallSkillProgressList.Find(x => x.NewStat == player.OverallSkill).IDsList.Add(player.ID);
                popularityProgressList.Find(x => x.NewStat == player.Popularity).IDsList.Add(player.ID);
            }

            query = "";
            //create queries
            foreach (var progress in overallSkillProgressList)
            {
                if (progress.IsEmpty == false)
                {
                    query += "UPDATE players SET OverallSkill = " + progress.NewStat + " WHERE ID IN(";
                    foreach (var id in progress.IDsList)
                        query += id + ",";

                    query = query.Remove(query.Length - 1);
                    query += ");";
                }
            }

            foreach (var progress in popularityProgressList)
            {
                if (progress.IsEmpty == false)
                {
                    query += "UPDATE players SET Popularity = " + progress.NewStat + " WHERE ID IN(";
                    foreach (var id in progress.IDsList)
                        query += id + ",";

                    query = query.Remove(query.Length - 1);
                    query += ");";
                }
            }

            query += "UPDATE players SET OverallSkill = OverallSkill - 1 WHERE Age > 42 AND ContractLenght == 0;";
            dbConnection.ModifyQuery(query);
        }
    }

    public void SetPlayersReady()
    {
        var query = "UPDATE players SET IsReady = 1 WHERE IsReady != 1;";
        dbConnection.ModifyQuery(query);
    }

    private int GetSkillProgress(int prospectiveness)
    {
        var skillProgress = 0;
        if (prospectiveness < 4)
            skillProgress = Random.Range(-1, 2);
        else if (prospectiveness < 8 && prospectiveness >= 4)
            skillProgress = Random.Range(-2, 3);
        else if (prospectiveness < 10 && prospectiveness >= 8)
            skillProgress = Random.Range(-3, 4);
        else
            skillProgress = Random.Range(-4, 5);

        return skillProgress;
    }

    private int GetPopularityProgress(int popularity)
    {
        var newPopularity = 0;
        if (popularity < 66)
            newPopularity = Random.Range(-1, 2);
        else if (popularity < 75 && popularity >= 66)
            newPopularity = Random.Range(-2, 3);
        else if (popularity < 90 && popularity >= 75)
            newPopularity = Random.Range(-3, 4);
        else
            newPopularity = Random.Range(-4, 5);

        return newPopularity;
    }
}
