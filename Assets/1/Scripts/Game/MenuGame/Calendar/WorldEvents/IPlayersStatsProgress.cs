﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPlayersStatsProgress {
    void PlayersProgress(MyDateTime dateTime);
    void PlayersAgeUp(MyDateTime dateTime);
    void SetPlayersReady();
}
