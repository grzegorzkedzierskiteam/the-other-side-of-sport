﻿using Mono.Data.Sqlite;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgentsWorldEvents : IAgentsWorldEvents {

    private readonly IDBConnection dbConnection;

    public AgentsWorldEvents(IDBConnection dbConnection)
    {
        this.dbConnection = dbConnection;
    }

    public void AgentsStatsUpdate(MyDateTime dateTime)
    {
        if (dateTime.IsLastWeekOfMonth)
        {
            var query = "SELECT ID,Popularity,Respectability FROM agents WHERE ID != 1";
            var reader = dbConnection.DownloadQuery(query);
            var agentsIDs = new List<int>();
            var agentsPopularities = new List<int>();
            var agentsRespectability = new List<int>();

            while (reader.Read())
            {
                agentsIDs.Add(reader.GetInt32(0));
                agentsPopularities.Add(reader.GetInt32(1));
                agentsRespectability.Add(reader.GetInt32(2));
            }

            reader.Close();
            reader = null;

            query = "UPDATE agents SET Respectability = CASE ID";

            for (var i = 0; i < agentsIDs.Count; i++)
            {
                var respectabilityProgress = GetPopularityProgress(agentsRespectability[i]);
                agentsRespectability[i] += respectabilityProgress;

                if (agentsRespectability[i] > 99)
                    agentsRespectability[i] = 99;

                if (agentsRespectability[i] < 50)
                    agentsRespectability[i] = 50;

                query += " WHEN " + agentsIDs[i] + " THEN " + agentsRespectability[i];
            }

            query += " END, Popularity = CASE ID";
            for (var i = 0; i < agentsIDs.Count; i++)
            {
                var popularityProgress = GetPopularityProgress(agentsPopularities[i]);
                agentsPopularities[i] += popularityProgress;

                if (agentsPopularities[i] > 99)
                    agentsPopularities[i] = 99;

                if (agentsPopularities[i] < 50)
                    agentsPopularities[i] = 50;

                query += " WHEN " + agentsIDs[i] + " THEN " + agentsPopularities[i];
            }
            query += " END WHERE ID != 1;";
            query += " UPDATE agents SET Energy = 100;";
            dbConnection.ModifyQuery(query);
        }
    }

    public void RegressRelations()
    {
        var query = "UPDATE players SET Relation = Relation-1 WHERE (Agent = 1 OR Relation  != 50) AND Relation > 0; ";
        dbConnection.ModifyQuery(query);

        query = "UPDATE teams SET Relation = Relation - 1 WHERE Relation  != 50 AND Relation > 0;";
        dbConnection.ModifyQuery(query);

        query = "UPDATE agents SET Relation = Relation - 1 WHERE (Relation != 50 OR Enemies LIKE '1') AND Relation > 0;";
        dbConnection.ModifyQuery(query);
    }

    private int GetSkillProgress(int prospectiveness)
    {
        var skillProgress = 0;
        if (prospectiveness < 4)
            skillProgress = Random.Range(-1, 2);
        else if (prospectiveness < 8 && prospectiveness >= 4)
            skillProgress = Random.Range(-2, 3);
        else if (prospectiveness < 10 && prospectiveness >= 8)
            skillProgress = Random.Range(-3, 4);
        else
            skillProgress = Random.Range(-4, 5);

        return skillProgress;
    }

    private int GetPopularityProgress(int popularity)
    {
        var newPopularity = 0;
        if (popularity < 66)
            newPopularity = Random.Range(-1, 2);
        else if (popularity < 75 && popularity >= 66)
            newPopularity = Random.Range(-2, 3);
        else if (popularity < 90 && popularity >= 75)
            newPopularity = Random.Range(-3, 4);
        else
            newPopularity = Random.Range(-4, 5);

        return newPopularity;
    }
}
