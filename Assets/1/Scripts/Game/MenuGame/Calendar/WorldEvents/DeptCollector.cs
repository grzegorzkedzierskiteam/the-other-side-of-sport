﻿using Mono.Data.Sqlite;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeptCollector : IDeptCollector {

    private readonly IDBConnection dBConnection;

    public DeptCollector(IDBConnection dBConnection)
    {
        this.dBConnection = dBConnection;
    }

    public void CollectDeptsOfAthletes()
    {
        var isDeptCollection = Random.Range(0, 10);
        if (isDeptCollection < 4)
        {
            var query = "SELECT ID, Wealth, Dept, Agent FROM players WHERE Dept > 0 ORDER BY random() * Dept LIMIT 3";
            var reader = dBConnection.DownloadQuery(query);

            var listOfAthletes = new List<IClientObject>();
            if (reader.HasRows)
            {
                while (reader.Read())
                    listOfAthletes.Add(new ClientObject(reader.GetInt32(0), "", "", "", "", "", "", 0, reader.GetInt32(1), reader.GetInt32(2), 0, null, 0, 0, 0, 0, 0, 0, 0, 0, reader.GetInt32(3), 0, "", 0));
            }

            foreach (var client in listOfAthletes)
                Money.DeptCollectionOfAthlete(dBConnection, client);
        }
    }
}
