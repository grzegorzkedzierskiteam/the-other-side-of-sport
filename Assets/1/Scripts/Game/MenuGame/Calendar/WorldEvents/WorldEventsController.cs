﻿using Mono.Data.Sqlite;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldEventsController {

    private readonly IAgentsWorldEvents agentsWorldEvents;
    private readonly IDeptCollector deptCollector;
    private readonly IFortuitousEventsManager fortuitousEventsManager;
    private readonly IPlayersStatsProgress playersStatsProgress;
    private readonly IClientsContractsEvents clientsContractsEvents;
    private readonly IMessageLifeController messageLifeController;
    private readonly IEnergyManager energyManager;

    public WorldEventsController(IAgentsWorldEvents agentsWorldEvents, IClientsContractsEvents clientsContractsEvents, IDeptCollector deptCollector, IEnergyManager energyManager, IFortuitousEventsManager fortuitousEventsManager, IMessageLifeController messageLifeController, IPlayersStatsProgress playersStatsProgress)
    {
        this.agentsWorldEvents = agentsWorldEvents;
        this.clientsContractsEvents = clientsContractsEvents;
        this.deptCollector = deptCollector;
        this.energyManager = energyManager;
        this.fortuitousEventsManager = fortuitousEventsManager;
        this.messageLifeController = messageLifeController;
        this.playersStatsProgress = playersStatsProgress;
    }

    public void ExecuteWorldEvents(MyDateTime dateTime)
    {
        agentsWorldEvents.RegressRelations();
        agentsWorldEvents.AgentsStatsUpdate(dateTime);

        clientsContractsEvents.PlayersContractWithYouExpiring(dateTime);

        playersStatsProgress.PlayersProgress(dateTime);
        playersStatsProgress.PlayersAgeUp(dateTime);

        energyManager.RestorePlayerEnergy();

        messageLifeController.MessageTimeLifeExpiring();

        playersStatsProgress.SetPlayersReady();

        deptCollector.CollectDeptsOfAthletes();

        fortuitousEventsManager.RandomFortuitousEventsForClients(dateTime);
        fortuitousEventsManager.RandomFortuitousEventsForPlayer();
    }
}
