﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IAgentsWorldEvents {
    void RegressRelations();
    void AgentsStatsUpdate(MyDateTime dateTime);
}
