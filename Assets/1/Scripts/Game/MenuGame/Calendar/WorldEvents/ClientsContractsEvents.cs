﻿using Mono.Data.Sqlite;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClientsContractsEvents : IClientsContractsEvents
{
    private readonly IDBConnection dbConnection;

    public ClientsContractsEvents(IDBConnection dbConnection)
    {
        this.dbConnection = dbConnection;
    }

    public void PlayersContractWithYouExpiring(MyDateTime dateTime)
    {
        var query = "SELECT ID FROM players WHERE AgentContractDateEndWeek = " + dateTime.Week + " AND AgentContractDateEndYear == " + dateTime.Year + " AND Agent = 1";
        var reader = dbConnection.DownloadQuery(query);
        if (reader.HasRows)
        {
            var expiringContractsPerClientID = new List<int>();
            while (reader.Read())
                expiringContractsPerClientID.Add(reader.GetInt32(0));

            reader.Close();
            reader = null;

            query = "SELECT Clients FROM agents WHERE ID = 1";
            reader = dbConnection.DownloadQuery(query);
            reader.Read();
            var clients = reader.GetString(0);
            reader.Close();
            reader = null;

            query = "UPDATE players SET Agent = -1 WHERE ID IN(";
            foreach (var id in expiringContractsPerClientID)
            {
                clients = GroupStringManager.DeletePersonFromGroup(clients, id.ToString());
                query += id.ToString() + ",";
            }
            query = query.Remove(query.Length - 1);
            query += ");";

            query += "UPDATE agents SET Clients = '" + clients + "' WHERE ID = 1;";

            dbConnection.ModifyQuery(query);
        }
        else
        {
            reader.Close();
            reader = null;
        }
    }
}
