﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerProgress {
    private readonly List<int> idsList;

    public PlayerProgress(int _newStat)
    {
        NewStat = _newStat;
        idsList = new List<int>();
    }

    public int NewStat { get; }

    public List<int> IDsList
    {
        get { return idsList; }
    }

    public bool IsEmpty
    {
        get
        {
            if (idsList.Count == 0)
                return true;
            else
                return false;
        }
    }
}
