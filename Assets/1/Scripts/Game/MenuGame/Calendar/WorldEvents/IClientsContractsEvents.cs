﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IClientsContractsEvents {
    void PlayersContractWithYouExpiring(MyDateTime dateTime);
}
