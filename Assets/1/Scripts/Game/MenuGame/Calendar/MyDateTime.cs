﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyDateTime {

    private string month;
    private int monthNumber;
    private int year;
    private int weekPerMonth;
    private int week;

    public int Week
    {
        get { return week; }
    }

    public string Month
    {
        get { return month; }
    }

    public int MonthNumber
    {
        get { return monthNumber; }
    }

    public int Year
    {
        get { return year; }
    }

    public int WeekPerMonth
    {
        get { return weekPerMonth; }
    }

    public MyDateTime(int _week, int _year)
    {
        year = _year;
        monthNumber = SetMonthNumber(_week);
        month = SetMonth(monthNumber);
        weekPerMonth = SetWeekPerMonth(_week);
        week = _week;
    }

    private int SetMonthNumber(int week)
    {
        if (week <= 4)
            return 1;

        if (week > 4 && week <= 8)
            return 2;

        if (week > 8 && week <= 13)
            return 3;

        if (week > 13 && week <= 17)
            return 4;

        if (week > 17 && week <= 22)
            return 5;

        if (week > 22 && week <= 26)
            return 6;

        if (week > 26 && week <= 31)
            return 7;

        if (week > 31 && week <= 36)
            return 8;

        if (week > 36 && week <= 40)
            return 9;

        if (week > 40 && week <= 44)
            return 10;

        if (week > 44 && week <= 48)
            return 11;

        if (week > 48 && week <= 52)
            return 12;

        return 0;
    }

    private string SetMonth(int _monthNumber)
    {
        switch(_monthNumber)
        {
            case 1:
                return "January";

            case 2:
                return "February";

            case 3:
                return "March";

            case 4:
                return "April";

            case 5:
                return "May";

            case 6:
                return "June";

            case 7:
                return "July";

            case 8:
                return "August";

            case 9:
                return "September";

            case 10:
                return "October";

            case 11:
                return "November";

            case 12:
                return "December";

            default:
                return "Non Month";
        }
    }

    public bool IsLastWeekOfMonth
    {
        get
        {
            if (week == 4 || week == 8 || week == 13 || week == 17 || week == 22 || week == 26 ||
                week == 31 || week == 36 || week == 40 || week == 44 || week == 48 || week == 52)
                return true;
            else
                return false;
        }
    }

    private int SetWeekPerMonth(int week)
    {
        if (week == 1 || week == 5 || week == 9 || week == 14 || week == 18 || week == 23 ||
            week == 27 || week == 32 || week == 37 || week == 41 || week == 45 || week == 49)
            return 1;

        if (week == 2 || week == 6 || week == 10 || week == 15 || week == 19 || week == 24 ||
            week == 28 || week == 33 || week == 38 || week == 42 || week == 46 || week == 50)
            return 2;

        if (week == 3 || week == 7 || week == 11 || week == 16 || week == 20 || week == 25 ||
            week == 29 || week == 34 || week == 39 || week == 43 || week == 47 || week == 51)
            return 3;

        if (week == 4 || week == 8 || week == 12 || week == 17 || week == 21 || week == 26 ||
            week == 30 || week == 35 || week == 40 || week == 44 || week == 48 || week == 52)
            return 4;

        if (week == 13 || week == 22 || week == 31 || week == 36)
            return 5;

        return 0;
    }

    public void AdvanceToNextWeek()
    {
        if (week == 52)
        {
            week = 0;
            year += 1;
        }
        week += 1;
        monthNumber = SetMonthNumber(week);
        month = SetMonth(monthNumber);
        weekPerMonth = SetWeekPerMonth(week);
    }

    public MyDateTime GetDateAfterAdvanceWeeks(int weeks)
    {
        if ((week + weeks) > 52)
            return new MyDateTime(week + weeks - 52, year + 1);
        else
            return new MyDateTime(week + weeks, year);
    }

    public string FullDateString
    {
        get
        {
            return weekPerMonth + " week" + " " + month + " " + year;
        }
    }
}
