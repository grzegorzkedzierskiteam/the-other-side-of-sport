﻿using Mono.Data.Sqlite;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TeamsMethods {

    public static ClientObject SearchPlayerToTrade(IDBConnection dbConnection, string sport, string position, string team)
    {
        ClientObject wantedPlayer;

        var query = "SELECT ID,FirstName,LastName,Team,Contract,ContractLenght,OverallSkill,Agent,Age,YearOfSign,ContractGuaranteedMoney FROM players WHERE Sport = '" + sport + "' AND Position = '" + position + "' AND InjuryTime == 0 AND Team NOT IN('Retirement','None','" + team + "') AND IsReady = 1 ORDER BY OverallSkill DESC LIMIT 10";
        var reader = dbConnection.DownloadQuery(query);
        if (reader.HasRows)
        {
            var selectedPlayer = Random.Range(1, 9);

            for (var i = 0; i < selectedPlayer; i++)
                reader.Read();

            wantedPlayer = new ClientObject(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), sport, position, "", reader.GetString(3), reader.GetInt32(8), 0, 0, 0, null, 0, reader.GetInt32(4), reader.GetInt32(10), reader.GetInt32(5), reader.GetInt32(9), 0, reader.GetInt32(6), 0, reader.GetInt32(7), 0, "", 0);
        }
        else
            return null;

        reader.Close();
        reader = null;

        return wantedPlayer;
    }

    public static ClientObject SearchPlayerToRetrade(IDBConnection dbConnection, string sport, string position, string team)
    {
        ClientObject wantedPlayer;

        var query = "SELECT ID,FirstName,LastName,Team,Contract,ContractLenght,OverallSkill,Agent,Age,YearOfSign,ContractGuaranteedMoney FROM players WHERE Sport = '" + sport + "' AND Position = '" + position + "' AND InjuryTime == 0 AND Team == '" + team + "' AND IsReady = 1 ORDER BY OverallSkill DESC LIMIT 10";
        var reader = dbConnection.DownloadQuery(query);
        if (reader.HasRows)
        {
            reader.Read();

            wantedPlayer = new ClientObject(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), sport, position, "", reader.GetString(3), reader.GetInt32(8), 0, 0, 0, null, 0, reader.GetInt32(4), reader.GetInt32(10), reader.GetInt32(5) , reader.GetInt32(9), 0, reader.GetInt32(6), 0, reader.GetInt32(7), 0, "", 0);
        }
        else
            return null;

        reader.Close();
        reader = null;

        return wantedPlayer;
    }

    public static void TradeWithTeam(IDBConnection dbConnection, FootballRosterStatus team1, FootballRosterStatus team2, ClientObject wantedPlayer1)
    {
        if (wantedPlayer1.Position == "QB" && GetDepthPosition(dbConnection, wantedPlayer1) == 1)
            return;

        if (team2.CanPlayerBeSold(wantedPlayer1.Position) == false)
            return;

        var random = Random.Range(0, 7);

        if(random != 0)
        {
            team2.SetDemandList();
            var wantedPlayer2 = SearchPlayerToRetrade(dbConnection, "Football", team2.DemandList[Random.Range(0, 3)].Position, team1.TeamName);
            if (wantedPlayer2 == null)
                return;

            if (wantedPlayer2.Position == "QB" && GetDepthPosition(dbConnection, wantedPlayer2) == 1 || team1.CanPlayerBeSold(wantedPlayer2.Position))
            {
                wantedPlayer2 = SearchPlayerToRetrade(dbConnection, "Football", team2.DemandList[Random.Range(0, 3)].Position, team1.TeamName);
                if (wantedPlayer2 == null)
                    return;

                if (wantedPlayer2.Position == "QB" && GetDepthPosition(dbConnection, wantedPlayer2) == 1 || team1.CanPlayerBeSold(wantedPlayer2.Position))
                    return;
            }

            var compromise = Random.Range(0, 4);

            if(compromise != 0)
            {
                SignContract(dbConnection, wantedPlayer1, wantedPlayer1.Contract.Value, wantedPlayer1.Contract.GuaranteedMoney, wantedPlayer1.Contract.Length, wantedPlayer1.Contract.YearOfSign, team1.TeamName, wantedPlayer1.Sport);
                SignContract(dbConnection, wantedPlayer2, wantedPlayer2.Contract.Value, wantedPlayer2.Contract.GuaranteedMoney, wantedPlayer2.Contract.Length, wantedPlayer2.Contract.YearOfSign,team2.TeamName, wantedPlayer2.Sport);
            }
        }
    }

    public static int GetDepthPosition(IDBConnection dbConnection,ClientObject player)
    {
        var query = "SELECT COUNT(*) FROM players WHERE Team = '" + player.Team + "' AND Position ='" + player.Position + "' AND (OverallSkill + Popularity + Prospectiveness) >= (SELECT OverallSkill+Popularity+Prospectiveness FROM players WHERE ID = " + player.ID + ") OR ID = " + player.ID;
        var reader = dbConnection.DownloadQuery(query);
        reader.Read();
        var depth = reader.GetInt32(0);

        reader.Close();
        reader = null;

        return depth;
    }

	public static int NegotiatingWithNewClient(IDBConnection dbConnection,ClientObject client, TeamDetails team, IContractMaker contractMaker, int yearOfSign, int negotiatingStatus = 50)
    {
        var contractOffer = Random.Range(40000, 2000000) * Sport.GetFootballPositionPrestige(client.Position);
        var contractDemand = Random.Range(40000, 2000000) * Sport.GetFootballPositionPrestige(client.Position);
        var contractLength = 0;

        //player's age
        if (client.Age > 45)
            contractLength = Random.Range(1, 2);
        else if (client.Age > 40)
            contractLength = Random.Range(1, 3);
        else if (client.Age > 35)
            contractLength = Random.Range(1, 4);
        else if (client.Age > 30)
            contractLength = Random.Range(1, 5);
        else if (client.Age > 25)
            contractLength = Random.Range(2, 8);
        else
            contractLength = Random.Range(2, 8);

        //player's mood
        var playerMood = Random.Range(-18, 19);
        negotiatingStatus += playerMood;

        //contract
        if (contractOffer >= contractDemand)
            negotiatingStatus += 10;
        else
            negotiatingStatus -= 10;

        //negotiating
        if(negotiatingStatus <= 50)
        {
            if (contractOffer < contractDemand)
            {
                contractOffer = contractDemand;
                negotiatingStatus += 5;
            }

            if(negotiatingStatus <= 50)
            {
                if (contractOffer <= contractDemand)
                {
                    contractOffer = contractOffer + Mathf.RoundToInt(contractOffer * Random.Range(1f,2f));
                    negotiatingStatus += 10;
                }

                if(negotiatingStatus <= 50)
                {
                    IContract contract = new Contract(contractOffer, contractLength, yearOfSign, contractMaker.GetGuaranteedMoneyFromRandomPercent(contractOffer), 0);
                    SignContract(dbConnection, client, contract.Value, contract.GuaranteedMoney, contract.Length, contract.YearOfSign, team.Name, team.Sport);
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            else
            {
                IContract contract = new Contract(contractOffer, contractLength, yearOfSign, contractMaker.GetGuaranteedMoneyFromRandomPercent(contractOffer), 0);
                SignContract(dbConnection, client, contract.Value, contract.GuaranteedMoney, contract.Length, contract.YearOfSign, team.Name, team.Sport);
                return 1;
            }
        }
        else
        {
            IContract contract = new Contract(contractOffer, contractLength, yearOfSign, contractMaker.GetGuaranteedMoneyFromRandomPercent(contractOffer), 0);
            SignContract(dbConnection, client, contract.Value, contract.GuaranteedMoney, contract.Length, contract.YearOfSign, team.Name, team.Sport);
            return 1;
        }
    }

    public static int GenerateNewPlayer(IDBConnection dbConnection, IAddingAthletesToDBController addingAthletesToDBController, IContractMaker contractMaker, int currentYear, string position, string sport, string team)
    {
        var firstName = PersonalDataGenerator.GenerateRandomName(Gender.Male);
        var lastName = PersonalDataGenerator.GenerateRandomSurname();
        var college = PersonalDataGenerator.GenerateRandomCollege();
        var age = Random.Range(21, 30);
        var wealth = PersonalDataGenerator.GenerateRandomWealth(position, age, "Active");
        var dept = 0;
        var popularity = Random.Range(4, 7) * Sport.GetFootballPositionPopularity(position);
        var character = CharacterManager.GetRandomCharacter();
        var prospectiveness = Random.Range(1, 11);
        var overallSkill = Random.Range(50, 81);
        var contract = contractMaker.GetRandomFootballContractFromCurrentYear(currentYear, position,overallSkill,prospectiveness);
        
        if(wealth < 0)
        {
            dept = wealth * -1;
            wealth = 0;
        }

        IClientObject newPlayer = new ClientObject(0, firstName, lastName, sport, position, college, team, age, wealth, dept, popularity, character, 50, contract.Value, contract.GuaranteedMoney, contract.Length, contract.YearOfSign, 0, overallSkill, prospectiveness, -1, 0, "", 0);

        addingAthletesToDBController.AddAthleteToDB(newPlayer);

        return 1;
    }

    public static void GivePlayersGuaranteedMoney(IDBConnection dbConnection, List<int> listOfFiredPlayersIDs)
    {
        var clientObjects = ClientData.GetSelectedPlayersDetails(dbConnection, listOfFiredPlayersIDs);
        
        foreach(IClientObject client in clientObjects)
        {
            if (client.Contract.GuaranteedMoney > 0)
            {
                if (client.Contract.GuaranteedMoney > client.Contract.GotMoney)
                {
                    var earnedMoney = client.Contract.GuaranteedMoney - client.Contract.GotMoney;

                    Money.AthleteEarnsMoney(dbConnection, earnedMoney, client);
                }      
            }
        }
    }

    public static void GivePlayerGuaranteedMoney(IDBConnection dbConnection, int firedPlayerID)
    {
        IClientObject client = ClientData.GetAllDetails(dbConnection, firedPlayerID);

        if(client.Contract.GuaranteedMoney > 0)
        {
            if (client.Contract.GuaranteedMoney > client.Contract.GotMoney)
            {
                var earnedMoney = client.Contract.GuaranteedMoney - client.Contract.GotMoney;
                Money.AthleteEarnsMoney(dbConnection, earnedMoney, client);
            }
        }
    }

    private static void SignContract(IDBConnection dbConnection, ClientObject client, int contract, int contractGuaranteedMoney, int contractLength, int yearOfSign, string team, string sport)
    {
        var query = "UPDATE players SET Contract = " + contract + ", ContractGuaranteedMoney = " + contractGuaranteedMoney + ", ContractLenght = " + contractLength + ", YearOfSign = " + yearOfSign + ", Team = '" + team + "', Sport = '" + sport + "' WHERE ID = " + client.ID + ";";
        dbConnection.ModifyQuery(query);
    }
}
