﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FootballPositionStatus {

    private string position;
    private int numberOfPlayers;
    private string teamName;

    public FootballPositionStatus(string _position, int _numberOfPlayers)
    {
        position = _position;
        numberOfPlayers = _numberOfPlayers;
    }

    public string Position
    {
        get { return position; }
    }

    public int NumberOfPlayers
    {
        get { return numberOfPlayers; }
        set { numberOfPlayers = value; }
    }
}
