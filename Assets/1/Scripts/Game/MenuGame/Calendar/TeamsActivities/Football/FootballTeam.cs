﻿using Mono.Data.Sqlite;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FootballTeam : TeamDetails {

    private IDBConnection dbConnection;
    private UserDataHolder userDataHolder;
    private IContractMaker contractMaker;
    public FootballRosterStatus rosterStatus;

    public FootballTeam(IDBConnection _dbConnection,string _name, string _sport, int _overall, int _popularity, int _relation, string _conference, string _division, UserDataHolder _userDataHolder, IContractMaker _contractMaker) : base( _name, _sport, _overall, _popularity, _relation, _conference, _division)
    {
        dbConnection = _dbConnection;
        rosterStatus = new FootballRosterStatus(dbConnection, _name);
        userDataHolder = _userDataHolder;
        contractMaker = _contractMaker;
    }

    public bool IsTeamHaveEnoughPlayers
    {
        get { return rosterStatus.IsTeamHaveEnoughPlayers; }
    }

    public void SigningFreeAgents()
    {
        var random = Random.Range(0, 20);
        if(random == 0)
            if(rosterStatus.GetMissingPositions().Count != 0)
            {
                var position = rosterStatus.GetMissingPositions();
                var selectedPosition = Random.Range(0, position.Count);

                var clientsList = new List<ClientObject>();
                var query = "SELECT ID,Popularity,OverallSkill,Prospectiveness,Age,Wealth,Dept FROM players WHERE Team = 'None' AND Agent != 1 AND Position ='" + position[selectedPosition].Position + "' AND InjuryTime == 0 ORDER BY OverallSkill DESC LIMIT 10";
                var reader = dbConnection.DownloadQuery(query);
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        clientsList.Add(new ClientObject(reader.GetInt32(0), "", "", "Football", position[selectedPosition].Position, "", "", reader.GetInt32(4), reader.GetInt32(5), reader.GetInt32(6), reader.GetInt32(1), null, 0, 0, 0, 0, 0, 0, reader.GetInt32(2), reader.GetInt32(3), 0, 0, "",0));
                    }
                }
                reader.Close();
                reader = null;

                var selectedPlayer = Random.Range(0, clientsList.Count);
                if(clientsList.Count != 0)
                    position[selectedPosition].NumberOfPlayers -= TeamsMethods.NegotiatingWithNewClient(dbConnection, clientsList[selectedPlayer], this, contractMaker, userDataHolder.DateTime.Year);
            }
    }

    public void ComplementingRosterBeforeStartSeason()
    {
        foreach (FootballPositionStatus position in rosterStatus.GetMissingPositions())
        {
            //-----signing free agents
            List<ClientObject> clientsList = new List<ClientObject>();

            string query = "SELECT ID,Popularity,OverallSkill,Prospectiveness,Age FROM players WHERE Team = 'None' AND Agent != 1 AND Position ='" + position.Position + "' AND InjuryTime == 0 ORDER BY OverallSkill DESC LIMIT 10";
            SqliteDataReader reader = dbConnection.DownloadQuery(query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    clientsList.Add(new ClientObject(reader.GetInt32(0), "", "", "Football", position.Position, "", "", reader.GetInt32(4), 0, 0, reader.GetInt32(1), null, 0, 0, 0, 0, 0, 0, reader.GetInt32(2), reader.GetInt32(3), 0,0,"",0));
                }
            }
            reader.Close();
            reader = null;

            for (int i = 0; i < 2 && position.NumberOfPlayers > 0; i++)
            {
                int numberOfPlayers = 0;
                if (clientsList.Count >= 5)
                    numberOfPlayers = 5;
                else if (clientsList.Count > 0)
                    numberOfPlayers = clientsList.Count;

                int[] selectedPlayers = ArrayMethods.RandomIntArrayRange(0, clientsList.Count - 1, numberOfPlayers);

                if (clientsList.Count >= 5)
                    for (int k = 0; k < 5 && position.NumberOfPlayers > 0; k++)
                        position.NumberOfPlayers -= TeamsMethods.NegotiatingWithNewClient(dbConnection, clientsList[selectedPlayers[k]], this, contractMaker, userDataHolder.DateTime.Year);
                else if(clientsList.Count > 0)
                    for (int k = 0; k < clientsList.Count && position.NumberOfPlayers > 0; k++)
                        position.NumberOfPlayers -= TeamsMethods.NegotiatingWithNewClient(dbConnection, clientsList[selectedPlayers[k]], this, contractMaker, userDataHolder.DateTime.Year);
            }

            //singing retirees
            clientsList = new List<ClientObject>();

            query = "SELECT ID,Popularity,OverallSkill,Prospectiveness,Age FROM players WHERE Team = 'Retirement' AND Agent != 1 AND Position ='" + position.Position + "' AND InjuryTime == 0 ORDER BY OverallSkill DESC LIMIT 10";
            reader = dbConnection.DownloadQuery(query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    clientsList.Add(new ClientObject(reader.GetInt32(0), "", "", "Football", position.Position, "", "", reader.GetInt32(4), 0, 0, reader.GetInt32(1), null, 0, 0, 0, 0, 0, 0, reader.GetInt32(2), reader.GetInt32(3), 0, 0, "",0));
                }
            }
            reader.Close();
            reader = null;

            for (int i = 0; i < 2 && position.NumberOfPlayers > 0; i++)
            {
                int numberOfPlayers = 0;
                if (clientsList.Count >= 5)
                    numberOfPlayers = 5;
                else if (clientsList.Count > 0)
                    numberOfPlayers = clientsList.Count;

                int[] selectedPlayers = ArrayMethods.RandomIntArrayRange(0, clientsList.Count - 1, numberOfPlayers);
                
                if (clientsList.Count >= 5)
                    for (int k = 0; k < 5 && position.NumberOfPlayers > 0; k++)
                        position.NumberOfPlayers -= TeamsMethods.NegotiatingWithNewClient(dbConnection, clientsList[selectedPlayers[k]], this, contractMaker, userDataHolder.DateTime.Year);
                else if (clientsList.Count > 0)
                    for (int k = 0; k < clientsList.Count && position.NumberOfPlayers > 0; k++)
                        position.NumberOfPlayers -= TeamsMethods.NegotiatingWithNewClient(dbConnection, clientsList[selectedPlayers[k]], this, contractMaker, userDataHolder.DateTime.Year);
            }

            //-------------- generate new players
            IAddingAthletesToDBController addingAthletesToDBController = new AddingAthletesToDBController(dbConnection);  

            while (position.NumberOfPlayers > 0)
            { 
                position.NumberOfPlayers -= TeamsMethods.GenerateNewPlayer(dbConnection, addingAthletesToDBController, contractMaker, userDataHolder.DateTime.Year, position.Position, "Football", Name);
            }
        }
    }

    public void CuttingRoster()
    {
        if (rosterStatus.IsTeamHaveTooManyPlayers == true)
        {
            List<FootballPositionStatus> excessPositionsList = rosterStatus.GetPlayersExcessByPosition();
            List<int> playersToCutting = new List<int>();

            foreach (FootballPositionStatus positionStatus in excessPositionsList)
            {
                string downloadQuery = "SELECT ID FROM players WHERE Team = '" + Name + "' AND Position = '" + positionStatus.Position + "' ORDER BY OverallSkill, Popularity, Age DESC";
                SqliteDataReader reader = dbConnection.DownloadQuery(downloadQuery);

                for (int i = 0; i < positionStatus.NumberOfPlayers; i++)
                {
                    reader.Read();
                    playersToCutting.Add(reader.GetInt32(0));
                }

                reader.Close();
                reader = null;
            }

            string query = "UPDATE players SET Team = 'None', Contract = 0, ContractLenght = 0 WHERE ID IN( ";
            foreach (int id in playersToCutting)
                query += id + ",";

            query = query.Remove(query.Length - 1);
            query += ");";

            dbConnection.ModifyQuery(query);

            TeamsMethods.GivePlayersGuaranteedMoney(dbConnection, playersToCutting);
        }
    }

    public void Trading()
    {
        int random = Random.Range(0, 6);

        if(random == 0)
        {
            int whichPosition = Random.Range(0, 3);
            rosterStatus.SetDemandList();
            ClientObject wantedPlayer = TeamsMethods.SearchPlayerToTrade(dbConnection, "Football", rosterStatus.DemandList[whichPosition].Position, Name);

            if(wantedPlayer != null)
                TeamsMethods.TradeWithTeam(dbConnection, rosterStatus, new FootballRosterStatus(dbConnection, wantedPlayer.Team), wantedPlayer);
        }
    }  
}
