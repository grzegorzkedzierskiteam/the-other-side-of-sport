﻿using Mono.Data.Sqlite;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FootballTeamsActivitiesController {

    private IDBConnection dbConnection;
    private UserDataHolder userDataHolder;
    private IContractMaker contractMaker;

    public FootballTeamsActivitiesController(IDBConnection _dbConnection, IContractMaker _contractMaker)
    {
        dbConnection = _dbConnection;
        userDataHolder = GameObject.FindGameObjectWithTag("UserDataHolder").GetComponent<UserDataHolder>();
        contractMaker = _contractMaker;
    }

    public void ExecuteFootballTeamsActivites()
    {
        List<FootballTeam> teams = new List<FootballTeam>();

        string query = "SELECT Name,Character,Popularity,OverallSkill,Conference,Division FROM teams WHERE Sport ='Football' ORDER BY RANDOM()";
        SqliteDataReader reader = dbConnection.DownloadQuery(query);
        if(reader.HasRows)
        {
            while (reader.Read())
                teams.Add(new FootballTeam(dbConnection, reader.GetString(0), "Football", reader.GetInt32(3), reader.GetInt32(2), 0, reader.GetString(4), reader.GetString(5), userDataHolder, contractMaker));
        }
        reader.Close();
        reader = null;

        foreach(FootballTeam team in teams)
        {
            if (team.IsTeamHaveEnoughPlayers == false && userDataHolder.DateTime.Week == 31)
            {
                team.ComplementingRosterBeforeStartSeason();
            }

            if (userDataHolder.DateTime.Week > 6 && userDataHolder.DateTime.Week <= 44)
            {
                team.Trading();
                team.SigningFreeAgents();
            }
                
            if (userDataHolder.DateTime.Week == 36)
                team.CuttingRoster();
        }
    }
}
