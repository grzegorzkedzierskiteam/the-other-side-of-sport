﻿using Mono.Data.Sqlite;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class FootballRosterStatus {

    private readonly IDBConnection dbConnection;
    private readonly string teamName;

    private List<FootballPositionStatus> demandList;
    private List<ClientObject> draftedPlayers;

    private readonly string[] positions = new string[] { "QB", "HB", "FB", "WR", "TE", "OL", "DL", "LB", "CB", "S", "P", "K" };

    private readonly int numberOfQB = 0;
    private readonly int numberOfHB = 0;
    private readonly int numberOfFB = 0;
    private readonly int numberOfWR = 0;
    private readonly int numberOfTE = 0;
    private readonly int numberOfOL = 0;
    private readonly int numberOfDL = 0;
    private readonly int numberOfLB = 0;
    private readonly int numberOfCB = 0;
    private readonly int numberOfS = 0;
    private readonly int numberOfP = 0;
    private readonly int numberOfK = 0;

    public FootballRosterStatus(IDBConnection _dbConnection, string _teamName)
    {
        dbConnection = _dbConnection;
        teamName = _teamName;

        string query = "SELECT Count(*), Position FROM players WHERE Team = '" + _teamName + "' AND Sport = 'Football' AND InjuryTime == 0 Group BY Position";
        SqliteDataReader reader = dbConnection.DownloadQuery(query);
        if(reader.HasRows)
        {
            while(reader.Read())
            {
                switch(reader.GetString(1))
                {
                    case"QB":
                        numberOfQB = reader.GetInt32(0);
                        break;

                    case "HB":
                        numberOfHB = reader.GetInt32(0);
                        break;

                    case "FB":
                        numberOfFB = reader.GetInt32(0);
                        break;

                    case "WR":
                        numberOfWR = reader.GetInt32(0);
                        break;

                    case "TE":
                        numberOfTE = reader.GetInt32(0);
                        break;

                    case "OL":
                        numberOfOL = reader.GetInt32(0);
                        break;

                    case "DL":
                        numberOfDL = reader.GetInt32(0);
                        break;

                    case "LB":
                        numberOfLB = reader.GetInt32(0);
                        break;

                    case "CB":
                        numberOfCB = reader.GetInt32(0);
                        break;

                    case "S":
                        numberOfS = reader.GetInt32(0);
                        break;

                    case "P":
                        numberOfP = reader.GetInt32(0);
                        break;

                    case "K":
                        numberOfK = reader.GetInt32(0);
                        break;

                    default:
                        break;
                }
            }
        }

        reader.Close();
        reader = null;
    }

    public bool IsTeamHaveEnoughPlayers
    {
        get
        {
            if (numberOfQB < 2)
                return false;
            else if (numberOfHB < 2)
                return false;
            else if (numberOfFB < 1)
                return false;
            else if (numberOfWR < 5)
                return false;
            else if (numberOfTE < 2)
                return false;
            else if (numberOfOL < 7)
                return false;
            else if (numberOfDL < 6)
                return false;
            else if (numberOfLB < 6)
                return false;
            else if (numberOfCB < 3)
                return false;
            else if (numberOfS < 4)
                return false;
            else if (numberOfK < 2)
                return false;
            else if (numberOfP < 2)
                return false;
            else
                return true;
        }
    }

    public List<FootballPositionStatus> GetMissingPositions()
    {
        List<FootballPositionStatus> list = new List<FootballPositionStatus>();

        if (numberOfQB < 2)
            list.Add(new FootballPositionStatus("QB", 2 - numberOfQB));
        if (numberOfHB < 2)
            list.Add(new FootballPositionStatus("HB", 2 - numberOfHB));
        if (numberOfFB < 1)
            list.Add(new FootballPositionStatus("FB", 1 - numberOfFB));
        if (numberOfWR < 5)
            list.Add(new FootballPositionStatus("WR", 5 - numberOfWR));
        if (numberOfTE < 2)
            list.Add(new FootballPositionStatus("TE", 2 - numberOfTE));
        if (numberOfOL < 7)
            list.Add(new FootballPositionStatus("OL", 7 - numberOfOL));
        if (numberOfDL < 6)
            list.Add(new FootballPositionStatus("DL", 6 - numberOfDL));
        if (numberOfLB < 6)
            list.Add(new FootballPositionStatus("LB", 6 - numberOfLB));
        if (numberOfCB < 3)
            list.Add(new FootballPositionStatus("CB", 3 - numberOfCB));
        if (numberOfS < 4)
            list.Add(new FootballPositionStatus("S", 4 - numberOfS));
        if (numberOfK < 2)
            list.Add(new FootballPositionStatus("K", 2 - numberOfK));
        if (numberOfP < 2)
            list.Add(new FootballPositionStatus("P", 2 - numberOfP));

        return list;
    }

    public int NumberOfAllPlayers
    {
        get
        {
            return numberOfCB + numberOfDL + numberOfFB + numberOfHB + numberOfK + numberOfLB + numberOfOL + numberOfP + numberOfQB + numberOfS + numberOfTE + numberOfWR;
        }
    }

    public bool AreThereAnyFreeSlotsAtPosition(string position)
    {
        switch(position)
        {
            case "QB":
                if (numberOfQB >= 3)
                    return false;
                else
                    return true;
            case "HB":
                if (numberOfHB >= 3)
                    return false;
                else
                    return true;
            case "FB":
                if (numberOfFB >= 3)
                    return false;
                else
                    return true;
            case "WR":
                if (numberOfWR >= 6)
                    return false;
                else
                    return true;
            case "TE":
                if (numberOfTE >= 3)
                    return false;
                else
                    return true;
            case "OL":
                if (numberOfOL >= 8)
                    return false;
                else
                    return true;
            case "DL":
                if (numberOfDL >= 7)
                    return false;
                else
                    return true;
            case "LB":
                if (numberOfLB >= 7)
                    return false;
                else
                    return true;
            case "CB":
                if (numberOfCB >= 4)
                    return false;
                else
                    return true;
            case "S":
                if (numberOfS >= 5)
                    return false;
                else
                    return true;
            case "P":
                if (numberOfK >= 2)
                    return false;
                else
                    return true;
            case "K":
                if (numberOfP >= 2)
                    return false;
                else
                    return true;

            default:
                return true;
        }
    }

    public bool IsTeamHaveTooManyPlayers
    {
        get
        {
            if (numberOfQB > 3)
                return true;
            else if (numberOfHB > 3)
                return true;
            else if (numberOfFB > 3)
                return true;
            else if (numberOfWR > 6)
                return true;
            else if (numberOfTE > 3)
                return true;
            else if (numberOfOL > 8)
                return true;
            else if (numberOfDL > 7)
                return true;
            else if (numberOfLB > 7)
                return true;
            else if (numberOfCB > 4)
                return true;
            else if (numberOfS > 5)
                return true;
            else if (numberOfK > 2)
                return true;
            else if (numberOfP > 2)
                return true;
            else
                return false;
        }
    }

    public List<FootballPositionStatus> GetPlayersExcessByPosition()
    {
        List<FootballPositionStatus> list = new List<FootballPositionStatus>();

        if (numberOfQB > 3)
            list.Add(new FootballPositionStatus("QB", numberOfQB - 3));

        if (numberOfHB > 3)
            list.Add(new FootballPositionStatus("HB", numberOfHB - 3));

        if (numberOfFB > 3)
            list.Add(new FootballPositionStatus("FB", numberOfFB - 3));

        if (numberOfWR > 6)
            list.Add(new FootballPositionStatus("WR", numberOfWR - 6));

        if (numberOfTE > 3)
            list.Add(new FootballPositionStatus("TE", numberOfTE - 3));

        if (numberOfOL > 8)
            list.Add(new FootballPositionStatus("OL", numberOfOL - 8));

        if (numberOfDL > 7)
            list.Add(new FootballPositionStatus("DL", numberOfDL - 7));

        if (numberOfLB > 7)
            list.Add(new FootballPositionStatus("LB", numberOfLB - 7));

        if (numberOfCB > 4)
            list.Add(new FootballPositionStatus("CB", numberOfCB - 4));

        if (numberOfS > 5)
            list.Add(new FootballPositionStatus("S", numberOfS - 5));

        if (numberOfK > 2)
            list.Add(new FootballPositionStatus("K", numberOfK - 2));

        if (numberOfP > 2)
            list.Add(new FootballPositionStatus("P", numberOfP - 2));

        return list;
    }

#region demandList
    public void SetDemandList()
    {
        demandList = new List<FootballPositionStatus>();
        foreach (string position in positions)
        {
            List<int> overallSkills = new List<int>();
            int numberOfRows = 0;
            string query = "SELECT OverallSkill FROM players WHERE  Team = '" + teamName + "' AND Position = '" + position + "' ORDER BY OverallSkill + Prospectiveness DESC LIMIT 2";
            SqliteDataReader reader = dbConnection.DownloadQuery(query);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    overallSkills.Add(reader.GetInt32(0));
                    numberOfRows += 1;
                }

                if (numberOfRows > 1)
                    demandList.Add(new FootballPositionStatus(position, GetPositionDemand(overallSkills[0], overallSkills[1])));
                else
                    demandList.Add(new FootballPositionStatus(position, GetPositionDemand(overallSkills[0])));
            }
            else
                demandList.Add(new FootballPositionStatus(position, 20));

            reader.Close();
            reader = null;
        }

        demandList = demandList.OrderByDescending(x => x.NumberOfPlayers).ToList();
    }

    public void PlayerWasBought(string position)
    {
        demandList.Find(x => x.Position == position).NumberOfPlayers -= 5;
        demandList = demandList.OrderByDescending(x => x.NumberOfPlayers).ToList();
    }

    private int GetPositionDemand(int overallSkill1)
    {
        int demand = 0;

        if (overallSkill1 > 95)
            demand = 0;
        else if (overallSkill1 > 85)
            demand = 1;
        else if (overallSkill1 > 75)
            demand = 2;
        else if (overallSkill1 > 65)
            demand = 3;
        else if (overallSkill1 > 58)
            demand = 4;
        else
            demand = 5;

        return demand;
    }

    private int GetPositionDemand(int overallSkill1, int overallSkill2)
    {
        int demand = 0;

        if (overallSkill1 > 95)
            demand = 0;
        else if (overallSkill1 > 85)
            demand = 1;
        else if (overallSkill1 > 75)
            demand = 2;
        else if (overallSkill1 > 65)
            demand = 3;
        else if (overallSkill1 > 58)
            demand = 4;
        else
            demand = 5;

        if (overallSkill1 > overallSkill1 + 50)
            demand += 3;
        else if (overallSkill1 > overallSkill1 + 20 || (overallSkill1 >= overallSkill1 + 10 && overallSkill1 < 80))
            demand += 2;
        else if (overallSkill1 > overallSkill1 + 10 || (overallSkill1 >= overallSkill1 + 2 && overallSkill1 < 80))
            demand += 1;

        return demand;
    }

    public List<FootballPositionStatus> DemandList
    {
        get { return demandList; }
    }

#endregion

    public void BeginDraftedPlayersList()
    {
        draftedPlayers = new List<ClientObject>();
    }

    public List<ClientObject> DraftedPlayers
    {
        get { return draftedPlayers; }
    }

    public bool CanPlayerBeSold(string position)
    {
        switch (position)
        {
            case "QB":
                if (numberOfQB < 2)
                    return false;
                else
                    return true;
            case "HB":
                if (numberOfHB < 2)
                    return false;
                else
                    return true;
            case "FB":
                if (numberOfFB < 2)
                    return false;
                else
                    return true;
            case "WR":
                if (numberOfWR < 4)
                    return false;
                else
                    return true;
            case "TE":
                if (numberOfTE < 2)
                    return false;
                else
                    return true;
            case "OL":
                if (numberOfOL < 6)
                    return false;
                else
                    return true;
            case "DL":
                if (numberOfDL < 5)
                    return false;
                else
                    return true;
            case "LB":
                if (numberOfLB < 5)
                    return false;
                else
                    return true;
            case "CB":
                if (numberOfCB < 3)
                    return false;
                else
                    return true;
            case "S":
                if (numberOfS < 3)
                    return false;
                else
                    return true;
            case "P":
                if (numberOfK < 2)
                    return false;
                else
                    return true;
            case "K":
                if (numberOfP < 2)
                    return false;
                else
                    return true;

            default:
                return true;
        }
    }

    public string TeamName
    {
        get { return teamName; }
    }
}
