﻿using Mono.Data.Sqlite;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class FootballDraftController {

    private IDBConnection dbConnection;
    private List<FootballRosterStatus> teamsRosters;
    private List<ClientObject> playersToDraft;
    private IContractMaker contractMaker;
    private int year;

    private IAddingAthletesToDBController addingAthletesController;

    public FootballDraftController(IDBConnection _dbConnection, IAddingAthletesToDBController _addingAthletesController, IContractMaker _contractMaker, int _year)
    {
        dbConnection = _dbConnection;
        addingAthletesController = _addingAthletesController;
        contractMaker = _contractMaker;
        year = _year;
        var teams = GetTeamsToDraft();
        teamsRosters = new List<FootballRosterStatus>();

        foreach (var team in teams)
        {
            var roster = new FootballRosterStatus(dbConnection, team.Name);
            roster.SetDemandList();
            roster.BeginDraftedPlayersList();
            teamsRosters.Add(roster);
        }
    }

    public void ExecuteDraft()
    {
        playersToDraft = GeneratePlayersToDraft();
        MakeDraft();
    }

	private void MakeDraft()
    {
        addingAthletesController.CreateGroupToAddToDB();

        for(var i = 0; i < 7; i++)
        {
            foreach(var team in teamsRosters)
            {
                SelectPlayer(team);
            }
        }

        foreach(var team in teamsRosters)
        {
            SelectProspectsToSign(team);
        }

        addingAthletesController.CommitGroupToDB();
    }

    private void SelectProspectsToSign(FootballRosterStatus roster)
    {
        for(var i = 0; i < 7; i++)
        {
            int isSigned;

            if (i < 3)
                isSigned = Random.Range(0, 100);
            else
                isSigned = Random.Range(0, 50);

            if(isSigned != 0)
            {
                var contractLength = GetContractLength(i+1);
                var contract = GetContract(i + 1, contractLength, roster.DraftedPlayers[i].OverallSkill, roster.DraftedPlayers[i].Prospectiveness) * contractLength;
                var guaranteedMoney = contractMaker.GetGuaranteedMoneyFromRandomPercent(contract);

                IClientObject newPlayer = new ClientObject(0, roster.DraftedPlayers[i].FirstName, roster.DraftedPlayers[i].LastName, "Football", roster.DraftedPlayers[i].Position, roster.DraftedPlayers[i].College, roster.TeamName, roster.DraftedPlayers[i].Age, roster.DraftedPlayers[i].Wealth, roster.DraftedPlayers[i].Dept, roster.DraftedPlayers[i].Popularity, roster.DraftedPlayers[i].Character, 50, contract, guaranteedMoney, contractLength, year, 0, roster.DraftedPlayers[i].OverallSkill, roster.DraftedPlayers[i].Prospectiveness, -1, 0, "", 0, 1);
                addingAthletesController.AddAthleteToGroupToAddToDB(newPlayer);
            }
            else
            {
                IClientObject newPlayer = new ClientObject(0, roster.DraftedPlayers[i].FirstName, roster.DraftedPlayers[i].LastName, "Football", roster.DraftedPlayers[i].Position, roster.DraftedPlayers[i].College, "None", roster.DraftedPlayers[i].Age, roster.DraftedPlayers[i].Wealth, roster.DraftedPlayers[i].Dept, roster.DraftedPlayers[i].Popularity, roster.DraftedPlayers[i].Character, 50, 0, 0, 0, 0, 0, roster.DraftedPlayers[i].OverallSkill, roster.DraftedPlayers[i].Prospectiveness, -1, 0, "", 0, 1);
                addingAthletesController.AddAthleteToGroupToAddToDB(newPlayer);
            }
        }
    }

    private void SelectPlayer(FootballRosterStatus team)
    {
        var position = CheckPositionAvaible(team);
        var newProspect = playersToDraft.First(x => x.Position == position);
        team.DraftedPlayers.Add(newProspect);
        team.PlayerWasBought(position);
        playersToDraft.Remove(newProspect);
    }

    private string CheckPositionAvaible(FootballRosterStatus team)
    {
        var position = "QB";
        
        foreach(var pos in team.DemandList)
        {
            if (playersToDraft.Any(x => x.Position == pos.Position))
            {
                position = pos.Position;
                break;
            }  
        }

        return position;
    }

    private int GetContractLength(int roundOfDraft)
    {
        if (roundOfDraft < 3)
            return Random.Range(3, 5);
        else if(roundOfDraft < 5)
            return Random.Range(2, 5);
        else
            return Random.Range(1, 4);
    }

    private int GetContract(int roundOfDraft,int contractLength, int overallSkill, int prospectiveness)
    {
        float contractTemp = 0;

        if(roundOfDraft == 1)
            contractTemp = Random.Range(1000000, 8500001);
        else if(roundOfDraft == 2)
            contractTemp = Random.Range(500000, 1500000);
        else if(roundOfDraft < 5)
            contractTemp = Random.Range(400000, 900000);
        else
            contractTemp = Random.Range(400000, 600000);

        var contract = (int)(Sport.GetOverallSkillAndProspectivenessEffect(overallSkill, prospectiveness) * contractTemp );

        return contract;
    }

    private List<ClientObject> GeneratePlayersToDraft()
    {
        var playersToDraft = new List<ClientObject>();
        var positions = new string[] { "QB", "HB", "FB", "WR", "TE", "OL", "DL", "LB", "CB", "S", "P", "K" };

        for (var i = 0; i < 300; i++)
        {
            var firstName = PersonalDataGenerator.GenerateRandomName(Gender.Male);
            var lastName = PersonalDataGenerator.GenerateRandomSurname();
            var college = PersonalDataGenerator.GenerateRandomCollege();
            var age = Random.Range(20, 25);

            var position = positions[Random.Range(0, 12)];
            var popularity = Random.Range(4, 10) * Sport.GetFootballPositionPopularity(position);
            var character = CharacterManager.GetRandomCharacter();
            var relation = 50;
            var prospectiveness = Random.Range(1, 11);
            var wealth = Random.Range(2000, 100000);
            var dept = 0;

            var overallSkill = Random.Range(50, 100);
            var player = new ClientObject(i, firstName, lastName, "Football", position, college, "", age, wealth, dept, popularity, character, relation, 0, 0, 0, 0, 0, overallSkill, prospectiveness, -1, 0, "",0);
            playersToDraft.Add(player);
        }


        playersToDraft = playersToDraft.OrderByDescending(x => x.OverallSkill).ToList();
        return playersToDraft;
    }

    private List<TeamDetails> GetTeamsToDraft()
    {
        var teams = new List<TeamDetails>();

        var query = "SELECT Name FROM teams WHERE Sport = 'Football' ORDER BY DraftNumber";
        var reader = dbConnection.DownloadQuery(query);
        if(reader.HasRows)
        {
            while(reader.Read())
            {
                teams.Add(new TeamDetails(reader.GetString(0), "Football", 0, 0, 0, "", ""));
            }
        }

        return teams;
    }
}
