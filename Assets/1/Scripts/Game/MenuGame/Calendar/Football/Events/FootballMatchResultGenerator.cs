﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class FootballMatchResultGenerator {

	public static MatchObject PlayMatchAndGetResult(int eventID, int year, int week, int seasonWeek, TeamDetails team1, TeamDetails team2)
    {
        int[] teamsOverallLevels = GetTeamOverallLevel(team1.Overall, team2.Overall);
        int[] result = GetMatchResult(teamsOverallLevels, seasonWeek);
        MatchObject matchObject = new MatchObject(eventID, year, week, seasonWeek, "Football", team1.Name, result[0], team2.Name, result[1], "", "", false);

        if (result[0] > result[1])
        {
            matchObject.Winner = team1.Name;
            matchObject.Loser = team2.Name;
        }
        else if (result[0] < result[1])
        {
            matchObject.Winner = team2.Name;
            matchObject.Loser = team1.Name;
        }
        else
        {
            matchObject.Draw = true;
        }

        return matchObject;
    }

    private static int[] GetTeamOverallLevel(int overall1, int overall2)
    {
        int difference = overall1 - overall2;

        int[] overallLevels = new int[2];
        overallLevels[0] = 4;
        overallLevels[1] = 4;

        if(difference >= 20)
        {
            overallLevels[0] = 9;
            overallLevels[1] = 0;
        }

        if (difference < 20 && difference >= 16)
        {
            overallLevels[0] = 8;
            overallLevels[1] = 1;
        }

        if (difference < 16 && difference >= 12)
        {
            overallLevels[0] = 7;
            overallLevels[1] = 2;
        }

        if (difference < 12 && difference >= 8)
        {
            overallLevels[0] = 6;
            overallLevels[1] = 3;
        }

        if (difference < 8 && difference >= 4)
        {
            overallLevels[0] = 5;
            overallLevels[1] = 4;
        }

        if (difference < 4 && difference >= -4)
        {
            overallLevels[0] = 4;
            overallLevels[1] = 4;
        }

        if (difference < -4 && difference >= -8)
        {
            overallLevels[0] = 4;
            overallLevels[1] = 5;
        }

        if (difference < -8 && difference >= -12)
        {
            overallLevels[0] = 3;
            overallLevels[1] = 6;
        }

        if (difference < -12 && difference >= -16)
        {
            overallLevels[0] = 2;
            overallLevels[1] = 7;
        }

        if (difference < -16 && difference >= -20)
        {
            overallLevels[0] = 1;
            overallLevels[1] = 8;
        }

        if (difference < -20)
        {
            overallLevels[0] = 0;
            overallLevels[1] = 9;
        }


        return overallLevels;
    }

    private static int[] GetMatchResult(int[] teamsOverallLevels, int seasonWeek)
    {
        int[,] pointsTable = new int[,] {
            { 0,3,0,7,0, 6,0,0,3,0, 0,6,0,0,0, 3,0,7,0,0 },
            { 3,0,6,3,0, 7,0,6,0,7, 3,0,3,7,0, 0,0,0,0,0 },
            { 7,0,0,3,0, 7,3,0,0,3, 0,0,3,0,0, 7,0,0,6,3 },
            { 7,3,3,3,7, 0,0,8,0,0, 7,3,0,7,0, 0,0,6,0,3 },
            { 7,7,3,3,0, 7,7,0,3,0, 7,0,0,0,0, 0,0,6,7,8 },

            { 7,7,7,7,0, 7,3,0,8,6, 7,3,0,3,7, 0,7,0,0,7 },
            { 7,3,0,3,7, 7,0,0,7,0, 7,7,0,0,0, 6,8,7,0,0 },
            { 7,3,0,6,7, 7,8,0,7,7, 7,0,0,0,0, 7,6,7,0,0 },
            { 7,7,0,3,7, 0,0,0,7,6, 0,7,3,8,0, 7,7,0,7,7 },
            { 7,3,3,0,7, 3,0,0,0,8, 7,6,0,7,7, 7,7,7,7,7 }
        };

        int[] finalResult = new int[2];

        for(int i = 0; i < 2; i++)
        {
            for(int k = 0; k < 8; k++)
            {
                int random = Random.Range(0, 20);
                finalResult[i] += pointsTable[teamsOverallLevels[i], random];
            }
        }

        if(finalResult[0] == finalResult[1])
        {
            if(seasonWeek == -2)
            {
                //playoff overtime
                while(finalResult[0] == finalResult[1])
                {
                    for (int i = 0; i < 2; i++)
                    {
                        int random = Random.Range(0, 20);
                        finalResult[i] += pointsTable[teamsOverallLevels[i], random];
                    }
                }
            }
            else
            {
                for (int i = 0; i < 2; i++)
                {
                    int random = Random.Range(0, 20);
                    finalResult[i] += pointsTable[teamsOverallLevels[i], random];
                }
            }
        }

        return finalResult;
    }
}
