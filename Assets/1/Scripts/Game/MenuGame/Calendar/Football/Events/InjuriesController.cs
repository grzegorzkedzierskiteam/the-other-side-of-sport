﻿using Mono.Data.Sqlite;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InjuriesController {

    private IDBConnection dbConnection;

    public InjuriesController(IDBConnection _dbConnection)
    {
        dbConnection = _dbConnection;
    }

    public void GeneratingInjuries(MyDateTime dateTime)
    {
        var query = "SELECT COUNT(*) FROM players WHERE InjuryTime == 0";
        var reader = dbConnection.DownloadQuery(query);
        reader.Read();
        var numberOfAllPlayers = reader.GetInt32(0);
        reader.Close();
        reader = null;

        query = "SELECT ID FROM players WHERE InjuryTime == 0";
        reader = dbConnection.DownloadQuery(query);
        var playersIDs = new List<int>();
        while (reader.Read())
            playersIDs.Add(reader.GetInt32(0));

        reader.Close();
        reader = null;

        query = "";

        for(var i = 0; i < numberOfAllPlayers; i++)
        {
            var injury = Random.Range(0, 451);
            if(injury <= 4 && IsSeasonContinuing(dateTime) == true)
            {
                var injuryName = GenerateInjuryName();
                var recoveryTime = GetInjuryRecoveryTime(injuryName);
                query += "UPDATE players SET InjuryTime = " + recoveryTime + ", InjuryName = '" + injuryName + "' WHERE ID = " + playersIDs[i] + ";";
            }

            if (injury <= 2 && IsSeasonContinuing(dateTime) == false)
            {
                var injuryName = GenerateInjuryName();
                var recoveryTime = GetInjuryRecoveryTime(injuryName);

                query += "UPDATE players SET InjuryTime = " + recoveryTime + ", InjuryName = '" + injuryName + "' WHERE ID = " + playersIDs[i] + ";";
            }
        }

        dbConnection.ModifyQuery(query);
    }

    public void Recovering()
    {
        var query = "UPDATE players SET InjuryName = '' WHERE InjuryTime == 1;";
        query += "UPDATE players SET InjuryTime = InjuryTime - 1 WHERE InjuryTime > 0;";

        dbConnection.ModifyQuery(query);
    }

    public InjuryStatus GenerateInjury()
    {
        var injuryName = GenerateInjuryName();
        var injuryTime = GetInjuryRecoveryTime(injuryName);

        return new InjuryStatus(injuryName, injuryTime);
    }

    private string GenerateInjuryName()
    {
        var injury = Random.Range(0, 101);

        if (injury <= 22)
            return "Knee";
        else if (injury <= 37)
            return "Ankle";
        else if (injury <= 49)
            return "Upper Leg";
        else if (injury <= 57)
            return "Shoulder";
        else if (injury <= 64)
            return "Head";
        else if (injury <= 71)
            return "Foot";
        else if (injury <= 76)
            return "Groin";
        else if (injury <= 81)
            return "Hand";
        else if (injury <= 85)
            return "Core";
        else if (injury <= 89)
            return "Back";
        else if (injury <= 91)
            return "Hip";
        else if (injury <= 94)
            return "Neck";
        else if (injury <= 96)
            return "Lower Leg";
        else if (injury <= 98)
            return "Elbow";
        else
            return "Arm";
    }

    private int GetInjuryRecoveryTime(string injuryName)
    {
        switch(injuryName)
        {
            case "Knee":
                return Random.Range(2, 53);
            case "Ankle":
                return Random.Range(6, 17);
            case "Upper Leg":
                return Random.Range(1, 25);
            case "Shoulder":
                return Random.Range(8, 13);
            case "Head":
                return Random.Range(1, 3);
            case "Foot":
                return Random.Range(1, 5);
            case "Groin":
                return Random.Range(1, 25);
            case "Hand":
                return Random.Range(1, 5);
            case "Core":
                return Random.Range(6, 25);
            case "Back":
                return Random.Range(6, 25);
            case "Hip":
                return Random.Range(3, 13);
            case "Neck":
                return Random.Range(10, 13);
            case "Lower Leg":
                return Random.Range(2, 9);
            case "Elbow":
                return Random.Range(2, 9);
            case "Arm":
                return Random.Range(4, 13);

            default:
                return 1;
        }
    }

    private bool IsSeasonContinuing(MyDateTime dateTime)
    {
        return dateTime.Week >= 37 || dateTime.Week <= 5;
    }
}
