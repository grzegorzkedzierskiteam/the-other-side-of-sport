﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FootballPlayoffGenerator : IFootballPlayoffGenerator{

    private DBConnection dbConnection;

    public FootballPlayoffGenerator(DBConnection dbConnection)
    {
        this.dbConnection = dbConnection;
    }

    public void CreateConferenceGames(int year)
    {
        throw new NotImplementedException();
    }

    public void CreateDivisionRoundGames(int year)
    {
        throw new NotImplementedException();
    }

    public void CreateGrandFinalGame(int year)
    {
        throw new NotImplementedException();
    }

    public void CreatePlayoffGames(int year)
    {
        throw new NotImplementedException();
    }
}
