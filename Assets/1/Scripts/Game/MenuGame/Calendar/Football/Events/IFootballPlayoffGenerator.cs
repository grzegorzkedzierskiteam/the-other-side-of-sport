﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IFootballPlayoffGenerator {
    void CreatePlayoffGames(int year);
    void CreateDivisionRoundGames(int year);
    void CreateConferenceGames(int year);
    void CreateGrandFinalGame(int year);
}
