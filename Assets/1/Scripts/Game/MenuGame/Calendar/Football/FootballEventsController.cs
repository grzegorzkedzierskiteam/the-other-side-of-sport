﻿using Mono.Data.Sqlite;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class FootballEventsController {

    private readonly IDBConnection dbConnection;
    private readonly IPensionersManager pensionerManager;
    private readonly UserDataHolder userDataHolder;
    private readonly InjuriesController injuriesController;

    private List<EventObject> allEventsList;
    private List<TeamDetails> allTeams;
    private List<MatchObject> allMatchesFinished;

    //draft teams
    List<TeamDetails> americanTeams;
    List<TeamDetails> nationalTeams;
    List<TeamDetails> americanTeams2;
    List<TeamDetails> nationalTeams2;
    List<string> wildCardsLosers;
    List<string> divisionsLosers;
    List<string> conferencesLosers;

    public FootballEventsController(IDBConnection _dbConnection, IPensionersManager pensionerManager)
    {
        dbConnection = _dbConnection;
        this.pensionerManager = pensionerManager;
        userDataHolder = GameObject.FindGameObjectWithTag("UserDataHolder").GetComponent<UserDataHolder>();
        injuriesController = new InjuriesController(dbConnection);
    }

    public void ExecuteFootballEvents()
    {
        if(EventsExist())
        {
            UpdateTeamsOveralls();
            allEventsList = GetAllEventsFromCurrentWeek(userDataHolder.DateTime.Year);
            allTeams = GetAllTeams();
            allMatchesFinished = new List<MatchObject>();
            foreach(var eventObject in allEventsList)
            {
                if(eventObject.EventType == "Game")
                    EventExecution(eventObject);
            }

            UpdateDataBase();

            foreach (var eventObject in allEventsList)
            {
                if (eventObject.EventType == "Event")
                    EventExecution(eventObject);
            }
        }

        //injuries
        injuriesController.Recovering();
        injuriesController.GeneratingInjuries(userDataHolder.DateTime);
    }

    private bool EventsExist()
    {
        int firstEventWeek;
        int lastEventWeek;

        var query = "SELECT Min(Week) FROM calendar where sport = 'Football'";
        var reader = dbConnection.DownloadQuery(query);
        reader.Read();
        firstEventWeek = reader.GetInt32(0);
        reader.Close();
        reader = null;

        query = "SELECT Max(Week) FROM calendar where sport = 'Football'";
        reader = dbConnection.DownloadQuery(query);
        reader.Read();
        lastEventWeek = reader.GetInt32(0);
        reader.Close();
        reader = null;

        if (userDataHolder.DateTime.Week >= firstEventWeek && userDataHolder.DateTime.Week <= lastEventWeek)
            return true;
        else
            return false;
    }

    private List<EventObject> GetAllEventsFromCurrentWeek(int year)
    {
        var allEvents = new List<EventObject>();
        var query = "SELECT EventType, Info, Team1, Team2, SeasonWeek, ID, MatchType FROM calendar WHERE Week = " + userDataHolder.DateTime.Week + " AND Sport = 'Football' AND Year = " + year + ";";
        var reader = dbConnection.DownloadQuery(query);
        while(reader.Read())
        {
            var eventObject = new EventObject();
            eventObject.Year = year;
            eventObject.Week = userDataHolder.DateTime.Week;
            eventObject.EventType = reader.GetString(0);
            eventObject.Info = reader.GetString(1);
            eventObject.Team1 = reader.GetString(2);
            eventObject.Team2 = reader.GetString(3);
            eventObject.SeasonWeek = reader.GetInt32(4);
            eventObject.Sport = "Football";
            eventObject.ID = reader.GetInt32(5);
            eventObject.MatchType = reader.GetString(6);
            allEvents.Add(eventObject);
        }
        reader.Close();
        reader = null;
        return allEvents;
    }

    private List<TeamDetails> GetAllTeams()
    {
        var allTeams = new List<TeamDetails>();
        var query = "SELECT Name, OverallSkill, Popularity, Relation, Conference, Division FROM teams WHERE Sport = 'Football';";
        var reader = dbConnection.DownloadQuery(query);
        while(reader.Read())
        {
            allTeams.Add(new TeamDetails(reader.GetString(0), "Football", reader.GetInt32(1), reader.GetInt32(2), reader.GetInt32(3), reader.GetString(4), reader.GetString(5)));
        }

        return allTeams;
    }

    private void EventExecution(EventObject eventObject)
    {
        if(eventObject.EventType == "Game")
        {
            var team1 = allTeams.Find(x => x.Name == eventObject.Team1);
            var team2 = allTeams.Find(x => x.Name == eventObject.Team2);
            allMatchesFinished.Add(FootballMatchResultGenerator.PlayMatchAndGetResult(eventObject.ID, eventObject.Year, eventObject.Week, eventObject.SeasonWeek,team1, team2));
            team1 = null;
            team2 = null;
        }

        if(eventObject.EventType == "Event")
        {
            switch(eventObject.Info)
            {
                case "End of Regular Season.":
                    CreatePlayoffGames(eventObject.Year);
                    SetFirstDraftNumbers();
                    break;

                case "End of Wild Cards Games.":
                    CreateDivisionRoundGames(eventObject.Year);
                    SetWildCardsDraftNumbers();
                    break;

                case "End of Division Games.":
                    CreateConferenceGames(eventObject.Year);
                    SetDivisionDraftNumbers();
                    break;

                case "End of Conference Games.":
                    CreateGrandFinalGame(eventObject.Year);
                    SetConferenceDraftNumbers();
                    break; 

                case "Start new football season.":
                    StartNewFootballSeason(eventObject.Year);
                    break;

                case "End of the season.":
                    SetGrandFinalDraftNumbers(eventObject.Year);
                    ContractsExpiring();
                    pensionerManager.RetirePlayers();
                    pensionerManager.DeletePlayers();
                    break;

                case "Football Draft.":
                    MakeDraft();
                    break;
            }
        }
    }

    private void UpdateDataBase()
    {
        var queryCalendar = "";
        var queryTeams = "";
        foreach (var match in allMatchesFinished)
        {
            queryCalendar += "UPDATE calendar SET Team1Score =" + match.Team1Score + ", Team2Score = " + match.Team2Score + ", Finished = 1 WHERE ID=" + match.ID + " AND Year = " + match.Year + ";";

            if(match.SeasonWeek != -2)
            {
                if (match.Draw == true)
                    queryTeams += "UPDATE teams SET Draws = Draws + 1 WHERE Name = '" + match.Team1 + "' OR Name = '" + match.Team2 + "';";
                else
                {
                    if (match.Winner == match.Team1)
                    {
                        queryTeams += "UPDATE teams SET Wins = Wins + 1 WHERE Name = '" + match.Team1 + "';";
                        queryTeams += "UPDATE teams SET Loses = Loses + 1 WHERE Name = '" + match.Team2 + "';";
                    }

                    if (match.Winner == match.Team2)
                    {
                        queryTeams += "UPDATE teams SET Loses = Loses + 1 WHERE Name = '" + match.Team1 + "';";
                        queryTeams += "UPDATE teams SET Wins = Wins + 1 WHERE Name = '" + match.Team2 + "';";
                    }
                }
            }
            
        }

        dbConnection.ModifyQuery(queryCalendar);
        dbConnection.ModifyQuery(queryTeams);
    }

    #region events methods

    private void SetFirstDraftNumbers()
    {
        var query = "SELECT Name FROM teams WHERE Sport = 'Football' ORDER BY Wins,Draws,Loses DESC";
        var reader = dbConnection.DownloadQuery(query);
        var teamsToDraft = new List<string>();
        var teamsToDeleteFromList = new List<string>();

        while (reader.Read())
            teamsToDraft.Add(reader.GetString(0));

        reader.Close();
        reader = null;

        foreach(var teamToDraft in teamsToDraft)
        {
            if (teamToDraft == americanTeams[0].Name)
                teamsToDeleteFromList.Add(teamToDraft);
            if (teamToDraft == americanTeams[1].Name)
                teamsToDeleteFromList.Add(teamToDraft);
            if (teamToDraft == americanTeams[2].Name)
                teamsToDeleteFromList.Add(teamToDraft);
            if (teamToDraft == americanTeams[3].Name)
                teamsToDeleteFromList.Add(teamToDraft);

            if (teamToDraft == americanTeams2[0].Name)
                teamsToDeleteFromList.Add(teamToDraft);
            if (teamToDraft == americanTeams2[1].Name)
                teamsToDeleteFromList.Add(teamToDraft);

            if (teamToDraft == nationalTeams[0].Name)
                teamsToDeleteFromList.Add(teamToDraft);
            if (teamToDraft == nationalTeams[1].Name)
                teamsToDeleteFromList.Add(teamToDraft);
            if (teamToDraft == nationalTeams[2].Name)
                teamsToDeleteFromList.Add(teamToDraft);
            if (teamToDraft == nationalTeams[3].Name)
                teamsToDeleteFromList.Add(teamToDraft);

            if (teamToDraft == nationalTeams2[0].Name)
                teamsToDeleteFromList.Add(teamToDraft);
            if (teamToDraft == nationalTeams2[1].Name)
                teamsToDeleteFromList.Add(teamToDraft);
        }

        foreach (var teamToDelete in teamsToDeleteFromList)
            teamsToDraft.Remove(teamToDelete);

        var draftNumber = 1;
        query = "";

        foreach(var team in teamsToDraft)
        {
            query += "UPDATE teams SET DraftNumber = " + draftNumber + " WHERE Sport = 'Football' AND Name = '" + team + "';";
            draftNumber += 1;
        }

        dbConnection.ModifyQuery(query);
    }

    private void SetWildCardsDraftNumbers()
    {
        var query = "SELECT ID FROM teams WHERE Name IN('" + wildCardsLosers[0] + "','" + wildCardsLosers[1] + "','" + wildCardsLosers[2] + "','" + wildCardsLosers[3] + "') ORDER BY Wins,Loses DESC,Draws";
        var reader = dbConnection.DownloadQuery(query);
        var teamsToDraft = new List<int>();
        while (reader.Read())
            teamsToDraft.Add(reader.GetInt32(0));

        reader.Close();
        reader = null;

        var draftNumber = 21;
        query = "";

        foreach (var team in teamsToDraft)
        {
            query += "UPDATE teams SET DraftNumber = " + draftNumber + " WHERE ID = " + team + ";";
            draftNumber += 1;
        }

        dbConnection.ModifyQuery(query);
    }

    private void SetDivisionDraftNumbers()
    {
        var query = "SELECT ID FROM teams WHERE Name IN('" + divisionsLosers[0] + "','" + divisionsLosers[1] + "','" + divisionsLosers[2] + "','" + divisionsLosers[3] + "') ORDER BY Wins,Loses DESC,Draws";
        var reader = dbConnection.DownloadQuery(query);
        var teamsToDraft = new List<int>();
        while (reader.Read())
            teamsToDraft.Add(reader.GetInt32(0));

        reader.Close();
        reader = null;

        var draftNumber = 25;
        query = "";

        foreach (var team in teamsToDraft)
        {
            query += "UPDATE teams SET DraftNumber = " + draftNumber + " WHERE ID = " + team + ";";
            draftNumber += 1;
        }

        dbConnection.ModifyQuery(query);
    }

    private void SetConferenceDraftNumbers()
    {
        var query = "SELECT ID FROM teams WHERE Name IN('" + conferencesLosers[0] + "','" + conferencesLosers[1] + "') ORDER BY Wins,Loses DESC,Draws";
        var reader = dbConnection.DownloadQuery(query);
        var teamsToDraft = new List<int>();
        while (reader.Read())
            teamsToDraft.Add(reader.GetInt32(0));

        reader.Close();
        reader = null;

        var draftNumber = 29;
        query = "";

        foreach (var team in teamsToDraft)
        {
            query += "UPDATE teams SET DraftNumber = " + draftNumber + " WHERE ID = " + team + ";";
            draftNumber += 1;
        }

        dbConnection.ModifyQuery(query);
    }

    private void SetGrandFinalDraftNumbers(int year)
    {
        var query = "SELECT Team1,Team1Score,Team2,Team2Score FROM calendar WHERE MatchType = 'Football Grand Final' AND Year = " + year + " AND Sport = 'Football' ORDER BY ID;";
        var reader = dbConnection.DownloadQuery(query);
        var grandFinalWinner = "";
        var grandFinalLoser = "";

        while (reader.Read())
        {
            if (reader.GetInt32(1) > reader.GetInt32(3))
            {
                grandFinalWinner = reader.GetString(0);
                grandFinalLoser = reader.GetString(2);
            }
            else
            {
                grandFinalWinner = reader.GetString(2);
                grandFinalLoser = reader.GetString(0);
            }
        }

        reader.Close();
        reader = null;

        query = "UPDATE teams SET DraftNumber = 31 WHERE Sport = 'Football' AND Name = '" + grandFinalLoser + "';";
        query += "UPDATE teams SET DraftNumber = 32 WHERE Sport = 'Football' AND Name = '" + grandFinalWinner + "';";

        dbConnection.ModifyQuery(query);
    }

    private void CreatePlayoffGames(int year)
    {
        //get teams
        var query = "SELECT Name,Conference,Division,Wins,Loses,Draws FROM teams ORDER BY Wins desc, Draws desc, Loses, RANDOM();";
        var reader = dbConnection.DownloadQuery(query);
        americanTeams = new List<TeamDetails>();
        nationalTeams = new List<TeamDetails>();
        americanTeams2 = new List<TeamDetails>();
        nationalTeams2 = new List<TeamDetails>();

        while (reader.Read())
        {
            if (reader.GetString(1) == "American")
            {
                if (americanTeams.FindIndex(x => x.Division == reader.GetString(2)) < 0)
                {
                    var team = new TeamDetails(reader.GetString(0), "", 0, 0, 0, reader.GetString(1), reader.GetString(2));
                    team.Wins = reader.GetInt32(3);
                    team.Loses = reader.GetInt32(4);
                    team.Draws = reader.GetInt32(5);
                    americanTeams.Add(team);
                }
                else if (americanTeams2.FindIndex(x => x.Division == reader.GetString(2)) < 0)
                {
                    var team = new TeamDetails(reader.GetString(0), "", 0, 0, 0, reader.GetString(1), reader.GetString(2));
                    team.Wins = reader.GetInt32(3);
                    team.Loses = reader.GetInt32(4);
                    team.Draws = reader.GetInt32(5);
                    americanTeams2.Add(team);
                }
            }

            if (reader.GetString(1) == "National")
            {
                if (nationalTeams.FindIndex(x => x.Division == reader.GetString(2)) < 0)
                {
                    var team = new TeamDetails(reader.GetString(0), "", 0, 0, 0, reader.GetString(1), reader.GetString(2));
                    team.Wins = reader.GetInt32(3);
                    team.Loses = reader.GetInt32(4);
                    team.Draws = reader.GetInt32(5);
                    nationalTeams.Add(team);
                }
                else if (nationalTeams2.FindIndex(x => x.Division == reader.GetString(2)) < 0)
                {
                    var team = new TeamDetails(reader.GetString(0), "", 0, 0, 0, reader.GetString(1), reader.GetString(2));
                    team.Wins = reader.GetInt32(3);
                    team.Loses = reader.GetInt32(4);
                    team.Draws = reader.GetInt32(5);
                    nationalTeams2.Add(team);
                }
            }
        }

        reader.Close();
        reader = null;

        //get wild cards and division games ids
        query = "SELECT ID FROM calendar WHERE MatchType = 'Wild Card Round' AND Team1 = '' AND Year = " + (year + 1).ToString() + " AND Sport = 'Football' ORDER BY ID;;";
        reader = dbConnection.DownloadQuery(query);
        var wildCardsGamesIds = new List<int>();
        while(reader.Read())
        {
            wildCardsGamesIds.Add(reader.GetInt32(0));
        }
        reader.Close();
        reader = null;

        query = "SELECT ID FROM calendar WHERE MatchType = 'Division Round' AND Team1 = '' AND Year = " + (year + 1).ToString() + " AND Sport = 'Football' ORDER BY ID;;";
        reader = dbConnection.DownloadQuery(query);
        var divisionGamesId = new List<int>();
        while (reader.Read())
        {
            divisionGamesId.Add(reader.GetInt32(0));
        }
        reader.Close();
        reader = null;

        query = @"UPDATE calendar SET team2 = '" + americanTeams[0].Name + "' WHERE ID = " + divisionGamesId[0] + ";" +
                "UPDATE calendar SET team2 = '" + americanTeams[1].Name + "' WHERE ID = " + divisionGamesId[1] + ";" +
                "UPDATE calendar SET team2 = '" + americanTeams[2].Name + "', team1 = '" + americanTeams2[1].Name + "' WHERE ID = " + wildCardsGamesIds[0] + ";" +
                "UPDATE calendar SET team2 = '" + americanTeams[3].Name + "', team1 = '" + americanTeams2[0].Name + "' WHERE ID = " + wildCardsGamesIds[1] + ";" +
                "UPDATE calendar SET team2 = '" + nationalTeams[0].Name + "' WHERE ID = " + divisionGamesId[2] + ";" +
                "UPDATE calendar SET team2 = '" + nationalTeams[1].Name + "' WHERE ID = " + divisionGamesId[3] + ";" +
                "UPDATE calendar SET team2 = '" + nationalTeams[2].Name + "', team1 = '" + nationalTeams2[1].Name + "' WHERE ID = " + wildCardsGamesIds[2] + ";" +
                "UPDATE calendar SET team2 = '" + nationalTeams[3].Name + "', team1 = '" + nationalTeams2[0].Name + "' WHERE ID = " + wildCardsGamesIds[3] + ";";
        dbConnection.ModifyQuery(query);
    }

    private void CreateDivisionRoundGames(int year)
    {
        var query = "SELECT Team1,Team1Score,Team2,Team2Score FROM calendar WHERE MatchType = 'Wild Card Round' AND Year = " + year + " AND Sport = 'Football' ORDER BY ID;";
        var reader = dbConnection.DownloadQuery(query);
        var wildCardsWinners = new List<string>();
        wildCardsLosers = new List<string>();
        while(reader.Read())
        {
            if(reader.GetInt32(1) > reader.GetInt32(3))
            {
                wildCardsWinners.Add(reader.GetString(0));
                wildCardsLosers.Add(reader.GetString(2));
            }
            else
            {
                wildCardsWinners.Add(reader.GetString(2));
                wildCardsLosers.Add(reader.GetString(0));
            }
        }

        reader.Close();
        reader = null;

        //get wild cards winners stats
        var americanWildCardsWinners = new List<string>();
        var nationalWildCardsWinners = new List<string>();
        query = "SELECT Name FROM teams WHERE Name = '" + wildCardsWinners[0] + "' OR Name = '" + wildCardsWinners[1] + "' ORDER BY Wins desc, Draws desc, Loses;";
        reader = dbConnection.DownloadQuery(query);
        while(reader.Read())
        {
            americanWildCardsWinners.Add(reader.GetString(0));
        }

        reader.Close();
        reader = null;

        query = "SELECT Name FROM teams WHERE Name = '" + wildCardsWinners[2] + "' OR Name = '" + wildCardsWinners[3] + "' ORDER BY Wins desc, Draws desc, Loses;";
        reader = dbConnection.DownloadQuery(query);
        while (reader.Read())
        {
            nationalWildCardsWinners.Add(reader.GetString(0));
        }

        reader.Close();
        reader = null;

        //get division games
        var divisionGamesIds = new List<int>();
        query = "SELECT ID FROM calendar WHERE MatchType = 'Division Round' AND Year = " + year + " AND Sport = 'Football' ORDER BY ID;";
        reader = dbConnection.DownloadQuery(query);
        while (reader.Read())
        {
            divisionGamesIds.Add(reader.GetInt32(0));
        }

        reader.Close();
        reader = null;

        //set division games
        query = @"UPDATE calendar SET team1 = '" + americanWildCardsWinners[1] + "' WHERE ID = " + divisionGamesIds[0] + ";" +
                "UPDATE calendar SET team1 = '" + americanWildCardsWinners[0] + "' WHERE ID = " + divisionGamesIds[1] + ";" +
                "UPDATE calendar SET team1 = '" + nationalWildCardsWinners[1] + "' WHERE ID = " + divisionGamesIds[2] + ";" +
                "UPDATE calendar SET team1 = '" + nationalWildCardsWinners[0] + "' WHERE ID = " + divisionGamesIds[3] + ";";
        dbConnection.ModifyQuery(query);
    }

    private void CreateConferenceGames(int year)
    {
        var query = "SELECT Team1,Team1Score,Team2,Team2Score FROM calendar WHERE MatchType = 'Division Round' AND Year = " + year + " AND Sport = 'Football' ORDER BY ID;";
        var reader = dbConnection.DownloadQuery(query);
        var divisionWinners = new List<string>();
        divisionsLosers = new List<string>();
        while (reader.Read())
        {
            if (reader.GetInt32(1) > reader.GetInt32(3))
            {
                divisionWinners.Add(reader.GetString(0));
                divisionsLosers.Add(reader.GetString(2));
            }
            else
            {
                divisionWinners.Add(reader.GetString(2));
                divisionsLosers.Add(reader.GetString(0));
            }
        }

        reader.Close();
        reader = null;

        //get division winners stats
        var americanDivisionWinners = new List<string>();
        var nationalDivisionWinners = new List<string>();
        query = "SELECT Name FROM teams WHERE Name = '" + divisionWinners[0] + "' OR Name = '" + divisionWinners[1] + "' ORDER BY Wins desc, Draws desc, Loses;";
        reader = dbConnection.DownloadQuery(query);
        while (reader.Read())
        {
            americanDivisionWinners.Add(reader.GetString(0));
        }

        reader.Close();
        reader = null;

        query = "SELECT Name FROM teams WHERE Name = '" + divisionWinners[2] + "' OR Name = '" + divisionWinners[3] + "' ORDER BY Wins desc, Draws desc, Loses;";
        reader = dbConnection.DownloadQuery(query);
        while (reader.Read())
        {
            nationalDivisionWinners.Add(reader.GetString(0));
        }

        reader.Close();
        reader = null;

        //get conference games
        var conferenceGamesIds = new List<int>();
        query = "SELECT ID FROM calendar WHERE MatchType = 'Conference Final' AND Year = " + year + " AND Sport = 'Football' ORDER BY ID;";
        reader = dbConnection.DownloadQuery(query);
        while (reader.Read())
        {
            conferenceGamesIds.Add(reader.GetInt32(0));
        }

        reader.Close();
        reader = null;

        //set conference games
        query = @"UPDATE calendar SET team1 = '" + americanDivisionWinners[1] + "', team2 = '" + americanDivisionWinners[0] + "' WHERE ID = " + conferenceGamesIds[0] + ";" +
                "UPDATE calendar SET team1 = '" + nationalDivisionWinners[1] + "', team2 = '" + nationalDivisionWinners[0] + "' WHERE ID = " + conferenceGamesIds[1] + ";";
        dbConnection.ModifyQuery(query);
    }

    private void CreateGrandFinalGame(int year)
    {
        var query = "SELECT Team1,Team1Score,Team2,Team2Score FROM calendar WHERE MatchType = 'Conference Final' AND Year = " + year + " AND Sport = 'Football' ORDER BY ID;";
        var reader = dbConnection.DownloadQuery(query);
        var conferenceWinners = new List<string>();
        conferencesLosers = new List<string>();
        while (reader.Read())
        {
            if (reader.GetInt32(1) > reader.GetInt32(3))
            {
                conferenceWinners.Add(reader.GetString(0));
                conferencesLosers.Add(reader.GetString(2));
            }
            else
            {
                conferenceWinners.Add(reader.GetString(2));
                conferencesLosers.Add(reader.GetString(0));
            }
        }

        reader.Close();
        reader = null;

        //get grand final game
        int grandFinalId;
        query = "SELECT ID FROM calendar WHERE MatchType = 'Football Grand Final' AND Year = " + year + " AND Sport = 'Football' ORDER BY ID;";
        reader = dbConnection.DownloadQuery(query);
        reader.Read();
        grandFinalId = reader.GetInt32(0);

        reader.Close();
        reader = null;

        //set grand final
        query = "UPDATE calendar SET team1 = '" + conferenceWinners[0] + "', team2 = '" + conferenceWinners[1] + "' WHERE ID = " + grandFinalId + ";";
        dbConnection.ModifyQuery(query);
    }

    private void StartNewFootballSeason(int year)
    {
        var query = "UPDATE teams SET Wins = 0, Loses = 0, Draws = 0;";
        dbConnection.ModifyQuery(query);

        var footballSeasonGenerator = new FootballSeasonGenerator(dbConnection);
        footballSeasonGenerator.GenerateNewSeasonSchedule(year);
        footballSeasonGenerator = null;
    }

    private void ContractsExpiring()
    {
        var query = "UPDATE players SET ContractLenght = ContractLenght - 1 WHERE ContractLenght > 0;" +
                       "UPDATE players SET Contract = 0, ContractGuaranteedMoney = 0, GotMoney = 0, YearOfSign = 0, Team = 'None' WHERE ContractLenght = 0;";
        dbConnection.ModifyQuery(query);
    }

    private void UpdateTeamsOveralls()
    {
        var allTeams = Teams.GetFootballTeams();
        var updateQuery = "";

        foreach(var team in allTeams)
        {
            var query = "SELECT OverallSkill FROM players WHERE Sport = 'Football' AND Team = '" + team.Name + "' AND InjuryTime == 0;";
            var reader = dbConnection.DownloadQuery(query);
            var overallSkills = new List<int>();
            while(reader.Read())
            {
                overallSkills.Add(reader.GetInt32(0));
            }

            reader.Close();
            reader = null;

            var newOverall = 0;
            foreach(var overall in overallSkills)
            {
                newOverall += overall;
            }

            newOverall = newOverall / overallSkills.Count;
            updateQuery += "UPDATE teams SET OverallSkill = " + newOverall + " WHERE Name = '" + team.Name + "' AND Sport = 'Football' ;";
        }

        dbConnection.ModifyQuery(updateQuery);
    }

    private void MakeDraft()
    {
        IAddingAthletesToDBController addingAthletesController = new AddingAthletesToDBController(dbConnection);
        IContractMaker contractMaker = new ContractMaker();
        var fdc = new FootballDraftController(dbConnection, addingAthletesController, contractMaker, userDataHolder.DateTime.Year);
        fdc.ExecuteDraft();
    }
    #endregion
}
