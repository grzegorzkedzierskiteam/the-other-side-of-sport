﻿using System.Collections.Generic;
using UnityEngine;

public class MessageLifeController : IMessageLifeController
{
    private readonly IDBConnection dBConnection;
    private readonly IMessageManagerDB messageManagerDb;
    private readonly IMessageManager messageManager;

    public MessageLifeController(IDBConnection dBConnection, IMessageManagerDB messageManagerDb , IMessageManager messageManager)
    {
        this.dBConnection = dBConnection;
        this.messageManagerDb = messageManagerDb;
        this.messageManager = messageManager;
    }

    public void MessageTimeLifeExpiring()
    {
        const string query = "UPDATE messages SET ExpiringTime = ExpiringTime - 1 WHERE ExpiringTime > 0;";
        dBConnection.ModifyQuery(query);

        var messages = messageManagerDb.GetAllMessagesFromDB(dBConnection);

        foreach (var message in messages.FindAll(x => x.ExpiringTime == 0))
        {
            if(DoesPersonStillExist(message.ParticipantType,message.ParticipantID))
                messageManager.DeleteMessage(dBConnection, message);
        }        
    }

    private bool DoesPersonStillExist(PersonType personType, int personID)
    {
        var query = personType == PersonType.Agent ? 
            $"SELECT ID FROM agents WHERE ID = {personID};" : 
            $"SELECT ID FROM players WHERE ID = {personID};";

        var reader = dBConnection.DownloadQuery(query);

        return reader.HasRows;
    }
}
