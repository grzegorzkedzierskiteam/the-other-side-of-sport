﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextWeekManager : INextWeekManager
{
    private readonly AgentsActivitiesController agentsActivities;
    private readonly FootballTeamsActivitiesController footballTeamsActivities;
    private readonly FootballEventsController footballEvents;
    private readonly IAthletesActivitiesController athletesActivities;
    private readonly WorldEventsController worldEvents;
    private readonly ISalariesController salariesController;
    private readonly CalendarController calendarController;

    public NextWeekManager(AgentsActivitiesController agentsActivities,
        FootballTeamsActivitiesController footballTeamsActivities,
        FootballEventsController footballEvents,
        IAthletesActivitiesController athletesActivities,
        WorldEventsController worldEvents,
        ISalariesController salariesController,
        CalendarController calendarController)
    {
        this.agentsActivities = agentsActivities;
        this.footballTeamsActivities = footballTeamsActivities;
        this.footballEvents = footballEvents;
        this.athletesActivities = athletesActivities;
        this.worldEvents = worldEvents;
        this.salariesController = salariesController;
        this.calendarController = calendarController;
    }

    public void TeamsActivities()
    {
        footballTeamsActivities.ExecuteFootballTeamsActivites();
    }

    public void AgentsActivities()
    {
        agentsActivities.ExecuteAgentsActivities();
    }

    public void SportsEvents()
    {
        footballEvents.ExecuteFootballEvents();
    }

    public void AthletesActivities()
    {
        athletesActivities.ExecuteAthletesActivites();
    }

    public void WorldEvents(MyDateTime currentDateTime)
    {
        worldEvents.ExecuteWorldEvents(currentDateTime);
    }

    public void SalariesExecute(List<AgentObject> agents, MyDateTime dateTime)
    {
        salariesController.ExecuteGettingSalaries(agents, dateTime);
    }

    public void AdvanceToNextWeek()
    {
        calendarController.AdvanceToNextWeek();
    }
}
