﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class SimulateMenuController : Menu
{
    public int numberOfWeeks;
    public Text txtNumberOfWeeks;
    public Button btnDecrease;
    public Button btnIncrease;
    public Button btnCancel;
    public Button btnSimulate;

    // Start is called before the first frame update
    private void Start()
    {
        btnCancel.onClick.AddListener(BtnCancel);
        btnDecrease.onClick.AddListener(BtnDecrease);
        btnIncrease.onClick.AddListener(BtnIncrease);
        btnSimulate.onClick.AddListener(BtnSimulate);
        numberOfWeeks = 1;
        SetTextNumberOfWeeks();
    }

    public void SetInteractableButtons(bool flag)
    {
        btnDecrease.interactable = flag;
        btnIncrease.interactable = flag;
        btnCancel.interactable = flag;
        btnSimulate.interactable = flag;
    }

    private void BtnSimulate()
    {
        SetInteractableButtons(false);
        var confirmMenu = MenuController.InitializePopMenu(MenuEnum.ConfirmSimulateWeeks);
        confirmMenu.GetComponent<ConfirmSimulateWeeksController>().SimulateMenuController = this;
        confirmMenu.GetComponent<ConfirmSimulateWeeksController>().NumberOfWeeks = numberOfWeeks;
    }

    private void BtnCancel()
    {
        var menuGame = GameObject.Find("MenuGame(Clone)");
        var menuGameController = menuGame.GetComponent<MenuGameController>();
        menuGameController.SetInteractableButtons(true);
        MenuController.DestroyPopMenu(gameObject);
    }

    private void BtnDecrease()
    {
        numberOfWeeks -= 1;
        if (numberOfWeeks < 1)
            numberOfWeeks = 1;

        SetTextNumberOfWeeks(); 
    }

    private void BtnIncrease()
    {
        numberOfWeeks += 1;
        if (numberOfWeeks > 52)
            numberOfWeeks = 52;

        SetTextNumberOfWeeks();
    }

    private void SetTextNumberOfWeeks()
    {
        txtNumberOfWeeks.text = numberOfWeeks.ToString();
        if (numberOfWeeks == 1)
            txtNumberOfWeeks.text += " week";
        else if (numberOfWeeks == 52)
            txtNumberOfWeeks.text = "One year";
        else
            txtNumberOfWeeks.text += " weeks";
    }

    public class Factory : PlaceholderFactory<SimulateMenuController> { }
}
