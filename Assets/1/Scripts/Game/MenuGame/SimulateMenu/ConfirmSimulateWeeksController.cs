﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
// ReSharper disable InconsistentNaming

public class ConfirmSimulateWeeksController : Menu
{
    private MyUnityAdsController myUnityAdsController;
    private IDBConnection dbConnection;

    private GameObject loadingProgressBarPanel;

    public SimulateMenuController SimulateMenuController;
    public int NumberOfWeeks;
    public GameObject loadingProgressBarPanelPrefab;
    public Text txtTitle;
    public Button btnYes;
    public Button btnNo;

    private IMessageManagerDB messageManagerDb;
    private INextWeekManager nextWeekManager;

    [Inject]
    public void Constructor(IDBConnection dbConnection, IMessageManagerDB messageManagerDb, INextWeekManager nextWeekManager)
    {
        this.dbConnection = dbConnection;
        this.messageManagerDb = messageManagerDb;
        this.nextWeekManager = nextWeekManager;
    }

    private void Start()
    {
        btnNo.onClick.AddListener(BtnNo);
        btnYes.onClick.AddListener(BtnYes);
        SetTitleTxt();
    }

    public void SetTitleTxt()
    {
        var currentDate = UserData.GetUserDataHolder().DateTime;
        var targetDate = currentDate.GetDateAfterAdvanceWeeks(NumberOfWeeks);
        txtTitle.text += $"\n{targetDate.FullDateString}?";
    }

    private void BtnNo()
    {
        SimulateMenuController.SetInteractableButtons(true);
        MenuController.DestroyPopMenu(gameObject);
    }

    private void BtnYes()
    {
        StartCoroutine(AdvanceToNextWeek());
    }

    private IEnumerator AdvanceToNextWeek()
    {
        //reklamy -------------------------------------------myUnityAdsController.ShowAd();
        var percentDivisor = (100.00d / NumberOfWeeks);
        var percent = 0.00d;

        dbConnection.OpenConnection();
        dbConnection.StartTransaction();

        loadingProgressBarPanel = MenuController.GetAndInitializeMenu(loadingProgressBarPanelPrefab);
        var txtProgress = loadingProgressBarPanel.transform.Find("TxtProgress").GetComponent<Text>();
        var progressBar = loadingProgressBarPanel.transform.Find("ProgressBar").GetComponent<Image>();

        progressBar.fillAmount = 0.0f;
        txtProgress.text = "0%";
        yield return null;

        for (var i = 0; i < NumberOfWeeks; i++)
        {          
            var userDataHolder = UserData.GetUserDataHolder();

            messageManagerDb.SetUpDate(userDataHolder.DateTime);
            nextWeekManager.TeamsActivities();
            nextWeekManager.AgentsActivities();
            nextWeekManager.SportsEvents();
            nextWeekManager.AthletesActivities();
            nextWeekManager.WorldEvents(userDataHolder.DateTime);
            nextWeekManager.SalariesExecute(AgentData.GetAllAgentsOrReturnNull(dbConnection), userDataHolder.DateTime);           
            nextWeekManager.AdvanceToNextWeek();
            yield return null;
            percent += percentDivisor;
            txtProgress.text = $"{(int)percent}%";
            progressBar.fillAmount = (float)(percent / 100f);
        }

        Destroy(loadingProgressBarPanel);

        dbConnection.CommitTransaction();
        dbConnection.CloseConnection();
        
        FinishSimulation();
    }

    private void FinishSimulation()
    {
        UserDataDB.UpdateUserDataHolder();

        var simulateMenu = GameObject.Find("SimulateMenu(Clone)");
        MenuController.DestroyPopMenu(simulateMenu);

        var menuGame = GameObject.Find("MenuGame(Clone)");
        var menuGameController = menuGame.GetComponent<MenuGameController>();
        menuGameController.SetMenuUserData();
        menuGameController.SetInteractableButtons(true);
        menuGameController.SetMessageCounter();

        MenuController.DestroyPopMenu(gameObject);
    }

    public class Factory : PlaceholderFactory<ConfirmSimulateWeeksController> { }
}
