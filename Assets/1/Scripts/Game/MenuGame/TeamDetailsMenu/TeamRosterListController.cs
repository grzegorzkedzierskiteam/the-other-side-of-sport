﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class TeamRosterListController : Menu
{
    private bool initialized = false;

    private IDBConnection dbConnection;

    public GameObject listElements;

    private enum SortingType { ascending, descending };
    private enum SelectedGroup { roster, favorites };
    private SelectedGroup groupToGet;

    public Dropdown dropdown;
    public Color favoriteColor;
    public string teamName;
    public string sport;

    public Text txtTeamName;
    public Text txtSport;
    public Button btnFilter;

    //sorting btns
    public Button btnNameSorting;
    public Button btnPositionSorting;
    public Button btnOverallSorting;
    public Button btnAgeSorting;
    public Button btnContractSorting;
    public Button btnAgentSorting;

    private SortingType lastNameSortingType = SortingType.descending;
    private SortingType overallSkillSortingType = SortingType.descending;
    private SortingType positionSortingType = SortingType.ascending;
    private SortingType ageSortingType = SortingType.ascending;
    private SortingType contractSortingType = SortingType.ascending;
    private SortingType agentSortingType = SortingType.ascending;
    //

    //filters
    public GameObject filterPanel;
    public Button btnSearch;
    public InputField inputLastName;
    public InputField inputOvr;
    public InputField inputPosition;
    public InputField inputAge;
    public InputField inputContract;
    //

    private List<ClientObject> playersList;

    [Inject]
    public void Constructor(IDBConnection dbConnection)
    {
        this.dbConnection = dbConnection;
    }

    // Use this for initialization
    void Start()
    {
        dropdown.onValueChanged.AddListener(delegate { Dropdown(dropdown); });
        txtTeamName.text = teamName;
        txtSport.text = sport;

        groupToGet = SelectedGroup.roster;
        playersList = GetPlayersFromDB(groupToGet);
        AddPlayersListToMenu();

        btnNameSorting.onClick.AddListener(SortByLastName);
        btnPositionSorting.onClick.AddListener(SortByPosition);
        btnOverallSorting.onClick.AddListener(SortByOverallSkill);
        btnAgeSorting.onClick.AddListener(SortByAge);
        btnContractSorting.onClick.AddListener(SortByContract);
        btnAgentSorting.onClick.AddListener(SortByAgent);

        btnFilter.onClick.AddListener(BtnFilter);
        btnSearch.onClick.AddListener(BtnSearch);

        initialized = true;
    }

    private void OnEnable()
    {
        if (initialized)
        {
            DestroyAllCurrentRows();
            playersList = GetPlayersFromDB(groupToGet);
            AddPlayersListToMenu();
        }
    }

    private void Dropdown(Dropdown dropdown)
    {
        DestroyAllCurrentRows();

        switch (this.dropdown.value)
        {
            case 0:
                groupToGet = SelectedGroup.roster;
                break;
            case 1:
                groupToGet = SelectedGroup.favorites;
                break;
            default:
                groupToGet = SelectedGroup.roster;
                break;
        }

        playersList = GetPlayersFromDB(groupToGet);
        AddPlayersListToMenu();
    }

    private void DestroyAllCurrentRows()
    {
        foreach (Transform child in listElements.transform)
        {
            Destroy(child.gameObject);
        }
    }

    private List<ClientObject> GetPlayersFromDB(SelectedGroup group)
    {
        var arrayList = new List<ClientObject>();
        dbConnection.OpenConnection();;
        var query = GetQuerySelectedGroup(group);
        var reader = dbConnection.DownloadQuery(query);

        while (reader.Read())
        {
            arrayList.Add(new ClientObject(
                    reader.GetInt32(0), reader.GetString(1), reader.GetString(2), sport, reader.GetString(3), "", teamName, reader.GetInt32(5), reader.GetInt32(11), 0, 0, null, 0, reader.GetInt32(6), 0, 0, 0, 0, reader.GetInt32(4), 0, reader.GetInt32(7), reader.GetInt32(8), reader.GetString(9), reader.GetInt32(10)
                ));
        }

        reader.Close();
        reader = null;

        dbConnection.CloseConnection();

        return arrayList;
    }

    private string GetQuerySelectedGroup(SelectedGroup group)
    {
        var query = "SELECT ID,FirstName,LastName,Position,OverallSkill,Age,Contract,Agent,InjuryTime,InjuryName,IsFavorite, Wealth FROM players WHERE Team ='" + teamName + "' AND Sport = '" + sport + "' ";

        switch (group)
        {
            case SelectedGroup.favorites:
                query += "AND IsFavorite == 1";
                break;
            default:
                break;
        }

        query += " ORDER BY LastName";
        return query;
    }

    private void AddPlayersListToMenu()
    {
        foreach (var client in playersList)
        {
            var newClient = MenuController.InitializeListItem(ListItem.TeamRosterItem, listElements.transform);
            var controller = newClient.GetComponent<TeamRosterItem>();
            controller.id = client.ID;
            controller.txtOverallSkill.text = client.OverallSkill.ToString();
            controller.txtName.text = client.FirstName + " " + client.LastName;
            controller.txtPosition.text = client.Position;
            controller.txtContract.text = Money.GetMoneyString(client.Contract.Value);
            controller.txtAge.text = client.Age.ToString();

            //favorite color set
            if (client.IsFavorite)
                controller.txtBtnMoreDetails.color = favoriteColor;

            //injury red text
            if (client.InjuryTime > 0)
                controller.txtName.color = Color.red;

            if (client.AgentID == -1)
                controller.txtAgent.text = "None";
            else if (client.AgentID == 1)
                controller.txtAgent.text = "You";
            else
                controller.txtAgent.text = "Has an agent";
        }
    }

    #region sorting methods
    private void SortByOverallSkill()
    {
        switch (overallSkillSortingType)
        {
            case SortingType.ascending:
                playersList = playersList.OrderBy(x => x.OverallSkill).ToList();
                overallSkillSortingType = SortingType.descending;
                break;

            case SortingType.descending:
                playersList = playersList.OrderByDescending(x => x.OverallSkill).ToList();
                overallSkillSortingType = SortingType.ascending;
                break;
        }

        DestroyAllCurrentRows();
        AddPlayersListToMenu();
    }

    private void SortByLastName()
    {
        switch (lastNameSortingType)
        {
            case SortingType.ascending:
                playersList = playersList.OrderBy(x => x.LastName).ToList();
                lastNameSortingType = SortingType.descending;
                break;

            case SortingType.descending:
                playersList = playersList.OrderByDescending(x => x.LastName).ToList();
                lastNameSortingType = SortingType.ascending;
                break;
        }

        DestroyAllCurrentRows();
        AddPlayersListToMenu();
    }

    private void SortByPosition()
    {
        switch (agentSortingType)
        {
            case SortingType.ascending:
                playersList = playersList.OrderBy(x => x.Position).ToList();
                agentSortingType = SortingType.descending;
                break;

            case SortingType.descending:
                playersList = playersList.OrderByDescending(x => x.Position).ToList();
                agentSortingType = SortingType.ascending;
                break;
        }
        DestroyAllCurrentRows();
        AddPlayersListToMenu();
    }

    private void SortByAge()
    {
        switch (ageSortingType)
        {
            case SortingType.ascending:
                playersList = playersList.OrderBy(x => x.Age).ToList();
                ageSortingType = SortingType.descending;
                break;

            case SortingType.descending:
                playersList = playersList.OrderByDescending(x => x.Age).ToList();
                ageSortingType = SortingType.ascending;
                break;
        }
        DestroyAllCurrentRows();
        AddPlayersListToMenu();
    }

    private void SortByContract()
    {
        switch (contractSortingType)
        {
            case SortingType.ascending:
                playersList = playersList.OrderBy(x => x.Contract.Value).ToList();
                contractSortingType = SortingType.descending;
                break;

            case SortingType.descending:
                playersList = playersList.OrderByDescending(x => x.Contract.Value).ToList();
                contractSortingType = SortingType.ascending;
                break;
        }
        DestroyAllCurrentRows();
        AddPlayersListToMenu();
    }

    private void SortByAgent()
    {
        switch (agentSortingType)
        {
            case SortingType.ascending:
                playersList = playersList.OrderBy(x => x.AgentID).ToList();
                agentSortingType = SortingType.descending;
                break;

            case SortingType.descending:
                playersList = playersList.OrderByDescending(x => x.AgentID).ToList();
                agentSortingType = SortingType.ascending;
                break;
        }
        DestroyAllCurrentRows();
        AddPlayersListToMenu();
    }
    #endregion

    #region filters

    private void BtnFilter()
    {
        filterPanel.SetActive(!filterPanel.activeSelf);
    }

    private void BtnSearch()
    {
        var arrayList = new List<ClientObject>();
        dbConnection.OpenConnection();;
        var query = GetQueryFilter();

        var reader = dbConnection.DownloadQuery(query);

        while (reader.Read())
        {
            arrayList.Add(new ClientObject(
                    reader.GetInt32(0), reader.GetString(1), reader.GetString(2), sport, reader.GetString(3), "", teamName, reader.GetInt32(5), reader.GetInt32(11), 0, 0, null, 0, reader.GetInt32(6), 0, 0, 0, 0, reader.GetInt32(4), 0, reader.GetInt32(7), reader.GetInt32(8), reader.GetString(9), reader.GetInt32(10)
                ));
        }

        reader.Close();
        reader = null;

        dbConnection.CloseConnection();

        playersList = arrayList;

        DestroyAllCurrentRows();
        AddPlayersListToMenu();
    }

    private string GetQueryFilter()
    {
        if (inputLastName.text == "" && inputOvr.text == "" && inputPosition.text == "" && inputAge.text == "" && inputContract.text == "" && dropdown.value == 0)
            return "SELECT ID,FirstName,LastName,Position,OverallSkill,Age,Contract,Agent,InjuryTime,InjuryName,IsFavorite,Wealth FROM players WHERE Team ='" + teamName + "' AND Sport = '" + sport + "' ORDER BY LastName;";

        if (inputLastName.text == "" && inputOvr.text == "" && inputPosition.text == "" && inputAge.text == "" && inputContract.text == "" && dropdown.value == 1)
            return "SELECT ID,FirstName,LastName,Position,OverallSkill,Age,Contract,Agent,InjuryTime,InjuryName,IsFavorite,Wealth FROM players WHERE Team ='" + teamName + "' AND Sport = '" + sport + "' AND IsFavorite == 1 ORDER BY LastName;";

        var query = "";
        if (dropdown.value == 1)
            query = "SELECT ID,FirstName,LastName,Position,OverallSkill,Age,Contract,Agent,InjuryTime,InjuryName,IsFavorite,Wealth FROM players WHERE Team ='" + teamName + "' AND Sport = '" + sport + "' AND IsFavorite == 1 AND ";
        else
            query = "SELECT ID,FirstName,LastName,Position,OverallSkill,Age,Contract,Agent,InjuryTime,InjuryName,IsFavorite,Wealth FROM players WHERE Team ='" + teamName + "' AND Sport = '" + sport + "' AND ";

        var inputs = new List<InputField>();
        var notNullInputs = new List<InputField>();
        inputs.Add(inputLastName);
        inputs.Add(inputOvr);
        inputs.Add(inputPosition);
        inputs.Add(inputAge);
        inputs.Add(inputContract);

        var numberOfFilters = 0;
        foreach (var input in inputs)
        {
            if (input.text != "")
            {
                numberOfFilters += 1;
                notNullInputs.Add(input);
            }
        }

        if (numberOfFilters == 1)
        {
            if (GetFilterName(notNullInputs[0]) == "OverallSkill" || GetFilterName(notNullInputs[0]) == "Age" || GetFilterName(notNullInputs[0]) == "Contract")
            {
                if (notNullInputs[0].text.Substring(0, 1) == ">" || notNullInputs[0].text.Substring(0, 1) == "<")
                    query += GetFilterName(notNullInputs[0]) + notNullInputs[0].text + "";
                else
                    query += GetFilterName(notNullInputs[0]) + "='" + notNullInputs[0].text + "'";
            }
            else
                query += GetFilterName(notNullInputs[0]) + " Like '" + notNullInputs[0].text + "%'";
        }
        else
        {
            var lastInput = notNullInputs.Last();
            foreach (var input in notNullInputs)
            {
                if (GetFilterName(input) == "OverallSkill" || GetFilterName(input) == "Age" || GetFilterName(input) == "Contract")
                {
                    if (input.text.Substring(0, 1) == ">" || input.text.Substring(0, 1) == "<")
                        query += GetFilterName(input) + input.text + "";
                    else
                        query += GetFilterName(input) + "='" + input.text + "'";
                }
                else
                    query += GetFilterName(input) + " Like '" + input.text + "%'";

                if (input != lastInput)
                    query += " AND ";
            }
        }


        query += " ORDER BY LastName;";
        return query;
    }

    private string GetFilterName(InputField input)
    {
        return input.name.Substring(5);
    }

    #endregion
}
