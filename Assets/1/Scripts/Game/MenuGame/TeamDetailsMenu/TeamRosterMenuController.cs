﻿using UnityEngine.UI;
using Zenject;

public class TeamRosterMenuController : Menu {

    public TeamRosterListController listController;

    public Button btnBack;

    // Use this for initialization
    void Start()
    {
        btnBack.onClick.AddListener(BtnBack);
    }

    private void BtnBack()
    {
        MenuController.DestroyMenu(gameObject);
    }

    public class Factory : PlaceholderFactory<TeamRosterMenuController> { }
}
