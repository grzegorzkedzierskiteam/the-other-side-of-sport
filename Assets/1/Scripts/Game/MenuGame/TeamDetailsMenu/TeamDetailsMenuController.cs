﻿using Mono.Data.Sqlite;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class TeamDetailsMenuController : Menu {

    private IDBConnection dbConnection;
    private TeamDetails teamDetails;
    public GameObject menuToSetActive;

    public string teamName;
    public string sport;
    public Button btnBack;

    #region UI Elements
    public Text txtName;
    public Text txtSport;
    public Text txtCharacter;
    public Text txtPopularity;
    public Text txtOverallSkill;
    public Text txtRelation;
    public Text txtConference;
    public Text txtConferenceRank;
    public Text txtDivision;
    public Text txtDivisionRank;
    public Text txtWins;
    public Text txtDraws;
    public Text txtLoses;
    public Button btnRoster;
    #endregion

    private int conferenceRank;
    private int divisionRank;

    [Inject]
    public void Constructor(IDBConnection dbConnection)
    {
        this.dbConnection = dbConnection;
    }

    // Use this for initialization
    void Start()
    {
        SetUIData();

        btnBack.onClick.AddListener(BtnBack);
        btnRoster.onClick.AddListener(BtnRoster);
    }

    private void BtnBack()
    {
        MenuController.DestroyMenu(gameObject);
    }

    private void BtnRoster()
    {
        var menuDetails = MenuController.InitializeMenu(MenuEnum.TeamRosterMenu);
        menuDetails.GetComponent<TeamRosterMenuController>().listController.teamName = teamName;
        menuDetails.GetComponent<TeamRosterMenuController>().listController.sport = sport;
    }

    private void SetUIData()
    {
        teamDetails = GetTeamDetailsFromDB();
        txtName.text = teamName;
        txtSport.text = sport;
        txtCharacter.text = teamDetails.Character;
        txtRelation.text = teamDetails.Relation.ToString();
        txtPopularity.text = teamDetails.Popularity.ToString();
        txtOverallSkill.text = teamDetails.Overall.ToString();
        txtConference.text = teamDetails.Conference;
        txtConferenceRank.text = conferenceRank.ToString() + "#";
        txtDivision.text = teamDetails.Division;
        txtDivisionRank.text = divisionRank.ToString() + "#";
        txtWins.text = teamDetails.Wins.ToString() + " W";
        txtDraws.text = teamDetails.Draws.ToString() + " D";
        txtLoses.text = teamDetails.Loses.ToString() + " L";
    }

    private TeamDetails GetTeamDetailsFromDB()
    {
        dbConnection.OpenConnection();;
        var query = "SELECT Character, Relation, Popularity, OverallSkill, Wins, Loses, Draws, Conference, Division FROM teams WHERE Name = '" + teamName + "' AND Sport = '" + sport + "';";
        var reader = dbConnection.DownloadQuery(query);
        reader.Read();
        var teamDetails = new TeamDetails(teamName, sport, reader.GetInt32(3), reader.GetInt32(2), reader.GetInt32(1),
            reader.GetString(7), reader.GetString(8))
        {
            Wins = reader.GetInt32(4),
            Loses = reader.GetInt32(5),
            Draws = reader.GetInt32(6),
            Character = reader.GetString(0)
        };

        reader.Close();
        reader = null;

        //division rank
        query = "SELECT Name FROM teams WHERE Conference ='" + teamDetails.Conference + "' AND Division = '" + teamDetails.Division + "' ORDER BY Wins desc, Draws desc, Loses;";
        reader = dbConnection.DownloadQuery(query);
        divisionRank = 1;
        while(reader.Read())
        {
            if (reader.GetString(0) == teamName)
                break;
            else
                divisionRank++;
        }

        reader.Close();
        reader = null;

        //conference rank
        query = "SELECT Name FROM teams WHERE Conference ='" + teamDetails.Conference + "' ORDER BY Wins desc, Draws desc, Loses;";
        reader = dbConnection.DownloadQuery(query);
        conferenceRank = 1;
        while (reader.Read())
        {
            if (reader.GetString(0) == teamName)
                break;
            else
                conferenceRank++;
        }

        reader.Close();
        reader = null;
        dbConnection.CloseConnection();

        return teamDetails;
    }

    public class Factory : PlaceholderFactory<TeamDetailsMenuController> { }
}
