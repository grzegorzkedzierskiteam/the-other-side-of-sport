﻿using UnityEngine.UI;
using Zenject;

public class TeamRosterItem : Menu {

    public int id;
    public Text txtName;
    public Text txtPosition;
    public Text txtOverallSkill;
    public Text txtAge;
    public Text txtContract;
    public Text txtAgent;
    public Text txtBtnMoreDetails;
    public Button btnMoreDetails;

	// Use this for initialization
	void Start () {
        btnMoreDetails.onClick.AddListener(BtnMoreDetails);
    }

    private void BtnMoreDetails()
    {
        var menuDetails = MenuController.InitializeMenu(MenuEnum.ClientDetailsMenu);
        menuDetails.GetComponent<ClientDetailsMenuController>().clientID = id;
    }

    public class Factory : PlaceholderFactory<TeamRosterItem> { }
}
