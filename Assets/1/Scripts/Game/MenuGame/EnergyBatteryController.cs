﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using UnityEditor;

public class EnergyBatteryController : MonoBehaviour {

    public Image imageBatteryStatus;
    public Text txtBatteryStatus;

    private UserDataHolder userDataHolder;
    private int energyLevel;

    // Use this for initialization
    void Start () {
        userDataHolder = UserData.GetUserDataHolder();
        energyLevel = userDataHolder.Energy;

        txtBatteryStatus.text = energyLevel + "%";
        imageBatteryStatus.fillAmount = (float)energyLevel / 100;
    }
	
	// Update is called once per frame
	void Update () {

        transform.SetAsLastSibling();

		if(energyLevel != userDataHolder.Energy)
        {
            if (energyLevel > userDataHolder.Energy)
                DecreaseBattery();
            else if (energyLevel < userDataHolder.Energy)
                IncreaseBattery();

            energyLevel = userDataHolder.Energy;
            txtBatteryStatus.text = energyLevel + "%";
            imageBatteryStatus.fillAmount = (float) energyLevel / 100;
        }
	}

    public void StartWarningAnimation()
    {
        GetComponent<Animation>().Play("EnergyWarning");
    }

    private void IncreaseBattery()
    {
        GetComponent<Animation>().AddClip(GetAnimationOfIncreasing(), "IncreasingClip");
        GetComponent<Animation>().Play("IncreasingClip");
    }

    private void DecreaseBattery()
    {
        GetComponent<Animation>().AddClip(GetAnimationOfDecreasing(), "DecreasingClip");
        GetComponent<Animation>().Play("DecreasingClip");
    }

    private AnimationClip GetAnimationOfDecreasing()
    {
        AnimationClip clip = new AnimationClip
        {
            name = "EnergyBatteryDecreasing",
            wrapMode = WrapMode.Once,
            frameRate = 250f,
            legacy = true 
        };

        //r
        Keyframe[] keyframesRA = new Keyframe[]
        {
            new Keyframe() {time = 0f, value = 1f},
            new Keyframe() {time = 0.3f, value = 1f},
            new Keyframe() {time = 0.6f, value = 1f}
        };

        //g
        Keyframe[] keyframesGB = new Keyframe[]
        {
            new Keyframe() {time = 0f, value = 1f},
            new Keyframe() {time = 0.3f, value = 0f},
            new Keyframe() {time = 0.6f, value = 1f}
        };

        AnimationCurve curveRA = new AnimationCurve() { keys = keyframesRA };
        AnimationCurve curveGB = new AnimationCurve() { keys = keyframesGB };

        clip.SetCurve("BatteryContainer/BatteryStatus", typeof(Image), "m_Color.r", curveRA);
        clip.SetCurve("BatteryContainer/BatteryStatus", typeof(Image), "m_Color.g", curveGB);
        clip.SetCurve("BatteryContainer/BatteryStatus", typeof(Image), "m_Color.b", curveGB);
        clip.SetCurve("BatteryContainer/BatteryStatus", typeof(Image), "m_Color.a", curveRA);

        clip.SetCurve("BatteryContainer/BatteryStatus", typeof(Image), "m_FillAmount", AnimationCurve.EaseInOut(0.6f, energyLevel / 100f, 1f, userDataHolder.Energy / 100f));
        
        return clip;
    }

    private AnimationClip GetAnimationOfIncreasing()
    {
        AnimationClip clip = new AnimationClip
        {
            name = "EnergyBatteryIncreasing",
            wrapMode = WrapMode.Once,
            frameRate = 250f,
            legacy = true
        };

        //r
        Keyframe[] keyframesGA = new Keyframe[]
        {
            new Keyframe() {time = 0f, value = 1f},
            new Keyframe() {time = 0.3f, value = 1f},
            new Keyframe() {time = 0.6f, value = 1f}
        };

        //g
        Keyframe[] keyframesRB = new Keyframe[]
        {
            new Keyframe() {time = 0f, value = 1f},
            new Keyframe() {time = 0.3f, value = 0f},
            new Keyframe() {time = 0.6f, value = 1f}
        };

        AnimationCurve curveGA = new AnimationCurve() { keys = keyframesGA };
        AnimationCurve curveRB = new AnimationCurve() { keys = keyframesRB };

        clip.SetCurve("BatteryContainer/BatteryStatus", typeof(Image), "m_Color.r", curveRB);
        clip.SetCurve("BatteryContainer/BatteryStatus", typeof(Image), "m_Color.g", curveGA);
        clip.SetCurve("BatteryContainer/BatteryStatus", typeof(Image), "m_Color.b", curveRB);
        clip.SetCurve("BatteryContainer/BatteryStatus", typeof(Image), "m_Color.a", curveGA);

        clip.SetCurve("BatteryContainer/BatteryStatus", typeof(Image), "m_FillAmount", AnimationCurve.EaseInOut(0.6f, energyLevel / 100f, 1f, userDataHolder.Energy / 100f));

        return clip;
    }
}
