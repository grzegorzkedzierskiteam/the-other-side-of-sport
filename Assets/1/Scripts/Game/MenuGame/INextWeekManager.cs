﻿using System.Collections.Generic;

public interface INextWeekManager
{
    void TeamsActivities();
    void AgentsActivities();
    void SportsEvents();
    void AthletesActivities();
    void WorldEvents(MyDateTime dateTime);
    void SalariesExecute(List<AgentObject> agents, MyDateTime dateTime);
    void AdvanceToNextWeek();
}
