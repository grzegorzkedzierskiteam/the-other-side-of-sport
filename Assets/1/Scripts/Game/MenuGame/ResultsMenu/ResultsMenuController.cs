﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class ResultsMenuController : Menu {

    public Button btnBack;

    // Use this for initialization
    void Start () {
        btnBack.onClick.AddListener(BtnBack);
    }

    private void BtnBack()
    {
        MenuController.DestroyMenu(gameObject);
    }

    public class Factory : PlaceholderFactory<ResultsMenuController> { }
}
