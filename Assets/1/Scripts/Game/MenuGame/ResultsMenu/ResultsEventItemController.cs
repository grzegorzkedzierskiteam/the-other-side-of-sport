﻿using UnityEngine.UI;
using Zenject;

public class ResultsEventItemController : Menu {

    public Text txtEventDescription;

    public class Factory : PlaceholderFactory<ResultsEventItemController> { }
}
