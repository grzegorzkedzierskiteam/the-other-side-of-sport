﻿using System;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class ResultsListController : Menu {

    private UserDataHolder userDataHolder;
    private IDBConnection dbConnection;

    private int currentWeek;
    private int currentYear;
    private string currentSport;

    public GameObject listElements;

    //header
    public Text txtHeaderDateMonth;
    public Text txtHeaderDateYear;
    public Text txtHeaderSport;
    public Button btnStandings;
    //list header
    public Button btnPreviousWeek;
    public Button btnNextWeek;
    public Text txtSelectedWeek;
    public Text txtSelectedDate;
    public Button btnPresent;
    public InputField inputWeek;
    public Button btnGoToSelectedWeek;
    public GameObject txtNoEvents;
    //footer
    public Button btnFootball;

    [Inject]
    public void Constructor(IDBConnection dbConnection)
    {
        this.dbConnection = dbConnection;
    }

	// Use this for initialization
	void Start () {
        userDataHolder = GameObject.FindGameObjectWithTag("UserDataHolder").GetComponent<UserDataHolder>();
        SetHeaderDate();
        btnStandings.onClick.AddListener(BtnStandings);
        btnPresent.onClick.AddListener(BtnPresent);
        btnNextWeek.onClick.AddListener(BtnNextWeek);
        btnPreviousWeek.onClick.AddListener(BtnPreviousWeek);
        btnGoToSelectedWeek.onClick.AddListener(BtnGo);
        
        btnFootball.onClick.AddListener(BtnFootball);
	}

    private void Update()
    {
        SetInteractableBtnGo();
    }

    private void SetInteractableHeaderButtons()
    {
        btnPreviousWeek.interactable = !btnPreviousWeek.interactable;
        btnNextWeek.interactable = !btnNextWeek.interactable;
        btnPresent.interactable = !btnPresent.interactable;
        inputWeek.interactable = !inputWeek.interactable;
        btnGoToSelectedWeek.interactable = !btnGoToSelectedWeek.interactable;
        btnStandings.interactable = !btnStandings.interactable;
    }

    private void BtnStandings()
    {
        var standingsMenu = MenuController.InitializeMenu(MenuEnum.StandingsMenu);
        standingsMenu.GetComponent<StandingsMenuController>().sport = currentSport;
    }

    private void SetHeaderDate()
    {
        txtHeaderDateMonth.text = userDataHolder.DateTime.WeekPerMonth.ToString() + " week " + userDataHolder.DateTime.Month.ToString();
        txtHeaderDateYear.text = userDataHolder.DateTime.Year.ToString();
    }

    private void BtnFootball()
    {
        SetInteractableHeaderButtons();
        btnFootball.interactable = false;
        txtHeaderSport.text = "Football";
        currentWeek = userDataHolder.DateTime.Week;
        currentYear = userDataHolder.DateTime.Year;
        currentSport = "Football";
        DestroyAllCurrentRows();
        DownloadAndSetWeek(currentWeek, currentYear, currentSport);
    }

    private void BtnPresent()
    {
        currentWeek = userDataHolder.DateTime.Week;
        currentYear = userDataHolder.DateTime.Year;
        DestroyAllCurrentRows();
        DownloadAndSetWeek(currentWeek, currentYear, currentSport);
    }

    private void BtnNextWeek()
    {
        if(currentWeek == 52)
        {
            currentWeek = 0;
            currentYear += 1;
        }
        currentWeek += 1;
        DestroyAllCurrentRows();
        DownloadAndSetWeek(currentWeek, currentYear, currentSport);
    }

    private void BtnPreviousWeek()
    {
        if (currentWeek == 1)
        {
            currentWeek = 53;
            currentYear -= 1;
        }
        currentWeek -= 1;
        DestroyAllCurrentRows();
        DownloadAndSetWeek(currentWeek, currentYear, currentSport);
    }

    private void BtnGo()
    {
        var searchWeek = Mathf.Clamp(Int32.Parse(inputWeek.text), 1, GetMaxSeasonWeek(currentSport));;
        inputWeek.text = searchWeek.ToString();
        DestroyAllCurrentRows();
        SearchDownloadAndSetWeek(searchWeek, currentYear, currentSport);
    }

    private void SetInteractableBtnGo()
    {
        if (inputWeek.text == "")
            btnGoToSelectedWeek.interactable = false;
        else
            btnGoToSelectedWeek.interactable = true;
    }

#region db connection
    private void DestroyAllCurrentRows()
    {
        foreach (Transform child in listElements.transform)
        {
            Destroy(child.gameObject);
        }
    }

    private void DownloadAndSetWeek(int weekNumber, int year, string sport)
    {
        var date = new MyDateTime(weekNumber, year);
        txtSelectedDate.text = date.WeekPerMonth.ToString() + " week " + date.Month + " " + date.Year.ToString();
        dbConnection.OpenConnection();;
        var query = "SELECT EventType, SeasonWeek, Info, Team1, Team2, Team1Score, Team2Score, MatchType, Finished From calendar WHERE Sport= '" + sport + "' AND Week = " + weekNumber + " AND Year = " + year + ";";
        var reader = dbConnection.DownloadQuery(query);

        if (reader.HasRows)
            txtNoEvents.SetActive(false);
        else
        {
            txtNoEvents.SetActive(true);
            txtSelectedWeek.text = "Offseason";
        }
            

        while (reader.Read())
        {
            GameObject newEvent;
            if (reader.GetString(0) == "Game")
            {
                newEvent = MenuController.InitializeListItem(ListItem.ResultsMatchItem, listElements.transform);
                var controller = newEvent.GetComponent<ResultsMatchItemController>();
                controller.txtMatchType.text = reader.GetString(7);
                controller.txtAwayTeam.text = reader.GetString(3);
                controller.txtHomeTeam.text = reader.GetString(4);
                if(reader.GetInt32(8) == 1)
                {
                    controller.txtAwayTeamScore.text = reader.GetInt32(5).ToString();
                    controller.txtHomeTeamScore.text = reader.GetInt32(6).ToString();
                }
            }
            else
            {
                newEvent = MenuController.InitializeListItem(ListItem.ResultsEventItem, listElements.transform);
                var controller = newEvent.GetComponent<ResultsEventItemController>();
                controller.txtEventDescription.text = reader.GetString(2);
            }

            if(txtSelectedWeek.text != "Week: " + reader.GetInt32(1).ToString())
                txtSelectedWeek.text = "Week: " + reader.GetInt32(1).ToString();

            if(reader.GetInt32(1) == -1)
                txtSelectedWeek.text = "Offseason";

            if (reader.GetInt32(1) == -2)
                txtSelectedWeek.text = "Playoffs";
        }
        reader.Close();
        reader = null;
        dbConnection.CloseConnection();
    }

    private void SearchDownloadAndSetWeek(int seasonWeek, int year, string sport)
    {
        var date = new MyDateTime(1,1);
        
        dbConnection.OpenConnection();;
        var query = "SELECT EventType, SeasonWeek, Info, Team1, Team2, Team1Score, Team2Score, MatchType, Finished, Week From calendar WHERE Sport= '" + sport + "' AND SeasonWeek = " + seasonWeek + ";";
        var reader = dbConnection.DownloadQuery(query);

        if (reader.HasRows)
            txtNoEvents.SetActive(false);
        else
        {
            txtNoEvents.SetActive(true);
            txtSelectedWeek.text = "Offseason";
        }

        while (reader.Read())
        {
            GameObject newEvent;
            if (reader.GetString(0) == "Game")
            {
                newEvent = MenuController.InitializeListItem(ListItem.ResultsMatchItem, listElements.transform); var controller = newEvent.GetComponent<ResultsMatchItemController>();
                controller.txtMatchType.text = reader.GetString(7);
                controller.txtAwayTeam.text = reader.GetString(3);
                controller.txtHomeTeam.text = reader.GetString(4);
                if (reader.GetInt32(8) == 1)
                {
                    controller.txtAwayTeamScore.text = reader.GetInt32(5).ToString();
                    controller.txtHomeTeamScore.text = reader.GetInt32(6).ToString();
                }
            }
            else
            {
                newEvent = MenuController.InitializeListItem(ListItem.ResultsEventItem, listElements.transform);
                var controller = newEvent.GetComponent<ResultsEventItemController>();
                controller.txtEventDescription.text = reader.GetString(2);
            }

            if (txtSelectedWeek.text != "Week: " + reader.GetInt32(1).ToString())
                txtSelectedWeek.text = "Week: " + reader.GetInt32(1).ToString();

            if (reader.GetInt32(1) == -1)
                txtSelectedWeek.text = "Offseason";

            if (currentWeek != reader.GetInt32(9))
                currentWeek = reader.GetInt32(9);

            if (date != new MyDateTime(currentWeek, currentYear))
            {
                date = new MyDateTime(currentWeek, currentYear);
                txtSelectedDate.text = date.WeekPerMonth.ToString() + " week " + date.Month + " " + date.Year.ToString();
            }
        }
        reader.Close();
        reader = null;
        dbConnection.CloseConnection();

    }

    private int GetMaxSeasonWeek(string sport)
    {
        int maxSeasonWeek;

        var query = "Select Max(SeasonWeek) as MaxSeason From Calendar";
        dbConnection.OpenConnection();;
        var reader = dbConnection.DownloadQuery(query);
        reader.Read();

        maxSeasonWeek = reader.GetInt32(0);

        reader.Close();
        reader = null;
        dbConnection.CloseConnection();

        return maxSeasonWeek;
    }
#endregion
}
