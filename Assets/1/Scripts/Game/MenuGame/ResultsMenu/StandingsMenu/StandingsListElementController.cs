﻿using UnityEngine.UI;
using Zenject;

public class StandingsListElementController : Menu {

    public int id;
    public string sport;
    public Text txtRank;
    public Text txtName;
    public Text txtGames;
    public Text txtWins;
    public Text txtLoses;
    public Text txtDraws;
    public Button btnTeamInfo;

	// Use this for initialization
	void Start () {
        btnTeamInfo.onClick.AddListener(BtnTeamInfo);
    }
	
    private void BtnTeamInfo()
    {
        var menuDetails = MenuController.InitializeMenu(MenuEnum.TeamDetailsMenu);
        menuDetails.GetComponent<TeamDetailsMenuController>().teamName = txtName.text;
        menuDetails.GetComponent<TeamDetailsMenuController>().sport = sport;
    }

    public class Factory : PlaceholderFactory<StandingsListElementController> { }
}
