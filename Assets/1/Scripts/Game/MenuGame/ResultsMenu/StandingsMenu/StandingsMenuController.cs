﻿using Mono.Data.Sqlite;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class StandingsMenuController : Menu {

    private IDBConnection dbConnection;

    public GameObject menuToSetActive;
    public string sport;
    public Button btnBack;
    public GameObject listElements;
    public GameObject listElementPrefab;

    public Button btnAmerican;
    public Button btnNational;
    public Button btnNorth;
    public Button btnSouth;
    public Button btnWest;
    public Button btnEast;

    private string currentConference;
    private string currentDivision;

    [Inject]
    public void Constructor(IDBConnection dbConnection)
    {
        this.dbConnection = dbConnection;
    }

    // Use this for initialization
    void Start()
    {
        btnBack.onClick.AddListener(BtnBack);
        btnAmerican.onClick.AddListener(BtnAmerican);
        btnNational.onClick.AddListener(BtnNational);
        btnNorth.onClick.AddListener(BtnNorth);
        btnSouth.onClick.AddListener(BtnSouth);
        btnWest.onClick.AddListener(BtnWest);
        btnEast.onClick.AddListener(BtnEast);
    }

    private void BtnBack()
    {
        MenuController.DestroyMenu(gameObject);
    }

    private void DownloadDivisionStanding(string sport,string conference, string division)
    {
        if(conference != "" && division != "")
        {
            var query = "SELECT ID,Name,Wins,Loses,Draws FROM teams WHERE Sport = '" + sport + "' AND Conference = '" + conference + "' AND Division = '" + division + "' ORDER BY Wins DESC,draws DESC,Loses;";
            dbConnection.OpenConnection();;
            var reader = dbConnection.DownloadQuery(query);
            var rank = 1;
            while (reader.Read())
            {
                var teamItem = MenuController.InitializeListItem(ListItem.StandingListItem, listElements.transform);
                var controller = teamItem.GetComponent<StandingsListElementController>();
                controller.id = reader.GetInt32(0);
                controller.sport = sport;
                controller.txtRank.text = rank.ToString() + "#";
                controller.txtName.text = reader.GetString(1);
                controller.txtGames.text = (reader.GetInt32(2) + reader.GetInt32(3) + reader.GetInt32(4)).ToString();
                controller.txtWins.text = reader.GetInt32(2).ToString();
                controller.txtLoses.text = reader.GetInt32(3).ToString();
                controller.txtDraws.text = reader.GetInt32(4).ToString();
                rank += 1;
            }

            reader.Close();
            reader = null;
            dbConnection.CloseConnection();
        }
    }

    private void DestroyAllCurrentRows()
    {
        foreach (Transform child in listElements.transform)
        {
            Destroy(child.gameObject);
        }
    }

    #region division conferences buttons

    private void BtnAmerican()
    {
        btnAmerican.interactable = false;
        btnNational.interactable = true;
        currentConference = "American";
        DestroyAllCurrentRows();
        DownloadDivisionStanding(sport, currentConference, currentDivision);
    }

    private void BtnNational()
    {
        btnAmerican.interactable = true;
        btnNational.interactable = false;
        currentConference = "National";
        DestroyAllCurrentRows();
        DownloadDivisionStanding(sport, currentConference, currentDivision);
    }

    private void BtnNorth()
    {
        btnNorth.interactable = false;
        btnSouth.interactable = true;
        btnWest.interactable = true;
        btnEast.interactable = true;
        currentDivision = "North";
        DestroyAllCurrentRows();
        DownloadDivisionStanding(sport, currentConference, currentDivision);
    }

    private void BtnSouth()
    {
        btnNorth.interactable = true;
        btnSouth.interactable = false;
        btnWest.interactable = true;
        btnEast.interactable = true;
        currentDivision = "South";
        DestroyAllCurrentRows();
        DownloadDivisionStanding(sport, currentConference, currentDivision);
    }

    private void BtnWest()
    {
        btnNorth.interactable = true;
        btnSouth.interactable = true;
        btnWest.interactable = false;
        btnEast.interactable = true;
        currentDivision = "West";
        DestroyAllCurrentRows();
        DownloadDivisionStanding(sport, currentConference, currentDivision);
    }

    private void BtnEast()
    {
        btnNorth.interactable = true;
        btnSouth.interactable = true;
        btnWest.interactable = true;
        btnEast.interactable = false;
        currentDivision = "East";
        DestroyAllCurrentRows();
        DownloadDivisionStanding(sport, currentConference, currentDivision);
    }

    #endregion

    public class Factory : PlaceholderFactory<StandingsMenuController> { }
}
