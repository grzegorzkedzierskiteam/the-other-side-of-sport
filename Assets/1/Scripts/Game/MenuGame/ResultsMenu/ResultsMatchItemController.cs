﻿using UnityEngine.UI;
using Zenject;

public class ResultsMatchItemController : Menu {

    public Text txtMatchType;
    public Text txtAwayTeam;
    public Text txtAwayTeamScore;
    public Text txtHomeTeam;
    public Text txtHomeTeamScore;

    public class Factory : PlaceholderFactory<ResultsMatchItemController> { }
}
