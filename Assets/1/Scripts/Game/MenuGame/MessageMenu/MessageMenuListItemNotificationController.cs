﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Zenject;

public class MessageMenuListItemNotificationController : Menu, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public class Factory : PlaceholderFactory<MessageMenuListItemNotificationController> { }

    private IDBConnection dbConnection;

    public Button btnDelete;

    public IMessage messageObject;
    public MessageMenuController messageMenuController;

    public GameObject messageDetailsMenuPrefab;
    public Text txtSender;
    public Text txtDate;
    public Text txtInfo;
    public Image btnRespondBackground;
    public Image backGround;

    private UserDataHolder userDataHolder;

    public ScrollRect scrollRect;
    private RectTransform canvas;
    private RectTransform rectTransform;
    public float startingXPosition;
    private float distanceToDeletingArea;
    public bool isMoving;
    private IMessageManager messageManager;

    [Inject]
    public void Constructor(IDBConnection dbConnection, IMessageManager messageManager)
    {
        this.dbConnection = dbConnection;
        this.messageManager = messageManager;
    }

    // Use this for initialization
    void Start()
    {
        btnDelete.onClick.AddListener(DeleteNotification);

        userDataHolder = UserData.GetUserDataHolder();

        canvas = GameObject.Find("Canvas").GetComponent<RectTransform>();
        rectTransform = GetComponent<RectTransform>();
        startingXPosition = transform.parent.GetComponent<RectTransform>().position.x;
        distanceToDeletingArea = rectTransform.rect.width;
        isMoving = false;
    }

    private void DeleteNotification()
    {
        dbConnection.OpenConnection();
        messageManager.DeleteMessage(dbConnection,messageObject);
        dbConnection.CloseConnection();

        Destroy(gameObject);
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        if (IsHorizontalDragging())
        {
            scrollRect.vertical = false;
            isMoving = true;
        }
        else if (IsVerticalDragging())
        {
            scrollRect.vertical = true;
            scrollRect.OnBeginDrag(eventData);
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (isMoving)
        {
            rectTransform.position = new Vector3(rectTransform.position.x + Input.GetTouch(0).deltaPosition.x, rectTransform.position.y, rectTransform.position.z);
            ListItemHiding(true);
        }
        else
            scrollRect.OnDrag(eventData);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        scrollRect.OnEndDrag(eventData);
        scrollRect.vertical = true;

        if (IsInDeletingArea() && messageObject.TypeOfMessage == TypeOfMessage.Notification)
        {
            DeleteNotification();
        }

        rectTransform.position = new Vector3(startingXPosition, rectTransform.position.y, rectTransform.position.z);
        ListItemHiding(false);
        isMoving = false;
    }

    private void ListItemHiding(bool isHiding)
    {
        if (isHiding && messageObject.TypeOfMessage == TypeOfMessage.Notification)
        {
            float alpha = GetAlphaColorOfNotification();
            backGround.color = new Color(backGround.color.r, backGround.color.g, backGround.color.b, alpha);
            txtDate.color = new Color(txtDate.color.r, txtDate.color.g, txtDate.color.b, alpha);
            txtInfo.color = new Color(txtInfo.color.r, txtInfo.color.g, txtInfo.color.b, alpha);
            txtSender.color = new Color(txtSender.color.r, txtSender.color.g, txtSender.color.b, alpha);
            //btnRespondBackground.color = new Color(btnRespondBackground.color.r, btnRespondBackground.color.g, btnRespondBackground.color.b, alpha);
        }
        else
        {
            float alpha = 255f;
            backGround.color = new Color(backGround.color.r, backGround.color.g, backGround.color.b, alpha);
            txtDate.color = new Color(txtDate.color.r, txtDate.color.g, txtDate.color.b, alpha);
            txtInfo.color = new Color(txtInfo.color.r, txtInfo.color.g, txtInfo.color.b, alpha);
            txtSender.color = new Color(txtSender.color.r, txtSender.color.g, txtSender.color.b, alpha);
            //btnRespondBackground.color = new Color(btnRespondBackground.color.r, btnRespondBackground.color.g, btnRespondBackground.color.b, alpha);
        }

    }

    private float GetAlphaColorOfNotification()
    {
        if (rectTransform.position.x > startingXPosition)
        {
            float distance = rectTransform.position.x - startingXPosition;
            float percentOfAlpha = 100 - distance * 100 / distanceToDeletingArea;
            return percentOfAlpha / 100;
        }
        else if (rectTransform.position.x < startingXPosition)
        {
            float distance = startingXPosition - rectTransform.position.x;
            float percentOfAlpha = 100 - distance * 100 / distanceToDeletingArea;
            return percentOfAlpha / 100;
        }
        else
            return 255;
    }

    private bool IsInDeletingArea()
    {
        if (rectTransform.position.x > startingXPosition + distanceToDeletingArea ||
                rectTransform.position.x < startingXPosition - distanceToDeletingArea)
            return true;
        else
            return false;
    }

    private bool IsVerticalDragging()
    {
        if (Input.GetTouch(0).deltaPosition.y > 0.2 || Input.GetTouch(0).deltaPosition.y < -0.2)
            return true;
        else
            return false;
    }

    private bool IsHorizontalDragging()
    {
        if (Input.GetTouch(0).deltaPosition.x > 3.3f || Input.GetTouch(0).deltaPosition.x < -3.3f)
            return true;
        else
            return false;
    }

}
