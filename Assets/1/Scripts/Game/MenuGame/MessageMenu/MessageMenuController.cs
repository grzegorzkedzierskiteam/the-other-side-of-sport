﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class MessageMenuController : Menu
{
    private IDBConnection dbConnection;
    private IMessageManagerDB messageManagerDb;
    private List<IMessage> listOfMessages;
    public MenuGameController menuGameController;

    public GameObject listElements;
    public ScrollRect scrollRect;

    public GameObject panel;
    public GameObject txtTitle;
    public GameObject txtNoMessages;

    public Button btnExit;
    private Color startedPanelColor;

    public GameObject ImageOfMessageCounter { get; set; }
    public Text TxtMessageCounter { get; set; }

    [Inject]
    public void Constructor(IDBConnection dbConnection, IMessageManagerDB messageManagerDb)
    {
        this.messageManagerDb = messageManagerDb;
        this.dbConnection = dbConnection;
        messageManagerDb.SetUpDate(UserData.GetUserDataHolder().DateTime);
    }

    // Use this for initialization
    void Start()
    {
        btnExit.onClick.AddListener(BtnBack);

        startedPanelColor = panel.GetComponent<Image>().color;

        SetListOrTextNoNotifications();
    }

    private void BtnBack()
    {
        menuGameController.SetInteractableButtons(true);
        menuGameController.SetMessageCounter();
        MenuController.DestroyPopMenu(gameObject);
    }

    public void SetListOrTextNoNotifications()
    {
        dbConnection.OpenConnection();

        DownloadListOfMessages(dbConnection);
        if (listOfMessages.Any())
            SetList();
        else
            SetTextNoMessages();

        dbConnection.CloseConnection();
    }

    private void SetList()
    {
        SetMessageCounter();
        DeleteAllMessagesFromList();

        panel.GetComponent<Image>().color = startedPanelColor;

        foreach (IMessage message in listOfMessages)
        {
            if (message.TypeOfMessage == TypeOfMessage.Notification)
            {
                var newMessage = MenuController.InitializeListItem(ListItem.MessageListItemNotification, listElements.transform);
                var controller = newMessage.GetComponent<MessageMenuListItemNotificationController>();
                controller.messageObject = message;
                controller.txtInfo.text = message.Description;
                controller.txtDate.text = message.DateTime.FullDateString;
                controller.scrollRect = scrollRect;

                controller.messageMenuController = this;
            }
            else
            {
                var newMessage = MenuController.InitializeListItem(ListItem.MessageListItem, listElements.transform);
                var controller = newMessage.GetComponent<MessageMenuListItemController>();
                controller.messageObject = message;
                controller.txtSender.text = message.ParticipantName;
                controller.txtInfo.text = message.Description;
                controller.txtDate.text = message.DateTime.FullDateString;

                controller.messageMenuController = this;
            }
        }
    }

    private void DownloadListOfMessages(IDBConnection dbConnection)
    {
        listOfMessages = messageManagerDb.GetAllMessagesFromDB(dbConnection);
    }

    private void DeleteAllMessagesFromList()
    {
        foreach (Transform child in listElements.transform)
        {
            Destroy(child.gameObject);
        }
    }

    private void SetMessageCounter()
    {
        if (listOfMessages.Count == 0)
            ImageOfMessageCounter.SetActive(false);
        else
        {
            ImageOfMessageCounter.SetActive(true);
            TxtMessageCounter.text = listOfMessages.Count.ToString();
        }
    }

    private void SetTextNoMessages()
    {
        txtTitle.SetActive(false);
        listElements.SetActive(false);
        txtNoMessages.SetActive(true);

        panel.GetComponent<Image>().color = new Color(startedPanelColor.r,
                                                        startedPanelColor.g,
                                                        startedPanelColor.b,
                                                        0f);
    }

    public class Factory : PlaceholderFactory<MessageMenuController>
    {
    }
}
