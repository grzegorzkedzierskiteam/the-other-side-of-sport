﻿using UnityEngine;
using UnityEngine.UI;
using Zenject;
using Debug = UnityEngine.Debug;

public class RespondProceedMenuController : Menu
{
    private IDBConnection dBConnection;
    private IPlayerEnergy playerEnergy;
    public IMessage messageObject;
    public TypeOfResponse selectedResponseType;
    public MessageDetailsMenuController messageDetailsMenuController;

    public Text txtCost;
    public Color positiveColor;
    public Color negativeColor;

    public GameObject costsList;

    public Button btnExit;
    public Button btnProceed;
    private IMessageManager messageManager;

    // Use this for initialization

    [Inject]
    public void Constructor(IDBConnection dbConnection, IMessageManager messageManager, IPlayerEnergy playerEnergy)
    {
        this.dBConnection = dbConnection;
        this.messageManager = messageManager;
        this.playerEnergy = playerEnergy;
    }

    void Start()
    {
        btnProceed.onClick.AddListener(BtnProceed);
        btnExit.onClick.AddListener(BtnExit);

        SetCostOfResponseText();
        SetCostsList();
    }

    private void BtnExit()
    {
        messageDetailsMenuController.SetInteractableRespondButtons(true);
        MenuController.DestroyPopMenu(gameObject);
    }

    private void BtnProceed()
    {
        dBConnection.OpenConnection();

        ProceedResponse();

        dBConnection.CloseConnection();
    }

    private void ProceedResponse()
    {
        var minEnergy = messageObject.MessageResponses.Find(x => x.TypeOfResponse == selectedResponseType)
            .ListOfCost.Find(k => k.Property == PropertyEnum.Energy).Value;

        if (!playerEnergy.IsEnoughEnergyToActivity(dBConnection, minEnergy))
            return;
        
        if (selectedResponseType == TypeOfResponse.Payment)
        {
            var moneyCost = messageObject.MessageResponses.Find(x => x.TypeOfResponse == TypeOfResponse.Payment)
                                .ListOfCost.Find(k => k.Property == PropertyEnum.Money).Value * -1;
            var wealth = ClientData.GetAllDetails(dBConnection, messageObject.ParticipantID).Wealth;

            if (wealth < moneyCost)
            {
                MenuController.InitializeWarning("Your client doesn't have enough money to pay.");
                return;
            }
        }

        messageManager.Respond(dBConnection, messageObject, selectedResponseType);

        messageDetailsMenuController.SetInteractableRespondButtons(true);
        TEMPORARY_CloseRespondMenu();
    }

    private void TEMPORARY_CloseRespondMenu()
    {
        messageDetailsMenuController.messageMenuController.SetListOrTextNoNotifications();
        MenuController.DestroyPopMenu(messageDetailsMenuController.gameObject);
        MenuController.DestroyPopMenu(gameObject);
    }

    private void SetCostOfResponseText()
    {
        txtCost.text = "Cost of " + selectedResponseType.ToString();
    }

    private void SetCostsList()
    {
        foreach (var cost in messageObject.MessageResponses.Find(x => x.TypeOfResponse == selectedResponseType).ListOfCost)
        {
            var costImage = Instantiate(cost.PrefabOfResponseCost) as GameObject;

            if (cost.Value >= 0)
            {
                costImage.GetComponentInChildren<Text>().text = cost.ValueString;
                costImage.GetComponentInChildren<Text>().color = positiveColor;
            }
            else
            {
                costImage.GetComponentInChildren<Text>().text = cost.ValueString.Remove(0, 1);
                costImage.GetComponentInChildren<Text>().color = negativeColor;
            }

            costImage.transform.SetParent(costsList.transform);
            costImage.transform.localScale = Vector3.one;
        }
    }

    public class Factory : PlaceholderFactory<RespondProceedMenuController>
    {
    }
}
