﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class MessageDetailsMenuController : Menu
{
    public class Factory : PlaceholderFactory<MessageDetailsMenuController> { }

    public IMessage MessageObject { get; set; }
    public MessageMenuController messageMenuController;

    public Button btnExit;
    public Text txtSender;
    public Text txtMessage;
    public GameObject responseButtonsList;

    public GameObject btnRespondPrefab;
    public GameObject respondProceedMenuPrefab;

    // Use this for initialization
    void Start()
    {
        btnExit.onClick.AddListener(BtnExit);
        txtSender.text = MessageObject.ParticipantName;
        SetMessageText();

        SetResponseButtonsList();
    }

    private void BtnExit()
    {
        messageMenuController.SetListOrTextNoNotifications();
        MenuController.DestroyPopMenu(gameObject);
    }

    public void SetInteractableRespondButtons(bool interactable)
    {
        btnExit.interactable = interactable;
        foreach (Transform child in responseButtonsList.transform)
            child.GetComponent<Button>().interactable = interactable;
    }

    private void SetMessageText()
    {
        string messageText;
        if (MessageObject.Description.Substring(0, 1) == "'")
        {
            messageText = MessageObject.Description.Remove(MessageObject.Description.Length - 1);
            messageText = messageText.Remove(0, 1);
        }
        else
            messageText = MessageObject.Description;

        txtMessage.text = messageText;
    }

    private void SetResponseButtonsList()
    {
        foreach (MessageResponse response in MessageObject.MessageResponses)
        {
            var btnRespond = Instantiate(btnRespondPrefab);
            btnRespond.GetComponent<Button>().onClick.AddListener(() =>
            {
                var respondProceedMenu = MenuController.InitializePopMenu(MenuEnum.RespondProceedMenu);
                respondProceedMenu.GetComponent<RespondProceedMenuController>().messageObject = MessageObject;
                respondProceedMenu.GetComponent<RespondProceedMenuController>().selectedResponseType = response.TypeOfResponse;
                respondProceedMenu.GetComponent<RespondProceedMenuController>().messageDetailsMenuController = this;
                SetInteractableRespondButtons(false);
            });

            btnRespond.GetComponentInChildren<Text>().text = response.TypeOfResponse.ToString();

            btnRespond.transform.SetParent(responseButtonsList.transform);
            btnRespond.transform.localScale = Vector3.one;
        }
    }
}
