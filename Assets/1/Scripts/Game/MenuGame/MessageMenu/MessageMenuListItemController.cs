﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Zenject;

public class MessageMenuListItemController : Menu
{
    public class Factory : PlaceholderFactory<MessageMenuListItemController> { }

    public IMessage messageObject;
    public MessageMenuController messageMenuController;

    public Text txtSender;
    public Text txtDate;
    public Text txtInfo;
    public Button btnMoreDetails;

    // Use this for initialization
    void Start()
    {
        btnMoreDetails.onClick.AddListener(BtnMoreDetails);
    }

    private void BtnMoreDetails()
    {
        var messageDetailsMenu = MenuController.InitializePopMenu(MenuEnum.MessageItemDetails);
        messageDetailsMenu.GetComponent<MessageDetailsMenuController>().MessageObject = messageObject;
        messageDetailsMenu.GetComponent<MessageDetailsMenuController>().messageMenuController = messageMenuController;
    }
}
