﻿using Mono.Data.Sqlite;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class UserDataDB {

    public static void UpdateUserDataHolder()
    {
        DBConnection dbConnection = new DBConnection();
        dbConnection.OpenConnection();;
        UserDataHolder userData = GameObject.FindGameObjectWithTag("UserDataHolder").GetComponent<UserDataHolder>();

        string query = "SELECT * FROM agents WHERE ID = 1;";
        SqliteDataReader reader = dbConnection.DownloadQuery(query);
        reader.Read();
        userData.FirstName = reader.GetString(1);
        userData.LastName = reader.GetString(2);
        userData.Wealth = reader.GetInt32(3);
        userData.Popularity = reader.GetInt32(4);
        userData.Respectability = reader.GetInt32(5);
        userData.Allies = reader.GetString(6);
        userData.Enemies = reader.GetString(7);
        userData.Energy = reader.GetInt32(8);
        userData.Health = reader.GetInt32(9);
        userData.Clients = reader.GetString(10);

        reader.Close();
        reader = null;

        query = "SELECT Week,Year FROM config WHERE ID = 1;";
        reader = dbConnection.DownloadQuery(query);
        reader.Read();
        int week = reader.GetInt32(0);
        int year = reader.GetInt32(1);
        MyDateTime date = new MyDateTime(week, year);
        userData.DateTime = date;

        reader.Close();
        reader = null;

        dbConnection.CloseConnection();

    }
}
