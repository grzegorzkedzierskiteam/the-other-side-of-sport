﻿using Mono.Data.Sqlite;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth {

	public static int GetPlayerHealth(IDBConnection dBConnection)
    {
        string query = "SELECT Health FROM agents WHERE ID = 1;";

        SqliteDataReader reader = dBConnection.DownloadQuery(query);
        reader.Read();
        int health = reader.GetInt32(0);

        reader.Close();
        reader = null;

        return health;
    }
}
