﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyManager : IEnergyManager{

    private IDBConnection dBConnection;
    private readonly IPlayerEnergy playerEnergy;

    public EnergyManager(IDBConnection dBConnection, IPlayerEnergy playerEnergy)
    {
        this.dBConnection = dBConnection;
        this.playerEnergy = playerEnergy;
    }

    public void RestorePlayerEnergy()
    {
        int restoredEnergy = 80;

        int health = PlayerHealth.GetPlayerHealth(dBConnection);
        restoredEnergy -= GetImpactOfHealth(health);

        playerEnergy.ChangePlayerEnergy(dBConnection, restoredEnergy);
    }

    private int GetImpactOfHealth(int health)
    {
        return (100 - health)/2;
    }
}
