﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPlayerEnergy
{
    void ChangePlayerEnergy(IDBConnection dBConnection, int difference);
    int GetPlayerEnergy(IDBConnection dBConnection);
    bool IsEnoughEnergyToActivity(IDBConnection dBConnection, int minValue);
}
