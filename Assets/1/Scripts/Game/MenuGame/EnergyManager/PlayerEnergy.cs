﻿using Mono.Data.Sqlite;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerEnergy : IPlayerEnergy
{
    private readonly MenuController menuController;

    public PlayerEnergy(MenuController menuController)
    {
        this.menuController = menuController;
    }

    public void ChangePlayerEnergy(IDBConnection dBConnection, int difference)
    {
        int newEnergy = GetPlayerEnergy(dBConnection) + difference;

        if (newEnergy > 100)
            newEnergy = 100;
        else if (newEnergy < 0)
            newEnergy = 0;

        GameObject.FindGameObjectWithTag("UserDataHolder").GetComponent<UserDataHolder>().Energy = newEnergy;

        string query = "UPDATE agents SET Energy = " + newEnergy + " WHERE ID = 1;";
        dBConnection.ModifyQuery(query);
    }

    public int GetPlayerEnergy(IDBConnection dBConnection)
    {
        string query = "SELECT Energy FROM Agents WHERE ID = 1;";
        SqliteDataReader reader = dBConnection.DownloadQuery(query);
        reader.Read();
        int energy = reader.GetInt32(0);

        reader.Close();
        reader = null;

        return energy;
    }

    public bool IsEnoughEnergyToActivity(IDBConnection dBConnection, int minValue)
    {
        if (minValue < 0)
            minValue *= -1;

        if (GetPlayerEnergy(dBConnection) < minValue)
        {
            var warningText = "You don't have enough energy.";

            menuController.InitializeWarning(warningText);

            GameObject.FindGameObjectWithTag("EnergyBattery").GetComponent<EnergyBatteryController>().StartWarningAnimation();

            return false;
        }
        else
            return true;
    }
}
