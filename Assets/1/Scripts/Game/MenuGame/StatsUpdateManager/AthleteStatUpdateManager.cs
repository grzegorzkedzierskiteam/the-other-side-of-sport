﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class AthleteStatUpdateManager {

	public static void ChangePopularity(IDBConnection dBConnection, IClientObject clientObject, int value)
    {
        int newPopularityOfAthlete = SetValueFrom0To100(clientObject.Popularity, value);

        string query = "UPDATE players SET Popularity = " + newPopularityOfAthlete + " WHERE ID = " + clientObject.ID + ";";

        if(clientObject.HasAgent && clientObject.AgentID > 1 && value > 9)
        {
            AgentObject agentObject = AgentData.GetAllDetails(dBConnection, clientObject.AgentID);
            int agentValue = Random.Range(1, 3);

            int newPopularityOfAgent = SetValueFrom0To100(agentObject.Popularity, agentValue);

            query += "UPDATE agents SET Popularity = " + newPopularityOfAgent + " WHERE ID = " + agentObject.ID + ";";
        }

        dBConnection.ModifyQuery(query);
    }

    public static void ChangeOverallSkill(IDBConnection dBConnection, IClientObject clientObject, int value)
    {
        int newOverallOfAthlete = SetValueFrom0To100(clientObject.OverallSkill, value);

        string query = "UPDATE players SET OverallSkill = " + newOverallOfAthlete + " WHERE ID = " + clientObject.ID + ";";

        dBConnection.ModifyQuery(query);
    }

    public static int SetValueFrom0To100(int oldValue, int addingValue)
    {
        oldValue += addingValue;

        if (oldValue > 100)
            return 100;
        else if (oldValue < 0)
            return 0;
        else
            return oldValue;
    }
}
