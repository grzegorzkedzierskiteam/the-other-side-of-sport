﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
// ReSharper disable InconsistentNaming

public class MenuGameController : Menu
{
    private IDBConnection dbConnection;
    private UserDataHolder userDataHolder;
    private IMessageManagerDB messageManagerDb;

    //menu prefabs
    public GameObject agentsMenuPrefab;
    public GameObject resultsMenuPrefab;
    public GameObject confirmNextWeekPrefab;

    //player info
    public Text txtNameSurname;
    public Text txtDate;
    public Text txtMoney;

    //notification button
    public Button btnNotification;
    public GameObject imageOfMessageCounter;
    public Text txtMessageCounter;

    //news panel
    public Text txtNews;

    //buttons
    public Button btnProfile;
    public Button btnClients;
    public Button btnAgents;
    public Button btnResults;
    public Button btnExit;
    public Button btnNextWeek;
    public Button btnSimulate;

    private List<IMessage> listOfMessages;
    
    [Inject]
    public void Constructor(IDBConnection dbConnection, IMessageManagerDB messageManagerDb)
    {
        this.dbConnection = dbConnection;
        this.messageManagerDb = messageManagerDb;
    }

    void Awake()
    {
        userDataHolder = GameObject.FindGameObjectWithTag("UserDataHolder").GetComponent<UserDataHolder>();
    }

    public class Factory : PlaceholderFactory<MenuGameController> { }

    // Use this for initialization
    void Start () {
        UserDataDB.UpdateUserDataHolder();
        SetMenuUserData();
        SetMessageCounter();

        btnExit.onClick.AddListener(BtnExit);
        btnNextWeek.onClick.AddListener(BtnNextWeek);
        btnSimulate.onClick.AddListener(BtnSimulate);
        btnNotification.onClick.AddListener(BtnNotification);
        btnProfile.onClick.AddListener(BtnProfile);
        btnClients.onClick.AddListener(BtnClients);
        btnAgents.onClick.AddListener(BtnAgents);
        btnResults.onClick.AddListener(BtnResults);
    }

    //buttons events
    private void BtnExit()
    {
        MenuController.DestroyMenu(gameObject);
        Destroy(GameObject.Find("Energy(Clone)"));
    }

    private void BtnNextWeek()
    {
        btnProfile.interactable = false;
        btnClients.interactable = false;
        btnAgents.interactable = false;
        btnResults.interactable = false;
        btnNextWeek.interactable = false;
        btnNotification.interactable = false;
        btnExit.interactable = false;
        MenuController.InitializePopMenu(MenuEnum.ConfirmNextWeek);
    }

    private void BtnSimulate()
    {
        SetInteractableButtons(false);
        MenuController.InitializePopMenu(MenuEnum.SimulateMenu);
    }

    private void BtnNotification()
    {
        var messageMenu = MenuController.InitializePopMenu(MenuEnum.MessagesPanel);
        var messageMenuController = messageMenu.GetComponent<MessageMenuController>();
        messageMenuController.menuGameController = this;
        messageMenuController.ImageOfMessageCounter = imageOfMessageCounter;
        messageMenuController.TxtMessageCounter = txtMessageCounter;
        SetInteractableButtons(false);
    }

    private void BtnProfile()
    {
        MenuController.InitializeMenu(MenuEnum.ProfileMenu);
    }

    private void BtnClients()
    {
        MenuController.InitializeMenu(MenuEnum.ClientsMenu);
    }

    private void BtnAgents()
    {
        MenuController.InitializeMenu(MenuEnum.AgentsMenu);
    }

    private void BtnResults()
    {
        MenuController.InitializeMenu(MenuEnum.ResultsMenu);
    }
    //

    public void SetInteractableButtons(bool value)
    {
        btnAgents.interactable = value;
        btnClients.interactable = value;
        btnExit.interactable = value;
        btnNextWeek.interactable = value;
        btnSimulate.interactable = value;
        btnNotification.interactable = value;
        btnProfile.interactable = value;
        btnResults.interactable = value;
    }

    public void SetMenuUserData()
    {
        txtNameSurname.text = userDataHolder.FirstName + " " + userDataHolder.LastName;
        txtMoney.text = Money.GetMoneyString(userDataHolder.Wealth);
        txtDate.text = userDataHolder.DateTime.WeekPerMonth.ToString() + " Week " + userDataHolder.DateTime.Month.ToString() + " " + userDataHolder.DateTime.Year.ToString();
    }

    public void SetMessageCounter()
    {
        dbConnection.OpenConnection();
        messageManagerDb.SetUpDate(userDataHolder.DateTime);

        listOfMessages = messageManagerDb.GetAllMessagesFromDB(dbConnection);

        dbConnection.CloseConnection();

        if(listOfMessages.Count == 0)
            imageOfMessageCounter.SetActive(false);
        else
        {
            imageOfMessageCounter.SetActive(true);
            txtMessageCounter.text = listOfMessages.Count.ToString();
        }
    }
}
