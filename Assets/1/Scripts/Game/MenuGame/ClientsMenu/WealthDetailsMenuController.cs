﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class WealthDetailsMenuController : Menu {

    public GameObject menuToSetActive;

    public Text txtWealth;
    public Text txtDept;
    public Button btnCancel;

    public int Wealth { get; set; }
    public int Dept { get; set; }

	// Use this for initialization
	void Start () {
        btnCancel.onClick.AddListener(BtnCancel);
        SetUIElements();
    }

    private void BtnCancel()
    {
        menuToSetActive.GetComponent<ClientDetailsMenuController>().SetInteractableButtons(true);

        Destroy(gameObject);
    }

    private void SetUIElements()
    {
        if (Dept == 0)
            txtDept.text = "No dept";
        else
            txtDept.text = Money.GetMoneyString(Dept);

        if (Wealth == 0)
            txtWealth.text = "Is bankrupt";
        else
            txtWealth.text = Money.GetMoneyString(Wealth);
    }

    public class Factory : PlaceholderFactory<WealthDetailsMenuController> { }
}
