﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class ClientsMenuController : Menu {

    public Button btnBack;
    public GameObject list;

	// Use this for initialization
	void Start () {
        btnBack.onClick.AddListener(BtnBack);
	}

    private void BtnBack()
    {
        MenuController.DestroyMenu(gameObject);
    }

    public class Factory : PlaceholderFactory<ClientsMenuController> { }
}
