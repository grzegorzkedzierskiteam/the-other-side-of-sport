﻿using Mono.Data.Sqlite;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class ClientsListController : Menu{

    private bool initialized = false;

    private IDBConnection dbConnection;
    private int currentIndexList = 0;
    private int allResultsNumber;
    private enum SortingType { ascending, descending };
    private enum SelectedGroupToGet { yourClients, allPlayers, favorites};
    private SelectedGroupToGet groupToGet;

    public GameObject listElements;

    public Dropdown dropdown;
    public Color favoriteColor;

    //------------------------------buttons
    public Button btnBack100;
    public Button btnNext100;
    public Text txtCurrentResults;
    public Text txtAllResults;
    public Button btnFilter;

    //sorting buttons
    public Button btnOverallSort;
    public Button btnLastNameSort;
    public Button btnPositionSort;
    public Button btnSportSort;
    public Button btnTeamSort;
    public Button btnAgentSort;
    private SortingType lastNameSortingType = SortingType.descending;
    private SortingType overallSkillSortingType = SortingType.descending;
    private SortingType positionSortingType = SortingType.ascending;
    private SortingType sportSortingType = SortingType.ascending;
    private SortingType teamSortingType = SortingType.ascending;
    private SortingType agentSortingType = SortingType.ascending;
    //------------------------------------

    //filters
    public GameObject filterPanel;
    public GameObject btnSearch;
    public InputField inputLastName;
    public InputField inputOvr;
    public InputField inputPosition;
    public InputField inputSport;
    public InputField inputTeam;

    //
    private List<ClientItemList> playersList;
    
    [Inject]
    public void Constructor(IDBConnection dbConnection)
    {
        this.dbConnection = dbConnection;
    }

	// Use this for initialization
	void Start () {
        groupToGet = SelectedGroupToGet.yourClients;
        playersList = GetPlayersFromDB(groupToGet);
        AddClientsListToMenu(0, 100);

        dropdown.onValueChanged.AddListener(delegate { DropdownMenu(dropdown); });
        //buttons listeners
        btnBack100.onClick.AddListener(BtnBack100);
        btnNext100.onClick.AddListener(BtnNext100);
        btnLastNameSort.onClick.AddListener(SortByLastName);
        btnPositionSort.onClick.AddListener(SortByPosition);
        btnSportSort.onClick.AddListener(SortBySport);
        btnTeamSort.onClick.AddListener(SortByTeam);
        btnAgentSort.onClick.AddListener(SortByAgent);
        btnOverallSort.onClick.AddListener(SortByOverallSkill);
        //
        btnFilter.onClick.AddListener(BtnFilter);
        btnSearch.GetComponent<Button>().onClick.AddListener(BtnSearch);
        //

        initialized = true;
    }

    void Update()
    {
        CheckButtonsInteractable();
    }

    private void OnEnable()
    {
        if(initialized)
        {
            DestroyAllCurrentRows();
            playersList = GetPlayersFromDB(groupToGet);
            AddClientsListToMenu(0, 100);
        }
    }

    private void DropdownMenu(Dropdown dropdown)
    {
        DestroyAllCurrentRows();

        switch (this.dropdown.value)
        {
            case 0:
                groupToGet = SelectedGroupToGet.yourClients;
                break;
            case 1:
                groupToGet = SelectedGroupToGet.allPlayers;
                break;
            case 2:
                groupToGet = SelectedGroupToGet.favorites;
                break;
            default:
                groupToGet = SelectedGroupToGet.allPlayers;
                break;
        }

        playersList = GetPlayersFromDB(groupToGet);
        AddClientsListToMenu(0, 100);
    }

    #region sorting methods
    private void SortByOverallSkill()
    {
        switch (overallSkillSortingType)
        {
            case SortingType.ascending:
                playersList = playersList.OrderBy(x => x.OverallSkill).ToList();
                overallSkillSortingType = SortingType.descending;
                break;

            case SortingType.descending:
                playersList = playersList.OrderByDescending(x => x.OverallSkill).ToList();
                overallSkillSortingType = SortingType.ascending;
                break;
        }

        DestroyAllCurrentRows();
        AddClientsListToMenu(0, 100);
    }

    private void SortByLastName()
    {
        switch(lastNameSortingType)
        {
            case SortingType.ascending:
                playersList = playersList.OrderBy(x => x.LastName).ToList();
                lastNameSortingType = SortingType.descending;
                break;

            case SortingType.descending:
                playersList = playersList.OrderByDescending(x => x.LastName).ToList();
                lastNameSortingType = SortingType.ascending;
                break;
        }

        DestroyAllCurrentRows();
        AddClientsListToMenu(0, 100);
    }

    private void SortByPosition()
    {
        switch (agentSortingType)
        {
            case SortingType.ascending:
                playersList = playersList.OrderBy(x => x.Position).ToList();
                agentSortingType = SortingType.descending;
                break;

            case SortingType.descending:
                playersList = playersList.OrderByDescending(x => x.Position).ToList();
                agentSortingType = SortingType.ascending;
                break;
        }
        DestroyAllCurrentRows();
        AddClientsListToMenu(0, 100);
    }

    private void SortBySport()
    {
        switch (sportSortingType)
        {
            case SortingType.ascending:
                playersList = playersList.OrderBy(x => x.Sport).ToList();
                sportSortingType = SortingType.descending;
                break;

            case SortingType.descending:
                playersList = playersList.OrderByDescending(x => x.Sport).ToList();
                sportSortingType = SortingType.ascending;
                break;
        }
        DestroyAllCurrentRows();
        AddClientsListToMenu(0, 100);
    }

    private void SortByTeam()
    {
        switch (teamSortingType)
        {
            case SortingType.ascending:
                playersList = playersList.OrderBy(x => x.Team).ToList();
                teamSortingType = SortingType.descending;
                break;

            case SortingType.descending:
                playersList = playersList.OrderByDescending(x => x.Team).ToList();
                teamSortingType = SortingType.ascending;
                break;
        }
        DestroyAllCurrentRows();
        AddClientsListToMenu(0, 100);
    }

    private void SortByAgent()
    {
        switch (agentSortingType)
        {
            case SortingType.ascending:
                playersList = playersList.OrderBy(x => x.AgentID).ToList();
                agentSortingType = SortingType.descending;
                break;

            case SortingType.descending:
                playersList = playersList.OrderByDescending(x => x.AgentID).ToList();
                agentSortingType = SortingType.ascending;
                break;
        }
        DestroyAllCurrentRows();
        AddClientsListToMenu(0, 100);
    }
#endregion

#region main table methods 
    private void CheckButtonsInteractable()
    {
        if (currentIndexList - 100 < 0)
            btnBack100.interactable = false;
        else
            btnBack100.interactable = true;

        if (currentIndexList + 101 > allResultsNumber)
            btnNext100.interactable = false;
        else
            btnNext100.interactable = true;
    }

    private void BtnBack100()
    {
        if (currentIndexList - 100 >= 0)
        {
            currentIndexList -= 100;
            DestroyAllCurrentRows();

            if (currentIndexList - 100 < 0)
                AddClientsListToMenu(0, currentIndexList + 100);
            else
                AddClientsListToMenu(currentIndexList, currentIndexList + 100);
        }
    }

    private void BtnNext100()
    {
        if (currentIndexList + 101 <= allResultsNumber)
        {
            currentIndexList += 100;
            DestroyAllCurrentRows();

            if (currentIndexList + 100 > allResultsNumber)
                AddClientsListToMenu(currentIndexList, currentIndexList + allResultsNumber - currentIndexList);
            else
                AddClientsListToMenu(currentIndexList, currentIndexList + 100);
        }
    }

    private void DestroyAllCurrentRows()
    {
        foreach (Transform child in listElements.transform)
        {
            Destroy(child.gameObject);
        }
    }

    private void AddClientsListToMenu(int min, int max)
    {
        allResultsNumber = playersList.Count;

        if (max > allResultsNumber)
        {
            max = allResultsNumber;
            if(allResultsNumber == 0)
                txtCurrentResults.text = (min).ToString() + " - " + allResultsNumber.ToString();
            else
                txtCurrentResults.text = (min + 1).ToString() + " - " + allResultsNumber.ToString();
        }
        else
            txtCurrentResults.text = (min + 1).ToString() + " - " + max.ToString();

        txtAllResults.text = "Results: " + allResultsNumber.ToString();
       
        for (var i = min; i < max; i++)
        {
            var newClient = MenuController.InitializeListItem(ListItem.ClientsListItem, listElements.transform);
            var controller = newClient.GetComponent<ClientsItemController>();
            controller.id = playersList[i].ID;
            controller.txtOverall.text = playersList[i].OverallSkill.ToString();
            controller.txtName.text = playersList[i].FirstName + " " + playersList[i].LastName;
            controller.txtPosition.text = playersList[i].Position;
            controller.txtSport.text = playersList[i].Sport;
            controller.txtTeam.text = playersList[i].Team;
            newClient.name = i.ToString();

            //favorite yellow text
            if (playersList[i].IsFavorite)
                controller.btnClientDetalisText.color = favoriteColor;

            //injury red text
            if (playersList[i].InjuryTime > 0)
                controller.imgRedCross.gameObject.SetActive(true);

            if (playersList[i].AgentID == -1)
                controller.txtAgent.text = "None";
            else if(playersList[i].AgentID == 1)
                controller.txtAgent.text = "You";
            else
                controller.txtAgent.text = "Has an agent";
        }
    }

    private List<ClientItemList> GetPlayersFromDB(SelectedGroupToGet group)
    {
        List<ClientItemList> arrayList = new List<ClientItemList>();
        dbConnection.OpenConnection();;
        string query = GetQuerySelectedGroup(group);
        SqliteDataReader reader = dbConnection.DownloadQuery(query);

        while (reader.Read())
        {
            arrayList.Add(new ClientItemList(
                    reader.GetInt32(0),
                    reader.GetString(1),
                    reader.GetString(2),
                    reader.GetString(3),
                    reader.GetString(4),
                    reader.GetString(5),
                    reader.GetInt32(6),
                    reader.GetInt32(7),
                    reader.GetInt32(8),
                    reader.GetInt32(9)
                ));
        }

        reader.Close();
        reader = null;

        dbConnection.CloseConnection();

        return arrayList;
    }

    private string GetQuerySelectedGroup(SelectedGroupToGet group)
    {
        string query = "SELECT ID,FirstName,LastName,Sport,Position,Team,OverallSkill,Agent,InjuryTime,IsFavorite FROM players ";

        switch (group)
        {
            case SelectedGroupToGet.yourClients:
                query += "WHERE Agent = 1";
                break;
            case SelectedGroupToGet.favorites:
                query += "WHERE IsFavorite = 1";
                break;
            default:
                break;
        }

        query += " ORDER BY LastName";

        return query;
    }

    #endregion

    #region filters

    private void BtnFilter()
    {
        filterPanel.SetActive(!filterPanel.activeSelf);
    }

    private void BtnSearch()
    {
        string query = GetQueryFilter();

        List<ClientItemList> arrayList = new List<ClientItemList>();
        dbConnection.OpenConnection();;
        SqliteDataReader reader = dbConnection.DownloadQuery(query);
        while (reader.Read())
        {
            arrayList.Add(new ClientItemList(
                    reader.GetInt32(0),
                    reader.GetString(1),
                    reader.GetString(2),
                    reader.GetString(3),
                    reader.GetString(4),
                    reader.GetString(5),
                    reader.GetInt32(6),
                    reader.GetInt32(7),
                    reader.GetInt32(8),
                    reader.GetInt32(9)
                ));
        }

        reader.Close();
        reader = null;

        dbConnection.CloseConnection();

        playersList = arrayList;

        DestroyAllCurrentRows();
        AddClientsListToMenu(0, 100);
    }

    private string GetQueryFilter()
    {
        if (inputLastName.text == "" && inputOvr.text == "" && inputPosition.text == "" && inputSport.text == "" && inputTeam.text == "" && dropdown.value == 0)
            return "SELECT ID,FirstName,LastName,Sport,Position,Team,OverallSkill,Agent,InjuryTime,IsFavorite FROM players WHERE Agent = 1 ORDER BY LastName;";

        if (inputLastName.text == "" && inputOvr.text == "" && inputPosition.text == "" && inputSport.text == "" && inputTeam.text == "" && dropdown.value == 1)
            return "SELECT ID,FirstName,LastName,Sport,Position,Team,OverallSkill,Agent,InjuryTime,IsFavorite FROM players ORDER BY LastName;";

        if (inputLastName.text == "" && inputOvr.text == "" && inputPosition.text == "" && inputSport.text == "" && inputTeam.text == "" && dropdown.value == 2)
            return "SELECT ID,FirstName,LastName,Sport,Position,Team,OverallSkill,Agent,InjuryTime,IsFavorite FROM players WHERE IsFavorite = 1 ORDER BY LastName;";

        string query;

        if (dropdown.value == 0)
            query = "SELECT ID,FirstName,LastName,Sport,Position,Team,OverallSkill,Agent,InjuryTime,IsFavorite FROM players WHERE Agent = 1 AND ";
        else if (dropdown.value == 2)
            query = "SELECT ID,FirstName,LastName,Sport,Position,Team,OverallSkill,Agent,InjuryTime,IsFavorite FROM players WHERE IsFavorite = 1 AND ";
        else
            query = "SELECT ID,FirstName,LastName,Sport,Position,Team,OverallSkill,Agent,InjuryTime,IsFavorite FROM players WHERE ";

        List<InputField> inputs = new List<InputField>();
        List<InputField> notNullInputs = new List<InputField>();
        inputs.Add(inputLastName);
        inputs.Add(inputOvr);
        inputs.Add(inputPosition);
        inputs.Add(inputSport);
        inputs.Add(inputTeam);

        int numberOfFilters = 0;
        foreach(InputField input in inputs)
        {
            if(input.text != "")
            {
                numberOfFilters += 1;
                notNullInputs.Add(input);
            }
        }

        if(numberOfFilters == 1)
        {
            if(GetFilterName(notNullInputs[0]) == "OverallSkill")
            {
                if(notNullInputs[0].text.Substring(0,1) == ">" || notNullInputs[0].text.Substring(0, 1) == "<")
                    query += GetFilterName(notNullInputs[0]) + notNullInputs[0].text + "";
                else
                    query += GetFilterName(notNullInputs[0]) + "='" + notNullInputs[0].text + "'";
            }
            else
                query += GetFilterName(notNullInputs[0]) + " Like '" + notNullInputs[0].text + "%'";
        }
        else
        {
            InputField lastInput = notNullInputs.Last();
            foreach (InputField input in notNullInputs)
            {
                if (GetFilterName(input) == "OverallSkill")
                {
                    if (input.text.Substring(0, 1) == ">" || input.text.Substring(0, 1) == "<")
                        query += GetFilterName(input) + input.text + "";
                    else
                        query += GetFilterName(input) + "='" + input.text + "'";
                }
                else
                    query += GetFilterName(input) + " Like '" + input.text + "%'";

                if (input != lastInput)
                    query += " AND ";
            }
        }


        query += " ORDER BY LastName;";
        return query;
    }

    private string GetFilterName(InputField input)
    {
        return input.name.Substring(5);
    }
    
    #endregion
}
