﻿using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class ContractWithTeamMenuController : Menu, ISigningContracts {

    private IDBConnection dbConnection;
    private UserDataHolder userDataHolder;

    public GameObject menuToSetActive;
    public int clientID;

    private ClientObject clientObject;

    public Text txtTeamName;
    public Text txtContractTerms;
    public Text txtGuaranteedMoney;
    public Text txtSignYear;
    public Text txtFreeAgentYear;
    public Button btnCancel;

    [Inject]
    public void Constructor(IDBConnection dbConnection)
    {
        this.dbConnection = dbConnection;
    }

    // Use this for initialization
    void Start()
    {
        userDataHolder = UserData.GetUserDataHolder();
        GetClientDetails();
        SetUIDataElements();
        btnCancel.onClick.AddListener(BtnCancel); 
    }

    private void BtnCancel()
    {
        menuToSetActive.GetComponent<ISigningContracts>().SetInteractableButtons(true);
        menuToSetActive.GetComponent<ISigningContracts>().GetClientDetails();
        menuToSetActive.GetComponent<ISigningContracts>().SetUIDataElements();

        Destroy(gameObject);
    }

    public void GetClientDetails()
    {
        dbConnection.OpenConnection();;
        clientObject = ClientData.GetAllDetails(dbConnection, clientID);
        dbConnection.CloseConnection();
    }

    public void SetUIDataElements()
    {
        txtTeamName.text = clientObject.Team;
        txtContractTerms.text = clientObject.Contract.GetFullLengthOfContract(userDataHolder.DateTime.Year).ToString() + "yr(s) / ";
        txtContractTerms.text += Money.GetMoneyString(clientObject.Contract.Value);
        txtGuaranteedMoney.text = Money.GetMoneyString(clientObject.Contract.GuaranteedMoney);
        txtSignYear.text = clientObject.Contract.YearOfSign.ToString();
        txtFreeAgentYear.text = (userDataHolder.DateTime.Year + clientObject.Contract.Length).ToString();
    }

    public void SetInteractableButtons(bool value)
    {
        btnCancel.interactable = value;
    }

    public class Factory : PlaceholderFactory<ContractWithTeamMenuController> { }
}
