﻿using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class SigningPlayerAsClientController : Menu
{
    private IDBConnection dbConnection;

    public GameObject menuToSetActive;
    public Offer Offer { get; set; }
    public ClientObject clientObject;

    public Color acceptedColor;
    public Color rejectedColor;
    public Text txtOfferStatus;
    public Text txtHavingAgent;
    public Text txtRemainingAttempts;
    public Text txtOurRelation;
    public Button btnMakeOffer;
    public Button btnConfirm;

    private Bidder playerAsBidder;
    private SigningPlayerAsClientNegotiatingButtonsController buttonsController;
    private UserDataHolder userDataHolder;
    private IPlayerEnergy playerEnergy;

    [Inject]
    public void Constructor(IDBConnection dbConnection, IPlayerEnergy playerEnergy)
    {
        this.dbConnection = dbConnection;
        this.playerEnergy = playerEnergy;
    }

    // Use this for initialization
    void Start()
    {
        btnMakeOffer.onClick.AddListener(BtnMakeOffer);
        btnConfirm.onClick.AddListener(BtnConfirmOffer);
        SetTxtHavingAgent();
        buttonsController = GetComponent<SigningPlayerAsClientNegotiatingButtonsController>();

        userDataHolder = UserData.GetUserDataHolder();
        playerAsBidder = new Bidder(userDataHolder.Respectability, userDataHolder.Popularity);
        AttemptsTextUpdate();
    }

    private void Update()
    {
        SetActiveControllerButtons();
    }

    private void BtnMakeOffer()
    {
        dbConnection.OpenConnection();;

        var canHeDoIt = playerEnergy.IsEnoughEnergyToActivity(dbConnection, 10);

        if (canHeDoIt)
        {
            playerEnergy.ChangePlayerEnergy(dbConnection, -10);

            dbConnection.CloseConnection();
    

            MakeOffer();
        }
        else
        {
            dbConnection.CloseConnection();
    
        }
    }

    private void MakeOffer()
    {
        PlayerNegotiatingWithClient playerNegotiating;
        Offer = new Offer(buttonsController.ContractLength, buttonsController.Wage);
        if (clientObject.HasAgent && clientObject.AgentID != 1)
        {
            dbConnection.OpenConnection();;
            IBidder currentAgent = AgentData.GetAgentAsBidder(dbConnection, clientObject.AgentID);
            dbConnection.CloseConnection();
    

            playerNegotiating = new PlayerNegotiatingWithClient(playerAsBidder, clientObject, Offer, currentAgent);
        }
        else
            playerNegotiating = new PlayerNegotiatingWithClient(playerAsBidder, clientObject, Offer);

        Offer.IsAccepted = playerNegotiating.MakeOfferToClient();
        UpdateData();
    }

    private void UpdateData()
    {
        if (Offer.IsAccepted)
            OfferWasAccepted();
        else
            OfferWasRejected();
    }

    private void OfferWasAccepted()
    {
        UserDataDB.UpdateUserDataHolder();
        UserDataHolder userDataHolder = UserData.GetUserDataHolder();
        playerAsBidder = new Bidder(userDataHolder.Respectability, userDataHolder.Popularity);

        txtOurRelation.text = clientObject.Relation + "%";
        DecreaseAttempt();
        UpdateClientObject();
        AttemptsTextUpdate();

        btnMakeOffer.gameObject.SetActive(false);
        btnConfirm.gameObject.SetActive(true);
        txtOfferStatus.text = "Offer was accepted";
        txtOfferStatus.color = acceptedColor;
    }

    private void OfferWasRejected()
    {
        clientObject.Relation -= 5;
        if (clientObject.Relation < 0)
            clientObject.Relation = 0;

        UpdateRelationWithClient();

        txtOurRelation.text = clientObject.Relation + "%";

        DecreaseAttempt();
        UpdateClientObject();
        AttemptsTextUpdate();

        txtOfferStatus.text = "Offer was rejected";
        txtOfferStatus.color = rejectedColor;
    }

    private void BtnConfirmOffer()
    {
        clientObject.Relation += 5;
        if (clientObject.Relation > 100)
            clientObject.Relation = 100;

        if (clientObject.HasAgent)
        {
            UpdateRelationWithExAgent();
            if (clientObject.AgentID != 1)
                ExAgentLosesClient(clientObject.AgentID);
        }

        UpdateRelationWithClient();
        UpdateClientObject();
        SignClient();

        menuToSetActive.GetComponent<ISigningContracts>().SetInteractableButtons(true);
        menuToSetActive.GetComponent<ISigningContracts>().GetClientDetails();
        menuToSetActive.GetComponent<ISigningContracts>().SetUIDataElements();

        Destroy(gameObject);
    }

    private void UpdateClientObject()
    {
        dbConnection.OpenConnection();;
        clientObject = ClientData.GetAllDetails(dbConnection, clientObject.ID);
        dbConnection.CloseConnection();
    }

    private void UpdateRelationWithClient()
    {
        dbConnection.OpenConnection();;
        string query = "UPDATE players SET Relation = " + clientObject.Relation + " WHERE ID = " + clientObject.ID;
        dbConnection.ModifyQuery(query);
        dbConnection.CloseConnection();
    }

    private void UpdateRelationWithExAgent()
    {
        dbConnection.OpenConnection();;
        AgentObject exAgent = AgentData.GetAllDetails(dbConnection, clientObject.AgentID);
        RelationManager.ChangeRelationWithOtherAgent(dbConnection, exAgent, -10);
        dbConnection.CloseConnection();
    }

    private void ExAgentLosesClient(int exAgentID)
    {
        dbConnection.OpenConnection();;
        AgentsSimpleMethods.AgentLosesClient(dbConnection, clientObject.ID, exAgentID);
        dbConnection.CloseConnection();
    }

    private void SignClient()
    {
        dbConnection.OpenConnection();;
        userDataHolder = UserData.GetUserDataHolder();
        AgentsSimpleMethods.SetAthleteAsClientOfPlayerInDB(dbConnection, userDataHolder, clientObject, Offer);
        dbConnection.CloseConnection();

    }

    private void DecreaseAttempt()
    {
        dbConnection.OpenConnection();;
        AgentsSimpleMethods.DecreaseAttempt(dbConnection, clientObject);
        dbConnection.CloseConnection();
    }

    private void AttemptsTextUpdate()
    {
        if (clientObject.ReadyStatus == 1)
            txtRemainingAttempts.text = "3";
        else if (clientObject.ReadyStatus == 2)
            txtRemainingAttempts.text = "2";
        else if (clientObject.ReadyStatus == 3)
            txtRemainingAttempts.text = "1";
        else
            txtRemainingAttempts.text = "0";
    }

    private void SetTxtHavingAgent()
    {
        if (clientObject.HasAgent)
        {
            UserDataHolder userDataHolder = UserData.GetUserDataHolder();
            if (clientObject.AgentID == 1)
            {
                txtHavingAgent.text = "This is your client.";
                txtHavingAgent.color = acceptedColor;
            }
            else if (IsThisClientOfYourAllie(userDataHolder))
                txtHavingAgent.text = "He's a client of your friend.";
            else if (IsThisClientOfYourEnemy(userDataHolder))
                txtHavingAgent.text = "He's a client of your enemy.";
            else
                txtHavingAgent.text = "He already has an agent.";
        }
    }

    private bool IsThisClientOfYourAllie(UserDataHolder userDataHolder)
    {
        if (GroupStringManager.IsPersonInGroup(userDataHolder.Allies, clientObject.AgentID.ToString()))
            return true;
        else
            return false;
    }

    private bool IsThisClientOfYourEnemy(UserDataHolder userDataHolder)
    {
        if (GroupStringManager.IsPersonInGroup(userDataHolder.Enemies, clientObject.AgentID.ToString()))
            return true;
        else
            return false;
    }

    private void SetActiveControllerButtons()
    {
        if (clientObject.ReadyStatus == 0)
            btnMakeOffer.gameObject.SetActive(false);
        else
            btnMakeOffer.gameObject.SetActive(true);
    }
}
