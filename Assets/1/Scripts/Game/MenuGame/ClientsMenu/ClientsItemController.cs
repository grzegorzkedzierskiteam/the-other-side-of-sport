﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class ClientsItemController : Menu {

    public int id;
    public Text txtOverall;
    public Text txtName;
    public Text txtPosition;
    public Text txtSport;
    public Text txtTeam;
    public Text txtAgent;
    public Text btnClientDetalisText;
    public Image imgRedCross;

    public Button btnClientDetails;

    private void Start()
    {
        btnClientDetails.onClick.AddListener(BtnClientsDetails);
    }

    private void BtnClientsDetails()
    {
        //GameObject clientsMenu = GameObject.Find("ClientsMenu(Clone)");
        
        //clientsMenu.SetActive(false);
        var menuDetails = MenuController.InitializeMenu(MenuEnum.ClientDetailsMenu);
        menuDetails.GetComponent<ClientDetailsMenuController>().clientID = id;
    }

    public class Factory : PlaceholderFactory<ClientsItemController> { }
}
