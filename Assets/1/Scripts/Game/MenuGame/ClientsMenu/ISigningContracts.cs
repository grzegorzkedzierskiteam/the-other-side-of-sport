﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISigningContracts
{
    void GetClientDetails();
    void SetUIDataElements();
    void SetInteractableButtons(bool value);
}

