﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SigningPlayerAsClientNegotiatingButtonsController : MonoBehaviour {

    public SigningPlayerAsClientController signingController;

    public Text txtWage;
    public Text txtContractLength;

    public Button btnContractLengthIncrease;
    public Button btnContractLengthDecrease;
    public Button btnWageIncrease;
    public Button btnWageDecrease;

    public int Wage { get; set; }
    public int ContractLength { get; set; }

    private const int MaxContractLength = 10;
    private const int MinContractLength = 1;
    private const int MaxWage = 20;
    private const int MinWage = 1;

    // Use this for initialization
    void Start () {
        signingController = GetComponent<SigningPlayerAsClientController>();

        btnContractLengthIncrease.onClick.AddListener(BtnContractLengthIncrease);
        btnContractLengthDecrease.onClick.AddListener(BtnContractLengthDecrease);
        btnWageIncrease.onClick.AddListener(BtnWageIncrease);
        btnWageDecrease.onClick.AddListener(BtnWageDecrease);
        Wage = 4;
        ContractLength = 2;
    }
	
	// Update is called once per frame
	void Update () {
        SetActiveControllerButtons();
    }

    private void BtnContractLengthIncrease()
    {
        ContractLength = MyMath.GetIncreasedValueInTheRange(MaxContractLength, ContractLength);
        txtContractLength.text = ContractLength.ToString() + Environment.NewLine;

        if (ContractLength == 1)
            txtContractLength.text += "year";
        else
            txtContractLength.text += "years";
    }

    private void BtnContractLengthDecrease()
    {
        ContractLength = MyMath.GetDecreasedValueInTheRange(MinContractLength, ContractLength);
        txtContractLength.text = ContractLength.ToString() + Environment.NewLine;

        if (ContractLength == 1)
            txtContractLength.text += "year";
        else
            txtContractLength.text += "years";
    }

    private void BtnWageIncrease()
    {
        Wage = MyMath.GetIncreasedValueInTheRange(MaxWage, Wage);
        txtWage.text = Wage.ToString() + "%";
    }

    private void BtnWageDecrease()
    {
        Wage = MyMath.GetDecreasedValueInTheRange(MinWage, Wage);
        txtWage.text = Wage.ToString() + "%";
    }

    private void SetActiveControllerButtons()
    {
        if (ContractLength == MaxContractLength)
            btnContractLengthIncrease.gameObject.SetActive(false);
        else
            btnContractLengthIncrease.gameObject.SetActive(true);

        if (ContractLength == MinContractLength)
            btnContractLengthDecrease.gameObject.SetActive(false);
        else
            btnContractLengthDecrease.gameObject.SetActive(true);

        if (Wage == MaxWage)
            btnWageIncrease.gameObject.SetActive(false);
        else
            btnWageIncrease.gameObject.SetActive(true);

        if (Wage == MinWage)
            btnWageDecrease.gameObject.SetActive(false);
        else
            btnWageDecrease.gameObject.SetActive(true);

        if(signingController.Offer != null)
        {
            if(signingController.Offer.IsAccepted)
            {
                btnWageDecrease.gameObject.SetActive(false);
                btnWageIncrease.gameObject.SetActive(false);
                btnContractLengthDecrease.gameObject.SetActive(false);
                btnContractLengthIncrease.gameObject.SetActive(false);
            }
        }
    }
}
