﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClientItemList {

    private int iD;
    private string firstName;
    private string lastName;
    private string sport;
    private string position;
    private string team;
    private int overallSkill;
    private int agentID;
    private int injuryTime;
    private bool isFavorite;

    public ClientItemList(int _id, string _firstName, string _lastName, string _sport, string _position, string _team, int _overallSkill, int _agentID, int _injuryTime, int _isFavorite)
    {
        iD = _id;
        firstName = _firstName;
        lastName = _lastName;
        sport = _sport;
        position = _position;
        team = _team;
        overallSkill = _overallSkill;
        agentID = _agentID;
        injuryTime = _injuryTime;

        if (_isFavorite == 0)
            isFavorite = false;
        else
            isFavorite = true;
    }

    public int ID
    {
        get { return iD; }
        set { iD = value; }
    }

    public string FirstName
    {
        get { return firstName; }
        set { firstName = value; }
    }

    public string LastName
    {
        get { return lastName; }
        set { lastName = value; }
    }

    public string Sport
    {
        get { return sport; }
        set { sport = value; }
    }

    public string Position
    {
        get { return position; }
        set { position = value; }
    }

    public string Team
    {
        get { return team; }
        set { team = value; }
    }

    public int OverallSkill
    {
        get { return overallSkill; }
        set { overallSkill = value; }
    }

    public int AgentID
    {
        get { return agentID; }
        set { agentID = value; }
    }

    public int InjuryTime
    {
        get { return injuryTime; }
        set { injuryTime = value; }
    }

    public bool IsFavorite
    {
        get { return isFavorite; }
        set { isFavorite = value; }
    }
}
