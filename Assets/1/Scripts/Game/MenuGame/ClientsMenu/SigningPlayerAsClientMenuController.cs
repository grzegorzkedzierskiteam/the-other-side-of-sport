﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class SigningPlayerAsClientMenuController : Menu {

    public GameObject menuToSetActive;
    public Button btnCancel;

	// Use this for initialization
	void Start () {
        btnCancel.onClick.AddListener(BtnCancel);
	}
	
	private void BtnCancel()
    {
        menuToSetActive.GetComponent<ISigningContracts>().SetInteractableButtons(true);
        menuToSetActive.GetComponent<ISigningContracts>().GetClientDetails();
        menuToSetActive.GetComponent<ISigningContracts>().SetUIDataElements();

        MenuController.DestroyPopMenu(gameObject);
    }

    public class Factory : PlaceholderFactory<SigningPlayerAsClientMenuController> { }
}
