﻿using System.Collections.Generic;
using Debug = UnityEngine.Debug;

public static class ClientData
{
    public static ClientObject GetAllDetails(IDBConnection dbConnection, int clientID)
    {
        var query = "SELECT * FROM players WHERE ID = " + clientID;
        var reader = dbConnection.DownloadQuery(query);

        if (reader.HasRows)
        {
            reader.Read();

            var clientObject = new ClientObject(
                    reader.GetInt32(0),
                    reader.GetString(1),
                    reader.GetString(2),
                    reader.GetString(3),
                    reader.GetString(4),
                    reader.GetString(5),
                    reader.GetString(6),
                    reader.GetInt32(7),
                    reader.GetInt32(8),
                    reader.GetInt32(9),
                    reader.GetInt32(10),
                    CharacterManager.GetCharacterFromNameOfCharacter(reader.GetString(11)),
                    reader.GetInt32(12),
                    reader.GetInt32(13),
                    reader.GetInt32(25),
                    reader.GetInt32(14),
                    reader.GetInt32(27),
                    reader.GetInt32(26),
                    reader.GetInt32(15),
                    reader.GetInt32(16),
                    reader.GetInt32(17),
                    reader.GetInt32(19),
                    reader.GetString(20),
                    reader.GetInt32(21),
                    reader.GetInt32(18),
                    reader.GetInt32(24),
                    reader.GetInt32(22),
                    reader.GetInt32(23)
                );

            reader.Close();
            reader = null;
            return clientObject;
        }
        else
        {
            reader.Close();
            reader = null;
            return null;
        }
    }

    public static List<ClientObject> GetSelectedPlayersDetails(IDBConnection dbConnection, List<int> clientsIDs)
    {
        var listOfSelectedAthletes = new List<ClientObject>();

        var query = "SELECT * FROM players WHERE ID IN(";

        foreach (var id in clientsIDs)
            query += id + ",";

        query = query.Remove(query.Length - 1);
        query += ");";

        var reader = dbConnection.DownloadQuery(query);
        if (reader.HasRows)
        {
            while (reader.Read())
            {
                var clientObject = new ClientObject(
                    reader.GetInt32(0),
                    reader.GetString(1),
                    reader.GetString(2),
                    reader.GetString(3),
                    reader.GetString(4),
                    reader.GetString(5),
                    reader.GetString(6),
                    reader.GetInt32(7),
                    reader.GetInt32(8),
                    reader.GetInt32(9),
                    reader.GetInt32(10),
                    CharacterManager.GetCharacterFromNameOfCharacter(reader.GetString(11)),
                    reader.GetInt32(12),
                    reader.GetInt32(13),
                    reader.GetInt32(25),
                    reader.GetInt32(14),
                    reader.GetInt32(27),
                    reader.GetInt32(26),
                    reader.GetInt32(15),
                    reader.GetInt32(16),
                    reader.GetInt32(17),
                    reader.GetInt32(19),
                    reader.GetString(20),
                    reader.GetInt32(21),
                    reader.GetInt32(18),
                    reader.GetInt32(24),
                    reader.GetInt32(22),
                    reader.GetInt32(23)
                );

                listOfSelectedAthletes.Add(clientObject);
            }

            reader.Close();
            reader = null;
            return listOfSelectedAthletes;
        }
        else
        {
            reader.Close();
            reader = null;
            return null;
        }
    }

    public static Contract GetClientContract(IDBConnection dbConnection, int clientID)
    {
        var query = "SELECT Contract, ContractLenght, ContractGuaranteedMoney, GotMoney, YearOfSign FROM players WHERE ID = " + clientID;
        var reader = dbConnection.DownloadQuery(query);
        if (reader.HasRows)
        {
            reader.Read();

            var contract = new Contract(reader.GetInt32(0), reader.GetInt32(1), reader.GetInt32(4), reader.GetInt32(2), reader.GetInt32(3));

            reader.Close();
            reader = null;
            return contract;
        }
        else
        {
            reader.Close();
            reader = null;
            return null;
        }
    }

    public static List<ClientObject> GetAllAthletesDetails(IDBConnection dbConnection)
    {
        var listOfAllAthletes = new List<ClientObject>();

        var query = "SELECT * FROM players";
        var reader = dbConnection.DownloadQuery(query);
        if (reader.HasRows)
        {
            while (reader.Read())
            {
                var clientObject = new ClientObject(
                    reader.GetInt32(0),
                    reader.GetString(1),
                    reader.GetString(2),
                    reader.GetString(3),
                    reader.GetString(4),
                    reader.GetString(5),
                    reader.GetString(6),
                    reader.GetInt32(7),
                    reader.GetInt32(8),
                    reader.GetInt32(9),
                    reader.GetInt32(10),
                    CharacterManager.GetCharacterFromNameOfCharacter(reader.GetString(11)),
                    reader.GetInt32(12),
                    reader.GetInt32(13),
                    reader.GetInt32(25),
                    reader.GetInt32(14),
                    reader.GetInt32(27),
                    reader.GetInt32(26),
                    reader.GetInt32(15),
                    reader.GetInt32(16),
                    reader.GetInt32(17),
                    reader.GetInt32(19),
                    reader.GetString(20),
                    reader.GetInt32(21),
                    reader.GetInt32(18),
                    reader.GetInt32(24),
                    reader.GetInt32(22),
                    reader.GetInt32(23)
                );

                listOfAllAthletes.Add(clientObject);
            }

            reader.Close();
            reader = null;
            return listOfAllAthletes;
        }
        else
        {
            reader.Close();
            reader = null;
            return null;
        }
    }

    public static void DeletePlayer(IDBConnection dbConnection, int clientID)
    {
        var query = "DELETE FROM players WHERE ID = " + clientID + ";";

        var client = GetAllDetails(dbConnection, clientID);

        if (client.HasAgent)
        {
            var agent = AgentData.GetAllDetails(dbConnection, client.AgentID);

            var groupString = GroupStringManager.DeletePersonFromGroup(agent.Clients, client.ID.ToString());

            AgentData.UpdateStringGroupDB(dbConnection, agent.ID, groupString, AgentGroupEnum.Clients);
        }

        dbConnection.ModifyQuery(query);

        DeleteAllMessagesOfPerson(dbConnection, PersonType.Athlete, clientID);
    }

    public static void DeleteAllMessagesOfPerson(IDBConnection dbConnection, PersonType personType,int personID)
    {
        var query = $"SELECT ID FROM messages WHERE ParticipantID = {personID} AND ParticipantType = '{personType.ToString()}';";
        var reader = dbConnection.DownloadQuery(query);

        if (!reader.HasRows) return;

        var messagesIDs = new List<int>();
        while (reader.Read())
            messagesIDs.Add(reader.GetInt32(0));

        reader.Close();

        query = "DELETE FROM messages_responses WHERE Message_ID IN (";
        foreach (var id in messagesIDs)
            query += $"{id},";

        query = query.Remove(query.Length - 1);
        query += $"); DELETE FROM messages WHERE ParticipantID = {personID};";

        dbConnection.ModifyQuery(query);
    }
}
