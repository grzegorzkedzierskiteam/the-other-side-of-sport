﻿using Mono.Data.Sqlite;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class ClientDetailsMenuController : Menu,ISigningContracts {

    private IDBConnection dbConnection;
    private ClientObject clientObject;

    public int clientID;
    private string agentName;
    public Button btnBack;

    #region UI Elements
    public Text txtName;
    public Text txtInjuryTime;
    public Text txtInjuryName;
    public Text txtAge;
    public Text txtSport;
    public Text txtPosition;
    public Text txtCollege;
    public Text txtTeam;
    public Text txtContract;
    public Text txtCharacter;
    public Text txtPopularity;
    public Text txtOverallSkill;
    public Text txtProspectiveness;
    public Text txtRelation;
    public Text txtAgent;
    public Text txtWealth;
    public GameObject wealthPanel;
    public Button btnAgentDetails;
    public Button btnTeamDetails;
    public Button btnFavorite;
    public Button btnSignClient;
    public Button btnContractWithYouInfo;
    public Button btnContractWithTeam;
    public Button btnWealthDetails;
    public Image imgStar;
    #endregion

    public Color favoriteColor;
    public Color noFavoriteColor;

    [Inject]
    public void Constructor(IDBConnection dbConnection)
    {
        this.dbConnection = dbConnection;
    }

    // Use this for initialization
    void Start () {
        GetClientDetails();

        btnAgentDetails.onClick.AddListener(BtnAgentDetails);
        SetInteractableButtons(true);
        SetShowingWealth();
        SelectButtonToShow();

        SetUIDataElements();
        btnBack.onClick.AddListener(BtnBack);
        btnTeamDetails.onClick.AddListener(BtnTeamDetails);
        btnFavorite.onClick.AddListener(BtnFavorite);
        btnSignClient.onClick.AddListener(BtnSignClient);
        btnContractWithYouInfo.onClick.AddListener(BtnContractWithYouInfo);
        btnContractWithTeam.onClick.AddListener(BtnContractWithTeamInfo);
        btnWealthDetails.onClick.AddListener(BtnWealthDetails);
	}

    private void BtnBack()
    {
        MenuController.DestroyMenu(gameObject);
    }

    public void SetInteractableButtons(bool active)
    {
        btnBack.interactable = active;
        btnAgentDetails.interactable = active;
        btnTeamDetails.interactable = active;
        btnFavorite.interactable = active;
        btnSignClient.interactable = active;
        btnContractWithTeam.interactable = active;
        btnContractWithYouInfo.interactable = active;
        btnWealthDetails.interactable = active;
    }

    private void SetShowingBtnAgent()
    {
        if (agentName == "None" || agentName == "You")
            btnAgentDetails.interactable = false;
        else
            btnAgentDetails.interactable = true;
    }

    private void SetShowingBtnContract()
    {
        if (clientObject.Contract.Value == 0 && clientObject.Contract.Length == 0)
            btnContractWithTeam.interactable = false;
        else
            btnContractWithTeam.interactable = true;
    }

    private void SetShowingWealth()
    {
        if (clientObject.AgentID == 1)
        {
            btnWealthDetails.interactable = true;
        }
        else if (clientObject.Wealth == 0)
        {
            btnWealthDetails.interactable = false;
        }    
        else
        {
            btnWealthDetails.interactable = false;
        }  
    }

    private void SelectButtonToShow()
    {
        if(agentName == "You")
        {
            btnSignClient.gameObject.SetActive(false);
            btnContractWithYouInfo.gameObject.SetActive(true);
        }
        else
        {
            btnSignClient.gameObject.SetActive(true);
            btnContractWithYouInfo.gameObject.SetActive(false);
        }
    }

    private void BtnSignClient()
    {
        var signingPlayerAsClientMenu = MenuController.InitializePopMenu(MenuEnum.SigningPlayerMenu);
        signingPlayerAsClientMenu.GetComponent<SigningPlayerAsClientMenuController>().menuToSetActive = gameObject;
        signingPlayerAsClientMenu.GetComponent<SigningPlayerAsClientController>().menuToSetActive = gameObject;
        signingPlayerAsClientMenu.GetComponent<SigningPlayerAsClientController>().txtOurRelation.text = clientObject.Relation.ToString() + "%";
        signingPlayerAsClientMenu.GetComponent<SigningPlayerAsClientController>().clientObject = clientObject;
        SetInteractableButtons(false);
    }

    private void BtnWealthDetails()
    {
        var menuDetails = MenuController.InitializePopMenu(MenuEnum.ClientWealthDetail);
        SetInteractableButtons(false);
        menuDetails.GetComponent<WealthDetailsMenuController>().menuToSetActive = gameObject;
        menuDetails.GetComponent<WealthDetailsMenuController>().Dept = clientObject.Dept;
        menuDetails.GetComponent<WealthDetailsMenuController>().Wealth = clientObject.Wealth;
    }

    private void BtnContractWithYouInfo()
    {
        var contractWithYouInfoMenu = MenuController.InitializePopMenu(MenuEnum.ClientContractWithYouDetail);
        contractWithYouInfoMenu.GetComponent<ContractWithYouMenuController>().menuToSetActive = gameObject;
        contractWithYouInfoMenu.GetComponent<ContractWithYouMenuController>().clientID = clientObject.ID;
        contractWithYouInfoMenu.GetComponent<ContractWithYouMenuController>().SetUIDataElements();
        SetInteractableButtons(false);
    }

    private void BtnContractWithTeamInfo()
    {
        var contractWithTeamMenu = MenuController.InitializePopMenu(MenuEnum.ClientContractWithTeamDetail);
        contractWithTeamMenu.GetComponent<ContractWithTeamMenuController>().menuToSetActive = gameObject;
        contractWithTeamMenu.GetComponent<ContractWithTeamMenuController>().clientID = clientObject.ID;
        SetInteractableButtons(false);
    }

    private void BtnTeamDetails()
    {
        var menuDetails = MenuController.InitializeMenu(MenuEnum.TeamDetailsMenu);
        menuDetails.GetComponent<TeamDetailsMenuController>().teamName = clientObject.Team;
        menuDetails.GetComponent<TeamDetailsMenuController>().sport = clientObject.Sport;
    }

    private void BtnAgentDetails()
    {
        var menuDetails = MenuController.InitializeMenu(MenuEnum.AgentDetailsMenu);
        menuDetails.GetComponent<AgentDetailsMenuController>().agentID = clientObject.AgentID;
    }

    private void BtnFavorite()
    {
        dbConnection.OpenConnection();;
        if (clientObject.IsFavorite)
        {
            imgStar.color = noFavoriteColor;
            clientObject.IsFavorite = false;
            clientObject.DeselectAsFavorite(dbConnection);
        }
        else
        {
            imgStar.color = favoriteColor;
            clientObject.IsFavorite = true;
            clientObject.SelectAsFavorite(dbConnection);
        }
        dbConnection.CloseConnection();

    }

    public void SetUIDataElements()
    {
        txtName.text = clientObject.FirstName + " " + clientObject.LastName;
        txtAge.text = clientObject.Age.ToString();
        txtSport.text = clientObject.Sport;
        txtPosition.text = clientObject.Position;
        txtCollege.text = clientObject.College;
        txtTeam.text = clientObject.Team;
        txtContract.text = Money.GetMoneyString(clientObject.Contract.Value);
        txtCharacter.text = clientObject.Character.NameOfCharacter;
        txtPopularity.text = clientObject.Popularity.ToString();
        txtOverallSkill.text = clientObject.OverallSkill.ToString();
        txtProspectiveness.text = clientObject.Prospectiveness.ToString();
        txtRelation.text = clientObject.Relation.ToString() + " %";
        txtAgent.text = agentName;
        txtWealth.text = Money.GetMoneyString(clientObject.Wealth);

        if (clientObject.Team == "Retirement" || clientObject.Team == "None")
            btnTeamDetails.interactable = false;

        if (clientObject.Wealth == 0)
            txtWealth.text = "Is bankrupt";
        else if (clientObject.AgentID != 1)
            txtWealth.text = "Unknown";


        //favorite
        if (clientObject.IsFavorite)
            imgStar.color = favoriteColor;

        //injury

        if(clientObject.InjuryTime > 0)
        {
            if(clientObject.InjuryTime == 1)
                txtInjuryTime.text = clientObject.InjuryTime.ToString() + " week";
            else
                txtInjuryTime.text = clientObject.InjuryTime.ToString() + " weeks";

            txtInjuryName.text = clientObject.InjuryName;
        }
        else
        {
            txtInjuryTime.text = "";
            txtInjuryName.text = "";
        }

        SelectButtonToShow();
        SetShowingBtnAgent();
        SetShowingBtnContract();
    }

    public void GetClientDetails()
    {
        dbConnection.OpenConnection();;

        clientObject = ClientData.GetAllDetails(dbConnection, clientID);

        if (clientObject.AgentID == -1)
            agentName = "None";
        else if (clientObject.AgentID == 1)
            agentName = "You";
        else
        {
            string query = "SELECT FirstName,LastName FROM agents WHERE ID = " + clientObject.AgentID + ";";
            SqliteDataReader reader = dbConnection.DownloadQuery(query);
            reader.Read();
            agentName = reader.GetString(0) + " " + reader.GetString(1);

            reader.Close();
            reader = null;
        }

        dbConnection.CloseConnection();

    }

    public class Factory : PlaceholderFactory<ClientDetailsMenuController> { }
}
