﻿using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class ContractWithYouMenuController : Menu,ISigningContracts {

    public GameObject menuToSetActive;
    public GameObject signingPlayerAsClientPrefab;
    public int clientID;
    public Text txtClientName;
    public Text txtContractDateEnd;
    public Text txtWage;
    public Button btnResign;
    public Button btnCancel;

    private ClientObject clientObject;
    private IDBConnection dbConnection;

    [Inject]
    public void Construct(IDBConnection dbConnection)
    {
        this.dbConnection = dbConnection;
    }

    // Use this for initialization
    void Start()
    {
        btnCancel.onClick.AddListener(BtnCancel);
        btnResign.onClick.AddListener(BtnResign);
    }

    private void BtnCancel()
    {
        menuToSetActive.GetComponent<ISigningContracts>().SetInteractableButtons(true);
        menuToSetActive.GetComponent<ISigningContracts>().GetClientDetails();
        menuToSetActive.GetComponent<ISigningContracts>().SetUIDataElements();

        Destroy(gameObject);
    }

    private void BtnResign()
    {
        var signingPlayerAsClientMenu = MenuController.InitializePopMenu(MenuEnum.SigningPlayerMenu);
        signingPlayerAsClientMenu.GetComponent<SigningPlayerAsClientMenuController>().menuToSetActive = gameObject;
        signingPlayerAsClientMenu.GetComponent<SigningPlayerAsClientController>().menuToSetActive = gameObject;
        signingPlayerAsClientMenu.GetComponent<SigningPlayerAsClientController>().txtOurRelation.text = clientObject.Relation.ToString() + "%";
        signingPlayerAsClientMenu.GetComponent<SigningPlayerAsClientController>().clientObject = clientObject;
    }

    public void SetUIDataElements()
    {
        GetClientDetails();
        txtClientName.text = clientObject.FirstName + " " + clientObject.LastName;

        txtContractDateEnd.text = clientObject.ContractWithAgent.DateEnd.FullDateString;

        txtWage.text = clientObject.ContractWithAgent.Wage + "%";
    }

    public void GetClientDetails()
    {
        dbConnection.OpenConnection();;
        clientObject = ClientData.GetAllDetails(dbConnection, clientID);
        dbConnection.CloseConnection();
    }

    public void SetInteractableButtons(bool value)
    {
        btnCancel.interactable = value;
        btnResign.interactable = value;
    }

    public class Factory : PlaceholderFactory<ContractWithYouMenuController> { }
}
