﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class WarningController : MonoBehaviour {

    public Text txtWarningText;
    public Button btnClosePanel;

	// Use this for initialization
	void Start () {
        btnClosePanel.onClick.AddListener(BtnClosePanel);
    }

    private void BtnClosePanel()
    {
        Destroy(gameObject);
    }

    public class Factory : PlaceholderFactory<WarningController> { }
}
