﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface INegotiating {
    int GetAndSetNegotiatingEffects();
    bool MakeOfferToClient();
}
