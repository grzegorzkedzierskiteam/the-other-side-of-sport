﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerNegotiatingWithClient : INegotiating
{
    private IBidder bidder;
    private IClientObject client;
    private IOffer offer;
    private IBidder currentAgent;
    private int decision;

    public PlayerNegotiatingWithClient(IBidder _bidder, IClientObject _client, IOffer _offer, IBidder _currentAgent = null)
    {
        bidder = _bidder;
        client = _client;
        offer = _offer;
        currentAgent = _currentAgent;
    }

    public bool MakeOfferToClient()
    {
        GetAndSetNegotiatingEffects();
        decision += GetClientDecision();

        if (decision > 16)
            return true;
        else
            return false;
    }

    public int GetAndSetNegotiatingEffects()
    {
        decision = 0;
        
        if(client.HasAgent && client.AgentID != 1)
        {
            decision += NegotiatingWithClientEffects.GetHavingOtherAgentEffect();
            int difference = NegotiatingWithClientEffects.GetDifferenceBetweenNewAgentAndCurrentAgentStats(bidder, currentAgent);
            decision += NegotiatingWithClientEffects.GetDifferenceBetweenAgentsEffect(difference);
        }
        
        decision += NegotiatingWithClientEffects.GetRatingOfWage(offer.ContractValue);
        decision += NegotiatingWithClientEffects.GetRatingOfContractMinLength(offer.ContractLength);
        decision += NegotiatingWithClientEffects.GetRespectabilityEffect(bidder.Respectability);
        decision += NegotiatingWithClientEffects.GetPopularityEffectByBidder(bidder.Popularity);
        decision += NegotiatingWithClientEffects.GetPopularityEffectByClient(client.Popularity);
        decision += NegotiatingWithClientEffects.GetOverallSkillEffect(client.OverallSkill);
        decision += NegotiatingWithClientEffects.GetRelationEffect(client.Relation);
        decision += NegotiatingWithClientEffects.GetRetirementOrFreeAgentEffect(client.Team);

        return decision;
    }

    private int GetClientDecision()
    {
        return UnityEngine.Random.Range(-5, 6);
    }
}
