﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IBidder{
    int Respectability { get; set; }
    int Popularity { get; set; }
}
