﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Offer : IOffer
{
    public bool IsAccepted { get; set; }
    public int ContractValue { get; set; }
    public int ContractLength { get; set; }

    public Offer(int contractLegnth, int contractValue)
    {
        IsAccepted = false;
        ContractLength = contractLegnth;
        ContractValue = contractValue;
    }
}
