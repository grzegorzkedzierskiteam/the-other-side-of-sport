﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Negotiator : INegotiator
{
    public int OverallSkill { get; set; }
    public int Popularity { get; set; }
    public int Relation { get; set; }
    public ICharacter Character { get; set; }

    public Negotiator(int popularity,int relation, ICharacter character)
    {
        Popularity = popularity;
        Relation = relation;
        Character = character;
    }
}
