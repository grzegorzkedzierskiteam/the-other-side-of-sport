﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class NegotiatingWithClientEffects {

    public static int GetCharacterEffect(ICharacter bidderCharacter, ICharacter clientCharacter)
    {
        if (bidderCharacter.NameOfCharacter == clientCharacter.NameOfCharacter)
            return 4;
        else
            return 0;
    }

    public static int GetRatingOfWage(int wage)
    {
        if (wage > 15)
            return 0;
        else if (wage > 10)
            return 1;
        else if (wage > 8)
            return 2;
        else if (wage > 5)
            return 3;
        else if (wage > 4)
            return 4;
        else if (wage > 3)
            return 6;
        else if (wage > 2)
            return 8;
        else
            return 9;
    }

    public static int GetHavingOtherAgentEffect()
    {
        return -5;
    }

    public static int GetDifferenceBetweenAgentsEffect(int difference)
    {
        if (difference > 60)
            return 6;
        else if (difference > 50)
            return 5;
        else if (difference > 40)
            return 4;
        else if (difference > 30)
            return 3;
        else if (difference > 20)
            return 2;
        else if (difference > 10)
            return 1;
        else if (difference >= -10)
            return 0;
        else if (difference >= -20)
            return -1;
        else if (difference >= -30)
            return -2;
        else if (difference >= -40)
            return -3;
        else if (difference >= -50)
            return -4;
        else if (difference >= -60)
            return -5;
        else
            return -6;
    }

    public static int GetDifferenceBetweenNewAgentAndCurrentAgentStats(IBidder bidder, IBidder exBidder)
    {
        int bidderStats = bidder.Popularity + bidder.Respectability;
        int exBidderStats = exBidder.Popularity + exBidder.Respectability;
        return bidderStats - exBidderStats;
    }

    public static int GetRatingOfContractMinLength(int contractLength)
    {
        if (contractLength > 8)
            return -8;
        else if (contractLength > 6)
            return -5;
        else if (contractLength > 4)
            return 0;
        else if (contractLength > 2)
            return 1;
        else if (contractLength > 1)
            return 2;
        else
            return 3;
    }

    public static int GetRelationEffect(int relation)
    {
        if (relation > 95)
            return 12;
        else if (relation > 90)
            return 11;
        else if (relation > 85)
            return 10;
        else if (relation > 80)
            return 9;
        else if (relation > 75)
            return 8;
        else if (relation > 70)
            return 7;
        else if (relation > 65)
            return 6;
        else if (relation > 60)
            return 5;
        else if (relation > 55)
            return 4;
        else if (relation > 50)
            return 3;
        else if (relation == 50)
            return 0;
        else if (relation > 45)
            return -3;
        else if (relation > 40)
            return -4;
        else if (relation > 35)
            return -5;
        else if (relation > 30)
            return -6;
        else if (relation > 25)
            return -7;
        else if (relation > 20)
            return -8;
        else if (relation > 15)
            return -9;
        else if (relation > 10)
            return -10;
        else if (relation > 5)
            return -11;
        else
            return -12;
    }

    public static int GetRetirementOrFreeAgentEffect(string team)
    {
        if (team == "Retirement")
            return 5;
        else if (team == "None")
            return 2;
        else
            return 0;
    }

    public static int GetRespectabilityEffect(int respectability)
    {
        if (respectability > 95)
            return 10;
        else if (respectability > 90)
            return 9;
        else if (respectability > 85)
            return 8;
        else if (respectability > 80)
            return 7;
        else if (respectability > 75)
            return 6;
        else if (respectability > 70)
            return 5;
        else if (respectability > 60)
            return 4;
        else if (respectability > 50)
            return 3;
        else if (respectability > 40)
            return 2;
        else if (respectability > 30)
            return 1;
        else
            return 0;
    }

    public static int GetPopularityEffectByBidder(int popularity)
    {
        if (popularity > 95)
            return 5;
        else if (popularity > 85)
            return 4;
        else if (popularity > 75)
            return 3;
        else if (popularity > 60)
            return 2;
        else if (popularity > 40)
            return 1;
        else 
            return 0;
    }

    public static int GetPopularityEffectByClient(int popularity)
    {
        if (popularity > 95)
            return -5;
        else if (popularity > 85)
            return -4;
        else if (popularity > 75)
            return -3;
        else if (popularity > 60)
            return -2;
        else if (popularity > 40)
            return -1;
        else if (popularity > 30)
            return 0;
        else if (popularity > 20)
            return 1;
        else if (popularity > 10)
            return 2;
        else
            return 3;
    }

    public static int GetOverallSkillEffect(int overallSkill)
    {
        if (overallSkill > 90)
            return 0;
        else if (overallSkill > 80)
            return 1;
        else if (overallSkill > 70)
            return 2;
        else if (overallSkill > 60)
            return 3;
        else if (overallSkill > 50)
            return 4;
        else if (overallSkill > 40)
            return 5;
        else if (overallSkill > 30)
            return 6;
        else if (overallSkill > 20)
            return 7;
        else if (overallSkill > 10)
            return 8;
        else
            return 9;
    }
}
