﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bidder : IBidder
{
    public int Respectability { get; set; }
    public int Popularity { get; set; }
    public int Relation { get; set; }
    public ICharacter Character { get; set; }

    public Bidder(int respectability, int popularity)
    {
        Respectability = respectability;
        Popularity = popularity;
    }
}
