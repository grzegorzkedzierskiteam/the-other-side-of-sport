﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface INegotiator {
    int Popularity { get; set; }
    int Relation { get; set; }
    ICharacter Character { get; set; }
}
