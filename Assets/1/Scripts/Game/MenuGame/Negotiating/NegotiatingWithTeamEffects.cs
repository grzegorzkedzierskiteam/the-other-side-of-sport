﻿using Mono.Data.Sqlite;
using UnityEngine;

public static class NegotiatingWithTeamEffects {

	public static int GetPositionInDepthChartEffect(IDBConnection dbConnection, string teamName, IClientObject client)
    {
        int effect = 0;

        string query = "SELECT COUNT(*) FROM players WHERE Team = '" + teamName + "' AND Position ='" + client.Position + "' AND (OverallSkill + Popularity + Prospectiveness) >= (SELECT OverallSkill+Popularity+Prospectiveness FROM players WHERE ID = " + client.ID + ") OR ID = " + client.ID;
        SqliteDataReader reader = dbConnection.DownloadQuery(query);
        if (reader.HasRows)
        {
            reader.Read();
            int position = reader.GetInt32(0);

            if (position == 1)
                effect = 30;
            else if (position == 2)
                effect = 5;
            else if (position == 3)
                effect = -30;
            else if (position >= 4)
                effect = -45;
        }

        reader.Close();
        reader = null;

        return effect;
    }

    public static int GetPlayerAgeEffect(int age)
    {
        int effect = 0;

        if (age > 45)
            effect = -80;
        else if (age > 40)
            effect = -60;
        else if (age > 35)
            effect = -35;
        else if (age > 30)
            effect = -19;
        else if (age < 25)
            effect = 6;

        return effect;
    }

    public static int GetPlayerInjuryEffect(int injuryTime)
    {
        int effect = 0;

        if (injuryTime > 16)
            effect = -35;
        else if (injuryTime > 10)
            effect = -15;
        else if (injuryTime > 0)
            effect = -UnityEngine.Random.Range(5, 11);

        return effect;
    }

    public static int GetAgentSkillEffect(int respectability, int popularity)
    {
        int agentSkill = Mathf.RoundToInt(((respectability + popularity) / 2) / 10);
        int effect = 0;

        if (agentSkill > 8)
            effect = 5;
        else if (agentSkill > 5)
            effect = 1;
        else if (agentSkill > 2)
            effect = 0;
        else
            effect = -5;

        return effect;
    }

    public static int GetTeamMoodEffect()
    {
        return Random.Range(-18, 19);
    }

    public static int GetLuckyEffect()
    {
        return Random.Range(-10, 11);
    }
}
