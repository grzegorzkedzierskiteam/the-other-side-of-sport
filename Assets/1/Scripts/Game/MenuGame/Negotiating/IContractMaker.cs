﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IContractMaker {
    int GetGuaranteedMoney(int contractValue, int percent);
    int GetGuaranteedMoneyFromRandomPercent(int contractValue);
    Contract GetNewGreaterContract(IContract oldContract, int yearOfSign);
    Contract GetNewLowerContract(IContract oldContract, int yearOfSign);
    Contract GetRandomFootballContract(int currentYear, string position, int overallSkill, int prospectiveness);
    Contract GetRandomFootballContractFromCurrentYear(int currentYear, string position, int overallSkill, int prospectiveness);
}
