﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IOffer { 
    bool IsAccepted { get; set; }
    int ContractValue { get; set; }
    int ContractLength { get; set; }
}
