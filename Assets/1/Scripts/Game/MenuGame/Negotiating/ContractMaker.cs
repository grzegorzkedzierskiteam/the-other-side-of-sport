﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContractMaker : IContractMaker {

    public int GetGuaranteedMoney(int contractValue, int percent)
    {
        return (int)(percent / 100.00d * contractValue);
    }

    public int GetGuaranteedMoneyFromRandomPercent(int contractValue)
    {
        return GetGuaranteedMoney(contractValue, GetRandomPercent());
    }

    public Contract GetNewGreaterContract(IContract oldContract, int yearOfSign)
    {
        var oldContractValuePerYear = oldContract.Value / (yearOfSign + oldContract.Length - oldContract.YearOfSign);
        var newContractValuePerYear = 0;

        if (oldContractValuePerYear >= 40000000)
        {
            var random = Convert.ToDouble(Math.Round(UnityEngine.Random.Range(1f, 1.005f), 2));
            newContractValuePerYear = Convert.ToInt32(oldContractValuePerYear * random);
            newContractValuePerYear = 10000000;
        }
        else if (oldContractValuePerYear >= 30000000)
        {
            var random = Convert.ToDouble(Math.Round(UnityEngine.Random.Range(1f, 1.05f),2));
            newContractValuePerYear = Convert.ToInt32(oldContractValuePerYear * random);
            newContractValuePerYear = 10000000;
        }
        else if(oldContractValuePerYear >= 20000000)
        {
            var random = Convert.ToDouble(Math.Round(UnityEngine.Random.Range(1f, 1.5f), 2));
            newContractValuePerYear = Convert.ToInt32(oldContractValuePerYear * random);
            newContractValuePerYear = 10000000;
        }
        else if (oldContractValuePerYear >= 10000000)
        {
            var random = Convert.ToDouble(Math.Round(UnityEngine.Random.Range(1f, 1.8f), 2));
            newContractValuePerYear = Convert.ToInt32(oldContractValuePerYear * random);
            newContractValuePerYear = 10000000;
        }
        else if (oldContractValuePerYear >= 5000000)
        {
            var random = Convert.ToDouble(Math.Round(UnityEngine.Random.Range(1f, 2f),2));
            newContractValuePerYear = Convert.ToInt32(oldContractValuePerYear * random);
            newContractValuePerYear = 10000000;
        }
        else if (oldContractValuePerYear >= 1000000)
        {
            var random = Convert.ToDouble(Math.Round(UnityEngine.Random.Range(1f, 3f), 2));
            newContractValuePerYear = Convert.ToInt32(oldContractValuePerYear * random);
            newContractValuePerYear = 10000000;
        }
        else
        {
            var random = Convert.ToDouble(Math.Round(UnityEngine.Random.Range(1f, 4f), 2));
            newContractValuePerYear = Convert.ToInt32(oldContractValuePerYear * random);
            newContractValuePerYear = 10000000;
        }

        var newContractLength = UnityEngine.Random.Range(1, 5);
        var newContractValue = newContractValuePerYear * newContractLength;

        var newContract = new Contract(newContractValue, newContractLength, yearOfSign, GetGuaranteedMoneyFromRandomPercent(newContractValue), 0);

        return newContract;
    }

    public Contract GetNewLowerContract(IContract oldContract, int yearOfSign)
    {
        var oldContractValuePerYear = oldContract.Value / (yearOfSign + oldContract.Length - oldContract.YearOfSign);
        var newContractValuePerYear = 0;

        if (oldContractValuePerYear > 30000000)
        {
            var random = Convert.ToDouble(Math.Round(UnityEngine.Random.Range(0.7f, 0.9f), 2));
            newContractValuePerYear = Convert.ToInt32(oldContractValuePerYear * random);
        }       
        else if (oldContractValuePerYear > 20000000)
        {
            var random = Convert.ToDouble(Math.Round(UnityEngine.Random.Range(0.6f, 0.9f), 2));
            newContractValuePerYear = Convert.ToInt32(oldContractValuePerYear * random);
        } 
        else
        {
            var random = Convert.ToDouble(Math.Round(UnityEngine.Random.Range(0.5f, 0.9f), 2));
            newContractValuePerYear = Convert.ToInt32(oldContractValuePerYear * random);
        }

        var newContractLength = UnityEngine.Random.Range(1, 5);
        var newContractValue = newContractValuePerYear * newContractLength;

        var newContract = new Contract(newContractValue, newContractLength, yearOfSign, GetGuaranteedMoneyFromRandomPercent(newContractValue), 0);

        return newContract;
    }

    public Contract GetRandomFootballContract(int currentYear, string position, int overallSkill, int prospectiveness)
    {
        var length = UnityEngine.Random.Range(1, 6);
        double contractValueTemp = UnityEngine.Random.Range(40000, 2000000) * Sport.GetFootballPositionPrestige(position) * length;
        contractValueTemp *= Sport.GetOverallSkillAndProspectivenessEffect(overallSkill, prospectiveness);
        var contractValue = Convert.ToInt32(contractValueTemp);
        var guaranteedMoney = GetGuaranteedMoneyFromRandomPercent(contractValue);
        var yearOfSign = GetYearOfSign(currentYear, length);
        var gotMoney = GetGotMoney(contractValue, currentYear, yearOfSign, length);

        return new Contract(contractValue, length, yearOfSign, guaranteedMoney, gotMoney);
    }

    public Contract GetRandomFootballContractFromCurrentYear(int currentYear, string position, int overallSkill, int prospectiveness)
    {
        var length = UnityEngine.Random.Range(1, 6);
        double contractValueTemp = UnityEngine.Random.Range(40000, 2000000) * Sport.GetFootballPositionPrestige(position) * length;
        contractValueTemp *= Sport.GetOverallSkillAndProspectivenessEffect(overallSkill, prospectiveness);
        var contractValue = Convert.ToInt32(contractValueTemp);
        var guaranteedMoney = GetGuaranteedMoneyFromRandomPercent(contractValue);

        return new Contract(contractValue, length, currentYear, guaranteedMoney, 0);
    }

    private int GetRandomPercent()
    {
        var percent = UnityEngine.Random.Range(0, 100);
        
        if (percent > 90)
            return 100;
        else if (percent < 30)
            return 0;

        return percent;
    }

    public int GetYearOfSign(int currentYear, int length)
    {
        if (length > 4)
            return currentYear;
        else if (length > 3)
        {
            var random = UnityEngine.Random.Range(0, 3);
            return currentYear - random;
        }
        else if (length > 2)
        {
            var random = UnityEngine.Random.Range(0, 4);
            return currentYear - random;
        }
        else if (length > 1)
        {
            var random = UnityEngine.Random.Range(0, 5);
            return currentYear - random;
        }
        else
        {
            var random = UnityEngine.Random.Range(0, 6);
            return currentYear - random;
        }
    }

    public int GetGotMoney(int contractValue, int currentYear, int yearOfSign, int length)
    {
        var numberOfYearsWithThisContract = currentYear - yearOfSign;
        var realContractLength = numberOfYearsWithThisContract + length;

        var yearlyWage = contractValue / realContractLength;
        var gotMoney = yearlyWage * numberOfYearsWithThisContract;

        return gotMoney;
    }
}
