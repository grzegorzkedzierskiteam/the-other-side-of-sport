﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgentsItemList {

    private int iD;
    private string firstName;
    private string lastName;
    private int wealth;
    private int popularity;
    private int respectability;
    private int relation;
    private bool isFavorite;

    public AgentsItemList(int _iD, string _name, string _surname, int _wealth, int _popularity, int _respectability, int _relation, int _isFavorite)
    {
        firstName = _name;
        lastName = _surname;
        wealth = _wealth;
        popularity = _popularity;
        respectability = _respectability;
        iD = _iD;
        relation = _relation;

        if (_isFavorite == 0)
            isFavorite = false;
        else
            isFavorite = true;
    }

    public string FirstName
    {
        get { return firstName; }
        set { firstName = value; }
    }

    public string LastName
    {
        get { return lastName; }
        set { lastName = value; }
    }

    public int Wealth
    {
        get { return wealth; }
        set { wealth = value; }
    }

    public int Popularity
    {
        get { return popularity; }
        set { popularity = value; }
    }

    public int Respectability
    {
        get { return respectability; }
        set { respectability = value; }
    }

    public int ID
    {
        get { return iD; }
        set { iD = value; }
    }

    public int Relation
    {
        get { return relation; }
        set { relation = value; }
    }

    public bool IsFavorite
    {
        get { return isFavorite; }
        set { isFavorite = value; }
    }
}
