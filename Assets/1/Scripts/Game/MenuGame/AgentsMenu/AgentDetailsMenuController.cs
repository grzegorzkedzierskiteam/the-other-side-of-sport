﻿using Mono.Data.Sqlite;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class AgentDetailsMenuController : Menu {

    private bool initialized = false;

    private IDBConnection dbConnection;
    private AgentObject agentObject;
    public GameObject menuToSetActive;

    public int agentID;
    private string agentName;
    public Button btnBack;
    public GameObject listElements;
    public GameObject listItemPrefab;

    #region UI Elements
    public Text txtName;
    public Text txtGender;
    public Text txtWealth;
    public Text txtPopularity;
    public Text txtRespectability;
    public Text txtRelation;
    public Text txtCharacter;
    public Button btnAllies;
    public Button btnEnemies;
    public Button btnClients;
    public Button btnFavorite;
    public Image imgStar;
    #endregion

    public Color favoriteColor;
    public Color noFavoriteColor;

    private enum SelectedList { Clients,Allies,Enemies }
    private SelectedList selectedList;

    [Inject]
    public void Constructor(IDBConnection dbConnection)
    {
        this.dbConnection = dbConnection;
    }

    // Use this for initialization
    void Start () {
        agentObject = GetAgentDetails(agentID);

        SetUIDataElements(agentObject);
        btnBack.onClick.AddListener(BtnBack);
        btnFavorite.onClick.AddListener(BtnFavorite);
        btnClients.onClick.AddListener(BtnClients);
        btnAllies.onClick.AddListener(BtnAllies);
        btnEnemies.onClick.AddListener(BtnEnemies);

        initialized = true;
	}
	
    private void BtnBack()
    {
        MenuController.DestroyMenu(gameObject);
    }

    private void OnEnable()
    {
        if(initialized)
        {
            agentObject = GetAgentDetails(agentID);
            SetUIDataElements(agentObject);

            string query = GetQuery(selectedList);
            DestroyAllCurrentRows();
            UpdateListElements(query, selectedList);
        }
    }

    private void BtnFavorite()
    {
        dbConnection = new DBConnection(); dbConnection.OpenConnection();;
        if (agentObject.IsFavorite)
        {
            imgStar.color = noFavoriteColor;
            agentObject.IsFavorite = false;
            agentObject.DeselectAsFavorite(dbConnection);
        }
        else
        {
            imgStar.color = favoriteColor;
            agentObject.IsFavorite = true;
            agentObject.SelectAsFavorite(dbConnection);
        }
        dbConnection.CloseConnection();

    }

    private void SetUIDataElements(AgentObject agentObject)
    {
        txtName.text = agentObject.FirstName + " " + agentObject.LastName;
        txtGender.text = agentObject.Gender.ToString();
        txtWealth.text = Money.GetMoneyString(agentObject.Wealth);
        txtPopularity.text = agentObject.Popularity.ToString();
        txtRespectability.text = agentObject.Respectability.ToString();

        if(agentID == 1)
        {
            txtRelation.text = "You";
            txtCharacter.text = "You";
            btnAllies.gameObject.SetActive(false);
            btnEnemies.gameObject.SetActive(false);
            btnClients.gameObject.SetActive(false);
        }
        else
        {
            txtRelation.text = agentObject.Relation + "%";
            txtCharacter.text = agentObject.Character.NameOfCharacter;
        }

        if (agentObject.IsFavorite)
            imgStar.color = favoriteColor;
        
    }

    private AgentObject GetAgentDetails(int id)
    {
        dbConnection = new DBConnection();
        dbConnection.OpenConnection();;

        var agentObject = AgentData.GetAllDetails(dbConnection, id);

        dbConnection.CloseConnection();

        return agentObject;
    }

    #region ListController

    private void BtnClients()
    {
        btnClients.interactable = false;
        btnAllies.interactable = true;
        btnEnemies.interactable = true;

        selectedList = SelectedList.Clients;
        string query = GetQuery(selectedList);
        DestroyAllCurrentRows();
        UpdateListElements(query, selectedList);
    }

    private void BtnAllies()
    {
        btnClients.interactable = true;
        btnAllies.interactable = false;
        btnEnemies.interactable = true;

        selectedList = SelectedList.Allies;
        string query = GetQuery(selectedList);
        DestroyAllCurrentRows();
        UpdateListElements(query, selectedList);
    }

    private void BtnEnemies()
    {
        btnClients.interactable = true;
        btnAllies.interactable = true;
        btnEnemies.interactable = false;

        selectedList = SelectedList.Enemies;
        string query = GetQuery(selectedList);
        DestroyAllCurrentRows();
        UpdateListElements(query, selectedList);
    }

    private void UpdateListElements(string query, SelectedList selectedList)
    {
        dbConnection.OpenConnection();;
        var reader = dbConnection.DownloadQuery(query);

        while (reader.Read())
        {
            var newItem = MenuController.InitializeListItem(ListItem.AgentsDetailsItem, listElements.transform);
            var controller = newItem.GetComponent<AgentsDetailsItemController>();
            controller.id = reader.GetInt32(0);
            controller.txtName.text = reader.GetString(1) + " " + reader.GetString(2);

            if (selectedList == SelectedList.Clients)
                controller.objectType = "Clients";
            else
                controller.objectType = "Agents";

            if (controller.objectType != "Clients")
                controller.txtData.text = Money.GetMoneyString(reader.GetInt32(3));
            else
            {
                controller.txtData.text = reader.GetInt32(3).ToString();
                if (reader.GetInt32(4) != 0)
                    controller.imgRedCross.gameObject.SetActive(true);
            }
                
            if (controller.id == 1)
                controller.btnDetails.gameObject.SetActive(false);
        }

        reader.Close();
        reader = null;

        dbConnection.CloseConnection();
    }

    private void DestroyAllCurrentRows()
    {
        foreach (Transform child in listElements.transform)
        {
            Destroy(child.gameObject);
        }
    }

    private string GetQuery(SelectedList selectedList)
    {
        string query = "";

        switch(selectedList)
        {
            case SelectedList.Clients:
                query = "SELECT ID,FirstName,LastName,OverallSkill,InjuryTime FROM players WHERE Agent =" + agentID + " ORDER BY LastName;";
                break;

            case SelectedList.Allies:
                List<string> allies = new List<string>(agentObject.Allies.Split('+'));

                if(allies.Count == 1)
                {
                    query = "SELECT ID,FirstName,LastName,Wealth FROM agents WHERE ID = -2 ORDER BY LastName;";
                }
                    
                else
                {
                    query = "SELECT ID,FirstName,LastName,Wealth FROM agents WHERE ";
                    allies.RemoveAt(0);
                    foreach(string ally in allies)
                    {
                        if(ally != allies.Last())
                            query += "ID = " + ally + " OR ";
                        else
                            query += "ID = " + ally;
                    }
                    query += " ORDER BY LastName";
                }
                break;

            case SelectedList.Enemies:
                List<string> enemies = new List<string>(agentObject.Enemies.Split('+'));

                if (enemies.Count == 1)
                {
                    query = "SELECT ID,FirstName,LastName,Wealth FROM agents WHERE ID = -2 ORDER BY LastName;";
                }

                else
                {
                    query = "SELECT ID,FirstName,LastName,Wealth FROM agents WHERE ";
                    enemies.RemoveAt(0);
                    foreach (string enemy in enemies)
                    {
                        if (enemy != enemies.Last())
                            query += "ID = " + enemy + " OR ";
                        else
                            query += "ID = " + enemy;
                    }
                    query += " ORDER BY LastName";
                }
                break;

            default:
                break;
        }

        return query;
    }

    #endregion

    public class Factory : PlaceholderFactory<AgentDetailsMenuController> { }
}
