﻿using Mono.Data.Sqlite;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class AgentsListController : Menu{

    private bool initialized = false;

    private IDBConnection dbConnection;
    private UserDataHolder userDataHolder;
    private int currentIndexList = 0;
    private int allResultsNumber;
    private enum SortingType { ascending, descending };
    private enum SelectedGroupToGet { allAgents, favorites, friends, enemies };
    private SelectedGroupToGet groupToGet;

    public GameObject listItemPrefab;
    public GameObject listElements;

    public Dropdown dropdown;
    public Color favoriteColor;
    public Color friendColor;
    public Color enemyColor;

    //------------------------------buttons
    public Button btnBack100;
    public Button btnNext100;
    public Text txtCurrentResults;
    public Text txtAllResults;
    public Button btnFilter;

    //sorting buttons
    public Button btnWealthSort;
    public Button btnLastNameSort;
    public Button btnPopularitySort;
    public Button btnRespectabilitySort;
    public Button btnRelationSort;
    private SortingType lastNameSortingType = SortingType.descending;
    private SortingType wealthSortingType = SortingType.descending;
    private SortingType popularitySortingType = SortingType.ascending;
    private SortingType respectabilitySortingType = SortingType.ascending;
    private SortingType relationSortingType = SortingType.ascending;
    //------------------------------------

    //filters
    public GameObject filterPanel;
    public GameObject btnSearch;
    public InputField inputLastName;
    public InputField inputWealth;
    public InputField inputPopularity;
    public InputField inputRespectability;
    public InputField inputRelation;

    //
    private List<AgentsItemList> agentsList;

    [Inject]
    public void Constructor(IDBConnection dbConnection)
    {
        this.dbConnection = dbConnection;
    }

	// Use this for initialization
	void Start () {
        userDataHolder = GameObject.FindGameObjectWithTag("UserDataHolder").GetComponent<UserDataHolder>();

        groupToGet = SelectedGroupToGet.allAgents;
        agentsList = GetAgentsFromDB(groupToGet);
        AddAgentsListToMenu(0, 100);

        dropdown.onValueChanged.AddListener(delegate { DropdownMenu(dropdown); });
        //buttons listeners
        btnBack100.onClick.AddListener(BtnBack100);
        btnNext100.onClick.AddListener(BtnNext100);
        btnLastNameSort.onClick.AddListener(SortByLastName);
        btnPopularitySort.onClick.AddListener(SortByPopularity);
        btnRespectabilitySort.onClick.AddListener(SortByRespectability);
        btnRelationSort.onClick.AddListener(SortByRelation);
        btnWealthSort.onClick.AddListener(SortByWealth);
        //
        btnFilter.onClick.AddListener(BtnFilter);
        btnSearch.GetComponent<Button>().onClick.AddListener(BtnSearch);
        //
        initialized = true;
    }

    void Update()
    {
        CheckButtonsInteractable();
    }

    private void OnEnable()
    {
        if(initialized)
        {
            UserDataDB.UpdateUserDataHolder();
            DestroyAllCurrentRows();
            agentsList = GetAgentsFromDB(groupToGet);
            AddAgentsListToMenu(0, 100);
        }
    }

    public void DropdownMenu(Dropdown dropdown)
    {
        DestroyAllCurrentRows();

        switch (this.dropdown.value)
        {
            case 0:
                groupToGet = SelectedGroupToGet.allAgents;
                break;
            case 1:
                groupToGet = SelectedGroupToGet.favorites;
                break;
            case 2:
                groupToGet = SelectedGroupToGet.friends;
                break;
            case 3:
                groupToGet = SelectedGroupToGet.enemies;
                break;
            default:
                groupToGet = SelectedGroupToGet.allAgents;
                break;
        }
        agentsList = GetAgentsFromDB(groupToGet);
        AddAgentsListToMenu(0, 100);
    }

    #region sorting methods
    private void SortByWealth()
    {
        switch (wealthSortingType)
        {
            case SortingType.ascending:
                agentsList = agentsList.OrderBy(x => x.Wealth).ToList();
                wealthSortingType = SortingType.descending;
                break;

            case SortingType.descending:
                agentsList = agentsList.OrderByDescending(x => x.Wealth).ToList();
                wealthSortingType = SortingType.ascending;
                break;
        }

        DestroyAllCurrentRows();
        AddAgentsListToMenu(0, 100);
    }

    private void SortByLastName()
    {
        switch(lastNameSortingType)
        {
            case SortingType.ascending:
                agentsList = agentsList.OrderBy(x => x.LastName).ToList();
                lastNameSortingType = SortingType.descending;
                break;

            case SortingType.descending:
                agentsList = agentsList.OrderByDescending(x => x.LastName).ToList();
                lastNameSortingType = SortingType.ascending;
                break;
        }

        DestroyAllCurrentRows();
        AddAgentsListToMenu(0, 100);
    }

    private void SortByPopularity()
    {
        switch (popularitySortingType)
        {
            case SortingType.ascending:
                agentsList = agentsList.OrderBy(x => x.Popularity).ToList();
                popularitySortingType = SortingType.descending;
                break;

            case SortingType.descending:
                agentsList = agentsList.OrderByDescending(x => x.Popularity).ToList();
                popularitySortingType = SortingType.ascending;
                break;
        }
        DestroyAllCurrentRows();
        AddAgentsListToMenu(0, 100);
    }

    private void SortByRespectability()
    {
        switch (respectabilitySortingType)
        {
            case SortingType.ascending:
                agentsList = agentsList.OrderBy(x => x.Respectability).ToList();
                respectabilitySortingType = SortingType.descending;
                break;

            case SortingType.descending:
                agentsList = agentsList.OrderByDescending(x => x.Respectability).ToList();
                respectabilitySortingType = SortingType.ascending;
                break;
        }
        DestroyAllCurrentRows();
        AddAgentsListToMenu(0, 100);
    }

    private void SortByRelation()
    {
        switch (relationSortingType)
        {
            case SortingType.ascending:
                agentsList = agentsList.OrderBy(x => x.Relation).ToList();
                relationSortingType = SortingType.descending;
                break;

            case SortingType.descending:
                agentsList = agentsList.OrderByDescending(x => x.Relation).ToList();
                relationSortingType = SortingType.ascending;
                break;
        }
        DestroyAllCurrentRows();
        AddAgentsListToMenu(0, 100);
    }

#endregion

    #region main table methods 
    private void CheckButtonsInteractable()
    {
        if (currentIndexList - 100 < 0)
            btnBack100.interactable = false;
        else
            btnBack100.interactable = true;

        if (currentIndexList + 101 > allResultsNumber)
            btnNext100.interactable = false;
        else
            btnNext100.interactable = true;
    }

    private void BtnBack100()
    {
        if (currentIndexList - 100 >= 0)
        {
            currentIndexList -= 100;
            DestroyAllCurrentRows();

            if (currentIndexList - 100 < 0)
                AddAgentsListToMenu(0, currentIndexList + 100);
            else
                AddAgentsListToMenu(currentIndexList, currentIndexList + 100);
        }
    }

    private void BtnNext100()
    {
        if (currentIndexList + 101 <= allResultsNumber)
        {
            currentIndexList += 100;
            DestroyAllCurrentRows();

            if (currentIndexList + 100 > allResultsNumber)
                AddAgentsListToMenu(currentIndexList, currentIndexList + allResultsNumber - currentIndexList);
            else
                AddAgentsListToMenu(currentIndexList, currentIndexList + 100);
        }
    }

    private void DestroyAllCurrentRows()
    {
        foreach (Transform child in listElements.transform)
        {
            Destroy(child.gameObject);
        }
    }

    private void AddAgentsListToMenu(int min, int max)
    {
        allResultsNumber = agentsList.Count;

        if (max > allResultsNumber)
        {
            max = allResultsNumber;
            txtCurrentResults.text = (min + 1).ToString() + " - " + allResultsNumber.ToString();
        }
        else
            txtCurrentResults.text = (min + 1).ToString() + " - " + max.ToString();

        txtAllResults.text = "Results: " + allResultsNumber.ToString();
       
        for (int i = min; i < max; i++)
        {
            var newAgent = MenuController.InitializeListItem(ListItem.AgentsListItem, listElements.transform);
            var controller = newAgent.GetComponent<AgentsItemController>();

            controller.id = agentsList[i].ID;
            controller.txtWealth.text = Money.GetMoneyString(agentsList[i].Wealth);
            controller.txtName.text = agentsList[i].FirstName + " " + agentsList[i].LastName;
            controller.txtPopularity.text = agentsList[i].Popularity.ToString();
            controller.txtRespectability.text = agentsList[i].Respectability.ToString();

            if(controller.id == 1)
            {
                controller.txtRelation.text = "";
                controller.txtName.color = Color.white;
                controller.btnAgentDetails.gameObject.SetActive(false);
            }  
            else
                controller.txtRelation.text = agentsList[i].Relation.ToString() + "%";

            if (agentsList[i].IsFavorite)
                controller.btnDetailsText.color = favoriteColor;

            if(GroupStringManager.IsPersonInGroup(userDataHolder.Allies,controller.id.ToString()))
                controller.txtName.color = friendColor;

            if (GroupStringManager.IsPersonInGroup(userDataHolder.Enemies, controller.id.ToString()))
                controller.txtName.color = enemyColor;

            newAgent.name = i.ToString();
        }
    }

    private List<AgentsItemList> GetAgentsFromDB(SelectedGroupToGet group)
    {
        List<AgentsItemList> arrayList = new List<AgentsItemList>();
        dbConnection.OpenConnection();;
        string query = GetQuerySelectedGroup(group);
        SqliteDataReader reader = dbConnection.DownloadQuery(query);

        while(reader.Read())
        {
            arrayList.Add(new AgentsItemList(
                    reader.GetInt32(0),
                    reader.GetString(1),
                    reader.GetString(2),
                    reader.GetInt32(3),
                    reader.GetInt32(4),
                    reader.GetInt32(5),
                    reader.GetInt32(6),
                    reader.GetInt32(7)
                ));
        }

        reader.Close();
        reader = null;

        dbConnection.CloseConnection();

        return arrayList;
    }

    private string GetQuerySelectedGroup(SelectedGroupToGet group)
    {
        string query = "SELECT ID,FirstName,LastName,Wealth,Popularity,Respectability,Relation,IsFavorite FROM agents ";
            
        switch(group)
        {
            case SelectedGroupToGet.allAgents:
                break;
            case SelectedGroupToGet.favorites:
                query += "WHERE IsFavorite = 1";
                break;
            case SelectedGroupToGet.friends:
                query += "WHERE " + GetQueryWithIDsINFromList(userDataHolder.AlliesIDs);
                break;
            case SelectedGroupToGet.enemies:
                query += "WHERE " + GetQueryWithIDsINFromList(userDataHolder.EnemiesIDs);
                break;
            default:
                break;
        }

        query += " ORDER BY Wealth DESC";

        return query;
    }
    #endregion

    #region filters

    private void BtnFilter()
    {
        filterPanel.SetActive(!filterPanel.activeSelf);
    }

    private void BtnSearch()
    {
        string query = GetQueryFilter();

        List<AgentsItemList> arrayList = new List<AgentsItemList>();
        dbConnection = new DBConnection(); dbConnection.OpenConnection();;
        SqliteDataReader reader = dbConnection.DownloadQuery(query);
        while (reader.Read())
        {
            arrayList.Add(new AgentsItemList(
                    reader.GetInt32(0),
                    reader.GetString(1),
                    reader.GetString(2),
                    reader.GetInt32(3),
                    reader.GetInt32(4),
                    reader.GetInt32(5),
                    reader.GetInt32(6),
                    reader.GetInt32(7)
                ));
        }

        reader.Close();
        reader = null;

        dbConnection.CloseConnection();

        agentsList = arrayList;

        DestroyAllCurrentRows();
        AddAgentsListToMenu(0, 100);
    }

    private string GetQueryFilter()
    {
        if (inputLastName.text == "" && inputWealth.text == "" && inputPopularity.text == "" && inputRespectability.text == "" && inputRelation.text == "" && dropdown.value == 0)
            return "SELECT ID,FirstName,LastName,Wealth,Popularity,Respectability,Relation,IsFavorite FROM agents ORDER BY Wealth;";

        if (inputLastName.text == "" && inputWealth.text == "" && inputPopularity.text == "" && inputRespectability.text == "" && inputRelation.text == "" && dropdown.value == 1)
            return "SELECT ID,FirstName,LastName,Wealth,Popularity,Respectability,Relation,IsFavorite FROM agents WHERE IsFavorite == 1 ORDER BY Wealth;";

        if (inputLastName.text == "" && inputWealth.text == "" && inputPopularity.text == "" && inputRespectability.text == "" && inputRelation.text == "" && dropdown.value == 2)
            return "SELECT ID,FirstName,LastName,Wealth,Popularity,Respectability,Relation,IsFavorite FROM agents WHERE " + GetQueryWithIDsINFromList(userDataHolder.AlliesIDs) + " ORDER BY Wealth;";

        if (inputLastName.text == "" && inputWealth.text == "" && inputPopularity.text == "" && inputRespectability.text == "" && inputRelation.text == "" && dropdown.value == 3)
            return "SELECT ID,FirstName,LastName,Wealth,Popularity,Respectability,Relation,IsFavorite FROM agents WHERE " + GetQueryWithIDsINFromList(userDataHolder.EnemiesIDs) + " ORDER BY Wealth;";

        string query;

        if(dropdown.value == 1)
            query = "SELECT ID,FirstName,LastName,Wealth,Popularity,Respectability,Relation,IsFavorite FROM agents WHERE IsFavorite == 1 AND ";
        else if (dropdown.value == 2)
            query = "SELECT ID,FirstName,LastName,Wealth,Popularity,Respectability,Relation,IsFavorite FROM agents WHERE " + GetQueryWithIDsINFromList(userDataHolder.AlliesIDs) + " AND ";
        else if (dropdown.value == 3)
            query = "SELECT ID,FirstName,LastName,Wealth,Popularity,Respectability,Relation,IsFavorite FROM agents WHERE " + GetQueryWithIDsINFromList(userDataHolder.EnemiesIDs) + " AND ";
        else
            query = "SELECT ID,FirstName,LastName,Wealth,Popularity,Respectability,Relation,IsFavorite FROM agents WHERE ";

        List<InputField> inputs = new List<InputField>();
        List<InputField> notNullInputs = new List<InputField>();
        inputs.Add(inputLastName);
        inputs.Add(inputWealth);
        inputs.Add(inputPopularity);
        inputs.Add(inputRespectability);
        inputs.Add(inputRelation);

        int numberOfFilters = 0;
        foreach(InputField input in inputs)
        {
            if(input.text != "")
            {
                numberOfFilters += 1;
                notNullInputs.Add(input);
            }
        }

        if(numberOfFilters == 1)
        {
            if(GetFilterName(notNullInputs[0]) == "Wealth" || GetFilterName(notNullInputs[0]) == "Popularity" || 
                GetFilterName(notNullInputs[0]) == "Respectability" || GetFilterName(notNullInputs[0]) == "Relation")
            {
                if(notNullInputs[0].text.Substring(0,1) == ">" || notNullInputs[0].text.Substring(0, 1) == "<")
                    query += GetFilterName(notNullInputs[0]) + notNullInputs[0].text + "";
                else
                    query += GetFilterName(notNullInputs[0]) + "='" + notNullInputs[0].text + "'";
            }
            else
                query += GetFilterName(notNullInputs[0]) + " Like '" + notNullInputs[0].text + "%'";
        }
        else
        {
            InputField lastInput = notNullInputs.Last();
            foreach(InputField input in notNullInputs)
            {
                if (GetFilterName(input) == "Wealth" || GetFilterName(input) == "Popularity" ||
                GetFilterName(input) == "Respectability" || GetFilterName(input) == "Relation")
                {
                    if (input.text.Substring(0, 1) == ">" || input.text.Substring(0, 1) == "<")
                        query += GetFilterName(input) + input.text + "";
                    else
                        query += GetFilterName(input) + "='" + input.text + "'";
                }
                else
                    query += GetFilterName(input) + " Like '" + input.text + "%'";

                if(input != lastInput)
                    query += " AND ";
            }
        }


        query += " ORDER BY Wealth;";
        return query;
    }

    private string GetFilterName(InputField input)
    {
        return input.name.Substring(5);
    }


    #endregion

    private string GetQueryWithIDsINFromList(List<int> list)
    {
        string query = "ID IN(";

        if(list.Any())
        {
            foreach (int id in list)
                query += id + ",";
            query = query.Remove(query.Length - 1);
        }
        query += ")";

        return query;
    }
}
