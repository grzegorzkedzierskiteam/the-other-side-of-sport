﻿using System;
using Mono.Data.Sqlite;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class AgentData {
    public static AgentObject GetAllDetails(IDBConnection dbConnection, int agentID)
    {
        string query = "SELECT * FROM agents WHERE ID = " + agentID;
        SqliteDataReader reader = dbConnection.DownloadQuery(query);

        if(reader.HasRows)
        {
            reader.Read();
            AgentObject agentObject = new AgentObject(
                reader.GetInt32(0),
                reader.GetString(1),
                reader.GetString(2),
                (Gender)Enum.Parse(typeof(Gender), reader.GetString(14)),
                reader.GetInt32(3),
                reader.GetInt32(4),
                reader.GetInt32(5),
                reader.GetString(6),
                reader.GetString(7),
                reader.GetInt32(8),
                reader.GetString(10),
                reader.GetInt32(12),
                reader.GetString(11),
                reader.GetInt32(13));

            reader.Close();
            reader = null;

            return agentObject;
        }
        else
        {
            reader.Close();
            reader = null;
            return null;
        }
    }

    public static IBidder GetAgentAsBidder(IDBConnection dbConnection, int agentID)
    {
        string query = "SELECT Respectability, Popularity FROM agents WHERE ID = " + agentID;
        SqliteDataReader reader = dbConnection.DownloadQuery(query);

        if (reader.HasRows)
        {
            reader.Read();
            Bidder bidder = new Bidder(reader.GetInt32(0),reader.GetInt32(1));

            reader.Close();
            reader = null;

            return bidder;
        }
        else
        {
            reader.Close();
            reader = null;
            return null;
        }
    }

    public static List<AgentObject> GetAllAgentsOrReturnNull(IDBConnection dbConnection)
    {
        string query = "SELECT * FROM agents";
        SqliteDataReader reader = dbConnection.DownloadQuery(query);

        if (reader.HasRows)
        {
            List<AgentObject> list = new List<AgentObject>();

            while(reader.Read())
            {
                AgentObject agentObject = new AgentObject(
                reader.GetInt32(0),
                reader.GetString(1),
                reader.GetString(2),
                (Gender)Enum.Parse(typeof(Gender), reader.GetString(14)),
                reader.GetInt32(3),
                reader.GetInt32(4),
                reader.GetInt32(5),
                reader.GetString(6),
                reader.GetString(7),
                reader.GetInt32(8),
                reader.GetString(10),
                reader.GetInt32(12),
                reader.GetString(11),
                reader.GetInt32(13));

                list.Add(agentObject);
            }

            reader.Close();
            reader = null;

            return list;
        }
        else
        {
            reader.Close();
            reader = null;
            return null;
        }
    }

    public static List<IClientObject> GetAllClientsOfAgent(IDBConnection dbConnection, int agentID)
    {
        string query = "SELECT * FROM players WHERE Agent = " + agentID;
        SqliteDataReader reader = dbConnection.DownloadQuery(query);
        if (reader.HasRows)
        {
            List<IClientObject> list = new List<IClientObject>();
            while(reader.Read())
            {
                ClientObject clientObject = new ClientObject(
                    reader.GetInt32(0),
                    reader.GetString(1),
                    reader.GetString(2),
                    reader.GetString(3),
                    reader.GetString(4),
                    reader.GetString(5),
                    reader.GetString(6),
                    reader.GetInt32(7),
                    reader.GetInt32(8),
                    reader.GetInt32(9),
                    reader.GetInt32(10),
                    CharacterManager.GetCharacterFromNameOfCharacter(reader.GetString(11)),
                    reader.GetInt32(12),
                    reader.GetInt32(13),
                    reader.GetInt32(25),
                    reader.GetInt32(14),
                    reader.GetInt32(27),
                    reader.GetInt32(26),
                    reader.GetInt32(15),
                    reader.GetInt32(16),
                    reader.GetInt32(17),
                    reader.GetInt32(19),
                    reader.GetString(20),
                    reader.GetInt32(21),
                    reader.GetInt32(18),
                    reader.GetInt32(24),
                    reader.GetInt32(22),
                    reader.GetInt32(23)
                );

                list.Add(clientObject);
            }
            reader.Close();
            reader = null;

            return list;
        }
        else
        {
            reader.Close();
            reader = null;
            return new List<IClientObject>();
        }
    }

    public static void UpdateStringGroupDB(IDBConnection dbConnection, int agentID, string group, AgentGroupEnum groupType)
    {
        string query = "UPDATE agents SET ";

        switch(groupType)
        {
            case AgentGroupEnum.Allies:
                query += "Allies = ";
                break;
            case AgentGroupEnum.Enemies:
                query += "Enemies = ";
                break;
            case AgentGroupEnum.Clients:
                query += "Clients = ";
                break;
        }
        query += "'" + group + "' WHERE ID = " + agentID;
        dbConnection.ModifyQuery(query);
    }
}
