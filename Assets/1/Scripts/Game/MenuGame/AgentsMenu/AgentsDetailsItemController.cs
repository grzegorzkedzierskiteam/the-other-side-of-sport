﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class AgentsDetailsItemController : Menu {

    private MenuController menuController;

    public string objectType;
    public int id;
    public Text txtName;
    public Text txtData;
    public Image imgRedCross;

    public Button btnDetails;
    public GameObject agentDetailsMenuPrefab;
    public GameObject clientDetailsMenuPrefab;

    // Use this for initialization
    void Start () {
        btnDetails.onClick.AddListener(BtnDetails);
	}

    private void BtnDetails()
    {
        if(objectType == "Clients")
        {
            var menuDetails = MenuController.InitializeMenu(MenuEnum.ClientDetailsMenu);
            menuDetails.GetComponent<ClientDetailsMenuController>().clientID = id;
        }

        if(objectType == "Agents")
        {
            var menuDetails = MenuController.InitializeMenu(MenuEnum.AgentDetailsMenu);
            menuDetails.GetComponent<AgentDetailsMenuController>().agentID = id;
        }
    }

    public class Factory : PlaceholderFactory<AgentsDetailsItemController> { }
}
