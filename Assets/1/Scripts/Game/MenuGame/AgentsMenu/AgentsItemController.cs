﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class AgentsItemController : Menu {

    public int id;
    public Text txtName;
    public Text txtWealth;
    public Text txtPopularity;
    public Text txtRespectability;
    public Text txtRelation;
    public Text btnDetailsText;

    public Button btnAgentDetails;
    public GameObject agentDetailsMenuPrefab;

    private void Start()
    {
        btnAgentDetails.onClick.AddListener(BtnAgentsDetails);
    }

    private void BtnAgentsDetails()
    {
        var menuDetails = MenuController.InitializeMenu(MenuEnum.AgentDetailsMenu);
        menuDetails.GetComponent<AgentDetailsMenuController>().agentID = id;
    }

    public class Factory : PlaceholderFactory<AgentsItemController> { }
}
