﻿using Messages;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class ConfirmNextWeekController : Menu {

    private MyUnityAdsController myUnityAdsController;
    private IDBConnection dbConnection;

    public GameObject loadingProgressBarPanelPrefab;
    private GameObject loadingProgressBarPanel;

    public Text txtTitle;
    public Button btnYes;
    public Button btnNo;

    private IMessageManagerDB messageManagerDb;
    private INextWeekManager nextWeekManager;

    [Inject]
    public void Constructor(IDBConnection dbConnection, IMessageManagerDB messageManagerDb, INextWeekManager nextWeekManager)
    {
        this.dbConnection = dbConnection;
        this.messageManagerDb = messageManagerDb;
        this.nextWeekManager = nextWeekManager;
    }

    void Start()
    {
        myUnityAdsController = new MyUnityAdsController();
        myUnityAdsController.InitializeAd();

        btnYes.onClick.AddListener(BtnYes);
        btnNo.onClick.AddListener(BtnNo);
    }

    private void BtnYes()
    {
        StartCoroutine(AdvanceToNextWeek());
    }

    private void BtnNo()
    {
        var menuGame = GameObject.Find("MenuGame(Clone)");
        var menuGameController = menuGame.GetComponent<MenuGameController>();
        menuGameController.btnProfile.interactable = true;
        menuGameController.btnClients.interactable = true;
        menuGameController.btnAgents.interactable = true;
        menuGameController.btnResults.interactable = true;
        menuGameController.btnNextWeek.interactable = true;
        menuGameController.btnNotification.interactable = true;
        menuGameController.btnExit.interactable = true;
        MenuController.DestroyPopMenu(gameObject);
    }

    private IEnumerator AdvanceToNextWeek()
    {
        //reklamy -------------------------------------------myUnityAdsController.ShowAd();
        
        yield return null;

        //-----------------------------------SETUP
        var userDataHolder = UserData.GetUserDataHolder();
        dbConnection.OpenConnection();;
                                
        messageManagerDb.SetUpDate(userDataHolder.DateTime);

        loadingProgressBarPanel = MenuController.GetAndInitializeMenu(loadingProgressBarPanelPrefab);
        var txtProgress = loadingProgressBarPanel.transform.Find("TxtProgress").GetComponent<Text>();
        var progressBar = loadingProgressBarPanel.transform.Find("ProgressBar").GetComponent<Image>();
        //--------------------------------

        dbConnection.StartTransaction();

        progressBar.fillAmount = 0.1f;
        txtProgress.text = "10%";
        yield return null;

        progressBar.fillAmount = 0.2f;
        txtProgress.text = "20%";
        yield return null;

        progressBar.fillAmount = 0.3f;
        txtProgress.text = "30%";
        yield return null;
        
        nextWeekManager.TeamsActivities();
        progressBar.fillAmount = 0.4f;
        txtProgress.text = "40%";
        yield return null;
        
        nextWeekManager.AgentsActivities();
        progressBar.fillAmount = 0.5f;
        txtProgress.text = "50%";
        txtProgress.color = new Color(215, 215, 215);
        yield return null;

        nextWeekManager.SportsEvents();
        progressBar.fillAmount = 0.6f;
        txtProgress.text = "60%";
        yield return null;

        nextWeekManager.AthletesActivities();
        progressBar.fillAmount = 0.65f;
        txtProgress.text = "65%";
        yield return null;
        
        nextWeekManager.WorldEvents(userDataHolder.DateTime);
        progressBar.fillAmount = 0.7f;
        txtProgress.text = "70%";
        yield return null;

        nextWeekManager.SalariesExecute(AgentData.GetAllAgentsOrReturnNull(dbConnection), userDataHolder.DateTime);
        progressBar.fillAmount = 0.75f;
        txtProgress.text = "75%";
        yield return null;

        progressBar.fillAmount = 0.8f;
        txtProgress.text = "80%";
        yield return null;

        nextWeekManager.AdvanceToNextWeek();
        progressBar.fillAmount = 0.95f;
        txtProgress.text = "95%";
        yield return null;

        progressBar.fillAmount = 1f;
        txtProgress.text = "100%";

        Destroy(loadingProgressBarPanel);
        dbConnection.CommitTransaction();
        dbConnection.CloseConnection();
   
        FinishAdvancing();
    }

    private void FinishAdvancing()
    {
        UserDataDB.UpdateUserDataHolder();
        var menuGame = GameObject.Find("MenuGame(Clone)");
        var menuGameController = menuGame.GetComponent<MenuGameController>();
        menuGameController.SetMenuUserData();
        menuGameController.btnProfile.interactable = true;
        menuGameController.btnClients.interactable = true;
        menuGameController.btnAgents.interactable = true;
        menuGameController.btnResults.interactable = true;
        menuGameController.btnNextWeek.interactable = true;
        menuGameController.btnNotification.interactable = true;
        menuGameController.btnExit.interactable = true;
        menuGameController.SetMessageCounter();

        Destroy(gameObject);
    }

    public class Factory : PlaceholderFactory<ConfirmNextWeekController> { }
}
