﻿public class NewGame {

    public IDBConnection dbConnection;

    //
    public PlayerGenerator playerGenerator;
    private PlayerObject newPlayer;
    //
    public FootballPlayersGenerator footballPlayersGenerator;
    public AgentsGenerator agentsGenerator;
    public TeamsGenerator teamsGenerator;
    //
    public FootballSeasonGenerator footballSeasonGenerator;

    public NewGame(IDBConnection dbConnection,
        PlayerGenerator playerGenerator,
        AgentsGenerator agentsGenerator,
        TeamsGenerator teamsGenerator,
        FootballSeasonGenerator footballSeasonGenerator,
        FootballPlayersGenerator footballPlayersGenerator)
    {
        this.dbConnection = dbConnection;
        this.playerGenerator = playerGenerator;
        this.agentsGenerator = agentsGenerator;
        this.teamsGenerator = teamsGenerator;
        this.footballSeasonGenerator = footballSeasonGenerator;
        this.footballPlayersGenerator = footballPlayersGenerator;
        this.footballPlayersGenerator.SetUp(2018);
    }

    public void SetUp(string newFirstName, string newLastName, Gender gender, string newSelectedCharacter)
    {
        newPlayer = playerGenerator.CreateNewPlayer(newFirstName, newLastName, gender, newSelectedCharacter, dbConnection);
    }

    public void SetStartDateTime()
    {
        var query = "UPDATE config SET Week = 32, Year = 2018 WHERE ID = 1;";
        dbConnection.ModifyQuery(query);
    }

    public void BeginCreatingNewGame()
    {
        var query = "UPDATE config SET BeginCreatingGame = 1 WHERE ID = 1;";
        dbConnection.ModifyQuery(query);
    }

    public void FinishCreatingNewGame()
    {
        var query = "UPDATE config SET FinishCreatingGame = 2 WHERE ID = 1;";
        dbConnection.ModifyQuery(query);
    }

    public void AddNewPlayerToDB()
    {
        var query = "DROP TABLE IF EXISTS agents";
        dbConnection.ModifyQuery(query);
        query = @"CREATE TABLE agents(  ID integer PRIMARY KEY AUTOINCREMENT,
                                        FirstName varchar(35),
                                        LastName varchar(35),
                                        Wealth integer(16),
                                        Popularity integer(4),
                                        Respectability integer(4),
                                        Allies varchar,
                                        Enemies varchar,
                                        Energy integer(4),
                                        Health integer(4),
                                        Clients varchar,
                                        Character varchar,
                                        Relation integer(4),
                                        IsFavorite integer(1),
                                        Gender varchar
                                      );";
        dbConnection.ModifyQuery(query);

        query = @"INSERT INTO agents(FirstName, LastName, Gender, Wealth, Popularity, Respectability, Allies, Enemies, Energy, Health, Clients, Character, Relation, IsFavorite)
                              VALUES( '" + newPlayer.Name + "', '" + newPlayer.LastName + "', '" + newPlayer.Gender.ToString() + "'," + newPlayer.Wealth + "," + newPlayer.Popularity + "," + newPlayer.Respectability + ",'" + newPlayer.Allies + "','" + newPlayer.Enemies + "'," + newPlayer.Energy + "," +newPlayer.Health + ",'" + newPlayer.Clients + "','',0,0);";
        dbConnection.ModifyQuery(query);
    }

    public void CreateTablePlayers()
    {
        var query = "DROP TABLE IF EXISTS players";
        dbConnection.ModifyQuery(query);
        query = @"CREATE TABLE players( ID integer PRIMARY KEY AUTOINCREMENT,
                                        FirstName varchar(35),
                                        LastName varchar(35),
                                        Sport varchar,
                                        Position varchar,
                                        College varchar,
                                        Team varchar,
                                        Age integer(3),
                                        Wealth integer(16),
                                        Dept integer(16),
                                        Popularity integer(4),
                                        Character varchar,
                                        Relation integer(4),
                                        Contract integer,
                                        ContractLenght integer,
                                        OverallSkill integer(3),
                                        Prospectiveness integer(2),
                                        Agent integer,
                                        IsReady integer,
                                        InjuryTime integer,
                                        InjuryName varchar,
                                        IsFavorite integer(1),
                                        AgentContractDateEndWeek integer(2),
                                        AgentContractDateEndYear integer(2),
                                        AgentWage integer(2),
                                        ContractGuaranteedMoney integer,
                                        GotMoney integer,
                                        YearOfSign integer
                                      );";
        dbConnection.ModifyQuery(query);
    }

    public void CreateTableTeams()
    {
        var query = "DROP TABLE IF EXISTS teams";
        dbConnection.ModifyQuery(query);
        query = @"CREATE TABLE teams( ID integer PRIMARY KEY AUTOINCREMENT,
                                        Name varchar(35),
                                        Sport varchar(35),
                                        Character varchar,
                                        Relation integer(4),
                                        Wealth integer(16),
                                        Popularity integer(4),
                                        OverallSkill integer(3),
                                        Wins integer(3),
                                        Loses integer(3),
                                        Draws integer(3),
                                        Conference varchar,
                                        Division varchar,
                                        DraftNumber integer
                                      );";
        dbConnection.ModifyQuery(query);
    }

    public void CreateTableCalendar()
    {
        var query = "DROP TABLE IF EXISTS calendar";
        dbConnection.ModifyQuery(query);
        query = @"CREATE TABLE calendar( ID integer PRIMARY KEY AUTOINCREMENT,
                                        Year integer(5),
                                        Week integer(3),
                                        EventType varchar,
                                        Sport varchar,
                                        Info varchar,
                                        Team1 varchar,
                                        Team1Score integer,
                                        Team2 varchar,
                                        Team2Score integer,
                                        MatchType varchar,
                                        SeasonWeek integer(4),
                                        Finished integer(1)
                                      );";
        dbConnection.ModifyQuery(query);
    }

    public void CreateTableFinance()
    {
        var query = "DROP TABLE IF EXISTS finance";
        dbConnection.ModifyQuery(query);
        query = @"CREATE TABLE finance( ID integer PRIMARY KEY AUTOINCREMENT,
                                        Year integer(5),
                                        Week integer(3),
                                        Value integer,
                                        PersonID integer,
                                        PersonName varchar,
                                        Info varchar
                                      );";
        dbConnection.ModifyQuery(query);
    }

    public void CreateTableNotifications()
    {
        var query = "DROP TABLE IF EXISTS messages";
        dbConnection.ModifyQuery(query);
        query = @"CREATE TABLE messages( ID integer PRIMARY KEY AUTOINCREMENT,
                                        Description varchar,
                                        ExpiringTime integer(3),
                                        DoesHaveEffect integer(1),
                                        ParticipantID integer,
                                        ParticipantName varchar,
                                        ParticipantType varchar,
                                        TypeOfMessage varchar,
                                        Week integer(2),
                                        Year integer(5)
                                      );";
        dbConnection.ModifyQuery(query);
    }

    public void CreateTableMessageResponses()
    {
        var query = "DROP TABLE IF EXISTS messages_responses";
        dbConnection.ModifyQuery(query);
        query = @"CREATE TABLE messages_responses( ID integer PRIMARY KEY AUTOINCREMENT,
                                        Message_ID integer,
                                        PropertyString varchar,
                                        Value integer,
                                        TypeOfResponse varchar
                                      );";
        dbConnection.ModifyQuery(query);
    }

}
