﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddingAthletesToDBController : IAddingAthletesToDBController{

    private IDBConnection dbConnection;
    private const string BeginningOfQuery = "INSERT INTO players(FirstName, LastName, Sport, Position, College, Team, Age, Wealth, Dept, Popularity, Character, Relation, Contract, ContractGuaranteedMoney, ContractLenght, YearOfSign, GotMoney, OverallSkill, Prospectiveness, Agent, IsReady, InjuryTime, InjuryName, IsFavorite, AgentContractDateEndWeek, AgentContractDateEndYear, AgentWage) VALUES";
    private string groupQuery;

    public AddingAthletesToDBController(IDBConnection dbConnection)
    {
        this.dbConnection = dbConnection;
    }

    public void AddAthleteToDB(IClientObject clientObject)
    {
        string query = BeginningOfQuery;

        query += CreateQueryValues(clientObject) + ";";

        dbConnection.ModifyQuery(query);
    }

    public void CreateGroupToAddToDB()
    {
        groupQuery = BeginningOfQuery;
    }

    public void AddAthleteToGroupToAddToDB(IClientObject clientObject)
    {
        groupQuery += CreateQueryValues(clientObject) + ",";
    }

    public void CommitGroupToDB()
    {
        groupQuery = groupQuery.Remove(groupQuery.Length - 1);
        groupQuery += ";";
        dbConnection.ModifyQuery(groupQuery);
    }

    private string CreateQueryValues(IClientObject clientObject)
    {
        int isFavorite = 0;
        if (clientObject.IsFavorite)
            isFavorite = 1;

        string query = "('" + clientObject.FirstName + "'," +
                    "'" + clientObject.LastName + "'," +
                    "'" + clientObject.Sport + "'," +
                    "'" + clientObject.Position + "'," +
                    "'" + clientObject.College + "'," +
                    "'" + clientObject.Team + "'," +
                        clientObject.Age + "," +
                        clientObject.Wealth + "," +
                        clientObject.Dept + "," +
                        clientObject.Popularity + "," +
                    "'" + clientObject.Character.NameOfCharacter + "'," +
                        clientObject.Relation + "," +
                        clientObject.Contract.Value + "," +
                        clientObject.Contract.GuaranteedMoney + "," +
                        clientObject.Contract.Length + "," +
                        clientObject.Contract.YearOfSign + "," +
                        clientObject.Contract.GotMoney + "," +
                        clientObject.OverallSkill + "," +
                        clientObject.Prospectiveness + "," +
                        clientObject.AgentID + "," +
                        clientObject.ReadyStatus + "," +
                        clientObject.InjuryTime + "," +
                    "'" + clientObject.InjuryName + "'," +
                        isFavorite + "," +
                        clientObject.ContractWithAgent.DateEnd.Week + "," +
                        clientObject.ContractWithAgent.DateEnd.Year + "," +
                        clientObject.ContractWithAgent.Wage + ")";

        return query;
    }
}
