using UnityEngine;
using Zenject;

public class GeneratorsInstaller : MonoInstaller
{
    public override void InstallBindings()
    {
        Container.Bind<PlayerGenerator>().AsSingle();
        Container.Bind<FootballPlayersGenerator>().AsSingle();
        Container.Bind<AgentsGenerator>().AsSingle();
        Container.Bind<IAddingAthletesToDBController>().To<AddingAthletesToDBController>().AsSingle();
        Container.Bind<TeamsGenerator>().AsSingle();
        Container.Bind<FootballSeasonGenerator>().AsSingle();
    }
}