﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Teams{

    public static TeamDetails[] GetFootballTeams()
    {
        TeamDetails[] footballTeams = new TeamDetails[]
        {
            new TeamDetails("Buffalo","Football",79,75,0,"American","East"),
            new TeamDetails("Miami","Football",81,81,0,"American","East"),
            new TeamDetails("New England","Football",91,98,0,"American","East"),
            new TeamDetails("New York Greens","Football",76,74,0,"American","East"),

            new TeamDetails("Baltimore","Football",85,84,0,"American","North"),
            new TeamDetails("Cincinnati","Football",84,80,0,"American","North"),
            new TeamDetails("Cleveland","Football",75,72,0,"American","North"),
            new TeamDetails("Pittsburgh","Football",84, 95,0,"American","North"),

            new TeamDetails("Houston","Football",79,75,0,"American","South"),
            new TeamDetails("Indianapolis","Football",87,82,0,"American","South"),
            new TeamDetails("Jacksonville","Football",73,73,0,"American","South"),
            new TeamDetails("Tennessee","Football",72,70,0,"American","South"),

            new TeamDetails("Denver","Football",89, 91,0,"American","West"),
            new TeamDetails("Kansas City","Football",82,76,0,"American","West"),
            new TeamDetails("Los Angeles Blues","Football",82,79,0,"American","West"),
            new TeamDetails("Oakland","Football",80,80,0,"American","West"),

            new TeamDetails("Dallas","Football",86, 93,0,"National","East"),
            new TeamDetails("New York Blues","Football",79,89,0,"National","East"),
            new TeamDetails("Philadelphia","Football",84,87,0,"National","East"),
            new TeamDetails("Washington","Football",77,77,0,"National","East"),

            new TeamDetails("Chicago","Football",75,80,0,"National","North"),
            new TeamDetails("Detroit","Football",83,83,0,"National","North"),
            new TeamDetails("Green Bay","Football",90,99,0,"National","North"),
            new TeamDetails("Minnesota","Football",77,76,0,"National","North"),

            new TeamDetails("Atlanta","Football",77,81,0,"National","South"),
            new TeamDetails("Carolina","Football",80,83,0,"National","South"),
            new TeamDetails("New Orleans","Football",78,80,0,"National","South"),
            new TeamDetails("Tamba Bay","Football",76,72,0,"National","South"),

            new TeamDetails("Arizona","Football",82,78,0,"National","West"),
            new TeamDetails("Los Angeles Golds","Football",78,71,0,"National","West"),
            new TeamDetails("San Francisco","Football",78,85,0,"National","West"),
            new TeamDetails("Seattle","Football",91,90,0,"National","West")
        };

        return footballTeams;
    }

    public static TeamDetails[] GetNumberRandomTeams(int numberOfTeams, TeamDetails[] teamsList)
    {
        TeamDetails[] listOfSelectedTeams = new TeamDetails[numberOfTeams];
        for (int i = 0; i < numberOfTeams; i++)
            listOfSelectedTeams[i] = teamsList[Random.Range(0, teamsList.Length)];

        return listOfSelectedTeams;
    }
}
