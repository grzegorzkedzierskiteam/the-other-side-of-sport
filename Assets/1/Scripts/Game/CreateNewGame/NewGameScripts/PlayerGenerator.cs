﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayerGenerator
{
    private IDBConnection dbConnection;

    //player's data
    private string firstName;
    private string lastName;
    private string selectedCharacter;

    private int wealth;
    private int popularity;
    private int respectability;
    private string allies;
    private string enemies;
    private int energy;
    private int health;
    private string clients;

    private int numberOfAgentsFriends;
    private int numberOfPlayersFriends;
    private int numberOfTeamsFriends;
    private string queryUpdate = "";


    public PlayerObject CreateNewPlayer(string newFirstName, string newLastName, Gender gender,string newSelectedCharacter, IDBConnection _dbConnection)
    {
        dbConnection = _dbConnection;
        firstName = newFirstName;
        lastName = newLastName;
        selectedCharacter = newSelectedCharacter;
        SetStats(selectedCharacter);
        PlayerObject newPlayer = new PlayerObject(firstName, lastName, gender, wealth, popularity, respectability, allies, enemies, energy, health, clients);
        return newPlayer;
    }

    private void SetStats(string selectedCharacter)
    {
        switch (selectedCharacter)
        {
            case "JuniorAgent":
                wealth = 75000;
                popularity = (int)Random.Range(0, 10);
                respectability = (int)Random.Range(0, 10);
                allies = "work in progress";
                enemies = "work in progress";
                energy = 100;
                health = 100;
                clients = "";
                break;

            case "ExperiencedAgent":
                wealth = (int)Random.Range(100000, 500000);
                popularity = (int)Random.Range(75, 90);
                respectability = (int)Random.Range(60, 90);
                allies = "work in progress";
                enemies = "work in progress";
                energy = 100;
                health = (int)Random.Range(50, 85);
                clients = "";
                break;

            case "FormerAthlete":
                wealth = (int)Random.Range(7500, 1000000);
                popularity = (int)Random.Range(60, 100);
                respectability = (int)Random.Range(40, 85);
                allies = "work in progress";
                enemies = "work in progress";
                energy = 100;
                health = (int)Random.Range(20, 75);
                clients = "";
                break;

            case "JetSetter":
                wealth = (int)Random.Range(400000, 10000000);
                popularity = (int)Random.Range(0, 10);
                respectability = (int)Random.Range(0, 10);
                allies = "work in progress";
                enemies = "work in progress";
                energy = 100;
                health = (int)Random.Range(50, 95);
                clients = "";
                break;

        }
    }

    public void SetRelations()
    {
        List<int> alliesList = new List<int>();
        List<int> enemiesList = new List<int>();

        numberOfAgentsFriends = SetNumberOfAgentsFriends();
        numberOfPlayersFriends = SetNumberOfPlayersFriends();
        numberOfTeamsFriends = SetNumberOfTeamsFriends();
        int[] agentsFirends = ArrayMethods.RandomIntArrayRange(2, 101, numberOfAgentsFriends);
        int[] playersFirends = ArrayMethods.RandomIntArrayRange(1, 1472, numberOfPlayersFriends);
        int[] teamsFirends = ArrayMethods.RandomIntArrayRange(1, 32, numberOfTeamsFriends);

        for (int i = 0; i < numberOfAgentsFriends; i++)
        {
            int newRelation = Random.Range(1, 101);
            queryUpdate += "UPDATE agents SET Relation = " + newRelation + " WHERE ID = " + agentsFirends[i] + ";";

            if (newRelation < 30)
                enemiesList.Add(agentsFirends[i]);
            else if (newRelation > 60)
                alliesList.Add(agentsFirends[i]);
        }

        for (int i = 0; i < numberOfPlayersFriends; i++)
        {
            int newRelation = Random.Range(1, 101);
            queryUpdate += "UPDATE players SET Relation = " + newRelation + " WHERE ID = " + playersFirends[i] + ";";
        }

        for (int i = 0; i < numberOfTeamsFriends; i++)
        {
            int newRelation = Random.Range(1, 101);
            queryUpdate += "UPDATE teams SET Relation = " + newRelation + " WHERE ID = " + teamsFirends[i] + ";";
        }

        queryUpdate += GetQueryAlliesEnemiesOfPlayer(alliesList, enemiesList);

        if(alliesList.Any())
            queryUpdate += GetQuerySetPlayerAsAlly(alliesList);
        if(enemiesList.Any())
            queryUpdate += GetQuerySetPlayerAsEnemy(enemiesList);

        dbConnection.ModifyQuery(queryUpdate);
    }

    private int SetNumberOfAgentsFriends()
    {
        switch (selectedCharacter)
        {
            case "JuniorAgent":
                return Random.Range(0, 4);
            case "ExperiencedAgent":
                return Random.Range(15, 23);
            case "FormerAthlete":
                return Random.Range(6, 20);
            case "JetSetter":
                return Random.Range(0, 30);
            default:
                return 0;
        }
    }

    private int SetNumberOfPlayersFriends()
    {
        switch (selectedCharacter)
        {
            case "JuniorAgent":
                return Random.Range(0, 10);
            case "ExperiencedAgent":
                return Random.Range(20, 50);
            case "FormerAthlete":
                return Random.Range(40, 100);
            case "JetSetter":
                return Random.Range(0, 40);
            default:
                return 0;
        }
    }

    private int SetNumberOfTeamsFriends()
    {
        switch (selectedCharacter)
        {
            case "JuniorAgent":
                return Random.Range(0, 2);
            case "ExperiencedAgent":
                return Random.Range(15, 25);
            case "FormerAthlete":
                return Random.Range(1, 13);
            case "JetSetter":
                return Random.Range(0, 33);
            default:
                return 0;
        }
    }

    private string GetQuerySetPlayerAsAlly(List<int> alliesList)
    {
        string query = "UPDATE agents SET Allies = Allies || '+1' WHERE ID IN(";

        foreach (int id in alliesList)
            query += id.ToString() + ",";

        query = query.Remove(query.Length - 1);
        query += ");";
        
        return query;
    }

    private string GetQuerySetPlayerAsEnemy(List<int> enemiesList)
    {
        string query = "UPDATE agents SET Enemies = Enemies || '+1' WHERE ID IN(";

        foreach (int id in enemiesList)
            query += id.ToString() + ",";

        query = query.Remove(query.Length - 1);
        query += ");";

        return query;
    }

    private string GetQueryAlliesEnemiesOfPlayer(List<int> alliesList, List<int> enemiesList)
    {
        string query = "UPDATE agents SET Allies = '";

        if (alliesList.Any())
        {
            foreach (int ally in alliesList)
                query += "+" + ally.ToString();
        }
        query += "', Enemies = '";

        if(enemiesList.Any())
        {
            foreach (int enemy in enemiesList)
                query += "+" + enemy.ToString();
        }

        query += "' WHERE ID = 1;";

        return query;
    }
}
