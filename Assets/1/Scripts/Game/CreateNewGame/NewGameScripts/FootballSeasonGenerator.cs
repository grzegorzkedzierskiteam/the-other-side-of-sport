﻿public class FootballSeasonGenerator {

    private IDBConnection dbConnection;

    private TeamDetails[] teams;
    private TeamDetails[] tempTeams;
    private TeamDetails[] conferenceTeams;

    private string query;

    private string[,,] firstHalfSchedule = new string[8, 16, 3];
    private string[,,] secondHalfSchedule = new string[8, 16, 3];

    private string[,] scheduleDivisional = new string[48,3];
    private string[,] scheduleConference = new string[80, 3];
    private string[,] scheduleNonConference = new string[80, 3];

    // Use this for initialization
    public FootballSeasonGenerator(IDBConnection _dbConnection)
    {
        dbConnection = _dbConnection;
        teams = Teams.GetFootballTeams();
        tempTeams = (TeamDetails[])teams.Clone();     
    }

    public void GenerateNewSeasonSchedule(int year)
    {
        SetQuery();
        GenerateDivisionGames();
        ShuffleDivisons();
        GenerateConferenceGames();
        GenerateNonConferenceGames();
        ScaleSchedules();

        AddScheduleToDB(year);
        AddEventsToDB(year);
    }

    private void SetQuery()
    {
        query = "INSERT INTO calendar(Year, Week, EventType, Sport, Info, Team1, Team2, MatchType, SeasonWeek, Finished, Team1Score, Team2Score) VALUES";
    }

    private void AddScheduleToDB(int year)
    {
        int seasonWeek = 1;
        int week = 37;
        for(int i = 0; i < 8; i++)
        {
            for(int k = 0; k < 16; k++)
            {
                query += "(" + year + ", " + week + ", 'Game', 'Football', '', '" + firstHalfSchedule[i,k,1] + "', '" + firstHalfSchedule[i, k, 2] + "', '" + firstHalfSchedule[i, k, 0] + "', " + seasonWeek + ",0,0,0 ),";
            }
            week += 1;
            seasonWeek += 1;
        }

        for (int i = 0; i < 8; i++)
        {
            for (int k = 0; k < 16; k++)
            {
                query += "(" + year + ", " + week + ", 'Game', 'Football', '', '" + secondHalfSchedule[i, k, 1] + "', '" + secondHalfSchedule[i, k, 2] + "', '" + secondHalfSchedule[i, k, 0] + "', " + seasonWeek + ",0,0,0 )";
                if (i == 7 && k == 15)
                    query += ";";
                else
                    query += ",";
            }
            week += 1;
            seasonWeek += 1;
        }

        dbConnection.ModifyQuery(query);
    }

    private void ScaleSchedules()
    {
        string[,,] divisionGames = DivisonGamesToWeeks(scheduleDivisional);
        string[,,] conferenceGames = NonDivisonGamesToWeeks(scheduleConference);
        string[,,] nonNonferenceGames = NonDivisonGamesToWeeks(scheduleNonConference);

        int[] selectedHalfConference = ArrayMethods.RandomIntArrayRange(1, 5, 5);
        int[] selectedHalfNonConference = ArrayMethods.RandomIntArrayRange(6, 10, 5);

        //Division
        for(int i = 0; i < 2; i++)
        {
            for (int k = 0; k < 3; k++)
            {
                if (i == 0)
                {
                    for(int numberOfGame = 0; numberOfGame < 16; numberOfGame++)
                    {
                        firstHalfSchedule[k, numberOfGame, 0] = "Regular";
                        firstHalfSchedule[k, numberOfGame, 1] = divisionGames[k, numberOfGame, 1];
                        firstHalfSchedule[k, numberOfGame, 2] = divisionGames[k, numberOfGame, 2];
                    }
                }
                else
                {
                    for (int numberOfGame = 0; numberOfGame < 16; numberOfGame++)
                    {
                        secondHalfSchedule[k, numberOfGame, 0] = "Regular";
                        secondHalfSchedule[k, numberOfGame, 1] = divisionGames[k, numberOfGame, 1];
                        secondHalfSchedule[k, numberOfGame, 2] = divisionGames[k, numberOfGame, 2];
                    }
                }
                    
            }
        }

        int firstHalfWeek = 3;
        int secondHalfWeek = 3;

        //Non Division
        for (int i = 0; i < 5; i++)
        {
            //conference
            if(selectedHalfConference[i] % 2 == 0)
            {
                for(int k = 0; k < 16; k++)
                {
                    firstHalfSchedule[firstHalfWeek, k, 0] = "Regular";
                    firstHalfSchedule[firstHalfWeek, k, 1] = conferenceGames[i, k, 1];
                    firstHalfSchedule[firstHalfWeek, k, 2] = conferenceGames[i, k, 2];
                }
                firstHalfWeek += 1;
            }
            else
            {
                for (int k = 0; k < 16; k++)
                {
                    secondHalfSchedule[secondHalfWeek, k, 0] = "Regular";
                    secondHalfSchedule[secondHalfWeek, k, 1] = conferenceGames[i, k, 1];
                    secondHalfSchedule[secondHalfWeek, k, 2] = conferenceGames[i, k, 2];
                }
                secondHalfWeek += 1;
            }

            //non conference
            if (selectedHalfNonConference[i] % 2 == 0)
            {
                for (int k = 0; k < 16; k++)
                {
                    firstHalfSchedule[firstHalfWeek, k, 0] = "Regular";
                    firstHalfSchedule[firstHalfWeek, k, 1] = nonNonferenceGames[i, k, 1];
                    firstHalfSchedule[firstHalfWeek, k, 2] = nonNonferenceGames[i, k, 2];
                }
                firstHalfWeek += 1;
            }
            else
            {
                for (int k = 0; k < 16; k++)
                {
                    secondHalfSchedule[secondHalfWeek, k, 0] = "Regular";
                    secondHalfSchedule[secondHalfWeek, k, 1] = nonNonferenceGames[i, k, 1];
                    secondHalfSchedule[secondHalfWeek, k, 2] = nonNonferenceGames[i, k, 2];
                }
                secondHalfWeek += 1;
            }
        }

        string[,,] tempFirstHalfSchedule = (string[,,])firstHalfSchedule.Clone();
        string[,,] tempSecondHalfSchedule = (string[,,])secondHalfSchedule.Clone();

        //Shuffle
        int[] newWeekFirstHalf = new int[] { 0, 1, 2, 3, 4, 5, 6, 7 };
        int[] newWeekSecondHalf = new int[] { 0, 1, 2, 3, 4, 5, 6, 7 };
        newWeekFirstHalf = ArrayMethods.ShuffleIntArray(newWeekFirstHalf);
        newWeekSecondHalf = ArrayMethods.ShuffleIntArray(newWeekSecondHalf);

        for (int i = 0; i < 8; i++)
        {
            for(int k = 0; k < 16; k++)
            {
                firstHalfSchedule[i, k, 0] = tempFirstHalfSchedule[newWeekFirstHalf[i], k, 0];
                firstHalfSchedule[i, k, 1] = tempFirstHalfSchedule[newWeekFirstHalf[i], k, 1];
                firstHalfSchedule[i, k, 2] = tempFirstHalfSchedule[newWeekFirstHalf[i], k, 2];
            }
        }

        for (int i = 0; i < 8; i++)
        {
            for (int k = 0; k < 16; k++)
            {
                secondHalfSchedule[i, k, 0] = tempSecondHalfSchedule[newWeekSecondHalf[i], k, 0];
                secondHalfSchedule[i, k, 1] = tempSecondHalfSchedule[newWeekSecondHalf[i], k, 1];
                secondHalfSchedule[i, k, 2] = tempSecondHalfSchedule[newWeekSecondHalf[i], k, 2];
            }
        }
    }

    private void GenerateDivisionGames()
    {
        int numberOfDivision = 0;
        //Division games
        for (int i = 0; i < 32; i += 4, numberOfDivision += 2)
        {
            int[] newPositions = new int[] { i, i+1, i+2, i+3 };
            newPositions = ArrayMethods.ShuffleIntArray(newPositions);
            teams[i] = tempTeams[newPositions[0]];
            teams[i + 1] = tempTeams[newPositions[1]];
            teams[i + 2] = tempTeams[newPositions[2]];
            teams[i + 3] = tempTeams[newPositions[3]];

            //one week
            scheduleDivisional[numberOfDivision, 0] = "Regular";
            scheduleDivisional[numberOfDivision, 1] = teams[i].Name;
            scheduleDivisional[numberOfDivision, 2] = teams[i + 1].Name;

            scheduleDivisional[numberOfDivision + 1, 0] = "Regular";
            scheduleDivisional[numberOfDivision + 1, 1] = teams[i + 2].Name;
            scheduleDivisional[numberOfDivision + 1, 2] = teams[i + 3].Name;
            //
            //second week
            scheduleDivisional[numberOfDivision + 16, 0] = "Regular";
            scheduleDivisional[numberOfDivision + 16, 1] = teams[i + 3].Name;
            scheduleDivisional[numberOfDivision + 16, 2] = teams[i].Name;

            scheduleDivisional[numberOfDivision + 17, 0] = "Regular";
            scheduleDivisional[numberOfDivision + 17, 1] = teams[i + 1].Name;
            scheduleDivisional[numberOfDivision + 17, 2] = teams[i + 2].Name;
            //
            //third week
            scheduleDivisional[numberOfDivision + 32, 0] = "Regular";
            scheduleDivisional[numberOfDivision + 32, 1] = teams[i + 3].Name;
            scheduleDivisional[numberOfDivision + 32, 2] = teams[i + 1].Name;

            scheduleDivisional[numberOfDivision + 33, 0] = "Regular";
            scheduleDivisional[numberOfDivision + 33, 1] = teams[i].Name;
            scheduleDivisional[numberOfDivision + 33, 2] = teams[i + 2].Name;
            //

        }
        tempTeams = (TeamDetails[])teams.Clone();

    }

    private void GenerateConferenceGames()
    {
        int matchNumber = 0;
        for (int shift = 4; shift < 9; shift++)
        {
            //AConference
            for(int i = 0; i < 8; i++)
            {
                int opponent = i + shift + 4;

                if (opponent > 15)
                    opponent = i + ((shift + 4) - 8);

                scheduleConference[matchNumber, 0] = "Regular";
                if (i % 2 == 0)
                {
                    scheduleConference[matchNumber, 1] = teams[i].Name;
                    scheduleConference[matchNumber, 2] = teams[opponent].Name;
                }
                else
                {
                    scheduleConference[matchNumber, 1] = teams[opponent].Name;
                    scheduleConference[matchNumber, 2] = teams[i].Name;
                }
                matchNumber += 1;
            }

            //NConference
            for (int i = 16; i < 24; i++)
            {
                int opponent = i + shift + 4;

                if (opponent > 31)
                    opponent = i + ((shift + 4) - 8);

                scheduleConference[matchNumber, 0] = "Regular";
                if (i % 2 == 0)
                {
                    scheduleConference[matchNumber, 1] = teams[i].Name;
                    scheduleConference[matchNumber, 2] = teams[opponent].Name;
                }
                else
                {
                    scheduleConference[matchNumber, 1] = teams[opponent].Name;
                    scheduleConference[matchNumber, 2] = teams[i].Name;
                }
                matchNumber += 1;
            }
        }
    }

    private void GenerateNonConferenceGames()
    {
        int matchNumber = 0;
        for (int shift = 8; shift < 13; shift++)
        {
            //AConference
            for (int i = 0; i < 16; i++)
            {
                int opponent = i + shift + 8;

                if (opponent > 31)
                    opponent = i + ((shift + 8) - 16);

                scheduleConference[matchNumber, 0] = "Regular";
                if (i % 2 == 0)
                {
                    scheduleNonConference[matchNumber, 1] = teams[i].Name;
                    scheduleNonConference[matchNumber, 2] = teams[opponent].Name;
                }
                else
                {
                    scheduleNonConference[matchNumber, 1] = teams[opponent].Name;
                    scheduleNonConference[matchNumber, 2] = teams[i].Name;
                }
                matchNumber += 1;
            }
        }
    }

    private void ShuffleDivisons()
    {
        int[] aConference = new int[] { 0, 1, 2, 3 };
        int[] nConference = new int[] { 4, 5, 6, 7 };
        aConference = ArrayMethods.ShuffleIntArray(aConference);
        nConference = ArrayMethods.ShuffleIntArray(nConference);

        int numberOfDivision = 0;
        for(int i = 0; i < 32; i += 4, numberOfDivision += 1)
        {
            if (numberOfDivision == 4)
                numberOfDivision = 0;
            if(i < 16)
            {
                teams[i] = tempTeams[aConference[numberOfDivision] * 4];
                teams[i + 1] = tempTeams[aConference[numberOfDivision] * 4 + 1];
                teams[i + 2] = tempTeams[aConference[numberOfDivision] * 4 + 2];
                teams[i + 3] = tempTeams[aConference[numberOfDivision] * 4 + 3];
            }
            else
            {
                teams[i] = tempTeams[nConference[numberOfDivision] * 4];
                teams[i + 1] = tempTeams[nConference[numberOfDivision] * 4 + 1];
                teams[i + 2] = tempTeams[nConference[numberOfDivision] * 4 + 2];
                teams[i + 3] = tempTeams[nConference[numberOfDivision] * 4 + 3];
            }
        }
    }

    private string[,,] DivisonGamesToWeeks(string[,] divisionSchedule)
    {
        string[,,] divisionGamesPerWeek = new string[3, 16, 3];
        int week = 0;
        int gamePerWeek = 0;

        for (int i = 0; i < 48; i++)
        {
            if (gamePerWeek == 16)
            {
                week += 1;
                gamePerWeek = 0;
            }

            divisionGamesPerWeek[week, gamePerWeek, 0] = "Regular";
            divisionGamesPerWeek[week, gamePerWeek, 1] = divisionSchedule[i, 1];
            divisionGamesPerWeek[week, gamePerWeek, 2] = divisionSchedule[i, 2];
            gamePerWeek += 1;
        }

        return divisionGamesPerWeek;
    }

    private string[,,] NonDivisonGamesToWeeks(string[,] nonDivisionSchedule)
    {
        string[,,] nonDivisionGamesPerWeek = new string[5, 16, 3];
        int week = 0;
        int gamePerWeek = 0;

        for (int i = 0; i < 80; i++)
        {
            if (gamePerWeek == 16)
            {
                week += 1;
                gamePerWeek = 0;
            }

            nonDivisionGamesPerWeek[week, gamePerWeek, 0] = "Regular";
            nonDivisionGamesPerWeek[week, gamePerWeek, 1] = nonDivisionSchedule[i, 1];
            nonDivisionGamesPerWeek[week, gamePerWeek, 2] = nonDivisionSchedule[i, 2];
            gamePerWeek += 1;
        }

        return nonDivisionGamesPerWeek;
    }

    //events

    private void AddEventsToDB(int year)
    {
        //playoffs
        string defaultQuery = "INSERT INTO calendar(Year, Week, SeasonWeek, EventType, Sport, Info, Team1, Team2, MatchType, Finished, Team1Score, Team2Score) VALUES";
        string newQuery = defaultQuery + "(" + year + ", 52, 16, 'Event', 'Football', 'End of Regular Season.','','','',0,0,0);";
        dbConnection.ModifyQuery(newQuery);

        //create playoff games slots
        defaultQuery = "INSERT INTO calendar(Year, Week, EventType, Sport, Info, Team1, Team2, MatchType, SeasonWeek, Finished, Team1Score, Team2Score) VALUES";
        query = defaultQuery + "(" + (year + 1).ToString() + ",1,'Game','Football','','','','Wild Card Round',-2,0,0,0),";
        query += "(" + (year + 1).ToString() + ",1,'Game','Football','','','','Wild Card Round',-2,0,0,0),";
        query += "(" + (year + 1).ToString() + ",1,'Game','Football','','','','Wild Card Round',-2,0,0,0),";
        query += "(" + (year + 1).ToString() + ",1,'Game','Football','','','','Wild Card Round',-2,0,0,0),";
        query += "(" + (year + 1).ToString() + ",2,'Game','Football','','','','Division Round',-2,0,0,0),";
        query += "(" + (year + 1).ToString() + ",2,'Game','Football','','','','Division Round',-2,0,0,0),";
        query += "(" + (year + 1).ToString() + ",2,'Game','Football','','','','Division Round',-2,0,0,0),";
        query += "(" + (year + 1).ToString() + ",2,'Game','Football','','','','Division Round',-2,0,0,0),";
        query += "(" + (year + 1).ToString() + ",3,'Game','Football','','','','Conference Final',-2,0,0,0),";
        query += "(" + (year + 1).ToString() + ",3,'Game','Football','','','','Conference Final',-2,0,0,0),";
        query += "(" + (year + 1).ToString() + ",4,'Event','Football','Road to Football Grand Final','','','',-2,0,0,0),";
        query += "(" + (year + 1).ToString() + ",5,'Game','Football','','','','Football Grand Final',-2,0,0,0);";
        dbConnection.ModifyQuery(query);

        //end of wild cards games
        defaultQuery = "INSERT INTO calendar(Year, Week, SeasonWeek, EventType, Sport, Info, Team1, Team2, MatchType, Finished, Team1Score, Team2Score) VALUES";
        newQuery = defaultQuery + "(" + (year + 1).ToString() + ", 1, -2, 'Event', 'Football', 'End of Wild Cards Games.','','','',0,0,0);";
        dbConnection.ModifyQuery(newQuery);

        //end of division games
        defaultQuery = "INSERT INTO calendar(Year, Week, SeasonWeek, EventType, Sport, Info, Team1, Team2, MatchType, Finished, Team1Score, Team2Score) VALUES";
        newQuery = defaultQuery + "(" + (year + 1).ToString() + ", 2, -2, 'Event', 'Football', 'End of Division Games.','','','',0,0,0);";
        dbConnection.ModifyQuery(newQuery);

        //end of conference games
        defaultQuery = "INSERT INTO calendar(Year, Week, SeasonWeek, EventType, Sport, Info, Team1, Team2, MatchType, Finished, Team1Score, Team2Score) VALUES";
        newQuery = defaultQuery + "(" + (year + 1).ToString() + ", 3, -2, 'Event', 'Football', 'End of Conference Games.','','','',0,0,0);";
        dbConnection.ModifyQuery(newQuery);

        //start new season
        defaultQuery = "INSERT INTO calendar(Year, Week, SeasonWeek, EventType, Sport, Info, Team1, Team2, MatchType, Finished, Team1Score, Team2Score) VALUES";
        newQuery = defaultQuery + "(" + (year + 1).ToString() + ", 32, -1, 'Event', 'Football', 'Start new football season.','','','',0,0,0);";
        dbConnection.ModifyQuery(newQuery);

        //end the season
        defaultQuery = "INSERT INTO calendar(Year, Week, SeasonWeek, EventType, Sport, Info, Team1, Team2, MatchType, Finished, Team1Score, Team2Score) VALUES";
        newQuery = defaultQuery + "(" + (year + 1).ToString() + ", 5, -2, 'Event', 'Football', 'End of the season.','','','',0,0,0);";
        dbConnection.ModifyQuery(newQuery);

        //draft
        defaultQuery = "INSERT INTO calendar(Year, Week, SeasonWeek, EventType, Sport, Info, Team1, Team2, MatchType, Finished, Team1Score, Team2Score) VALUES";
        newQuery = defaultQuery + "(" + (year + 1).ToString() + ", 17, -1, 'Event', 'Football', 'Football Draft.','','','',0,0,0);";
        dbConnection.ModifyQuery(newQuery);
    }
}
