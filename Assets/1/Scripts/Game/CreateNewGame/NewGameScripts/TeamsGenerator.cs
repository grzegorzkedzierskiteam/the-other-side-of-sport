﻿public class TeamsGenerator
{

    private IDBConnection dbConnection;

    private int relation = 50;
    private int wealth = 0;
    private ICharacter characterOfManager;

    public TeamsGenerator(IDBConnection _dbConnection)
    {
        dbConnection = _dbConnection;
    }

    public void GenerateNewTeams()
    {
        string queryInsertNewTeams = "INSERT INTO teams(Name, Sport, Character, Relation, Wealth, Popularity, OverallSkill, Wins, Loses, Draws, Conference, Division, DraftNumber) VALUES";
        //football
        TeamDetails[] footballTeams = Teams.GetFootballTeams();
        for(int i = 0; i < 32; i++)
        {
            characterOfManager = CharacterManager.GetRandomCharacter();
            //Create query
            queryInsertNewTeams += "( '" + footballTeams[i].Name + "', '" + footballTeams[i].Sport + "','" + characterOfManager.NameOfCharacter + "'," + relation + "," + wealth +
                        "," + footballTeams[i].Popularity + "," + footballTeams[i].Overall + ",0,0,0,'" + footballTeams[i].Conference + "','" + footballTeams[i].Division + "', 0)";

            if (i == 31)
                queryInsertNewTeams += ";";
            else
                queryInsertNewTeams += ",";
        }

        dbConnection.ModifyQuery(queryInsertNewTeams);
    }
}
