﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IAddingAthletesToDBController {
    void AddAthleteToDB(IClientObject clientObject);
    void CreateGroupToAddToDB();
    void AddAthleteToGroupToAddToDB(IClientObject clientObject);
    void CommitGroupToDB();
}
