﻿using UnityEngine;

public class FootballPlayersGenerator {

    private int creatingGameYear;

    private int[] overallSkill = new int[46];
    private TeamDetails[] footballTeams;

    private string[][] positions;
    private string[] defaultPositions = new string[] { "QB", "HB", "FB", "WR", "TE", "OL", "DL", "LB", "CB", "S", "P", "K" };

    private string firstName;
    private string lastName;
    private string college;
    private int age;
    private int popularity;
    private ICharacter character;
    private int relation;
    private int prospectiveness;
    private int wealth;
    private int dept;

    private IDBConnection dbConnection;
    private IAddingAthletesToDBController addingAthletesController;
    private Contract contract;
    private IContractMaker contractMaker;

    public FootballPlayersGenerator(IDBConnection dbConnection, IAddingAthletesToDBController addingAthletesController, IContractMaker contractMaker)
    {
        this.dbConnection = dbConnection;
        this.addingAthletesController = addingAthletesController;
        this.contractMaker = contractMaker;
        footballTeams = Teams.GetFootballTeams();
        SetPositions();
    }

    public void SetUp(int creatingGameYear)
    {
        this.creatingGameYear = creatingGameYear;
    }

    public void GenerateNewPlayers()
    {
        addingAthletesController.CreateGroupToAddToDB();
        for (int i = 0; i < 32; i++)
        {
            GenerateOverallPerPlayer(footballTeams[i].Overall);
            overallSkill = ArrayMethods.ShuffleIntArray(overallSkill);
            int selectedRoster = Random.Range(0, 5);
            
            for (int k = 0; k < 46; k++)
            {
                CreateNewPlayer(selectedRoster, k);
                IClientObject newPlayer = new ClientObject(0, firstName, lastName, "Football", positions[selectedRoster][k], college, footballTeams[i].Name, age, wealth, dept, popularity, character, relation, contract.Value, contract.GuaranteedMoney, contract.Length, contract.YearOfSign, contract.GotMoney, overallSkill[k], prospectiveness, -1, 0, "", 0, 1);

                addingAthletesController.AddAthleteToGroupToAddToDB(newPlayer);
            }
        }

        //Add players to the database
        addingAthletesController.CommitGroupToDB();
    }

    public void GenerateNewFreeAgents()
    {
        addingAthletesController.CreateGroupToAddToDB();

        for (int i = 0; i < 12; i++)
        {
            for(int k = 0; k < 20; k++)
            {
                CreateNewPlayer(i);
                int overallSkill = Random.Range(50, 100);
                
                IClientObject newPlayer = new ClientObject(0, firstName, lastName,"Football", defaultPositions[i], college, "None", age, wealth, dept, popularity, character, relation, contract.Value, contract.GuaranteedMoney, contract.Length, contract.YearOfSign, contract.GotMoney, overallSkill, prospectiveness, -1,0,"",0, 1);
                addingAthletesController.AddAthleteToGroupToAddToDB(newPlayer);
            }
        }

        //Add players to the database
        addingAthletesController.CommitGroupToDB();
    }

    public void GeneratePensioners()
    {
        addingAthletesController.CreateGroupToAddToDB();

        for (int i = 0; i < 123; i++)
        {
            int position = Random.Range(0, 12);
            CreateNewPlayer(position, "Retirement");
            int overallSkill = Random.Range(50, 100);
            IClientObject newPlayer = new ClientObject(0, firstName, lastName, "Football", defaultPositions[position], college, "Retirement", age, wealth, dept, popularity, character, relation, contract.Value, contract.GuaranteedMoney, contract.Length, contract.YearOfSign, contract.GotMoney, overallSkill, prospectiveness, -1, 0, "", 0, 1);
            addingAthletesController.AddAthleteToGroupToAddToDB(newPlayer);
        }

        //Add players to the database
        addingAthletesController.CommitGroupToDB();
    }

    //players per team
    private void CreateNewPlayer(int numberOfSelectedRoster, int numberOfPlayer)
    {
        firstName = PersonalDataGenerator.GenerateRandomName(Gender.Male);
        lastName = PersonalDataGenerator.GenerateRandomSurname();
        college = PersonalDataGenerator.GenerateRandomCollege();
        age = Random.Range(21, 37);
        popularity = Random.Range(4,11) * Sport.GetFootballPositionPopularity(positions[numberOfSelectedRoster][numberOfPlayer]);
        character = CharacterManager.GetRandomCharacter();
        relation = 50;
        wealth = PersonalDataGenerator.GenerateRandomWealth(positions[numberOfSelectedRoster][numberOfPlayer], age, "Active");
        if(wealth < 0)
        {
            dept = wealth * -1;
            wealth = 0;
        }
        else
            dept = 0;

        contract = contractMaker.GetRandomFootballContract(creatingGameYear,positions[numberOfSelectedRoster][numberOfPlayer],overallSkill[numberOfPlayer],prospectiveness);
        prospectiveness = Random.Range(1, 11);
    }

    //free agents
    private void CreateNewPlayer(int numberOfPlayer, string occupationalStatus = "Active")
    {
        if(occupationalStatus == "Active")
            age = Random.Range(21, 43);
        else if(occupationalStatus == "Retirement")
            age = Random.Range(43, 51);

        firstName = PersonalDataGenerator.GenerateRandomName(Gender.Male);
        lastName = PersonalDataGenerator.GenerateRandomSurname();
        college = PersonalDataGenerator.GenerateRandomCollege();
        
        popularity = Random.Range(4, 11) * Sport.GetFootballPositionPopularity(defaultPositions[numberOfPlayer]);
        character = CharacterManager.GetRandomCharacter();
        relation = 50;

        wealth = PersonalDataGenerator.GenerateRandomWealth(defaultPositions[numberOfPlayer], age, occupationalStatus);

        if (wealth < 0)
        {
            dept = wealth * -1;
            wealth = 0;
        }
        else
            dept = 0;

        contract = new Contract(0, 0, 0, 0, 0);
        prospectiveness = Random.Range(1, 11);
    }

    private void GenerateOverallPerPlayer(int overall)
    {
        for (int i = 0; i < overallSkill.Length; i += 2)
        {
            int random = Random.Range(0, MyMath.FindAmplitude(overall, 50, 99) + 1);
            overallSkill[i] = overall + random;
            overallSkill[i + 1] = overall - random;
        }
    }

    private void SetPositions()
    {
        positions = new string[][]
        {
            new string[]
            {
                "QB", "QB", "QB", "HB", "HB", "FB", "WR", "WR", "WR", "WR", "WR", "WR", "TE", "TE", "TE", "OL", "OL", "OL", "OL", "OL", "OL", "OL", "OL",
                "DL", "DL", "DL", "DL", "DL", "DL", "DL", "DL", "LB", "LB", "LB", "LB", "LB", "CB", "CB", "CB", "CB", "S", "S", "S", "S",
                "P", "K"
            },

            new string[]
            {
                "QB", "QB", "HB", "HB", "FB", "WR", "WR", "WR", "WR", "WR", "TE", "TE", "TE", "TE", "OL", "OL", "OL", "OL", "OL", "OL", "OL", "OL",
                "DL", "DL", "DL", "DL", "DL", "DL", "DL", "LB", "LB", "LB", "LB", "LB", "LB", "CB", "CB", "CB", "CB", "CB", "S", "S", "S",
                "P", "K", "K"
            },

            new string[]
            {
                "QB", "QB", "QB", "HB", "HB", "FB", "WR", "WR", "WR", "WR", "WR", "WR", "WR", "TE", "TE", "OL", "OL", "OL", "OL", "OL", "OL", "OL", "OL", "OL",
                "DL", "DL", "DL", "DL", "DL", "DL", "DL", "DL", "LB", "LB", "LB", "LB", "CB", "CB", "CB", "CB", "CB", "CB", "S", "S",
                "P", "K"
            },

            new string[]
            {
                "QB", "QB", "QB", "QB", "HB", "FB", "WR", "WR", "WR", "WR", "WR", "TE", "TE", "TE", "TE", "OL", "OL", "OL", "OL", "OL", "OL", "OL", "OL",
                "DL", "DL", "DL", "DL", "DL", "DL", "DL", "LB", "LB", "LB", "LB", "LB", "CB", "CB", "CB", "S", "S", "S", "S",
                "P", "K", "P", "K"
            },

            new string[]
            {
                "QB", "QB", "QB", "HB", "HB", "FB", "WR", "WR", "WR", "WR", "TE", "TE", "TE", "TE", "OL", "OL", "OL", "OL", "OL", "OL", "OL", "OL", "OL",
                "DL", "DL", "DL", "DL", "DL", "DL", "DL", "LB", "LB", "LB", "LB", "LB", "LB", "CB", "CB", "CB", "CB", "CB", "S", "S", "S",
                "P", "K"
            }
        };
    }
}
