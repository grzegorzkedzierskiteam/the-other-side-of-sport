﻿using Mono.Data.Sqlite;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgentsGenerator {

    private const int numberOfAgents = 99;

    private string firstName;
    private string lastName;
    private Gender gender;
    private int wealth;
    private int respectability;
    private int popularity;
    private int relation;
    private int energy = 100;

    private int numberOfClients;
    private string clients = "";
    private string allies;
    private string enemies;
    private ICharacter character;
 
    private List<string> alliesList;
    private List<string> enemiesList;

    SqliteDataReader reader;

    private IDBConnection dbConnection;

    public AgentsGenerator(IDBConnection _dbConnection)
    {
        dbConnection = _dbConnection;       
    }

    public void GenerateNewAgents()
    {
        alliesList = ResetGroup(numberOfAgents + 2);
        enemiesList = ResetGroup(numberOfAgents + 2);
        var query = "INSERT INTO agents(FirstName, LastName, Gender, Wealth, Popularity, Respectability, Allies, Enemies, Energy, Health, Clients, Character, Relation, IsFavorite) VALUES";
        SetAlliesAndEnemies();

        for(var i = 2; i <= numberOfAgents + 1; i++)
        {
            clients = "";

            if(i <= numberOfAgents / 10) // 1-10
            {
                wealth = Random.Range(100000000, 600000001);
                respectability = Random.Range(85, 100);
                popularity = Random.Range(85, 100);
                numberOfClients = Random.Range(15, 26);
            }

            if (i > numberOfAgents / 10 && i <= (numberOfAgents / 5) * 2) // 11-40
            {
                wealth = Random.Range(10000000, 100000001);
                respectability = Random.Range(65, 85);
                popularity = Random.Range(65, 85);
                numberOfClients = Random.Range(7, 21);
            }

            if (i > (numberOfAgents / 5) * 2 && i <= (numberOfAgents / 5) * 3) // 41-60
            {
                wealth = Random.Range(1000000, 10000001);
                respectability = Random.Range(45, 65);
                popularity = Random.Range(45, 65);
                numberOfClients = Random.Range(6, 18);
            }

            if (i > (numberOfAgents / 5) * 3 && i <= (numberOfAgents / 5) * 4) // 61-80
            {
                wealth = Random.Range(100000, 1000001);
                respectability = Random.Range(35, 45);
                popularity = Random.Range(35, 45);
                numberOfClients = Random.Range(3, 16);
            }

            if (i > (numberOfAgents / 5) * 4) // 81-100
            {
                wealth = Random.Range(50000, 100001);
                respectability = Random.Range(5, 35);
                popularity = Random.Range(5, 35);
                numberOfClients = Random.Range(2, 14);
            }

            CreateNewPerson();
            SetClients(i, numberOfClients);

            allies = alliesList[i];
            enemies = enemiesList[i];

            relation = 50;

            //Create query
            query += "( '" + firstName + "', '" + lastName + "', '" + gender.ToString() +"'," + wealth + "," + popularity + "," + respectability + ",'" + allies +
                        "','" + enemies + "'," + energy + ", 100, '" + clients + "','" + character.NameOfCharacter + "'," + relation + ",0)";

            if (i == numberOfAgents + 1)
                query += ";";
            else
                query += ",";
        }

        //Add agents to the database
        dbConnection.ModifyQuery(query);
    }

    private void CreateNewPerson()
    {
        var isItMale = Random.Range(0, 25);
        gender = isItMale < 23 ? Gender.Male : Gender.Female;

        firstName = PersonalDataGenerator.GenerateRandomName(gender);
        lastName = PersonalDataGenerator.GenerateRandomSurname();
        character = CharacterManager.GetRandomCharacter();
    }

    private void SetClients(int idAgent, int numberOfClients)
    {
        var queryUpdateAgentOfPlayer = "";
        string queryClients; 
        int playerId;

        if (idAgent <= numberOfAgents / 2) // 1-50
            queryClients = "SELECT ID FROM players WHERE OverallSkill > 80 AND Agent == -1 ORDER BY RANDOM() LIMIT 30";
        else
            queryClients = "SELECT ID FROM players WHERE OverallSkill <= 85 AND Agent == -1 ORDER BY RANDOM() LIMIT 30";

        reader = dbConnection.DownloadQuery(queryClients);

        for(var i = 0; i < numberOfClients; i++)
        {
            reader.Read();
            playerId = reader.GetInt32(0);
            clients += "+" + playerId.ToString();
            queryUpdateAgentOfPlayer += "UPDATE players SET Agent = " + idAgent + " WHERE ID == " + playerId + ";";
        }
        reader.Close();
        reader = null;

        dbConnection.ModifyQuery(queryUpdateAgentOfPlayer);
    }

    private void SetAlliesAndEnemies()
    {
        for (var i = 2; i <= numberOfAgents + 1; i++)
        {
            var numberOfAllies = Random.Range(0, 5);
            int allie;
            var numberOfEnemies = Random.Range(0, 5);
            int enemy;

            for (var k = 1; k <= numberOfAllies; k++)
            {
                do
                {
                    allie = Random.Range(2, 101);
                }
                while (allie == i || GroupStringManager.IsPersonInGroup(alliesList[i], allie.ToString())  || GroupStringManager.IsPersonInGroup(enemiesList[i],allie.ToString()));
                alliesList[i] += "+" + allie.ToString();
                alliesList[allie] += "+" + i.ToString();
            }

            for (var k = 1; k <= numberOfEnemies; k++)
            {
                do
                {
                    enemy = Random.Range(2, 101);
                }
                while (enemy == i || GroupStringManager.IsPersonInGroup(alliesList[i], enemy.ToString()) || GroupStringManager.IsPersonInGroup(enemiesList[i], enemy.ToString()));
                enemiesList[i] += "+" + enemy.ToString();
                enemiesList[enemy] += "+" + i.ToString();
            }
        }
    }

    private List<string> ResetGroup(int numberOfElements)
    {
        var group = new List<string>();
        for (var i = 0; i < numberOfElements; i++)
            group.Add("");

        return group;
    }
}