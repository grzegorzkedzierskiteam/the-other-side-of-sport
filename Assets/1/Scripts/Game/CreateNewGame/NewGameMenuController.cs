﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
// ReSharper disable InconsistentNaming

public class NewGameMenuController : Menu {

    public class Factory : PlaceholderFactory<NewGameMenuController> { }

    private NewGame newGame;

    private int step;

    //input data
    private string selectedCharacter;
    private Gender gender;
    private bool genderWasSelected;

    //loading panel
    public GameObject loadingPanelPrefab;
    private Text loadingTextInfo;

    //gender
    public GameObject genderPanel;
    public Button btnFemale;
    public Button btnMale;

	// first step 
	public GameObject nameSurnamePanel;
	public Button btnNext;
	public Button btnCancel;
	public InputField inputName;
	public InputField inputSurname;
	public GameObject notificationPrefab;

	// second step
	public Button btnNewGame;
	public GameObject chooseCharacterPanel;
	public Button btnJuniorAgent;
	public Button btnExperiencedAgent;
	public Button btnFormerAthlete;
	public Button btnJetSetter;

	public Color selectOptionColor;
	public Color nonSelectOptionColor;

    //Menu Game
    public GameObject menuGamePrefab;
    public GameObject energyBatteryPrefab;

    [Inject]
    public void Constructor(NewGame newGame)
    {
        this.newGame = newGame;
    }

	// Use this for initialization
	void Start () {
		btnCancel.onClick.AddListener(BtnCancel);
		btnNext.onClick.AddListener(BtnNext);
        btnNewGame.onClick.AddListener(BtnNewGame);

        btnFemale.onClick.AddListener(BtnFemale);
        btnMale.onClick.AddListener(BtnMale);

        btnJuniorAgent.onClick.AddListener(BtnJuniorAgent);
		btnExperiencedAgent.onClick.AddListener(BtnExperiencedAgent);
		btnFormerAthlete.onClick.AddListener(BtnFormerAthlete);
		btnJetSetter.onClick.AddListener(BtnJetSetter);

		btnNewGame.gameObject.SetActive(false);
		chooseCharacterPanel.SetActive(false);

        step = 1;
        genderWasSelected = false;
        //---------------------------------temporary
        inputName.text = "Grzegorz";
        inputSurname.text = "Kedzierski";
        BtnExperiencedAgent();
        BtnMale();
        //
    }

    private void BtnNewGame()
	{
		if(selectedCharacter == null)
		{
			var notificationText = "Please choose a character";
            MenuController.InitializeNotification(notificationText);
			return;
		}
       
        StartCoroutine(NewGame());
	}

    private IEnumerator NewGame()
    {
        var loadingPanel = MenuController.GetAndInitializeMenu(loadingPanelPrefab);
        loadingTextInfo = loadingPanel.transform.Find("TxtLoadingInfo").GetComponent<Text>();
        loadingTextInfo.text = "Create new player...";
        yield return null;

        newGame.SetUp(inputName.text, inputSurname.text, gender, selectedCharacter);

        newGame.dbConnection.OpenConnection();
        newGame.dbConnection.StartTransaction();

        newGame.BeginCreatingNewGame();
        newGame.AddNewPlayerToDB();
        newGame.CreateTablePlayers();
        newGame.CreateTableTeams();
        newGame.CreateTableCalendar();
        newGame.CreateTableFinance();
        newGame.CreateTableNotifications();
        newGame.CreateTableMessageResponses();
        yield return null;

        loadingTextInfo.text = "Generate new athletes...";
        newGame.footballPlayersGenerator.GenerateNewPlayers();
        yield return null;

        loadingTextInfo.text = "Generate new free agents...";
        newGame.footballPlayersGenerator.GenerateNewFreeAgents();
        newGame.footballPlayersGenerator.GeneratePensioners();
        yield return null;

        loadingTextInfo.text = "Generate new agents...";
        newGame.agentsGenerator.GenerateNewAgents();
        yield return null;

        loadingTextInfo.text = "Generate teams...";
        newGame.teamsGenerator.GenerateNewTeams();
        yield return null;

        loadingTextInfo.text = "Generate schedule...";
        newGame.footballSeasonGenerator.GenerateNewSeasonSchedule(2018);
        yield return null;

        newGame.playerGenerator.SetRelations();
        newGame.SetStartDateTime();
        newGame.FinishCreatingNewGame();

        newGame.dbConnection.CommitTransaction();
        newGame.dbConnection.CloseConnection();

        MenuController.DestroyPopMenu(loadingPanel);

        GoToMenuGame();
    }

    private void GoToMenuGame()
    {
        var mainMenuController = GameObject.Find("MainMenu(Clone)").GetComponent<MainMenuController>();
        mainMenuController.btnNewGame.interactable = true;
        mainMenuController.btnResumeGame.interactable = true;
        mainMenuController.btnSettings.interactable = true;
        mainMenuController.btnCredits.interactable = true;

        MenuController.InitializeMenu(MenuEnum.MenuGame);
        MenuController.InitializeMenu(energyBatteryPrefab);

        MenuController.DestroyPopMenu(gameObject);
    }

	private void BtnNext()
    {
        switch (step)
        {
            case 1:
                if(!GenderStep())
                    return;
                break;

            case 2:
                if (!NameAndSurnameStep())
                    return;
                break;
        } 

        if (step > 1)
            btnNext.gameObject.SetActive(false);

        step += 1;
    }

    private bool GenderStep()
    {
        if (!genderWasSelected)
        {
            var notificationText = "Please select your gender.";
            MenuController.InitializeNotification(notificationText);
            return false;
        }

        nameSurnamePanel.SetActive(true);
        genderPanel.SetActive(false);
        return true;
    }

    private bool NameAndSurnameStep()
    {
        if (inputName.text == "")
        {
            var notificationText = "Please enter a name.";
            MenuController.InitializeNotification(notificationText);
            return false;
        }

        if (inputSurname.text == "")
        {
            var notificationText = "Please enter a surname.";
            MenuController.InitializeNotification(notificationText);
            return false;
        }

        chooseCharacterPanel.SetActive(true);
        btnNewGame.gameObject.SetActive(true);
        nameSurnamePanel.SetActive(false);

        return true;
    }

    private void BtnCancel()
	{
		MainMenuController mainMenuController = GameObject.Find("MainMenu(Clone)").GetComponent<MainMenuController>();
		mainMenuController.btnNewGame.interactable = true;
		mainMenuController.btnResumeGame.interactable = true;
		mainMenuController.btnSettings.interactable = true;
		mainMenuController.btnCredits.interactable = true;
		Destroy(gameObject);
	}

	// select choose character
	private void BtnJuniorAgent()
	{
		selectedCharacter = "JuniorAgent";
		btnJuniorAgent.gameObject.GetComponent<Image>().color = selectOptionColor;
		btnExperiencedAgent.gameObject.GetComponent<Image>().color = nonSelectOptionColor;
		btnFormerAthlete.gameObject.GetComponent<Image>().color = nonSelectOptionColor;
		btnJetSetter.gameObject.GetComponent<Image>().color = nonSelectOptionColor;
	}

	private void BtnExperiencedAgent()
	{
		selectedCharacter = "ExperiencedAgent";
		btnJuniorAgent.gameObject.GetComponent<Image>().color = nonSelectOptionColor;
		btnExperiencedAgent.gameObject.GetComponent<Image>().color = selectOptionColor;
		btnFormerAthlete.gameObject.GetComponent<Image>().color = nonSelectOptionColor;
		btnJetSetter.gameObject.GetComponent<Image>().color = nonSelectOptionColor;
	}

	private void BtnFormerAthlete()
	{
		selectedCharacter = "FormerAthlete";
		btnJuniorAgent.gameObject.GetComponent<Image>().color = nonSelectOptionColor;
		btnExperiencedAgent.gameObject.GetComponent<Image>().color = nonSelectOptionColor;
		btnFormerAthlete.gameObject.GetComponent<Image>().color = selectOptionColor;
		btnJetSetter.gameObject.GetComponent<Image>().color = nonSelectOptionColor;
	}

	private void BtnJetSetter()
	{
		selectedCharacter = "JetSetter";
		btnJuniorAgent.gameObject.GetComponent<Image>().color = nonSelectOptionColor;
		btnExperiencedAgent.gameObject.GetComponent<Image>().color = nonSelectOptionColor;
		btnFormerAthlete.gameObject.GetComponent<Image>().color = nonSelectOptionColor;
		btnJetSetter.gameObject.GetComponent<Image>().color = selectOptionColor;
	}

    // select gender
    private void BtnFemale()
    {
        gender = Gender.Female;
        genderWasSelected = true;
        btnMale.gameObject.GetComponent<Image>().color = nonSelectOptionColor;
        btnFemale.gameObject.GetComponent<Image>().color = selectOptionColor;
    }

    private void BtnMale()
    {
        gender = Gender.Male;
        genderWasSelected = true;
        btnFemale.gameObject.GetComponent<Image>().color = nonSelectOptionColor;
        btnMale.gameObject.GetComponent<Image>().color = selectOptionColor;
    }
}
