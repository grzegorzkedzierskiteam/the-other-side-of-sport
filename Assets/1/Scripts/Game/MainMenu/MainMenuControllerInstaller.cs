using UnityEngine;
using Zenject;

public class MainMenuControllerInstaller : MonoInstaller<MainMenuControllerInstaller>
{
    public override void InstallBindings()
    {
        Container.Bind<IEngineRun>().To<EngineRun>().AsTransient();
        Container.Bind<CarStart>().AsTransient();
        Container.Bind<MenuOne>().AsSingle();
        Container.Bind<MenuThree>().AsSingle();
        Container.Bind<MenuTwo>().AsSingle();
    }
}