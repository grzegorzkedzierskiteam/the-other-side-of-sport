﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarStart
{
    private readonly IEngineRun engineRun;
    private MenuOne menuOne;

    public CarStart(IEngineRun engineRun, MenuOne menuOne)
    {
        this.engineRun = engineRun;
        this.menuOne = menuOne;
    }

    public void Run()
    {
        engineRun.Run();
        menuOne.Check();
    }
}

public class MenuOne
{
    private MenuTwo menu2;

    public MenuOne(MenuTwo menuTwo)
    {
        this.menu2 = menuTwo;
        menu2.Check();
    }

    public void Check()
    {
        Debug.Log("1");
    }
}

public class MenuTwo
{
    private MenuThree menu3;

    public MenuTwo(MenuThree menuThree)
    {
        this.menu3 = menuThree;
        menu3.Check();
    }

    public void Check()
    {
        Debug.Log("2");
    }
}

public class MenuThree
{
    public void Check()
    {
        Debug.Log("3");
    }
}