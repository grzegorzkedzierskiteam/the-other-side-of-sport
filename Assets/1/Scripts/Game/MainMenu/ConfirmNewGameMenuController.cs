﻿using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class ConfirmNewGameMenuController : Menu {

    public class Factory : PlaceholderFactory<ConfirmNewGameMenuController> { }

	public Button btnYes;
	public Button btnNo;

	private GameObject mainMenu;

	void Start()
	{
		btnYes.onClick.AddListener(BtnYes);
		btnNo.onClick.AddListener(BtnNo);
	}

	private void BtnYes()
	{
		MenuController.InitializePopMenu(MenuEnum.NewGameMenuInstaller);
		Destroy(gameObject);
	}

	private void BtnNo()
	{
		mainMenu = GameObject.Find("MainMenu(Clone)");
		MainMenuController mainMenuController = mainMenu.GetComponent<MainMenuController>();
		mainMenuController.btnNewGame.interactable = true;
		mainMenuController.btnResumeGame.interactable = true;
		mainMenuController.btnSettings.interactable = true;
		mainMenuController.btnCredits.interactable = true;
		Destroy(gameObject);
	}
	
	
}
