﻿using Mono.Data.Sqlite;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class MainMenuController : Menu {

    private IDBConnection dbConnection;

    private IMessageManagerDB messageManagerDb;

    public GameObject energyBatteryPrefab;

	public Button btnNewGame;
	public Button btnResumeGame;
	public Button btnSettings;
	public Button btnCredits;
    
    public class Factory : PlaceholderFactory<MainMenuController> { }

    [Inject]
    public void Constructor(IDBConnection dbConnection, IMessageManagerDB messageManagerDb)
    {
        this.dbConnection = dbConnection;
        this.messageManagerDb = messageManagerDb;
        MenuController.Push(gameObject);
    }

	// Use this for initialization
	void Start () {
        dbConnection.OpenConnection();

        //create config table db
        CreateConfigTableDB();

        //Ineractable ResumeButton
        btnResumeGame.interactable = IsTheGameCreated();

        btnSettings.onClick.AddListener(BtnSettings);
        btnCredits.onClick.AddListener(BtnCredits);
        dbConnection.CloseConnection();
    }

    private void BtnCredits()
    {

    }

    public void BtnNewGame()
	{
		MenuController.InitializePopMenu(MenuEnum.ConfirmNewGame);
		btnNewGame.interactable = false;
		btnResumeGame.interactable = false;
		btnSettings.interactable = false;
		btnCredits.interactable = false;
	}

    public void BtnResumeGame()
    {
        MenuController.InitializeMenu(MenuEnum.MenuGame);
        //var menuGameObject = menuController.GetAndInitializeMenu(menuGamePrefab);
        //menuGameObject.GetComponents<MenuGameController>().Initialize();
        MenuController.InitializeMenu(energyBatteryPrefab);
    }

    private void BtnSettings()
    {
        dbConnection.OpenConnection();

        MyDateTime myDateTime = new MyDateTime(1, 2018);

        string query = "SELECT ID FROM players ORDER BY RANDOM() LIMIT 2;";
        SqliteDataReader reader = dbConnection.DownloadQuery(query);
        reader.Read();
        int id1 = reader.GetInt32(0);
        reader.Read();
        int id2 = reader.GetInt32(0);

        reader.Close();

        messageManagerDb.SetUpDate(myDateTime);

        InjuriesController injuriesController = new InjuriesController(dbConnection);
        IMessageGenerator messageGenerator = new Messages.MessageGenerator(dbConnection, injuriesController);

        IPerson person1 = ClientData.GetAllDetails(dbConnection, id1);
        IPerson person2 = ClientData.GetAllDetails(dbConnection, id2);

        IMessage message1 = messageGenerator.GetMessage(person1, TypeOfMessage.PersonalProblems, myDateTime);
        IMessage message2 = messageGenerator.GetMessage(person2, TypeOfMessage.AccusationOfDomesticViolence, myDateTime);
        IMessage message3 = new Messages.Notification(4, "Powiadomienie", 6, 5, "Tester Tester", PersonType.Athlete, new MyDateTime(28, 2018));

        messageManagerDb.AddMessageToDB(dbConnection, message1);
        messageManagerDb.AddMessageToDB(dbConnection, message2);
        messageManagerDb.AddMessageToDB(dbConnection, message3);

        dbConnection.CloseConnection();

    }

    private void CreateConfigTableDB()
    {
        string query = @"CREATE TABLE IF NOT EXISTS config(  ID integer PRIMARY KEY AUTOINCREMENT,
                                        BeginCreatingGame int(3),
                                        FinishCreatingGame int(3),
                                        Week int(3),
                                        Year int(5)
                                      );";
        dbConnection.ModifyQuery(query);

        query = @"INSERT INTO config (BeginCreatingGame,FinishCreatingGame, Week, Year)
                  SELECT 0,0,0,0
                  WHERE NOT EXISTS(SELECT * FROM config); ";
        dbConnection.ModifyQuery(query);
    }

    private bool IsTheGameCreated()
    {
        string query = "SELECT BeginCreatingGame, FinishCreatingGame FROM config WHERE ID = 1;";
        SqliteDataReader reader = dbConnection.DownloadQuery(query);
        reader.Read();

        int checkSum = reader.GetInt32(0) + reader.GetInt32(1);
        
        reader.Close();
        reader = null;

        if (checkSum == 3)
            return true;
        else
            return false;
    }
}
