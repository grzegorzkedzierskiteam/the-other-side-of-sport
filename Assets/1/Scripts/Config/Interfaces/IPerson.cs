﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPerson {
    int ID { get; set; }
    string FirstName { get; set; }
    string LastName { get; set; }
    Gender Gender { get; set; }
    string FullName { get; }
    int Relation { get; set; }
    ICharacter Character { get; set; }
    PersonType Type { get; set; }
}