﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPersonAsFavorite {
    bool IsFavorite { get; set; }
    void SelectAsFavorite(IDBConnection dbConnection);
    void DeselectAsFavorite(IDBConnection dbConnection);
}
