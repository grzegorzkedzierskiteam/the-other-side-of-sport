﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IAgentObject : IPerson,IPersonAsFavorite
{
    int Wealth { get; set; }
    int Popularity { get; set; }
    int Respectability { get; set; }
    string Allies { get; set; }
    string Enemies { get; set; }
    int Energy { get; set; }
    string Clients { get; set; }
}
