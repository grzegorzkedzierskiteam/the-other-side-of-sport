﻿using Mono.Data.Sqlite;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using UnityEngine;

public interface IDBConnection
{
    ConnectionState GetConnectionStatus();
    void StartTransaction();
    void CommitTransaction();
    void OpenConnection();
    void CloseConnection();
    void ModifyQuery(string query);
    SqliteDataReader DownloadQuery(string query);
}
