﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IContractWithAgent {
    int Wage { get; set; }
    MyDateTime DateEnd { get; set; }
}
