﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IContract {
    int Value { get; set; }
    int Length { get; set; }
    int YearOfSign { get; set; }
    int GuaranteedMoney { get; set; } 
    int GotMoney { get; set; }
    int GetFullLengthOfContract(int currentYear);
}
