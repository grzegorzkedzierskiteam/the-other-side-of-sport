﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICharacter {
    string NameOfCharacter { get; set; }
}
