﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IClientObject : IPerson,IInjury,IPersonAsFavorite {
    string Sport { get; set; }
    string Position { get; set; }
    string College { get; set; }
    string Team { get; set; }
    int Age { get; set; }
    int Wealth { get; set; }
    int Dept { get; set; }
    int Popularity { get; set; }
    int OverallSkill { get; set; }
    int Prospectiveness { get; set; }
    int AgentID { get; set; }
    bool HasAgent { get; }
    int ReadyStatus { get; set; }
    Contract Contract { get; set; }
    ContractWithAgent ContractWithAgent { get; set; }
    void UpdateData(IDBConnection dBConnection);
}
