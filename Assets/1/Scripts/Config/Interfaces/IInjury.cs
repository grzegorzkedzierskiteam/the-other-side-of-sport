﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInjury {
    int InjuryTime { get; set; }
    string InjuryName { get; set; }
}
