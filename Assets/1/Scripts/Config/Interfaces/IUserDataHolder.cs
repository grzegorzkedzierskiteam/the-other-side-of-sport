﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IUserDataHolder {
    MyDateTime DateTime { get; set; }
    string FirstName { get; set; }
    string LastName { get; set; }
    int Wealth { get; set; }
    int Popularity { get; set; }
    int Respectability { get; set; }
    int Energy { get; set; }
    int Health { get; set; }
    string Allies { get; set; }
    string Enemies { get; set; }
    string Clients { get; set; }
    List<int> AlliesIDs { get; }
    List<int> EnemiesIDs { get; }
    List<int> ClientsIDs { get; }
    void SetAlliesIDs(string alliesString);
    void SetEnemiesIDs(string enemiesString);
    void SetClientsIDs(string clientsString);
}
