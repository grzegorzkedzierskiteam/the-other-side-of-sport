﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgentObject : IAgentObject{

    public AgentObject(int _iD, string _name, string _surname, Gender _gender, int _wealth, int _popularity, int _respectability, string _allies, string _enemies, int _energy, string _clients, int _relation, string _character, int _isFavorite)
    {
        FirstName = _name;
        LastName = _surname;
        Gender = _gender;
        Wealth = _wealth;
        Popularity = _popularity;
        Respectability = _respectability;
        Allies = _allies;
        Enemies = _enemies;
        Energy = _energy;
        ID = _iD;
        Clients = _clients;
        Relation = _relation;
        Character = CharacterManager.GetCharacterFromNameOfCharacter(_character);
        Type = PersonType.Agent;

        if (_isFavorite == 0)
            IsFavorite = false;
        else
            IsFavorite = true;
    }

    public string FirstName { get; set; }

    public string LastName { get; set; }

    public int Wealth { get; set; }

    public int Popularity { get; set; }

    public int Respectability { get; set; }

    public string Allies { get; set; }

    public string Enemies { get; set; }

    public int Energy { get; set; }

    public int ID { get; set; }

    public string Clients { get; set; }

    public int Relation { get; set; }

    public ICharacter Character { get; set; }

    public bool IsFavorite { get; set; }

    public PersonType Type { get; set; }

    public Gender Gender { get; set; }

    public string FullName => $"{FirstName} {LastName}";

    public void SelectAsFavorite(IDBConnection dbConnection)
    {
        string query = "UPDATE agents SET IsFavorite = 1 WHERE ID = " + ID;
        dbConnection.ModifyQuery(query);
    }

    public void DeselectAsFavorite(IDBConnection dbConnection)
    {
        string query = "UPDATE agents SET IsFavorite = 0 WHERE ID = " + ID;
        dbConnection.ModifyQuery(query);
    }
}
