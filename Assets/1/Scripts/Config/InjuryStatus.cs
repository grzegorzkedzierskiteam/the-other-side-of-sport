﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InjuryStatus {

    private int playerId;
    private string injuryName;
    private int injuryTime;

    public InjuryStatus(int _playerId, string _injuryName, int _injuryTime)
    {
        playerId = _playerId;
        injuryName = _injuryName;
        injuryTime = _injuryTime;
    }

    public InjuryStatus(string _injuryName, int _injuryTime)
    {
        playerId = 0;
        injuryName = _injuryName;
        injuryTime = _injuryTime;
    }

    public int PlayerId
    {
        get { return playerId; }
    }

    public string InjuryName
    {
        get { return injuryName; }
        set { injuryName = value; }
    }

    public int InjuryTime
    {
        get { return injuryTime; }
        set { injuryTime = value; }
    }

}
