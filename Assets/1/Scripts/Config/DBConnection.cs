﻿using System;
using System.Data;
using Mono.Data.Sqlite;
using System.IO;
using UnityEngine;

public class DBConnection : IDBConnection {

	private string nameDB = "DataBase.s3db";
	private SqliteConnection connDB;
	private SqliteTransaction transaction;

	public DBConnection()
	{
		connDB = StartConnection();
        SetDatabasePragma();
    }

	private string GetPathDB()
	{
		var path = Application.persistentDataPath + "/" + nameDB;
		if(!File.Exists(path))
		{
			var load = new WWW("jar://" + Application.dataPath + "!/assets/" + nameDB);
			while(!load.isDone){}
			File.WriteAllBytes(path, load.bytes);
		}

		return "URI=file:" + path;
	}

	private SqliteConnection StartConnection()
	{
		var connDB = new SqliteConnection(GetPathDB());
		connDB.Open();
        
        return connDB;
	}

    public ConnectionState GetConnectionStatus()
    {
        return connDB.State;
    }

	public void StartTransaction()
	{
		transaction = connDB.BeginTransaction();
	}

	public void CommitTransaction()
	{
		transaction.Commit();
        transaction.Dispose();
        transaction = null;
    }

    public void OpenConnection()
    {
        connDB = new SqliteConnection(GetPathDB());
        connDB.Open();
        SetDatabasePragma();
    }

    public void CloseConnection()
	{
		connDB.Close();
        //connDB = null;
    }

	public void ModifyQuery(string query)
	{
        if(connDB.State == ConnectionState.Closed)
            throw new Exception("DBConnection is closed.");
        var comm = connDB.CreateCommand();
        comm.CommandText = query;
        comm.ExecuteNonQuery();
        comm.Dispose();
        comm = null;
	}

	public SqliteDataReader DownloadQuery(string query)
	{     
        if (connDB.State == ConnectionState.Closed)
            throw new Exception("DBConnection is closed.");
        var comm = connDB.CreateCommand();
        comm.CommandText = query;
        var reader = comm.ExecuteReader();
        comm.Dispose();
        comm = null;

        return reader;
	}

    private void SetDatabasePragma()
    {
        var query = "";
        query += "PRAGMA temp_store = 2;";
        query += "PRAGMA synchronous = 0;";
        ModifyQuery(query);
    }
}
