using Messages;
using Zenject;

public class GameInstaller : MonoInstaller
{
    public MenuController menuController;

    public override void InstallBindings()
    {
        Container.Bind<IDBConnection>().To<DBConnection>().AsSingle();

        Container.BindInstance(menuController).AsSingle();
        Container.Bind<IMenuFactory>().To<MenuFactory>().AsSingle();

        Container.Bind<NewGame>().AsSingle();
        Container.Bind<IMessageManagerDB>().To<MessageManagerDB>().AsSingle();
        Container.Bind<IPlayerEnergy>().To<PlayerEnergy>().AsSingle();

        Container.Bind<IMessageManager>().To<MessageManager>().AsSingle();
        Container.Bind<IMessageEffects>().To<MessageEffects>().AsSingle();

        BindNextWeekManager();
    }

    private void BindNextWeekManager()
    {
        //Container.Bind<>().To<>().AsSingle();
        Container.Bind<IContractMaker>().To<ContractMaker>().AsSingle();
        Container.Bind<IAgentsContractsManager>().To<AgentsContractsManager>().AsSingle();
        Container.Bind<FootballTeamsActivitiesController>().AsSingle();
        Container.Bind<AgentsActivitiesController>().AsSingle();

        Container.Bind<IPensionersManager>().To<PensionersManager>().AsSingle();
        Container.Bind<FootballEventsController>().AsSingle();

        Container.Bind<IAthletesActivitiesController>().To<AthletesActivitiesController>().AsSingle();
        Container.Bind<InjuriesController>().AsSingle();

        Container.Bind<IMessageGenerator>().To<MessageGenerator>().AsSingle();
        Container.Bind<IFortuitousEventsGenerator>().To<FortuitousEventsGenerator>().AsSingle();
        Container.Bind<IFortuitousEventsManager>().To<FortuitousEventsManager>().AsSingle();
        Container.Bind<IDeptCollector>().To<DeptCollector>().AsSingle();
        Container.Bind<IPlayersStatsProgress>().To<PlayersStatsProgress>().AsSingle();
        Container.Bind<IMessageLifeController>().To<MessageLifeController>().AsSingle();
        Container.Bind<IEnergyManager>().To<EnergyManager>().AsSingle();
        Container.Bind<IAgentsWorldEvents>().To<AgentsWorldEvents>().AsSingle();
        Container.Bind<IClientsContractsEvents>().To<ClientsContractsEvents>().AsSingle();
        Container.Bind<WorldEventsController>().AsSingle();

        Container.Bind<IPlayerFinanceQueryMaker>().To<PlayerFinanceQueryMaker>().AsSingle();
        Container.Bind<IPlayerFinanceManager>().To<PlayerFinanceManager>().AsSingle();
        Container.Bind<ISportSalariesController>().To<FootballSalariesController>().AsSingle();
        Container.Bind<ISalariesController>().To<SalariesController>().AsSingle();

        Container.Bind<CalendarController>().AsSingle();

        Container.Bind<INextWeekManager>().To<NextWeekManager>().AsSingle();
    }
}