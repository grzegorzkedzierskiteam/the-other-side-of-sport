﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class PersonalDataGenerator
{
    private static string[] names = new string[]
    {
        "Jacob", "Michael", "Joshua", "Matthew", "Ethan", "Andrew", "Daniel", "Anthony", "Christopher", "Joseph", "William"
        , "Alexander", "Ryan", "David", "Nicholas", "Tyler", "James", "John", "Jonathan", "Nathan", "Samuel", "Christian", "Noah", "Dylan", "Benjamin", "Logan", "Brandon", "Gabriel", "Zachary"
        , "Jose", "Elijah", "Angel", "Kevin", "Jack", "Caleb", "Justin", "Austin", "Evan", "Robert", "Thomas", "Luke", "Mason", "Aidan", "Jackson", "Isaiah", "Jordan", "Gavin"
        , "Connor", "Aiden", "Isaac", "Jason", "Cameron", "Hunter", "Jayden", "Juan", "Charles", "Aaron", "Lucas", "Luis", "Owen", "Landon", "Diego", "Brian", "Adam", "Adrian"
        , "Kyle", "Eric", "Ian", "Nathaniel", "Carlos", "Alex", "Bryan", "Jesus", "Julian", "Sean", "Carter", "Hayden", "Jeremiah", "Cole", "Brayden", "Wyatt", "Chase", "Steven"
        , "Timothy", "Dominic", "Sebastian", "Xavier", "Jaden", "Jesse", "Devin", "Seth", "Antonio", "Richard", "Miguel", "Colin", "Cody", "Alejandro", "Caden", "Blake", "Carson", "Kaden"
        , "Jake", "Henry", "Liam", "Victor", "Riley", "Ashton", "Patrick", "Bryce", "Brady", "Vincent", "Trevor", "Tristan", "Mark", "Jeremy", "Oscar", "Marcus", "Parker", "Jorge"
        , "Jared", "Nicolas", "Omar", "Edward", "Alexis", "Braden", "Tanner", "Paul", "Eduardo", "Conner", "Alan", "Josiah", "Ivan", "Joel", "Garrett", "Kenneth", "Cooper", "Kaleb"
        , "Colton", "Grant", "Manuel", "Eli", "Micah", "Dakota", "Levi", "Gage", "Ayden", "Stephen", "Preston", "Nolan", "Collin", "Francisco", "Cristian", "Maxwell", "George", "Peyton"
        , "Derek", "Bradley", "Jeffrey", "Ty", "Fernando", "Shane", "Edgar", "Hector", "Max", "Javier", "Cesar", "Malachi", "Peter", "Emmanuel", "Andres", "Giovanni", "Ricardo", "Damian"
        , "Devon", "Trenton", "Marco", "Elias", "Wesley", "Leonardo", "Donovan", "Jonah", "Erik", "Johnathan", "Erick", "Dalton", "Mario", "Spencer", "Edwin", "Shawn", "Brendan", "Travis"
        , "Oliver", "Jace", "Andre", "Martin", "Roman", "Roberto", "Gregory", "Dillon", "Andy", "Raymond", "Damien", "Drew", "Sergio", "Josue", "Bryson", "Jaylen", "Abraham", "Brody"
        , "Julio", "Troy", "Landen", "Taylor", "Jaiden", "Cayden", "Dawson", "Colby", "Calvin", "Israel", "Pedro", "Clayton", "Avery", "Dominick", "Corey", "Jalen", "Harrison", "Miles"
        , "Marcos", "Griffin", "Johnny", "Brett", "Skyler", "Keegan", "Kai", "Payton", "Zane", "Mitchell", "Scott", "Frank", "Camden", "Ruben", "Dustin", "Rafael", "Jaxon", "Trey"
        , "Derrick", "Drake", "Raul", "Kaiden", "Gerardo"
    };

    private static string[] femaleNames = new string[]
    {
        "Emily", "Madison", "Emma", "Hannah", "Olivia", "Abigail", "Isabella", "Ashley", "Samantha", "Elizabeth", "Alexis", "Sarah", "Alyssa", "Grace", "Sophia", "Taylor", "Brianna", "Lauren"
        , "Ava", "Kayla", "Jessica", "Natalie", "Chloe", "Anna", "Victoria", "Hailey", "Mia", "Sydney", "Jasmine", "Morgan", "Julia", "Destiny", "Rachel", "Megan", "Kaitlyn", "Jennifer", "Savannah"
        , "Ella", "Alexandra", "Haley", "Allison", "Maria", "Nicole", "Mackenzie", "Brooke", "Monica", "Kaylee", "Lily", "Stephanie", "Andrea", "Faith", "Amanda", "Katelyn", "Kimberly", "Madaline"
        , "Gabrielle", "Zoe", "Trinity", "Alexa", "Mary", "Jenna", "Lillian", "Paige", "Kylie", "Gabriella", "Rebbeca", "Jordan", "Sara", "Addison", "Michelle", "Riley", "Vanessa", "Angelina", "Leah"
        , "Caroline", "Sofia", "Audrey", "Maya", "Avery", "Evelyn", "Autumn", "Amber", "Ariana", "Jocelyn", "Claire", "Jada", "Danielle", "Bailey", "Isabel", "Arianna", "Sierra", "Mariah", "Aaliyah"
        , "Melanie", "Erin", "Nevaeh", "Brooklyn", "Marissa", "Jacqueline"
    };

    private static string[] surnames = new string[]
    {
        "Martin", "Harris", "White", "Jackson", "Thomas", "Anderson", "Taylor", "Moore", "Wilson", "Miller", "Davis", "Brown", "Jones", "Williams", "Johnson", "Smith",
        "Lopez", "Wright", "King", "Hernandez", "Young", "Allen", "Hall", "Walker", "Lee", "Lewis", "Rodriguez", "Clark", "Robinson", "Martinez", "Garcia", "Thompson",
        "Evans", "Parker", "Campbell", "Phillips", "Turner", "Roberts", "Perez", "Mitchell", "Carter", "Nelson", "Gonzalez", "Baker", "Adams", "Green", "Scott", "Hill",
        "Cox", "Richardson", "Cooper", "Rivera", "Bailey", "Murphy", "Bell", "Morgan", "Cook", "Reed", "Rogers", "Morris", "Sanchez", "Stewart", "Collins", "Edwards",
        "Ross", "Barnes", "Wood", "Bennett", "Price", "Sanders", "Kelly", "Brooks", "Watson", "James", "Ramirez", "Gray", "Peterson", "Torres", "Ward", "Howard",
        "Alexander", "Bryant", "Gonzales", "Foster", "Simmons", "Butler", "Washington", "Flores", "Hughes", "Patterson", "Long", "Powell", "Perry", "Jenkins", "Coleman", "Henderson",
        "Reynolds", "Owens", "Jordan", "West", "Cole", "Woods", "Wallace", "Sullivan", "Graham", "Hamilton", "Ford", "Myers", "Hayes", "Diaz", "Griffin", "Russell",
        "Tucker", "Stevens", "Simpson", "Webb", "Wells", "Freeman", "Murray", "Gomez", "Ortiz", "Marshall", "Cruz", "McDonald", "Gibson", "Harrison", "Ellis", "Fisher",
        "Shaw", "Gordon", "Burns", "Reyes", "Ramos", "Dixon", "Warren", "Kennedy", "Morales", "Mason", "Boyd", "Henry", "Crawford", "Hicks", "Hunter", "Porter",
        "Dunn", "Hawkins", "Stone", "Rose", "Ferguson", "Knight", "Grant", "Nichols", "Mills", "Palmer", "Daniels", "Black", "Hunt", "Robertson", "Rice", "Holmes",
        "Carroll", "Olson", "Watkins", "Ray", "Willis", "Wagner", "Arnold", "Matthews", "Berry", "Pierce", "Payne", "Stephens", "Gardner", "Spencer", "Hudson", "Perkins",
        "Lawrence", "Greene", "Weaver", "Carpenter", "Armstrong", "Riley", "Fox", "Harper", "Ruiz", "Andrews", "Lane", "Bradley", "Cunningham", "Hart", "Snyder", "Duncan",
        "Wheeler", "Castillo", "Vasquez", "Carr", "Schmidt", "Ryan", "Gutierrez", "Fields", "Lawson", "Franklin", "Kelley", "Peters", "Austin", "Sims", "Chavez", "Elliott",
        "Garza", "Fernandez", "Hansen", "Morrison", "Alvarez", "Howell", "McCoy", "Bishop", "Meyer", "Banks", "Johnston", "Williamson", "Richards", "Montgomery", "Oliver", "Chapman",
        "Welch", "Romero", "Garrett", "Gilbert", "Dean", "Lynch", "Fuller", "Kim", "Reid", "Jacobs", "George", "Nguyen", "Stanley", "Burton", "Little", "Harvey",
        "Holland", "Pearson", "Silva", "Carlson", "Hoffman", "Brewer", "Fowler", "Medina", "Bowman", "Moreno", "Mendoza", "Day", "Hanson", "Burke", "Frazier", "Larson",
        "Caldwell", "Neal", "Curtis", "Walters", "Soto", "Wade", "Herrera", "Terry", "May", "Hopkins", "Davidson", "Byrd", "Vargas", "Jensen", "Fleming", "Douglas",
        "Craig", "Miles", "Lucas", "McKinney", "Gregory", "Sutton", "Castro", "Bush", "Barrett", "Shelton", "Horton", "Jimenez", "Graves", "Barnett", "Jennings", "Lowe",
        "Rodriquez", "Chambers", "Holt", "Lambert", "Fletcher", "Watts", "Newman", "Bates", "Beck", "Pena", "Rhodes", "Hale"
    };

    private static string[] colleges = new string[]
    {
        "Clemson", "Cincinnati", "Central Michigan", "California", "Buffalo", "Bowling Green", "Boston College", "Boise State", "Baylor", "Ball State", "BYU", "Auburn", "Army", "Arkansas State", "Arkansas", "Arizona State", "Arizona", "Alabama", "Akron", "Air Force",
        "Iowa", "Indiana", "Illinois", "Idaho", "Houston", "Hawaii", "Georgia Tech", "Georgia", "Georgia State", "Fresno State", "Florida State", "FAU", "Florida", "FIU", "Eastern Michigan", "ECU", "Duke", "UConn", "Colorado State", "Colorado",
        "Missouri", "Mississippi State", "Minnesota State", "Minnesota", "MTSU", "Michigan State", "Michigan", "Miami (OH)", "Miami (FL)", "Memphis", "Maryland", "Marshall", "Louisville", "La Tech", "LSU", "Kentucky", "Kent State", "Kansas State", "Kansas", "Iowa State",
        "Penn State", "Oregon State", "Oregon", "Ole Miss", "Old Dominion", "Oklahoma State", "Oklahoma", "Ohio State", "Ohio", "Notre Dame", "Northwestern", "NIU", "North Texas", "North Carolina", "New Mexico State", "New Mexico", "Nevada", "Nebraska", "Navy", "North Carolina State",
        "Toledo", "Texas Tech", "Texas State", "Texas A&M", "Texas", "Tennessee", "Temple", "TCU", "Syracuse", "Stanford", "Southern Miss", "South Alabama", "South Carolina", "San Jose", "San Diego State", "SMU", "Rutgers", "Rice", "Purdue", "Pittsburgh",
        "Wake Forest", "Virginia Tech", "Virginia", "Vanderbilt", "Utah State", "Utah", "UTSA", "UTEP", "USF", "USC", "UNLV", "UMass", "UL Monroe", "UL Lafayette", "UCLA", "UCF", "UAB", "Tulsa", "Tulane", "Troy",
        "Wyoming", "Wisconsin", "Western Michigan", "Western Kentucky", "West Virginia", "Washington State", "Washington"
    };

    public static string GenerateRandomName(Gender gender)
    {
        if (gender == Gender.Male)
        {
            var random = (int)Random.Range(0, names.Length - 1);
            return names[random];
        }
        else
        {
            var random = (int)Random.Range(0, femaleNames.Length - 1);
            return femaleNames[random];
        }
    }

    public static string GenerateRandomSurname()
    {
        var random = (int)Random.Range(0, surnames.Length - 1);
        return surnames[random];
    }

    public static string GenerateRandomCollege()
    {
        var random = (int)Random.Range(0, colleges.Length - 1);
        return colleges[random];
    }

    public static int GenerateRandomWealth(string position, int age, string occupationalStatus)
    {
        var wealth = 0;

        if (IsBankrupt(occupationalStatus))
        {
            if (IsInDept())
            {
                wealth -= GenerateRandomDebt();
                return wealth;
            }
            else
                return wealth;
        }
        else
        {
            if (position == "QB" || position == "TE")
                wealth = Random.Range(200000, 5000000);
            else if (position == "HB" || position == "FB" || position == "LB" || position == "CB" || position == "S" || position == "K" || position == "P")
                wealth = Random.Range(100000, 2000000);
            else if (position == "OL" || position == "DL")
                wealth = Random.Range(300000, 4000000);
            else if (position == "WR" || position == "FB")
                wealth = Random.Range(100000, 1500000);

            wealth *= Random.Range(1, age - 19);

            if (occupationalStatus == "Retirement")
                wealth = (int)(wealth * Random.Range(0.1f, 4f));

            return wealth;
        }
    }

    private static bool IsBankrupt(string occupationalStatus)
    {
        var isBankrupt = 0;
        if (occupationalStatus == "Retirement")
            isBankrupt = Random.Range(0, 13);
        else
            isBankrupt = Random.Range(0, 101);

        if (isBankrupt <= 7)
            return true;
        else
            return false;
    }

    private static bool IsInDept()
    {
        var isInDept = Random.Range(0, 11);
        if (isInDept < 4)
            return false;
        else
            return true;
    }

    private static int GenerateRandomDebt()
    {
        var howBigIsDept = Random.Range(0, 21);
        if (howBigIsDept > 19)
            return Random.Range(500000, 5000000);
        else if (howBigIsDept > 15)
            return Random.Range(10000, 100000);
        else if (howBigIsDept > 9)
            return Random.Range(5000, 500000);
        else if (howBigIsDept > 4)
            return Random.Range(1000, 250000);
        else
            return Random.Range(500, 100000);
    }
}
