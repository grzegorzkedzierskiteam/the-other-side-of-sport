﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public abstract class Menu : MonoBehaviour
{
    [Inject]
    protected MenuController MenuController { get; set; }
}
