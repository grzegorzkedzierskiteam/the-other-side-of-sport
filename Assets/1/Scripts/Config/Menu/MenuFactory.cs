﻿using System;
using UnityEngine;

public class MenuFactory : IMenuFactory
{
    private readonly NotificationLife.Factory notification;
    private readonly WarningController.Factory warning;
    private readonly MessageMenuController.Factory messageMenu;

    private readonly MainMenuController.Factory mainMenu;
    private readonly MenuGameController.Factory menuGame;
    private readonly ConfirmNextWeekController.Factory confirmNextWeek;
    private readonly SimulateMenuController.Factory simulateMenu;
    private readonly ConfirmSimulateWeeksController.Factory confirmSimulateWeeks;
    private readonly MessageMenuListItemController.Factory messageMenuItem;
    private readonly MessageMenuListItemNotificationController.Factory messageItemNotification;
    private readonly MessageDetailsMenuController.Factory messageItemDetailsMenu;
    private readonly RespondProceedMenuController.Factory respondProceedMenu;
    private readonly ProfileMenuController.Factory profileMenu;
    private readonly ClientsMenuController.Factory clientsMenu;
    private readonly ClientsItemController.Factory clientsListItem;
    private readonly ClientDetailsMenuController.Factory clientDetailsMenu;
    private readonly WealthDetailsMenuController.Factory clientWealthDetail;
    private readonly ContractWithYouMenuController.Factory clientContractWithYouDetail;
    private readonly ContractWithTeamMenuController.Factory clientContractWithTeamDetail;
    private readonly SigningPlayerAsClientMenuController.Factory signingPlayerMenu;
    private readonly AgentsMenuController.Factory agentsMenu;
    private readonly AgentsItemController.Factory agentsListItem;
    private readonly AgentDetailsMenuController.Factory agentDetailsMenu;
    private readonly AgentsDetailsItemController.Factory agentsDetailsItem;
    private readonly ResultsMenuController.Factory resultsMenu;
    private readonly ResultsMatchItemController.Factory resultsMatchItem;
    private readonly ResultsEventItemController.Factory resultsEventItem;
    private readonly StandingsMenuController.Factory standingsMenu;
    private readonly StandingsListElementController.Factory standingsListItem;
    private readonly TeamDetailsMenuController.Factory teamDetailsMenu;
    private readonly TeamRosterMenuController.Factory teamRosterMenu;
    private readonly TeamRosterItem.Factory teamRosterItem;
    private readonly ConfirmNewGameMenuController.Factory confirmNewGame;
    private readonly NewGameMenuController.Factory newGameMenu;

    public MenuFactory(
        NotificationLife.Factory notification,
        WarningController.Factory warning,
        MessageMenuController.Factory messageMenu,
        MainMenuController.Factory mainMenu,
        ConfirmNewGameMenuController.Factory confirmNewGame,
        NewGameMenuController.Factory newGameMenu,
        MenuGameController.Factory menuGame,
        ConfirmNextWeekController.Factory confirmNextWeek,
        SimulateMenuController.Factory simulateMenu,
        ConfirmSimulateWeeksController.Factory confirmSimulateWeeks,
        MessageMenuListItemController.Factory messageMenuItem,
        MessageMenuListItemNotificationController.Factory messageItemNotification,
        MessageDetailsMenuController.Factory messageItemDetailsMenu,
        RespondProceedMenuController.Factory respondProceedMenu,
        ProfileMenuController.Factory profileMenu,
        ClientsMenuController.Factory clientsMenu,
        ClientsItemController.Factory clientsListItem,
        ClientDetailsMenuController.Factory clientDetailsMenu,
        WealthDetailsMenuController.Factory clientWealthDetail,
        ContractWithYouMenuController.Factory clientContractWithYouDetail,
        ContractWithTeamMenuController.Factory clientContractWithTeamDetail,
        SigningPlayerAsClientMenuController.Factory signingPlayerMenu,
        AgentsMenuController.Factory agentsMenu,
        AgentsItemController.Factory agentsListItem,
        AgentDetailsMenuController.Factory agentDetailsMenu,
        AgentsDetailsItemController.Factory agentsDetailsItem,
        ResultsMenuController.Factory resultsMenu,
        ResultsMatchItemController.Factory resultsMatchItem,
        ResultsEventItemController.Factory resultsEventItem,
        StandingsMenuController.Factory standingsMenu,
        StandingsListElementController.Factory standingsListItem,
        TeamDetailsMenuController.Factory teamDetailsMenu,
        TeamRosterMenuController.Factory teamRosterMenu,
        TeamRosterItem.Factory teamRosterItem)
    {
        this.notification = notification;
        this.warning = warning;
        this.messageMenu = messageMenu;

        this.mainMenu = mainMenu;
        this.confirmNewGame = confirmNewGame;
        this.newGameMenu = newGameMenu;

        this.menuGame = menuGame;
        this.confirmNextWeek = confirmNextWeek;
        this.simulateMenu = simulateMenu;
        this.confirmSimulateWeeks = confirmSimulateWeeks;

        this.messageMenuItem = messageMenuItem;
        this.messageItemNotification = messageItemNotification;
        this.messageItemDetailsMenu = messageItemDetailsMenu;
        this.respondProceedMenu = respondProceedMenu;

        this.profileMenu = profileMenu;

        this.clientsMenu = clientsMenu;
        this.clientsListItem = clientsListItem;
        this.clientDetailsMenu = clientDetailsMenu;
        this.clientWealthDetail = clientWealthDetail;
        this.clientContractWithYouDetail = clientContractWithYouDetail;
        this.clientContractWithTeamDetail = clientContractWithTeamDetail;
        this.signingPlayerMenu = signingPlayerMenu;

        this.agentsMenu = agentsMenu;
        this.agentsListItem = agentsListItem;
        this.agentDetailsMenu = agentDetailsMenu;
        this.agentsDetailsItem = agentsDetailsItem;

        this.resultsMenu = resultsMenu;
        this.resultsMatchItem = resultsMatchItem;
        this.resultsEventItem = resultsEventItem;
        this.standingsMenu = standingsMenu;
        this.standingsListItem = standingsListItem;

        this.teamDetailsMenu = teamDetailsMenu;
        this.teamRosterMenu = teamRosterMenu;
        this.teamRosterItem = teamRosterItem;
    }

    public GameObject InitializeMenu(MenuEnum menuEnum, Transform mainCanvas)
    {
        GameObject menu;
        switch (menuEnum)
        {
            case MenuEnum.MainMenu:
                menu = mainMenu.Create().gameObject;
                menu.transform.SetParent(mainCanvas, false);
                return menu;

            case MenuEnum.ConfirmNewGame:
                menu = confirmNewGame.Create().gameObject;
                menu.transform.SetParent(mainCanvas, false);
                return menu;

            case MenuEnum.NewGameMenuInstaller:
                menu = newGameMenu.Create().gameObject;
                menu.transform.SetParent(mainCanvas, false);
                return menu;

            case MenuEnum.MenuGame:
                menu = menuGame.Create().gameObject;
                menu.transform.SetParent(mainCanvas, false);
                return menu;

            case MenuEnum.ConfirmNextWeek:
                menu = confirmNextWeek.Create().gameObject;
                menu.transform.SetParent(mainCanvas, false);
                return menu;

            case MenuEnum.SimulateMenu:
                menu = simulateMenu.Create().gameObject;
                menu.transform.SetParent(mainCanvas, false);
                return menu;

            case MenuEnum.ConfirmSimulateWeeks:
                menu = confirmSimulateWeeks.Create().gameObject;
                menu.transform.SetParent(mainCanvas, false);
                return menu;

            case MenuEnum.MessagesPanel:
                menu = messageMenu.Create().gameObject;
                menu.transform.SetParent(mainCanvas, false);
                return menu;

            case MenuEnum.MessageItemDetails:
                menu = messageItemDetailsMenu.Create().gameObject;
                menu.transform.SetParent(mainCanvas, false);
                return menu;

            case MenuEnum.RespondProceedMenu:
                menu = respondProceedMenu.Create().gameObject;
                menu.transform.SetParent(mainCanvas, false);
                return menu;

            case MenuEnum.ProfileMenu:
                menu = profileMenu.Create().gameObject;
                menu.transform.SetParent(mainCanvas, false);
                return menu;

            case MenuEnum.ClientsMenu:
                menu = clientsMenu.Create().gameObject;
                menu.transform.SetParent(mainCanvas, false);
                return menu;

            case MenuEnum.ClientDetailsMenu:
                menu = clientDetailsMenu.Create().gameObject;
                menu.transform.SetParent(mainCanvas, false);
                return menu;

            case MenuEnum.ClientWealthDetail:
                menu = clientWealthDetail.Create().gameObject;
                menu.transform.SetParent(mainCanvas, false);
                return menu;

            case MenuEnum.ClientContractWithYouDetail:
                menu = clientContractWithYouDetail.Create().gameObject;
                menu.transform.SetParent(mainCanvas, false);
                return menu;

            case MenuEnum.ClientContractWithTeamDetail:
                menu = clientContractWithTeamDetail.Create().gameObject;
                menu.transform.SetParent(mainCanvas, false);
                return menu;

            case MenuEnum.SigningPlayerMenu:
                menu = signingPlayerMenu.Create().gameObject;
                menu.transform.SetParent(mainCanvas, false);
                return menu;

            case MenuEnum.AgentsMenu:
                menu = agentsMenu.Create().gameObject;
                menu.transform.SetParent(mainCanvas, false);
                return menu;

            case MenuEnum.AgentDetailsMenu:
                menu = agentDetailsMenu.Create().gameObject;
                menu.transform.SetParent(mainCanvas, false);
                return menu;

            case MenuEnum.ResultsMenu:
                menu = resultsMenu.Create().gameObject;
                menu.transform.SetParent(mainCanvas, false);
                return menu;

            case MenuEnum.StandingsMenu:
                menu = standingsMenu.Create().gameObject;
                menu.transform.SetParent(mainCanvas, false);
                return menu;

            case MenuEnum.TeamDetailsMenu:
                menu = teamDetailsMenu.Create().gameObject;
                menu.transform.SetParent(mainCanvas, false);
                return menu;

            case MenuEnum.TeamRosterMenu:
                menu = teamRosterMenu.Create().gameObject;
                menu.transform.SetParent(mainCanvas, false);
                return menu;

            default:
                throw new Exception("Selected menu does not exist");
        }
    }

    public GameObject InitializeListItem(ListItem item, Transform mainCanvas)
    {
        GameObject listItem;
        switch (item)
        {
            case ListItem.ClientsListItem:
                listItem = clientsListItem.Create().gameObject;
                return listItem;

            case ListItem.MessageListItem:
                listItem = messageMenuItem.Create().gameObject;
                return listItem;

            case ListItem.MessageListItemNotification:
                listItem = messageItemNotification.Create().gameObject;
                return listItem;

            case ListItem.AgentsListItem:
                listItem = agentsListItem.Create().gameObject;
                return listItem;

            case ListItem.AgentsDetailsItem:
                listItem = agentsDetailsItem.Create().gameObject;
                return listItem;

            case ListItem.ResultsMatchItem:
                listItem = resultsMatchItem.Create().gameObject;
                return listItem;

            case ListItem.ResultsEventItem:
                listItem = resultsEventItem.Create().gameObject;
                return listItem;

            case ListItem.StandingListItem:
                listItem = standingsListItem.Create().gameObject;
                return listItem;

            case ListItem.TeamRosterItem:
                listItem = teamRosterItem.Create().gameObject;
                return listItem;

            default:
                throw new Exception("Selected list's item does not exist");
        }
    }

    public GameObject InitializeNotification(Transform mainCanvas)
    {
        var notificationPanel = this.notification.Create().gameObject;
        notificationPanel.transform.SetParent(mainCanvas, false);
        return notificationPanel;
    }

    public GameObject InitializeWarning(Transform mainCanvas)
    {
        var warningPanel = warning.Create().gameObject;
        warningPanel.transform.SetParent(mainCanvas, false);
        return warningPanel;
    }
}
