using UnityEngine;
using Zenject;

public class MenuControllerInstaller : MonoInstaller
{
    public GameObject notification;
    public GameObject warning;
    public GameObject messageMenu;
    public GameObject messageMenuItem;
    public GameObject messageMenuItemNotification;
    public GameObject messageMenuItemDetails;
    public GameObject respondMessageMenu;

    public GameObject mainMenuController;
    public GameObject confirmNewGameController;
    public GameObject newGameMenuController;

    public GameObject menuGameController;
    public GameObject confirmNextWeekController;
    public GameObject simulateMenu;
    public GameObject confirmSimulateWeeks;

    public GameObject profileMenuController;

    public GameObject clientsMenu;
    public GameObject clientsListItem;
    public GameObject clientDetailsMenu;
    public GameObject clientWealth;
    public GameObject signingPlayerMenu;
    public GameObject clientContractWithYouDetailMenu;
    public GameObject clientContractWithTeamDetailMenu;

    public GameObject agentsMenu;
    public GameObject agentsListItem;
    public GameObject agentDetailsMenu;
    public GameObject agentsDetailsListItem;

    public GameObject resultsMenu;
    public GameObject resultsMatchItem;
    public GameObject resultsEventItem;
    public GameObject standingsMenu;
    public GameObject standingsListItem;

    public GameObject teamDetailsMenu;
    public GameObject teamRosterMenu;
    public GameObject teamRosterItem;

    public override void InstallBindings()
    {
        Container.BindFactory<NotificationLife, NotificationLife.Factory>()
            .FromComponentInNewPrefab(notification);
        Container.BindFactory<WarningController, WarningController.Factory>()
            .FromComponentInNewPrefab(warning);

        Container.BindFactory<MessageMenuController, MessageMenuController.Factory>()
            .FromComponentInNewPrefab(messageMenu);
        Container.BindFactory<MessageMenuListItemController, MessageMenuListItemController.Factory>()
            .FromComponentInNewPrefab(messageMenuItem);
        Container.BindFactory<MessageDetailsMenuController, MessageDetailsMenuController.Factory>()
            .FromComponentInNewPrefab(messageMenuItemDetails);
        Container.BindFactory<MessageMenuListItemNotificationController, MessageMenuListItemNotificationController.Factory>()
            .FromComponentInNewPrefab(messageMenuItemNotification);
        Container.BindFactory<RespondProceedMenuController, RespondProceedMenuController.Factory>()
            .FromComponentInNewPrefab(respondMessageMenu);

        Container.BindFactory<MainMenuController, MainMenuController.Factory>()
            .FromComponentInNewPrefab(mainMenuController);
        Container.BindFactory<ConfirmNewGameMenuController, ConfirmNewGameMenuController.Factory>()
            .FromComponentInNewPrefab(confirmNewGameController);
        Container.BindFactory<NewGameMenuController, NewGameMenuController.Factory>()
            .FromComponentInNewPrefab(newGameMenuController);

        Container.BindFactory<MenuGameController, MenuGameController.Factory>()
            .FromComponentInNewPrefab(menuGameController);
        Container.BindFactory<ConfirmNextWeekController, ConfirmNextWeekController.Factory>()
            .FromComponentInNewPrefab(confirmNextWeekController);
        Container.BindFactory<SimulateMenuController, SimulateMenuController.Factory>()
            .FromComponentInNewPrefab(simulateMenu);
        Container.BindFactory<ConfirmSimulateWeeksController, ConfirmSimulateWeeksController.Factory>()
            .FromComponentInNewPrefab(confirmSimulateWeeks);

        Container.BindFactory<ProfileMenuController, ProfileMenuController.Factory>()
            .FromComponentInNewPrefab(profileMenuController);

        Container.BindFactory<ClientsMenuController, ClientsMenuController.Factory>()
            .FromComponentInNewPrefab(clientsMenu);
        Container.BindFactory<ClientsItemController, ClientsItemController.Factory>()
            .FromComponentInNewPrefab(clientsListItem);
        Container.BindFactory<ClientDetailsMenuController, ClientDetailsMenuController.Factory>()
            .FromComponentInNewPrefab(clientDetailsMenu);
        Container.BindFactory<WealthDetailsMenuController, WealthDetailsMenuController.Factory>()
            .FromComponentInNewPrefab(clientWealth);
        Container.BindFactory<ContractWithYouMenuController, ContractWithYouMenuController.Factory>()
            .FromComponentInNewPrefab(clientContractWithYouDetailMenu);
        Container.BindFactory<ContractWithTeamMenuController, ContractWithTeamMenuController.Factory>()
            .FromComponentInNewPrefab(clientContractWithTeamDetailMenu);
        Container.BindFactory<SigningPlayerAsClientMenuController, SigningPlayerAsClientMenuController.Factory>()
            .FromComponentInNewPrefab(signingPlayerMenu);

        Container.BindFactory<AgentsMenuController, AgentsMenuController.Factory>()
            .FromComponentInNewPrefab(agentsMenu);
        Container.BindFactory<AgentsItemController, AgentsItemController.Factory>()
            .FromComponentInNewPrefab(agentsListItem);
        Container.BindFactory<AgentDetailsMenuController, AgentDetailsMenuController.Factory>()
            .FromComponentInNewPrefab(agentDetailsMenu);
        Container.BindFactory<AgentsDetailsItemController, AgentsDetailsItemController.Factory>()
            .FromComponentInNewPrefab(agentsDetailsListItem);

        Container.BindFactory<ResultsMenuController, ResultsMenuController.Factory>()
            .FromComponentInNewPrefab(resultsMenu);
        Container.BindFactory<ResultsMatchItemController, ResultsMatchItemController.Factory>()
            .FromComponentInNewPrefab(resultsMatchItem);
        Container.BindFactory<ResultsEventItemController, ResultsEventItemController.Factory>()
            .FromComponentInNewPrefab(resultsEventItem);
        Container.BindFactory<StandingsMenuController, StandingsMenuController.Factory>()
            .FromComponentInNewPrefab(standingsMenu);
        Container.BindFactory<StandingsListElementController, StandingsListElementController.Factory>()
            .FromComponentInNewPrefab(standingsListItem);

        Container.BindFactory<TeamDetailsMenuController, TeamDetailsMenuController.Factory>()
            .FromComponentInNewPrefab(teamDetailsMenu);
        Container.BindFactory<TeamRosterMenuController, TeamRosterMenuController.Factory>()
            .FromComponentInNewPrefab(teamRosterMenu);
        Container.BindFactory<TeamRosterItem, TeamRosterItem.Factory>()
            .FromComponentInNewPrefab(teamRosterItem);
    }
}