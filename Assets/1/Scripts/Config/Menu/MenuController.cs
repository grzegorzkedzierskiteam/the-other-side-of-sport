﻿using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class MenuController : MonoBehaviour {

	public GameObject mainCanvas;
    private IMenuFactory menuFactory;
    public List<GameObject> menuCollector;

    [Inject]
    public void Constructor(IMenuFactory menuFactory)
    {
        this.menuFactory = menuFactory;
        menuCollector = new List<GameObject>();
    }

    public GameObject InitializeMenu(MenuEnum menu)
    {
        var menuObject = menuFactory.InitializeMenu(menu, mainCanvas.transform);
        Push(menuObject);
        return menuObject;
    }

    public GameObject InitializePopMenu(MenuEnum menu)
    {
        var menuObject = menuFactory.InitializeMenu(menu, mainCanvas.transform);
        return menuObject;
    }

    public GameObject InitializeListItem(ListItem item, Transform listContainer)
    {
        var listItem = menuFactory.InitializeListItem(item, mainCanvas.transform);
        listItem.transform.SetParent(listContainer);
        listItem.transform.localScale = Vector3.one;

        return listItem;
    }

    public void InitializeMenu(GameObject menu)
	{
		var newMenu = Instantiate(menu, menu.transform.position, menu.transform.rotation) as GameObject;
		newMenu.transform.SetParent(mainCanvas.transform,false);
	}

    public GameObject GetAndInitializeMenu(GameObject menu)
    {
        var newMenu = Instantiate(menu, menu.transform.position, menu.transform.rotation) as GameObject;
        newMenu.transform.SetParent(mainCanvas.transform, false);
        return newMenu;
    }

    public GameObject InitializeNotification(GameObject notification)
	{
		var newNotification = Instantiate(notification,notification.transform.position, notification.transform.rotation);
		newNotification.transform.SetParent(mainCanvas.transform,false);
		return newNotification;
	}

    public GameObject InitializeNotification(string text)
    {
        var notification = menuFactory.InitializeNotification(mainCanvas.transform);
        notification.GetComponent<NotificationLife>().SetNotificationText(text);
        return notification;
    }

    public void InitializeWarning(GameObject warningPrefab, string textOfWarning)
    {
        var newWarning = Instantiate(warningPrefab, warningPrefab.transform.position, warningPrefab.transform.rotation) as GameObject;
        newWarning.transform.SetParent(mainCanvas.transform, false);
        newWarning.GetComponent<WarningController>().txtWarningText.text = textOfWarning;
    }

    public void InitializeWarning(string textOfWarning)
    {
        var newWarning = menuFactory.InitializeWarning(mainCanvas.transform);
        newWarning.GetComponent<WarningController>().txtWarningText.text = textOfWarning;
    }

    public void DestroyMenu(string menuName)
	{
        Pop();
		Destroy(GameObject.Find(menuName));
	}

    public void DestroyMenu(GameObject menu)
    {
        Pop();
        Destroy(menu);
    }

    public void DestroyPopMenu(GameObject menu)
    {
        Destroy(menu);
    }

    public void SetActiveMenu(string menuName, bool active)
    {
        GameObject.Find(menuName).SetActive(active);
    }

    public void Push(GameObject menu)
    {
        if(menuCollector.Count > 0)
            menuCollector[menuCollector.Count - 1].SetActive(false);
        menuCollector.Add(menu);
    }

    public void Pop()
    {
        menuCollector.RemoveAt(menuCollector.Count - 1);
        menuCollector[menuCollector.Count - 1].SetActive(true);
    }
}
