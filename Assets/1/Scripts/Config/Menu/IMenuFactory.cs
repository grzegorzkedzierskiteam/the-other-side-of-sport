﻿using UnityEngine;

public interface IMenuFactory
{
    GameObject InitializeMenu(MenuEnum menuEnum, Transform mainCanvas);
    GameObject InitializeListItem(ListItem item, Transform mainCanvas);
    GameObject InitializeNotification(Transform mainCanvas);
    GameObject InitializeWarning(Transform mainCanvasTransform);
}
