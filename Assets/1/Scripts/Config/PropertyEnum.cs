﻿using UnityEngine;

public enum PropertyEnum
{
    Health,
    Money,
    Energy,
    Relation
}