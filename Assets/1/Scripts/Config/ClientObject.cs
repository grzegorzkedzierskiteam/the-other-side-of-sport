﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClientObject : IClientObject{

    public ClientObject(int _id, string _firstName, string _lastName, string _sport, string _position, string _college, string _team, int _age, int _wealth, int _dept, int _popularity, ICharacter _character, int _relation, int _contract, int _contractGuaranteedMoney, int _contractLenght, int _yearOfSign, int _gotMoney, int _overallSkill, int _prospectiveness, int _agentID, int _injuryTime, string _injuryName, int _isFavorite, int _readyStatus = 0, int _contractWithYouWage = 0, int _contractWithYouDateEndWeek = 1, int _contractWithYouDateEndYear = 1)
    {
        ID = _id;
        FirstName = _firstName;
        LastName = _lastName;
        Gender = Gender.Male;
        Sport = _sport;
        Position = _position;
        College = _college;
        Team = _team;
        Age = _age;
        Wealth = _wealth;
        Dept = _dept;

        Popularity = _popularity;
        Character = _character;
        Relation = _relation; 
        OverallSkill = _overallSkill;
        Prospectiveness = _prospectiveness;
        AgentID = _agentID;
        InjuryTime = _injuryTime;
        InjuryName = _injuryName;
        Type = PersonType.Athlete;
        Contract = new Contract(_contract, _contractLenght, _yearOfSign, _contractGuaranteedMoney, _gotMoney);
        ContractWithAgent = new ContractWithAgent(_contractWithYouWage, new MyDateTime(_contractWithYouDateEndWeek, _contractWithYouDateEndYear));
        
        ReadyStatus = _readyStatus;
        Activities = SetAthletesActivities();

        if (_isFavorite == 0)
            IsFavorite = false;
        else
            IsFavorite = true;

        if (Team == "Retirement" || Team == "None")
            HasTeam = false;
        else
            HasTeam = true;
    }

    private IAthletesActivities SetAthletesActivities()
    {
        if (Character == null)
            return new MelancholicActivities();
        else
            switch(Character.NameOfCharacter)
            {
                case "Born leader":
                    return new BornLeaderActivities();
                case "Melancholic":
                    return new MelancholicActivities();
                case "Phlegmatic":
                    return new PhlegmaticActivities();
                case "Sanguine":
                    return new SanguineActivities();
                default:
                    return null;
            }
    }
	
    public int ID { get; set; }

    public string FirstName { get; set; }

    public string LastName { get; set; }

    public string FullName
    {
        get { return FirstName + " " + LastName; }
    }

    public string Sport { get; set; }

    public string Position { get; set; }

    public string College { get; set; }

    public string Team { get; set; }

    public bool HasTeam { get; set; }

    public int Age { get; set; }

    public int Wealth { get; set; }

    public int Dept { get; set; }

    public int Popularity { get; set; }

    public ICharacter Character { get; set; }

    public int Relation { get; set; }

    public Contract Contract { get; set; }

    public ContractWithAgent ContractWithAgent { get; set; }

    public int OverallSkill { get; set; }

    public int Prospectiveness { get; set; }

    public int AgentID { get; set; }

    public int InjuryTime { get; set; }

    public string InjuryName { get; set; }

    public bool IsFavorite { get; set; }

    public PersonType Type { get; set; }

    public Gender Gender { get; set; }

    public bool HasAgent
    {
        get
        {
            if (AgentID == -1)
                return false;
            else
                return true;
        }
    }

    public int ReadyStatus { get; set; }

    public IAthletesActivities Activities { get; private set; }

    public void UpdateData(IDBConnection dBConnection)
    {
        ClientObject newClient = ClientData.GetAllDetails(dBConnection, ID);

        ID = newClient.ID;
        FirstName = newClient.FirstName;
        LastName = newClient.LastName;
        Sport = newClient.Sport;
        Position = newClient.Position;
        College = newClient.College;
        Team = newClient.Team;
        Age = newClient.Age;
        Wealth = newClient.Wealth;
        Dept = newClient.Dept;
        Popularity = newClient.Popularity;
        Character = newClient.Character;
        Relation = newClient.Relation;
        OverallSkill = newClient.OverallSkill;
        Prospectiveness = newClient.Prospectiveness;
        AgentID = newClient.AgentID;
        InjuryTime = newClient.InjuryTime;
        InjuryName = newClient.InjuryName;
        IsFavorite = newClient.IsFavorite;
        Type = newClient.Type;
        Contract = newClient.Contract;
        ContractWithAgent = newClient.ContractWithAgent;
        ReadyStatus = newClient.ReadyStatus;
        Activities = newClient.Activities;
    }

    public void SelectAsFavorite(IDBConnection dbConnection)
    {
        string query = "UPDATE players SET IsFavorite = 1 WHERE ID = " + ID;
        dbConnection.ModifyQuery(query);
    }

    public void DeselectAsFavorite(IDBConnection dbConnection)
    {
        string query = "UPDATE players SET IsFavorite = 0 WHERE ID = " + ID;
        dbConnection.ModifyQuery(query);
    }
}
