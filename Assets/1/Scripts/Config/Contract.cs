﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Contract : IContract
{
    public int Value { get; set; }
    public int Length { get; set; }
    public int YearOfSign { get; set; }
    public int GuaranteedMoney { get; set; }
    public int GotMoney { get; set; }

    public Contract(int value, int length, int yearOfSign, int guaranteedMoney, int gotMoney)
    {
        Value = value;
        Length = length;
        YearOfSign = yearOfSign;
        GuaranteedMoney = guaranteedMoney;
        GotMoney = gotMoney;
    }

    public int GetFullLengthOfContract(int currentYear)
    {
        return currentYear + Length - YearOfSign;
    }
}
