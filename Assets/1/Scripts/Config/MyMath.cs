﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MyMath{

    public static int FindAmplitude(int number, int min, int max)
    {
        int amplitude = number / 2;

        while (number - amplitude < min || number + amplitude > max)
        {
            amplitude -= 1;
        }

        return amplitude;
    }

    public static int GetIncreasedValueInTheRange(int max, int value)
    {
        if (value + 1 <= max)
            return value + 1;
        else
            return value;
    }

    public static int GetDecreasedValueInTheRange(int min, int value)
    {
        if (value - 1 >= min)
            return value - 1;
        else
            return value;
    }
}
