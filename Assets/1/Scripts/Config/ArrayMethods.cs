﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ArrayMethods {

    public static int[] ShuffleIntArray(int[] array)
    {
        int[] newArray = new int[array.Length];
        int index;

        for (int i = 0; i < array.Length; i++)
        {
            do
            {
                index = Random.Range(0, array.Length);
            }
            while (newArray[index] != 0);

            newArray[index] = array[i];

        }

        return newArray;
    }

    public static string[] ShuffleStringArray(string[] array)
    {
        string[] newArray = new string[array.Length];
        int index;

        for (int i = 0; i < array.Length; i++)
        {
            do
            {
                index = Random.Range(0, array.Length);
            }
            while (newArray[index] != null);

            newArray[index] = array[i];

        }

        return newArray;
    }

    public static bool IsValueInStringArray(string[] array, string value)
    {
        for(int i = 0; i < array.Length; i++)
        {
            if(array[i] == value)
                return true;
        }
        return false;
    }

    public static bool IsValueInIntArray(int[] array, int value)
    {
        for (int i = 0; i < array.Length; i++)
        {
            if (array[i] == value)
                return true;
        }
        return false;
    }

    public static void ResetStringArray(string[] array)
    {
        for (int i = 0; i < array.Length; i++)
            array[i] = "";
    }

    public static int[] RandomIntArrayRange(int min, int max, int numbers)
    {
        int[] array = new int[numbers];
        if (max >= numbers)
        {
            int random;

            for (int i = 0; i < numbers; i++)
            {
                do
                {
                    random = Random.Range(min, max + 1);
                }
                while (IsValueInIntArray(array, random));
                array[i] = random;
            }
            return array;
        }
        else
            return array;
    }
}
    