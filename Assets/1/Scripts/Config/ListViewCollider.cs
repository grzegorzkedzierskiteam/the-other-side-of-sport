﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ListViewCollider : MonoBehaviour {

    void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("enter");
        if (collision.tag == "ListViewItem")
            collision.gameObject.SetActive(false);
    }
}
