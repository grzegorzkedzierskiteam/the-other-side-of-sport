﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Text;
using UnityEngine;
using Debug = UnityEngine.Debug;

public static class Money
{

    public static string GetMoneyString(int money)
    {
        var moneyString = money.ToString("C", System.Globalization.CultureInfo.GetCultureInfo("en-us"));

        if (money < 0)
        {
            moneyString = moneyString.Remove(0, 1);
            moneyString = moneyString.Remove(moneyString.Length - 1);
            moneyString = "- " + moneyString;
        }

        return moneyString;
    }

    public static string GetShortMoneyString(int money)
    {
        if (money >= 1000000)
        {
            money = (int)Math.Ceiling(money / 1000000f);
            return new StringBuilder("$").Append(money).Append("M").ToString();
        }
        else if (money >= 1000)
        {
            money = (int)Math.Ceiling(money / 1000f);
            return new StringBuilder("$").Append(money).Append("k").ToString();
        }
        else if (money >= 0)
        {
            return new StringBuilder("$").Append(money).ToString();
        }
        else if (money > -1000)
        {
            money = (int)Math.Ceiling(money / -1f);
            return new StringBuilder("-$").Append(money).ToString();
        }
        else if (money > -1000000)
        {
            money = (int)Math.Ceiling(money / -1000f);
            return new StringBuilder("-$").Append(money).Append("k").ToString();
        }
        else if (money > -1000000000)
        {
            money = (int)Math.Ceiling(money / -1000000f);
            return new StringBuilder("-$").Append(money).Append("M").ToString();
        }
        else
        {
            money = (int)Math.Ceiling(money / -1000000000f);
            return new StringBuilder("-$").Append(money).Append("B").ToString();
        }
    }

    public static void AthleteWastesMoney(IDBConnection dBConnection, int wastedMoney, IClientObject athlete)
    {
        if (wastedMoney > athlete.Wealth)
        {
            var wealth = 0;
            var dept = wastedMoney - athlete.Wealth;

            var query = "UPDATE players SET Wealth = " + wealth + ", Dept = Dept + " + dept + " WHERE ID = " + athlete.ID + ";";
            dBConnection.ModifyQuery(query);

            athlete.Wealth = wealth;
            athlete.Dept = dept;
        }
        else
        {
            var query = "UPDATE players SET Wealth = Wealth - " + wastedMoney + " WHERE ID = " + athlete.ID + ";";
            dBConnection.ModifyQuery(query);

            athlete.Wealth -= wastedMoney;
        }
    }

    public static void AthleteEarnsMoney(IDBConnection dBConnection, int earnedMoney, IClientObject athlete)
    {
        if (athlete.AgentID > 1)
        {
            var query = "UPDATE players SET Wealth = Wealth + " + Convert.ToInt32(earnedMoney * 0.96f) + " WHERE ID = " + athlete.ID + ";" +
                "UPDATE agents SET Wealth = Wealth + " + Convert.ToInt32(earnedMoney * 0.04f) + " WHERE ID = " + athlete.AgentID + ";";

            dBConnection.ModifyQuery(query);
            athlete.Wealth = Convert.ToInt32(athlete.Wealth + (earnedMoney * 0.96f));
        }
        else if (athlete.AgentID == -1)
        {
            var query = "UPDATE players SET Wealth = Wealth + " + earnedMoney + " WHERE ID = " + athlete.ID + ";";
            dBConnection.ModifyQuery(query);

            athlete.Wealth += earnedMoney;
        }
    }

    public static void AthletePayOffDept(IDBConnection dBConnection, int money, IClientObject athlete)
    {
        var query = "UPDATE players SET Wealth = Wealth - " + money + ", Dept = Dept - " + money + " WHERE ID = " + athlete.ID + ";";
        dBConnection.ModifyQuery(query);

        athlete.Wealth -= money;
        athlete.Dept -= money;
    }

    public static void AthleteInDeptBorrowsMoney(IDBConnection dBConnection, IClientObject athlete)
    {
        var maxMoneyWhichCanBeBorrowed = 1000000;
        var popularityDecreased = 0;
        var dept = athlete.Wealth * -1;

        if (dept < maxMoneyWhichCanBeBorrowed)
            maxMoneyWhichCanBeBorrowed = dept + 5000;

        var howMuchMoneyAthleteBorrow = UnityEngine.Random.Range(0, 20);

        var borrowedMoney = 0;

        if (howMuchMoneyAthleteBorrow > 17)
        {
            borrowedMoney = maxMoneyWhichCanBeBorrowed;
            popularityDecreased = -8;
        }
        else if (howMuchMoneyAthleteBorrow > 12)
        {
            borrowedMoney = UnityEngine.Random.Range(dept / 2, dept);
            popularityDecreased = -6;
        }
        else if (howMuchMoneyAthleteBorrow > 6)
        {
            borrowedMoney = UnityEngine.Random.Range(dept / 4, dept / 2);
            popularityDecreased = -4;
        }
        else
        {
            borrowedMoney = UnityEngine.Random.Range(dept / 10, dept / 4);
            popularityDecreased = -3;
        }

        var newPopularity = AthleteStatUpdateManager.SetValueFrom0To100(athlete.Popularity, popularityDecreased);

        var query = "UPDATE players SET Dept = Dept - " + borrowedMoney + ", Popularity = " + newPopularity + " WHERE ID = " + athlete.ID + ";";
        dBConnection.ModifyQuery(query);

        query = "UPDATE players SET Wealth = Wealth + (Dept * -1), Dept = 0 WHERE Dept < 0;";
        dBConnection.ModifyQuery(query);

        athlete.Popularity = newPopularity;
        athlete.Dept -= borrowedMoney;

        if (athlete.Dept < 0)
        {
            athlete.Wealth += athlete.Dept * -1;
            athlete.Dept = 0;
        }
    }

    public static void AthleteInDeptBorrowsMoneyFromAgent(IDBConnection dBConnection, IClientObject athlete)
    {
        if (athlete.AgentID > 1)
        {
            IAgentObject agent = AgentData.GetAllDetails(dBConnection, athlete.AgentID);

            var maxMoneyWhichCanBeBorrowed = 2000000;

            if (agent.Wealth < 4000000)
                maxMoneyWhichCanBeBorrowed = Convert.ToInt32(agent.Wealth * 0.65f);

            var dept = athlete.Wealth * -1;

            if (dept < maxMoneyWhichCanBeBorrowed)
                maxMoneyWhichCanBeBorrowed = dept + 5000;

            var howMuchMoneyAthleteBorrow = UnityEngine.Random.Range(0, 20);
            var borrowedMoney = 0;

            if (howMuchMoneyAthleteBorrow > 17)
                borrowedMoney = maxMoneyWhichCanBeBorrowed;
            else if (howMuchMoneyAthleteBorrow > 12)
                borrowedMoney = UnityEngine.Random.Range(dept / 2, dept);
            else if (howMuchMoneyAthleteBorrow > 6)
                borrowedMoney = UnityEngine.Random.Range(dept / 4, dept / 2);
            else
                borrowedMoney = UnityEngine.Random.Range(dept / 10, dept / 4);

            var query = "UPDATE players SET Dept = Dept - " + borrowedMoney + ", WHERE ID = " + athlete.ID + ";" +
                            "UPDATE agents SET Wealth = Wealth - " + borrowedMoney + " WHERE ID = " + agent.ID + ";";
            dBConnection.ModifyQuery(query);

            query = "UPDATE players SET Wealth = Wealth + (Dept * -1), Dept = 0 WHERE Dept < 0;";
            dBConnection.ModifyQuery(query);

            athlete.Dept -= borrowedMoney;

            if (athlete.Dept < 0)
            {
                athlete.Wealth += athlete.Dept * -1;
                athlete.Dept = 0;
            }
        }
    }

    public static void DeptCollectionOfAthlete(IDBConnection dBConnection, IClientObject athlete)
    {
        var percentOfDeptToCollection = UnityEngine.Random.Range(0f, 1f);

        if (athlete.AgentID != 1)
        {
            var moneyToCollection = Convert.ToInt32(athlete.Dept * percentOfDeptToCollection);
            if (moneyToCollection > athlete.Wealth)
                moneyToCollection = athlete.Wealth;

            athlete.Wealth -= moneyToCollection;
            athlete.Dept -= moneyToCollection;

            var query = "UPDATE players SET Wealth = " + athlete.Wealth + ", Dept = " + athlete.Dept + " WHERE ID = " + athlete.ID + ";";
            dBConnection.ModifyQuery(query);
        }
    }
}
