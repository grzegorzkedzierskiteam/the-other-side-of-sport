﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContractWithAgent : IContractWithAgent
{
    public int Wage { get; set; } 
    public MyDateTime DateEnd { get; set; } 

    public ContractWithAgent(int wage, MyDateTime dateEnd)
    {
        Wage = wage;
        DateEnd = dateEnd;
    }
}
