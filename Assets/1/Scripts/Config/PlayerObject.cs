﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerObject{
    public PlayerObject(string _name, string _surname, Gender _gender, int _wealth, int _popularity, int _respectability, string _allies, string _enemies, int _energy, int _health, string _clients)
	{
		Name = _name;
		LastName = _surname;
        Gender = _gender;
		Wealth = _wealth;
		Popularity = _popularity;
		Respectability = _respectability;
		Allies = _allies;
		Enemies = _enemies;
		Energy = _energy;
		Health = _health;
        Clients = _clients;
	}

	public string Name { get; set; }

    public string LastName { get; set; }

    public Gender Gender { get; set; }

	public int Wealth { get; set; }

    public int Popularity { get; set; }

    public int Respectability { get; set; }

    public string Allies { get; set; }

    public string Enemies { get; set; }

    public int Energy { get; set; }

    public int Health { get; set; }

    public string Clients { get; set; }
}
