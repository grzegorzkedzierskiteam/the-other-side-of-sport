﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Person : IPerson
{
    public int ID { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public Gender Gender { get; set; }
    public string FullName => $"{FirstName} {LastName}";
    public ICharacter Character { get; set; }
    public PersonType Type { get; set; }
    public int Relation { get; set; }

    public Person(int id, string firstName, string lastName, Gender gender, ICharacter character, PersonType type, int relation)
    {
        ID = id;
        FirstName = firstName;
        LastName = lastName;
        Gender = gender;
        Character = character;
        Type = type;
        Relation = relation;
    }
}
