﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MatchObject {

    private int id;
    private int year;
    private int week;
    private int seasonWeek;
    private string sport;
    private string team1;
    private int team1Score;
    private string team2;
    private int team2Score;
    private string winner;
    private string loser;
    private bool draw;

    public MatchObject(int _id, int _year, int _week ,int _seasonWeek, string _sport, string _team1, int _team1Score, string _team2, int _team2Score, string _winner, string _loser, bool _draw)
    {
        id = _id;
        year = _year;
        week = _week;
        seasonWeek = _seasonWeek;
        sport = _sport;
        team1 = _team1;
        team1Score = _team1Score;
        team2 = _team2;
        team2Score = _team2Score;
        winner = _winner;
        loser = _loser;
        draw = _draw;
    }

    public int ID
    {
        get { return id; }
        set { id = value; }
    }

    public int Year
    {
        get { return year; }
        set { year = value; }
    }

    public int Week
    {
        get { return week; }
        set { week = value; }
    }

    public int SeasonWeek
    {
        get { return seasonWeek; }
        set { seasonWeek = value; }
    }

    public string Sport
    {
        get { return sport; }
        set { sport = value; }
    }

    public string Team1
    {
        get { return team1; }
        set { team1 = value; }
    }

    public int Team1Score
    {
        get { return team1Score; }
        set { team1Score = value; }
    }

    public string Team2
    {
        get { return team2; }
        set { team2 = value; }
    }

    public int Team2Score
    {
        get { return team2Score; }
        set { team2Score = value; }
    }

    public string Winner
    {
        get { return winner; }
        set { winner = value; }
    }

    public string Loser
    {
        get { return loser; }
        set { loser = value; }
    }

    public bool Draw
    {
        get { return draw; }
        set { draw = value; }
    }
}
