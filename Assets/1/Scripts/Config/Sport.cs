﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Sport {

    public static int GetFootballPositionPrestige(string pos)
    {
        switch(pos)
        {
            case "QB":
                return 12;
            case "DL":
                return 7;
            case "WR":
                return 6;
            case "TE":
                return 6;
            case "LB":
                return 5;
            case "OL":
                return 5;
            case "S":
                return 4;
            case "CB":
                return 4;
            case "K":
                return 3;
            case "P":
                return 3;
            case "HB":
                return 2;
            case "FB":
                return 1;
            default:
                return 0;
        }
    }

    public static int GetFootballPositionPopularity(string pos)
    {
        switch (pos)
        {
            case "QB":
                return 10;
            case "DL":
                return 8;
            case "WR":
                return 9;
            case "TE":
                return 9;
            case "LB":
                return 8;
            case "OL":
                return 8;
            case "S":
                return 8;
            case "CB":
                return 7;
            case "K":
                return 9;
            case "P":
                return 7;
            case "HB":
                return 9;
            case "FB":
                return 6;
            default:
                return 0;
        }
    }

    public static float GetOverallSkillAndProspectivenessEffect(int overallSkill, int prospectiveness)
    {
        int effect = overallSkill + prospectiveness;

        if (effect > 95)
            return 1;
        else if (effect > 90)
            return 0.8f;
        else if (effect > 80)
            return 0.7f;
        else if (effect > 70)
            return 0.6f;
        else if (effect > 60)
            return 0.3f;
        else if (effect > 50)
            return 0.15f;
        else if (effect > 40)
            return 0.05f;
        else if (effect > 30)
            return 0.02f;
        else
            return 0.01f;
    }
}
