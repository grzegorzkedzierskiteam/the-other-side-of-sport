﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CharacterManager {

    private static ICharacter[] characters = new ICharacter[] { new BornLeader(), new Phlegmatic(), new Melancholic(), new Sanguine() };

    public static ICharacter GetRandomCharacter()
    {
        int random = Random.Range(0, 4);
        return characters[random];
    }

    public static ICharacter GetCharacterFromNameOfCharacter(string nameOfCharacter)
    {
        switch(nameOfCharacter)
        {
            case "Born leader":
                return new BornLeader();
            case "Phlegmatic":
                return new Phlegmatic();
            case "Melancholic":
                return new Melancholic();
            case "Sanguine":
                return new Sanguine();
            default:
                return new Melancholic();
        }
    }
}
