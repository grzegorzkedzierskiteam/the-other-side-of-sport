﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class UserData { 
    public static UserDataHolder GetUserDataHolder()
    {
        return GameObject.FindGameObjectWithTag("UserDataHolder").GetComponent<UserDataHolder>();
    }
}
