﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Melancholic : ICharacter {

    public string NameOfCharacter { get; set; }

    public Melancholic()
    {
        NameOfCharacter = "Melancholic";
    }
}
