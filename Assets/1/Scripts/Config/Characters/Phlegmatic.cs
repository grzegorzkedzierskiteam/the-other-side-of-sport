﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Phlegmatic : ICharacter {

    public string NameOfCharacter { get; set; }

    public Phlegmatic()
    {
        NameOfCharacter = "Phlegmatic";
    }
}
