﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BornLeader : ICharacter
{
    public string NameOfCharacter { get; set; }

    public BornLeader()
    {
        NameOfCharacter = "Born leader";
    }
}
