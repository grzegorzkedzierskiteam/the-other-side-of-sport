﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sanguine : ICharacter {

    public string NameOfCharacter { get; set; }

    public Sanguine()
    {
        NameOfCharacter = "Sanguine";
    }
}
