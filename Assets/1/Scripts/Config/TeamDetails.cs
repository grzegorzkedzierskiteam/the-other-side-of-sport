﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeamDetails {

    private string name;
    private string sport;
    private int overall;
    private int popularity;
    private int relation;
    private string character;
    private string conference;
    private string division;
    private int wins;
    private int loses;
    private int draws;

    public TeamDetails(string _name, string _sport, int _overall, int _popularity, int _relation, string _conference, string _division)
    {
        name = _name;
        sport = _sport;
        overall = _overall;
        popularity = _popularity;
        relation = _relation;
        conference = _conference;
        division = _division;
    }

    public string Name
    {
        get { return name; }
    }

    public string Sport
    {
        get { return sport; }
    }

    public int Overall
    {
        get { return overall; }
    }

    public int Popularity
    {
        get { return popularity; }
    }

    public int Relation
    {
        get { return relation; }
    }

    public string Conference
    {
        get { return conference; }
    }

    public string Division
    {
        get { return division; }
    }

    public string Character
    {
        get { return character; }
        set { character = value; }
    }

    public int Wins
    {
        get { return wins; }
        set { wins = value; }
    }

    public int Loses
    {
        get { return loses; }
        set { loses = value; }
    }

    public int Draws
    {
        get { return draws; }
        set { draws = value; }
    }
}
