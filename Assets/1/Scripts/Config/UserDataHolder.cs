﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserDataHolder : MonoBehaviour, IUserDataHolder {

    private string firstName;
    private string lastName;
    private int wealth;
    private int popularity;
    private int respectability;
    private string allies;
    private string enemies;
    private List<int> alliesIDs;
    private List<int> enemiesIDs;
    private int energy;
    private int health;
    private string clients;
    private List<int> clientsIDs;
    private MyDateTime dateTime;

    public GameObject WarningPrefab;

    public MyDateTime DateTime
    {
        get { return dateTime; }
        set { dateTime = value; }
    }

    public string FirstName
    {
        get { return firstName; }
        set { firstName = value; }
    }

    public string LastName
    {
        get { return lastName; }
        set { lastName = value; }
    }

    public int Wealth
    {
        get { return wealth; }
        set { wealth = value; }
    }

    public int Popularity
    {
        get { return popularity; }
        set { popularity = value; }
    }

    public int Respectability
    {
        get { return respectability; }
        set { respectability = value; }
    }

    public int Energy
    {
        get { return energy; }
        set { energy = value; }
    }

    public int Health
    {
        get { return health; }
        set { health = value; }
    }

    public string Allies
    {
        get { return allies; }
        set
        {
            allies = value;
            SetAlliesIDs(allies);
        }
    }

    public string Enemies
    {
        get { return enemies; }
        set
        {
            enemies = value;
            SetEnemiesIDs(enemies);
        }
    }

    public string Clients
    {
        get { return clients; }
        set
        {
            clients = value;
            SetClientsIDs(clients);
        }
    }

    public List<int> AlliesIDs
    {
        get { return alliesIDs; }
    }

    public List<int> EnemiesIDs
    {
        get { return enemiesIDs; }
    }

    public List<int> ClientsIDs
    {
        get { return clientsIDs; }
    }

    public void SetAlliesIDs(string alliesString)
    {
        alliesIDs = GroupStringManager.GetAlliesEnemiesClientsIDs(alliesString);
    }

    public void SetEnemiesIDs(string enemiesString)
    {
        enemiesIDs = GroupStringManager.GetAlliesEnemiesClientsIDs(enemiesString);
    }

    public void SetClientsIDs(string clientsString)
    {
        clientsIDs = GroupStringManager.GetAlliesEnemiesClientsIDs(clientsString);
    }
}
