﻿using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using Mono.Data.Sqlite;
using Moq;

public class AthleteStatsUpdateManager_Test {

    private DBConnection dbConnection;

    [OneTimeSetUp]
    public void Init()
    {
        dbConnection = new DBConnection();
    }

    [OneTimeTearDown]
    public void Disconnect()
    {
        dbConnection.CloseConnection();
        dbConnection = null;
    }

    [Test]
    public void ChangePopularity_AthleteHaveNoAgent_AthletesPopularityWasChanged()
    {
        dbConnection.StartTransaction();

        string query = "SELECT ID, Popularity FROM players WHERE Popularity BETWEEN 10 AND 80 LIMIT 1;";
        SqliteDataReader reader = dbConnection.DownloadQuery(query);
        reader.Read();

        int athleteID = reader.GetInt32(0);
        int athletePopularity = reader.GetInt32(1);
        int value = 10;
        int expectedValue = athletePopularity + value;

        reader.Close();
        reader = null;

        var athleteMock = new Mock<IClientObject>();
        athleteMock.Setup(x => x.ID).Returns(athleteID);
        athleteMock.Setup(x => x.Popularity).Returns(athletePopularity);
        athleteMock.Setup(x => x.HasAgent).Returns(false);

        AthleteStatUpdateManager.ChangePopularity(dbConnection, athleteMock.Object, value);

        query = "SELECT Popularity FROM players WHERE ID = " + athleteID + ";";
        reader = dbConnection.DownloadQuery(query);
        reader.Read();

        Assert.AreEqual(expectedValue, reader.GetInt32(0));

        reader.Close();
        reader = null;
    }

    [Test]
    public void ChangePopularity_AthleteHaveAgentAndValueIsGreaterThan9_AthletesAndAgentsPopularitiesWereChanged()
    {
        dbConnection.StartTransaction();

        string query = "SELECT p.ID, p.Popularity, a.ID, a.Popularity FROM players p " +
                       "JOIN agents a ON p.Agent = a.ID " +
                       "WHERE(p.Popularity BETWEEN 10 AND 80) AND(a.Popularity BETWEEN 10 AND 80) " +
                       "AND p.Agent > 1 ORDER BY RANDOM() LIMIT 1; ";
        SqliteDataReader reader = dbConnection.DownloadQuery(query);
        reader.Read();

        int athleteID = reader.GetInt32(0);
        int athletePopularity = reader.GetInt32(1);
        int agentID = reader.GetInt32(2);
        int agentPopularity = reader.GetInt32(3);

        int value = 12;
        int expectedValueOfAthlete = athletePopularity + value;

        reader.Close();
        reader = null;

        var athleteMock = new Mock<IClientObject>();
        athleteMock.Setup(x => x.ID).Returns(athleteID);
        athleteMock.Setup(x => x.Popularity).Returns(athletePopularity);
        athleteMock.Setup(x => x.HasAgent).Returns(true);
        athleteMock.Setup(x => x.AgentID).Returns(agentID);

        AthleteStatUpdateManager.ChangePopularity(dbConnection, athleteMock.Object, value);

        query = "SELECT p.Popularity, a.Popularity FROM players p " +
                       "JOIN agents a ON p.Agent = a.ID " +
                       "WHERE p.ID = " + athleteID + " AND a.ID = " + agentID + ";";
        reader = dbConnection.DownloadQuery(query);
        reader.Read();

        Assert.AreEqual(expectedValueOfAthlete, reader.GetInt32(0));
        Assert.Greater(reader.GetInt32(1), agentPopularity);

        reader.Close();
        reader = null;
    }

    [Test]
    [Sequential]
    public void SetValueFrom0To100_ReturnsCorrectlyValues(
        [Values(50, 0, 100, 40, 100)] int oldValue,
        [Values(14, 3, 40, -62, -1)] int addingValue,
        [Values(64, 3, 100, 0, 99)] int expectedNewValue)
    {
        int newValue = AthleteStatUpdateManager.SetValueFrom0To100(oldValue, addingValue);

        Assert.AreEqual(expectedNewValue, newValue);
    }
}
