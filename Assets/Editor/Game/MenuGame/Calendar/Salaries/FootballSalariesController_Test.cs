﻿using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using Mono.Data.Sqlite;
using System.Collections.Generic;

public class FootballSalariesController_Test {

    private DBConnection dbConnection;
    private FootballSalariesController footballSalariesController;
    private List<AgentObject> listOfAllAgents;
    private UserDataHolder userDataHolder;

    [OneTimeSetUp]
    public void Init()
    {
        dbConnection = new DBConnection();

        listOfAllAgents = AgentData.GetAllAgentsOrReturnNull(dbConnection);

        IPlayerFinanceQueryMaker playerFinanceQueryMaker = new PlayerFinanceQueryMaker();
        IPlayerFinanceManager playerFinanceManager = new PlayerFinanceManager(dbConnection, playerFinanceQueryMaker);

        userDataHolder = new UserDataHolder
        {
            DateTime = new MyDateTime(50, 2018)
        };

        footballSalariesController = new FootballSalariesController(dbConnection, playerFinanceManager);
    }

    [OneTimeTearDown]
    public void Disconnect()
    {
        dbConnection.CloseConnection();
    }

    [Test]
	public void SeasonPaymentsAthletesWithoutAgents_PlayersGotMoney() {
        dbConnection.StartTransaction();

        var query = "SELECT ID, Wealth FROM players WHERE Contract > 0 AND Agent < 0 LIMIT 2";
        var reader = dbConnection.DownloadQuery(query);

        var playersIDs = new List<int>();
        var playersWealths = new List<int>();

        while (reader.Read())
        {
            playersIDs.Add(reader.GetInt32(0));
            playersWealths.Add(reader.GetInt32(1));
        }

        reader.Close();
        reader = null;

        footballSalariesController.ExecutePayments(listOfAllAgents, userDataHolder.DateTime);

        query = "SELECT Wealth FROM players WHERE ID IN(" + playersIDs[0] + ", " + playersIDs[1] + ");";
        reader = dbConnection.DownloadQuery(query);

        reader.Read();
        Assert.Greater(reader.GetInt32(0), playersWealths[0]);

        reader.Read();
        Assert.Greater(reader.GetInt32(0), playersWealths[1]);

        reader.Close();
        reader = null;
    }

    [Test]
    public void SeasonPaymentsAthletesWithAgents_PlayersGotMoney()
    {
        dbConnection.StartTransaction();

        var query = "SELECT ID, Wealth FROM players WHERE Contract > 0 AND Agent > 1 LIMIT 2";
        var reader = dbConnection.DownloadQuery(query);

        var playersIDs = new List<int>();
        var playersWealths = new List<int>();

        while (reader.Read())
        {
            playersIDs.Add(reader.GetInt32(0));
            playersWealths.Add(reader.GetInt32(1));
        }

        reader.Close();
        reader = null;

        footballSalariesController.ExecutePayments(listOfAllAgents, userDataHolder.DateTime);

        query = "SELECT Wealth FROM players WHERE ID IN(" + playersIDs[0] + ", " + playersIDs[1] + ");";
        reader = dbConnection.DownloadQuery(query);

        reader.Read();
        Assert.Greater(reader.GetInt32(0), playersWealths[0]);

        reader.Read();
        Assert.Greater(reader.GetInt32(0), playersWealths[1]);

        reader.Close();
        reader = null;
    }

    [Test]
    public void SeasonPaymentsAthletesWithPlayerAsAgent_PlayersGotMoney()
    {
        dbConnection.StartTransaction();

        var query = "SELECT ID, Wealth FROM players WHERE Contract > 0 AND Agent > 1 LIMIT 2";
        var reader = dbConnection.DownloadQuery(query);

        var playersIDs = new List<int>();
        var playersWealths = new List<int>();

        while (reader.Read())
        {
            playersIDs.Add(reader.GetInt32(0));
            playersWealths.Add(reader.GetInt32(1));
        }

        reader.Close();
        reader = null;

        query = "UPDATE players SET AgentWage = 6, Agent = 1 WHERE ID IN(" + playersIDs[0] + ", " + playersIDs[1] + ");";
        dbConnection.ModifyQuery(query);

        footballSalariesController.ExecutePayments(listOfAllAgents, userDataHolder.DateTime);

        query = "SELECT Wealth FROM players WHERE ID IN(" + playersIDs[0] + ", " + playersIDs[1] + ");";
        reader = dbConnection.DownloadQuery(query);

        reader.Read();
        Assert.Greater(reader.GetInt32(0), playersWealths[0]);

        reader.Read();
        Assert.Greater(reader.GetInt32(0), playersWealths[1]);

        reader.Close();
        reader = null;
    }

    [Test]
    public void SetPlayersGotMoney_GotMoneyWasIncreased()
    {
        dbConnection.StartTransaction();

        var query = "SELECT ID, GotMoney FROM players WHERE Contract > 0 AND ContractLenght > 1 LIMIT 2";
        var reader = dbConnection.DownloadQuery(query);

        var playersIDs = new List<int>();
        var playersGotMoney = new List<int>();

        while (reader.Read())
        {
            playersIDs.Add(reader.GetInt32(0));
            playersGotMoney.Add(reader.GetInt32(1));
        }

        reader.Close();
        reader = null;

        query = "UPDATE players SET AgentWage = 6, Agent = 1 WHERE ID IN(" + playersIDs[0] + ", " + playersIDs[1] + ");";
        dbConnection.ModifyQuery(query);

        footballSalariesController.ExecutePayments(listOfAllAgents, userDataHolder.DateTime);

        query = "SELECT GotMoney FROM players WHERE ID IN(" + playersIDs[0] + ", " + playersIDs[1] + ");";
        reader = dbConnection.DownloadQuery(query);

        reader.Read();
        Assert.Greater(reader.GetInt32(0), playersGotMoney[0]);

        reader.Read();
        Assert.Greater(reader.GetInt32(0), playersGotMoney[1]);

        reader.Close();
        reader = null;
    }
}
