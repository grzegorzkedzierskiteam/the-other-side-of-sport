﻿using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;

public class SportsWeeksManager_Test {

	[Test]
	public void GetSeasonWeekFootball_1Week_ReturnsPlayoffs() {
        int result = SportsWeeksManager.GetSeasonWeekFootball(1);

        Assert.AreEqual(0, result);
	}

    [Test]
    public void GetSeasonWeekFootball_5Week_ReturnsPlayoffs()
    {
        int result = SportsWeeksManager.GetSeasonWeekFootball(5);

        Assert.AreEqual(0, result);
    }

    [Test]
    public void GetSeasonWeekFootball_37Week_Returns1Week()
    {
        int result = SportsWeeksManager.GetSeasonWeekFootball(37);

        Assert.AreEqual(1, result);
    }

    [Test]
    public void GetSeasonWeekFootball_49Week_Returns13Week()
    {
        int result = SportsWeeksManager.GetSeasonWeekFootball(49);

        Assert.AreEqual(13, result);
    }

    [Test]
    public void GetSeasonWeekFootball_52Week_Returns16Week()
    {
        int result = SportsWeeksManager.GetSeasonWeekFootball(52);

        Assert.AreEqual(16, result);
    }

    [Test]
    public void GetSeasonWeekFootball_12Week_ReturnsOffseason()
    {
        int result = SportsWeeksManager.GetSeasonWeekFootball(12);

        Assert.AreEqual(-1, result);
    }
}
