﻿using Messages;
using Mono.Data.Sqlite;
using NUnit.Framework;

namespace Messages_Tests
{
    public class MessageLifeController_Test
    {
        private DBConnection dbConnection;
        private IMessageLifeController messageLifeController;
        private IMessageGenerator messageGenerator;
        private UserDataHolder userDataHolder;
        private IMessageManagerDB messageManagerDb;
        private IMessageManager messageManager;
        private MenuController menuController;

        [SetUp]
        public void Init()
        {
            dbConnection = new DBConnection();
            menuController = new MenuController();
            userDataHolder = new UserDataHolder
            {
                DateTime = new MyDateTime(1, 2018)
            };
            messageManagerDb = new MessageManagerDB(menuController);
            messageManagerDb.SetUpDate(userDataHolder.DateTime);
            var messageEffects = new MessageEffects(menuController, messageManagerDb, new PlayerEnergy(menuController));
            messageManager = new MessageManager(messageEffects);
            messageLifeController = new MessageLifeController(dbConnection, messageManagerDb, messageManager);
            messageGenerator = new MessageGenerator(dbConnection,
                new InjuriesController(dbConnection));
        }

        [TearDown]
        public void Disconnect()
        {
            dbConnection.CloseConnection();
            dbConnection = null;
        }

        [Test]
        public void MessagesTimeLifeExpiring_ThereAre3Messages_ExpiringTimesWereDecreased()
        {
            dbConnection.StartTransaction();

            var query = "DELETE FROM messages;";
            dbConnection.ModifyQuery(query);

            IPerson person = new ClientObject(1, "", "", "", "", "", "", 4, 5, 5, 5, new BornLeader(), 5, 5, 5, 5, 5, 5,
                5, 5, 5, 5, "", 4);

            IMessage notification = new Notification(33333, "", 3, 3, "", PersonType.Agent, new MyDateTime(1, 2016));
            var message1 = messageGenerator.GetMessage(person, TypeOfMessage.Assault, userDataHolder.DateTime);
            var message2 = messageGenerator.GetMessage(person, TypeOfMessage.Assault, userDataHolder.DateTime);

            messageManagerDb.AddMessageToDB(dbConnection, message1);
            messageManagerDb.AddMessageToDB(dbConnection, message2);
            messageManagerDb.AddMessageToDB(dbConnection, notification);

            messageLifeController.MessageTimeLifeExpiring();

            query = "SELECT ExpiringTime FROM messages";
            var reader = dbConnection.DownloadQuery(query);
            reader.Read();
            var firstTime = reader.GetInt32(0);
            reader.Read();
            var secondTime = reader.GetInt32(0);
            reader.Read();
            var thirdTime = reader.GetInt32(0);
            reader.Close();
            reader = null;

            Assert.AreEqual(message1.ExpiringTime - 1, firstTime);
            Assert.AreEqual(message2.ExpiringTime - 1, secondTime);
            Assert.AreEqual(notification.ExpiringTime - 1, thirdTime);
        }

        [Test]
        public void MessagesTimeLifeExpiring_ThereAre3Messages_WereDeleted()
        {
            dbConnection.StartTransaction();

            var query = "DELETE FROM messages;";
            dbConnection.ModifyQuery(query);

            IPerson person = new ClientObject(1, "", "", "", "", "", "", 4, 5, 5, 5, new BornLeader(), 5, 5, 5, 5, 5, 5,
                5, 5, 5, 5, "", 4);


            IMessage notification =
                new Notification(33333, "Test", 3, 3, "", PersonType.Agent, new MyDateTime(1, 2016));
            var message1 = messageGenerator.GetMessage(person, TypeOfMessage.Assault, userDataHolder.DateTime);
            var message2 = messageGenerator.GetMessage(person, TypeOfMessage.Assault, userDataHolder.DateTime);

            messageManagerDb.AddMessageToDB(dbConnection, message1);
            messageManagerDb.AddMessageToDB(dbConnection, message2);
            messageManagerDb.AddMessageToDB(dbConnection, notification);

            for (var i = 0; i < 10; i++)
                messageLifeController.MessageTimeLifeExpiring();

            query = "SELECT ExpiringTime FROM messages WHERE TypeOfMessage != 'Notification';";
            var reader = dbConnection.DownloadQuery(query);
            var messagesExisting = reader.HasRows;
            reader.Close();
            reader = null;

            query =
                "SELECT ExpiringTime FROM messages WHERE TypeOfMessage == 'Notification' AND Description == 'Test';";
            reader = dbConnection.DownloadQuery(query);
            var notificationExisting = reader.HasRows;
            reader.Close();
            reader = null;

            Assert.False(notificationExisting);
            Assert.False(messagesExisting);
        }
    }
}