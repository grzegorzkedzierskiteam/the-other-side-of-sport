﻿using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;

public class MyDataTime_Test {

	[Test]
	public void IsLastWeekOfMonth_IsWeekLastWeekOfMonth_TheyAre( [Values(4,8,13,17,22,26,31,36,40,44,48,52)]int week)
    {
        MyDateTime dataTime = new MyDateTime(week, 0);

        Assert.IsTrue(dataTime.IsLastWeekOfMonth);
	}

	[Test]
	public void IsLastWeekOfMonth_IsWeekLastWeekOfMonth_TheyAreNot([Values(1,3,11,14,24,27,33,42,50,51)]int week)
    {
        MyDateTime dataTime = new MyDateTime(week, 0);

        Assert.IsFalse(dataTime.IsLastWeekOfMonth);
    }

    [Test]
    public void AdvanceToNextWeek_WeekIsInTheMiddleOfYear_WeekWasCorrectlyChanged()
    {
        MyDateTime dataTime = new MyDateTime(3, 2017);

        int previousWeek = dataTime.Week;
        dataTime.AdvanceToNextWeek();

        Assert.AreEqual(previousWeek + 1, dataTime.Week);
    }

    [Test]
    public void AdvanceToNextWeek_LastWeekOfYear_YearWasCorrectlyChanged()
    {
        MyDateTime dataTime = new MyDateTime(52, 2017);

        int previousYear = dataTime.Year;
        dataTime.AdvanceToNextWeek();

        Assert.AreEqual(previousYear + 1, dataTime.Year);
    }

    [Test]
    public void AdvanceToNextWeek_LastWeekOfYear_WeekWasCorrectlyChanged()
    {
        MyDateTime dataTime = new MyDateTime(52, 2017);

        dataTime.AdvanceToNextWeek();

        Assert.AreEqual(1, dataTime.Week);
    }

    [Test]
    public void SetMonth_Number7ofMonth_ReturnsJuly()
    {
        MyDateTime dataTime = new MyDateTime(27, 2017);

        Assert.AreEqual("July", dataTime.Month);
    }

    [Test]
    public void SetMonthNumber_Number2ofWeek_Returns1()
    {
        MyDateTime dataTime = new MyDateTime(2, 2017);

        Assert.AreEqual(1, dataTime.MonthNumber);
    }

    [Test]
    public void SetWeekPerMonth_Number43ofWeek_Returns3()
    {
        MyDateTime dataTime = new MyDateTime(43, 2017);

        Assert.AreEqual(3, dataTime.WeekPerMonth);
    }
}
