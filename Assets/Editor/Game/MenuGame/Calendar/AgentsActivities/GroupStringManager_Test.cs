﻿using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;

public class GroupStringManager_Test {

	[Test]
	public void GetAlliesEnemiesClientsIDs_1WithPlus_ReturnsCorrectlyList() {
        string group = "+1";
        List<int> idList = GroupStringManager.GetAlliesEnemiesClientsIDs(group);

        Assert.AreEqual(1, idList[0]);
	}

    [Test]
    public void GetAlliesEnemiesClientsIDs_24And3332And3WithPlus_ReturnsCorrectlyList()
    {
        string group = "+24+3332+3";
        List<int> idList = GroupStringManager.GetAlliesEnemiesClientsIDs(group);

        Assert.AreEqual(24, idList[0]);
        Assert.AreEqual(3332, idList[1]);
        Assert.AreEqual(3, idList[2]);
    }

    [Test]
    public void GetAlliesEnemiesClientsIDs_5WithoutPlus_ListHaveAnyID()
    {
        string group = "5";
        List<int> idList = GroupStringManager.GetAlliesEnemiesClientsIDs(group);

        Assert.True(idList.Count == 0);
    }

    [Test]
    public void AddPersonToGroup_AddingTwoIDs_IDsWasCorrectlyAdded()
    {
        string group = "+4+1";
        group = GroupStringManager.AddPersonToGroup(group, "5");
        group = GroupStringManager.AddPersonToGroup(group, "3");

        Assert.AreEqual("+4+1+5+3", group);
    }

    [Test]
    public void AddPersonToGroup_AddingStringIsEmpty_ReturnsGroupStringWithoutChanges()
    {
        string group = "+2222+1231";
        group = GroupStringManager.AddPersonToGroup(group, "");

        Assert.AreEqual(group, group);
    }

    [Test]
    public void DeletePersonFromGroup_DeleteOneID_IDWasCorrectlyDeleted()
    {
        string group = "+2222+1231";
        group = GroupStringManager.DeletePersonFromGroup(group, "1231");

        Assert.AreEqual("+2222", group);
    }

    [Test]
    public void DeletePersonFromGroup_DeletingStringIsEmpty_ReturnsGroupStringWithoutChanges()
    {
        string group = "+523+42";
        group = GroupStringManager.DeletePersonFromGroup(group, "");

        Assert.AreEqual(group, group);
    }

    [Test]
    public void IsPersonInGroup_PersonIsInGroup_ReturnsTrue()
    {
        string group = "+22+322";
        Assert.True(GroupStringManager.IsPersonInGroup(group, "22"));
    }

    [Test]
    public void IsPersonInGroup_PersonIsNotInGroup_ReturnsFalse()
    {
        string group = "+22+322";
        Assert.False(GroupStringManager.IsPersonInGroup(group, "4"));
    }

    [Test]
    public void IsPersonInGroup_GroupIsEmpty_ReturnsFalse()
    {
        string group = "";
        Assert.False(GroupStringManager.IsPersonInGroup(group, "223"));
    }

    [Test]
    public void IsPersonInGroup_SearchingStringIsEmpty_ReturnsFalse()
    {
        string group = "+23+4";
        Assert.False(GroupStringManager.IsPersonInGroup(group, ""));
    }

    [Test]
    public void GetListAvailableResponses_ThereAreNoResponses_ReturnsEmptyList()
    {
        string group = "";
        List<TypeOfResponse> list = GroupStringManager.GetListAvailableResponses(group);
        Assert.Zero(list.Count);
    }

    [Test]
    public void GetListAvailableResponses_ThereAre2Responses_ReturnsCorrectList()
    {
        string group = "+Talk+Payment";
        List<TypeOfResponse> list = GroupStringManager.GetListAvailableResponses(group);
        Assert.AreEqual(TypeOfResponse.Talk, list[0]);
        Assert.AreEqual(TypeOfResponse.Payment, list[1]);
    }

    [Test]
    public void GetStringAvailableResponses_ThereAreNoResponses_ReturnsEmptyString()
    {
        List<TypeOfResponse> list = new List<TypeOfResponse>();
        string group = GroupStringManager.GetStringAvailableResponses(list);
        Assert.AreEqual("", group);
    }

    [Test]
    public void GetStringAvailableResponses_ThereAre3Responses_ReturnsCorrectString()
    {
        List<TypeOfResponse> list = new List<TypeOfResponse>()
        {
            TypeOfResponse.Payment,TypeOfResponse.Promise,TypeOfResponse.Talk
        };
        string group = GroupStringManager.GetStringAvailableResponses(list);
        Assert.AreEqual("+Payment+Promise+Talk", group);
    }
}
