﻿using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using Moq;

public class RelationManager_Test {

	[Test]
	public void ChangeRelationNumber_StartingRelationEquals50Adding20_Returns70() {
        int startingRelation = 50;
        int difference = 20;

        int newRelation = RelationManager.ChangeRelationNumber(startingRelation, difference);

        Assert.AreEqual(70, newRelation);
	}

    [Test]
    public void ChangeRelationNumber_StartingRelationEquals97Adding40_Returns100()
    {
        int startingRelation = 97;
        int difference = 40;

        int newRelation = RelationManager.ChangeRelationNumber(startingRelation, difference);

        Assert.AreEqual(100, newRelation);
    }

    [Test]
    public void ChangeRelationNumber_StartingRelationEquals54Substracting20_Returns34()
    {
        int startingRelation = 54;
        int difference = -20;

        int newRelation = RelationManager.ChangeRelationNumber(startingRelation, difference);

        Assert.AreEqual(34, newRelation);
    }

    [Test]
    public void ChangeRelationNumber_StartingRelationEquals35Substracting50_Returns0()
    {
        int startingRelation = 35;
        int difference = -50;

        int newRelation = RelationManager.ChangeRelationNumber(startingRelation, difference);

        Assert.AreEqual(0, newRelation);
    }

    [Test]
    public void ChangeRelationNumber_StartingRelationEquals100Substracting100_Returns0()
    {
        int startingRelation = 100;
        int difference = -100;

        int newRelation = RelationManager.ChangeRelationNumber(startingRelation, difference);

        Assert.AreEqual(0, newRelation);
    }

    [Test]
    public void ChangeRelationNumber_StartingRelationEquals0Adding100_Returns100()
    {
        int startingRelation = 0;
        int difference = 100;

        int newRelation = RelationManager.ChangeRelationNumber(startingRelation, difference);

        Assert.AreEqual(100, newRelation);
    }
}
