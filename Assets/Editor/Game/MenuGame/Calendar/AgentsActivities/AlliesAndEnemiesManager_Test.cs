﻿using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using Moq;
using System.Collections.Generic;

public class AlliesAndEnemiesManager_Test {

	[Test]
	public void AreTheyAllis_TheyAreAllies_ReturnsTrue() {
        var firstAgent = new Mock<IAgentObject>();
        var secondAgent = new Mock<IAgentObject>();
        firstAgent.Setup(x => x.ID).Returns(5);
        firstAgent.Setup(x => x.Allies).Returns("+1+4");
        secondAgent.Setup(x => x.ID).Returns(1);
        secondAgent.Setup(x => x.Allies).Returns("+5+4");

        Assert.True(AlliesAndEnemiesManager.AreTheyAllis(firstAgent.Object, secondAgent.Object));
    }

    [Test]
    public void AreTheyAllis_TheyAreNotAllies_ReturnsFalse()
    {
        var firstAgent = new Mock<IAgentObject>();
        var secondAgent = new Mock<IAgentObject>();
        firstAgent.Setup(x => x.ID).Returns(5);
        firstAgent.Setup(x => x.Allies).Returns("+1+4");
        secondAgent.Setup(x => x.ID).Returns(32);
        secondAgent.Setup(x => x.Allies).Returns("+5+4");

        Assert.False(AlliesAndEnemiesManager.AreTheyAllis(firstAgent.Object, secondAgent.Object));
    }

    [Test]
    public void AreTheyEnemies_TheyAreEnemies_ReturnsTrue()
    {
        var firstAgent = new Mock<IAgentObject>();
        var secondAgent = new Mock<IAgentObject>();
        firstAgent.Setup(x => x.ID).Returns(5);
        firstAgent.Setup(x => x.Enemies).Returns("+1+4");
        secondAgent.Setup(x => x.ID).Returns(1);
        secondAgent.Setup(x => x.Enemies).Returns("+5+4");

        Assert.True(AlliesAndEnemiesManager.AreTheyEnemies(firstAgent.Object, secondAgent.Object));
    }

    [Test]
    public void AreTheyEnemies_TheyAreNotEnemies_ReturnsFalse()
    {
        var firstAgent = new Mock<IAgentObject>();
        var secondAgent = new Mock<IAgentObject>();
        firstAgent.Setup(x => x.ID).Returns(5);
        firstAgent.Setup(x => x.Enemies).Returns("+1+4");
        secondAgent.Setup(x => x.ID).Returns(32);
        secondAgent.Setup(x => x.Enemies).Returns("+5+4");

        Assert.False(AlliesAndEnemiesManager.AreTheyEnemies(firstAgent.Object, secondAgent.Object));
    }
    
    [Test]
    public void GetDeleteAgentFromEnemiesQuery_DeletePerson_ReturnsCorrectlyQuery()
    {
        AgentObject firstAgent = new AgentObject(1, null, null, Gender.Male,0,0,0,null,"+4+3",0,null,0,null,0);
        AgentObject secondAgent = new AgentObject(4, null, null, Gender.Male, 0, 0, 0, null, "+6+1", 0, null, 0, null, 0);

        string query = AlliesAndEnemiesManager.GetDeleteAgentFromEnemiesQuery(firstAgent, secondAgent);

        string expectedQuery = "UPDATE agents SET Enemies = '+3' WHERE ID =1;" +
                                "UPDATE agents SET Enemies = '+6' WHERE ID =4;";
        Assert.AreEqual(expectedQuery, query);
    }

    [Test]
    public void GetDeleteAgentFromAlliesQuery_DeletePerson_ReturnsCorrectlyQuery()
    {
        AgentObject firstAgent = new AgentObject(1, null, null, Gender.Male, 0, 0, 0, "+4+3", null, 0, null, 0, null, 0);
        AgentObject secondAgent = new AgentObject(4, null, null, Gender.Male, 0, 0, 0, "+6+1", null, 0, null, 0, null, 0);

        string query = AlliesAndEnemiesManager.GetDeleteAgentFromAlliesQuery(firstAgent, secondAgent);

        string expectedQuery = "UPDATE agents SET Allies = '+3' WHERE ID =1;" +
                                "UPDATE agents SET Allies = '+6' WHERE ID =4;";
        Assert.AreEqual(expectedQuery, query);
    }

    [Test]
    public void GetSetAgentAsEnemyQuery_TheirRelationWasNeutral_ReturnsCorrectlyQuery()
    {
        AgentObject firstAgent = new AgentObject(1, null, null, Gender.Male, 0, 0, 0, "", "+3", 0, null, 0, null, 0);
        AgentObject secondAgent = new AgentObject(4, null, null, Gender.Male, 0, 0, 0, "", "+6", 0, null, 0, null, 0);

        string query = AlliesAndEnemiesManager.GetSetAgentAsEnemyQuery(firstAgent, secondAgent);

        string expectedQuery = "UPDATE agents SET Enemies = '+3+4' WHERE ID =1;" +
                                "UPDATE agents SET Enemies = '+6+1' WHERE ID =4;";
        Assert.AreEqual(expectedQuery, query);
    }

    [Test]
    public void GetSetAgentAsEnemyQuery_TheyWereAllies_ReturnsCorrectlyQuery()
    {
        AgentObject firstAgent = new AgentObject(1, null, null, Gender.Male, 0, 0, 0, "+4", "+3", 0, null, 0, null, 0);
        AgentObject secondAgent = new AgentObject(4, null, null, Gender.Male, 0, 0, 0, "+1", "+6", 0, null, 0, null, 0);

        string query = AlliesAndEnemiesManager.GetSetAgentAsEnemyQuery(firstAgent, secondAgent);

        string expectedQuery = "UPDATE agents SET Enemies = '+3+4' WHERE ID =1;" +
                                "UPDATE agents SET Enemies = '+6+1' WHERE ID =4;" +
                                "UPDATE agents SET Allies = '' WHERE ID =1;" +
                                "UPDATE agents SET Allies = '' WHERE ID =4;";
        Assert.AreEqual(expectedQuery, query);
    }

    [Test]
    public void GetSetAgentAsAllyQuery_TheirRelationWasNeutral_ReturnsCorrectlyQuery()
    {
        AgentObject firstAgent = new AgentObject(1, null, null, Gender.Male, 0, 0, 0, "+3", "", 0, null, 0, null, 0);
        AgentObject secondAgent = new AgentObject(4, null, null, Gender.Male, 0, 0, 0, "+6", "", 0, null, 0, null, 0);

        string query = AlliesAndEnemiesManager.GetSetAgentAsAllyQuery(firstAgent, secondAgent);

        string expectedQuery = "UPDATE agents SET Allies = '+3+4' WHERE ID =1;" +
                                "UPDATE agents SET Allies = '+6+1' WHERE ID =4;";
        Assert.AreEqual(expectedQuery, query);
    }

    [Test]
    public void GetSetAgentAsAllyQuery_TheyWereEnemies_ReturnsCorrectlyQuery()
    {
        AgentObject firstAgent = new AgentObject(1, null, null, Gender.Male, 0, 0, 0, "+3", "+4", 0, null, 0, null, 0);
        AgentObject secondAgent = new AgentObject(4, null, null, Gender.Male, 0, 0, 0, "+6", "+1", 0, null, 0, null, 0);

        string query = AlliesAndEnemiesManager.GetSetAgentAsAllyQuery(firstAgent, secondAgent);

        string expectedQuery = "UPDATE agents SET Allies = '+3+4' WHERE ID =1;" +
                                "UPDATE agents SET Allies = '+6+1' WHERE ID =4;" +
                                "UPDATE agents SET Enemies = '' WHERE ID =1;" +
                                "UPDATE agents SET Enemies = '' WHERE ID =4;";
        Assert.AreEqual(expectedQuery, query);
    }

    
}
