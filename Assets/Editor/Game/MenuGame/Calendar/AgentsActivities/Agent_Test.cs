﻿using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using Moq;
using System.Collections.Generic;
using Mono.Data.Sqlite;

public class Agent_Test {

    private DBConnection dbConnection;
    private IAgentsContractsManager agentsContractsManager;
    private List<Agent> listOfAgents;

    [OneTimeSetUp]
    public void Init()
    {
        listOfAgents = new List<Agent>();
        dbConnection = new DBConnection();
        var userDataHolderMock = new Mock<UserDataHolder>();
        IContractMaker contractMaker = new ContractMaker();
        agentsContractsManager = new AgentsContractsManager(dbConnection, contractMaker);

        List<AgentObject> listOfAgentsObjects = AgentData.GetAllAgentsOrReturnNull(dbConnection);

        foreach (AgentObject agent in listOfAgentsObjects)
        {
            int favorite = 0;
            if (agent.IsFavorite)
                favorite = 1;
            else
                favorite = 0;

            listOfAgents.Add(new Agent(dbConnection, agentsContractsManager, userDataHolderMock.Object, agent.ID, agent.FirstName, agent.LastName, agent.Gender,agent.Wealth, agent.Popularity, agent.Respectability, agent.Allies, agent.Enemies, agent.Energy, agent.Clients, agent.Relation, agent.Character.NameOfCharacter, favorite));
        }
    }

    [OneTimeTearDown]
    public void Disconnect()
    {
        dbConnection.CloseConnection();
        dbConnection = null;
    }
}
