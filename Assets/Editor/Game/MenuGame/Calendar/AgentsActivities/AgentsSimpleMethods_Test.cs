﻿using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using Moq;
using System;

public class AgentsSimpleMethods_Test {

    private DBConnection dbConnection;

    [OneTimeSetUp]
    public void Init()
    {
        dbConnection = new DBConnection();
    }

    [OneTimeTearDown]
    public void Disconnect()
    {
        dbConnection.CloseConnection();
        dbConnection = null;
    }

    [Test]
    public void AgentLosesClient_AthleteWasClientOfAgent_ClientWasDeletedFromClients()
    {
        dbConnection.StartTransaction();
        ClientObject client = new ClientObject(0, "", "", "", "", "", "", 0, 0, 0, 0, new Sanguine(), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "", 0);
        Offer offer = new Offer(1, 1);
        var userDataMock = new Mock<IUserDataHolder>();
        MyDateTime date = new MyDateTime(1, 1);
        userDataMock.Setup(x => x.DateTime).Returns(date);
        userDataMock.Setup(x => x.Clients).Returns("");
        AgentsSimpleMethods.SetAthleteAsClientOfPlayerInDB(dbConnection, userDataMock.Object, client, offer);

        int clientID = 0;
        AgentsSimpleMethods.AgentLosesClient(dbConnection, clientID, 1);

        AgentObject agent = AgentData.GetAllDetails(dbConnection, 1);
        
        Assert.False(GroupStringManager.IsPersonInGroup(agent.Clients, clientID.ToString()));
    }

    [Test]
    public void AgentLosesClient_AthleteWasNotClientOfAgent_Throws()
    {
        dbConnection.StartTransaction();

        int clientID = 0;

        Assert.Throws<Exception>(() => AgentsSimpleMethods.AgentLosesClient(dbConnection, clientID, 1));   
    }
}
