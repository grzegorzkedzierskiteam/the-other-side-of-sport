﻿using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using Moq;
using System;
using System.Linq;

public class AlliesAndEnemiesAdding_Test {

    private DBConnection dbConnection;
    private List<IAgent> listOfAgents;

    [OneTimeSetUp]
    public void Init()
    {
        var userDataHolderMock = new Mock<UserDataHolder>();
        listOfAgents = new List<IAgent>();
        dbConnection = new DBConnection();
        IContractMaker contractMaker = new ContractMaker();
        IAgentsContractsManager agentsContractsManager = new AgentsContractsManager(dbConnection, contractMaker);
        List<AgentObject> listOfAgentsObjects = AgentData.GetAllAgentsOrReturnNull(dbConnection);

        foreach (AgentObject agent in listOfAgentsObjects)
        {
           if(agent.ID != 1)
                listOfAgents.Add(new Agent(dbConnection, agentsContractsManager, userDataHolderMock.Object, agent.ID, agent.FirstName, agent.LastName, agent.Gender, agent.Wealth, agent.Popularity, agent.Respectability, agent.Allies, agent.Enemies, agent.Energy, agent.Clients, agent.Relation, agent.Character.NameOfCharacter, 0));
        }    
    }

    [OneTimeTearDown]
    public void Disconnect()
    {
        dbConnection.CloseConnection();
        dbConnection = null;
    }

    [Test]
    public void GetAgentToDeleteFromAlliesOrEnemy_AgentHasManyAllies_ReturnsAllyToDelete()
    {
        var agentMock = new Mock<IAgent>();
        agentMock.Setup(x => x.AlliesIDs).Returns(new List<int>() { 2, 92 });

        IAgent allyToDelete = AlliesAndEnemiesManager.GetAgentToDeleteFromAlliesOrEnemy(listOfAgents, agentMock.Object, AlliesAndEnemiesManager.GroupType.Allies);

        Assert.IsNotNull(allyToDelete);
    }

    [Test]
    public void GetAgentToDeleteFromAlliesOrEnemy_AgentHas1Ally_ReturnsAllyToDelete()
    {
        var agentMock = new Mock<IAgent>();
        agentMock.Setup(x => x.AlliesIDs).Returns(new List<int>() { 5 });

        IAgent allyToDelete = AlliesAndEnemiesManager.GetAgentToDeleteFromAlliesOrEnemy(listOfAgents, agentMock.Object, AlliesAndEnemiesManager.GroupType.Allies);

        Assert.IsNotNull(allyToDelete);
    }

    [Test]
    public void GetAgentToDeleteFromAlliesOrEnemy_AgentHas1AllyWithID1_ThrowsException()
    {
        var agentMock = new Mock<IAgent>();
        agentMock.Setup(x => x.AlliesIDs).Returns(new List<int>() { 1 });

        Assert.Throws<Exception>(() => AlliesAndEnemiesManager.GetAgentToDeleteFromAlliesOrEnemy(listOfAgents, agentMock.Object, AlliesAndEnemiesManager.GroupType.Allies));
    }

    [Test]
    public void GetAgentToDeleteFromAlliesOrEnemy_AgentHasNoAlly_ThrowsException()
    {
        var agentMock = new Mock<IAgent>();
        agentMock.Setup(x => x.AlliesIDs).Returns(new List<int>());

        Assert.Throws<Exception>(() => AlliesAndEnemiesManager.GetAgentToDeleteFromAlliesOrEnemy(listOfAgents, agentMock.Object, AlliesAndEnemiesManager.GroupType.Allies));
    }

    [Test]
    public void GetAgentToDeleteFromAlliesOrEnemy_AgentHasManyEnemies_ReturnsEnemyToDelete()
    {
        var agentMock = new Mock<IAgent>();
        agentMock.Setup(x => x.EnemiesIDs).Returns(new List<int>() { 33, 51, 53 });

        IAgent enemyToDelete = AlliesAndEnemiesManager.GetAgentToDeleteFromAlliesOrEnemy(listOfAgents, agentMock.Object, AlliesAndEnemiesManager.GroupType.Enemies);

        Assert.IsNotNull(enemyToDelete);
    }

    [Test]
    public void GetAgentToDeleteFromAlliesOrEnemy_AgentHas1Enemy_ReturnsEnemyToDelete()
    {
        var agentMock = new Mock<IAgent>();
        agentMock.Setup(x => x.EnemiesIDs).Returns(new List<int>() { 24 });

        IAgent enemyToDelete = AlliesAndEnemiesManager.GetAgentToDeleteFromAlliesOrEnemy(listOfAgents, agentMock.Object, AlliesAndEnemiesManager.GroupType.Enemies);

        Assert.IsNotNull(enemyToDelete);
    }

    [Test]
    public void GetAgentToDeleteFromAlliesOrEnemy_AgentHas1EnemyWithID1_ThrowsException()
    {
        var agentMock = new Mock<IAgent>();
        agentMock.Setup(x => x.EnemiesIDs).Returns(new List<int>() { 1 });

        Assert.Throws<Exception>(() => AlliesAndEnemiesManager.GetAgentToDeleteFromAlliesOrEnemy(listOfAgents, agentMock.Object, AlliesAndEnemiesManager.GroupType.Enemies));
    }

    [Test]
    public void GetAgentToDeleteFromAlliesOrEnemy_AgentHasNoEnemy_ThrowsException()
    {
        var agentMock = new Mock<IAgent>();
        agentMock.Setup(x => x.EnemiesIDs).Returns(new List<int>());

        Assert.Throws<Exception>(() => AlliesAndEnemiesManager.GetAgentToDeleteFromAlliesOrEnemy(listOfAgents, agentMock.Object, AlliesAndEnemiesManager.GroupType.Enemies));
    }

    [Test]
    public void GetIDOfProbablyNewAllyOfEnemy_AgentIsPoor_ReturnsAlly()
    {
        var agentMock = new Mock<IAgent>();
        agentMock.Setup(x => x.Popularity).Returns(40);
        agentMock.Setup(x => x.Respectability).Returns(40);
        agentMock.Setup(x => x.Wealth).Returns(1000000);
        agentMock.Setup(x => x.AlliesIDs).Returns(new List<int>());
        agentMock.Setup(x => x.EnemiesIDs).Returns(new List<int>());

        IAgent newAlly = AlliesAndEnemiesManager.GetIDOfProbablyNewAllyOrEnemy(listOfAgents,agentMock.Object);

        Assert.IsNotNull(newAlly);
    }

    [Test]
    public void GetIDOfProbablyNewAllyOfEnemy_AgentIsRich_ReturnsAlly()
    {
        var agentMock = new Mock<IAgent>();
        agentMock.Setup(x => x.Popularity).Returns(100);
        agentMock.Setup(x => x.Respectability).Returns(100);
        agentMock.Setup(x => x.Wealth).Returns(1000000000);
        agentMock.Setup(x => x.AlliesIDs).Returns(new List<int>());
        agentMock.Setup(x => x.EnemiesIDs).Returns(new List<int>());

        IAgent newAlly = AlliesAndEnemiesManager.GetIDOfProbablyNewAllyOrEnemy(listOfAgents, agentMock.Object);

        Assert.IsNotNull(newAlly);
    }

    [Test]
    public void GetIDOfProbablyNewAllyOfEnemy_AgentIsOrdinary_ReturnsAlly()
    {
        var agentMock = new Mock<IAgent>();
        agentMock.Setup(x => x.Popularity).Returns(75);
        agentMock.Setup(x => x.Respectability).Returns(74);
        agentMock.Setup(x => x.Wealth).Returns(70000000);
        agentMock.Setup(x => x.AlliesIDs).Returns(new List<int>());
        agentMock.Setup(x => x.EnemiesIDs).Returns(new List<int>());

        IAgent newAlly = AlliesAndEnemiesManager.GetIDOfProbablyNewAllyOrEnemy(listOfAgents, agentMock.Object);

        Assert.IsNotNull(newAlly);
    }

    [Test]
    public void GetIDOfProbablyNewAllyOfEnemy_AgentHas98Allies_ReturnsLastAgent()
    {
        var agentMock = new Mock<IAgent>();
        agentMock.Setup(x => x.ID).Returns(0);
        agentMock.Setup(x => x.Popularity).Returns(75);
        agentMock.Setup(x => x.Respectability).Returns(74);
        agentMock.Setup(x => x.Wealth).Returns(70000000);
        List<int> alliesIDs = new List<int>();
        List<int> enemiesIDs = new List<int>();
        for (int i = 2; i < 100; i++)
            alliesIDs.Add(i);

        agentMock.Setup(x => x.AlliesIDs).Returns(alliesIDs);
        agentMock.Setup(x => x.EnemiesIDs).Returns(enemiesIDs);

        IAgent newAlly = AlliesAndEnemiesManager.GetIDOfProbablyNewAllyOrEnemy(listOfAgents, agentMock.Object);

        Assert.AreEqual(100,newAlly.ID);
    }

    [Test]
    public void GetIDOfProbablyNewAllyOfEnemy_AgentHas98Enemies_ReturnsLastAgent()
    {
        var agentMock = new Mock<IAgent>();
        agentMock.Setup(x => x.ID).Returns(0);
        agentMock.Setup(x => x.Popularity).Returns(75);
        agentMock.Setup(x => x.Respectability).Returns(74);
        agentMock.Setup(x => x.Wealth).Returns(70000000);
        List<int> alliesIDs = new List<int>();
        List<int> enemiesIDs = new List<int>();
        for (int i = 2; i < 100; i++)
            enemiesIDs.Add(i);

        agentMock.Setup(x => x.AlliesIDs).Returns(alliesIDs);
        agentMock.Setup(x => x.EnemiesIDs).Returns(enemiesIDs);

        IAgent newEnemy = AlliesAndEnemiesManager.GetIDOfProbablyNewAllyOrEnemy(listOfAgents, agentMock.Object);

        Assert.AreEqual(100, newEnemy.ID);
    }

    [Test]
    public void GetIDOfProbablyNewAllyOfEnemy_AgentHas49AlliesAnd49Enemies_ReturnsLastAgent()
    {
        var agentMock = new Mock<IAgent>();
        agentMock.Setup(x => x.ID).Returns(0);
        agentMock.Setup(x => x.Popularity).Returns(75);
        agentMock.Setup(x => x.Respectability).Returns(74);
        agentMock.Setup(x => x.Wealth).Returns(70000000);
        List<int> alliesIDs = new List<int>();
        List<int> enemiesIDs = new List<int>();
        for (int i = 2; i < 100; i++)
        {
            if (i % 2 == 0)
                enemiesIDs.Add(i);
            else
                alliesIDs.Add(i);
        }
            
        agentMock.Setup(x => x.AlliesIDs).Returns(alliesIDs);
        agentMock.Setup(x => x.EnemiesIDs).Returns(enemiesIDs);

        IAgent newEnemy = AlliesAndEnemiesManager.GetIDOfProbablyNewAllyOrEnemy(listOfAgents, agentMock.Object);

        Assert.AreEqual(100, newEnemy.ID);
    }

    [Test]
    public void GetIDOfProbablyNewAllyOfEnemy_AgentHas3AlliesAnd94Enemies_ReturnsLastAgent()
    {
        var agentMock = new Mock<IAgent>();
        agentMock.Setup(x => x.ID).Returns(0);
        agentMock.Setup(x => x.Popularity).Returns(75);
        agentMock.Setup(x => x.Respectability).Returns(74);
        agentMock.Setup(x => x.Wealth).Returns(70000000);
        List<int> alliesIDs = new List<int>();
        List<int> enemiesIDs = new List<int>();

        alliesIDs.Add(43);
        alliesIDs.Add(29);
        alliesIDs.Add(87);

        for (int i = 3; i < 100; i++)
        {
            if(!alliesIDs.Contains(i))
                enemiesIDs.Add(i);
        }

        agentMock.Setup(x => x.AlliesIDs).Returns(alliesIDs);
        agentMock.Setup(x => x.EnemiesIDs).Returns(enemiesIDs);

        IAgent newEnemy = AlliesAndEnemiesManager.GetIDOfProbablyNewAllyOrEnemy(listOfAgents, agentMock.Object);

        Assert.IsTrue(newEnemy.ID == 100 || newEnemy.ID == 2);
    }

    [Test]
    public void GetIDOfProbablyNewAllyOfEnemy_AgentHas50AlliesAnd49Enemies_ThrowsException()
    {
        var agentMock = new Mock<IAgent>();
        agentMock.Setup(x => x.ID).Returns(0);
        agentMock.Setup(x => x.Popularity).Returns(75);
        agentMock.Setup(x => x.Respectability).Returns(74);
        agentMock.Setup(x => x.Wealth).Returns(70000000);
        List<int> alliesIDs = new List<int>();
        List<int> enemiesIDs = new List<int>();

        alliesIDs.Add(43);
        alliesIDs.Add(29);
        alliesIDs.Add(87);

        for (int i = 2; i < 101; i++)
        {
            if (i % 2 == 0)
                alliesIDs.Add(i);
            else
                enemiesIDs.Add(i);
        }

        agentMock.Setup(x => x.AlliesIDs).Returns(alliesIDs);
        agentMock.Setup(x => x.EnemiesIDs).Returns(enemiesIDs);

        Assert.Throws<Exception>(() => AlliesAndEnemiesManager.GetIDOfProbablyNewAllyOrEnemy(listOfAgents, agentMock.Object));
    }

    [Test]
    public void CanAgentHaveMoreAlliesOrEnemies_AgentHasNoAlliesAndNoEnemies_ReturnsTrue()
    {
        var agentMock = new Mock<IAgent>();
        agentMock.Setup(x => x.ID).Returns(0);
        List<int> alliesIDs = new List<int>();
        List<int> enemiesIDs = new List<int>();

        agentMock.Setup(x => x.AlliesIDs).Returns(alliesIDs);
        agentMock.Setup(x => x.EnemiesIDs).Returns(enemiesIDs);

        bool decision = AlliesAndEnemiesManager.CanAgentHaveMoreAlliesOrEnemies(listOfAgents, agentMock.Object);

        Assert.IsTrue(decision);
    }

    [Test]
    public void CanAgentHaveMoreAlliesOrEnemies_AgentHas3AlliesAnd1Enemies_ReturnsTrue()
    {
        var agentMock = new Mock<IAgent>();
        agentMock.Setup(x => x.ID).Returns(0);
        List<int> alliesIDs = new List<int>();
        List<int> enemiesIDs = new List<int>();

        alliesIDs.Add(43);
        alliesIDs.Add(29);
        alliesIDs.Add(87);

        enemiesIDs.Add(5);

        agentMock.Setup(x => x.AlliesIDs).Returns(alliesIDs);
        agentMock.Setup(x => x.EnemiesIDs).Returns(enemiesIDs);

        bool decision = AlliesAndEnemiesManager.CanAgentHaveMoreAlliesOrEnemies(listOfAgents, agentMock.Object);

        Assert.IsTrue(decision);
    }

    [Test]
    public void CanAgentHaveMoreAlliesOrEnemies_AgentHas99Allies_ReturnsFalse()
    {
        var agentMock = new Mock<IAgent>();
        agentMock.Setup(x => x.ID).Returns(0);
        List<int> alliesIDs = new List<int>();
        List<int> enemiesIDs = new List<int>();

        for (int i = 2; i < 101; i++)
            alliesIDs.Add(i);

        agentMock.Setup(x => x.AlliesIDs).Returns(alliesIDs);
        agentMock.Setup(x => x.EnemiesIDs).Returns(enemiesIDs);

        bool decision = AlliesAndEnemiesManager.CanAgentHaveMoreAlliesOrEnemies(listOfAgents, agentMock.Object);

        Assert.IsFalse(decision);
    }

    [Test]
    public void CanAgentHaveMoreAlliesOrEnemies_AgentHas99Enemies_ReturnsFalse()
    {
        var agentMock = new Mock<IAgent>();
        agentMock.Setup(x => x.ID).Returns(0);
        List<int> alliesIDs = new List<int>();
        List<int> enemiesIDs = new List<int>();

        for (int i = 2; i < 101; i++)
            enemiesIDs.Add(i);

        agentMock.Setup(x => x.AlliesIDs).Returns(alliesIDs);
        agentMock.Setup(x => x.EnemiesIDs).Returns(enemiesIDs);

        bool decision = AlliesAndEnemiesManager.CanAgentHaveMoreAlliesOrEnemies(listOfAgents, agentMock.Object);

        Assert.IsFalse(decision);
    }

    [Test]
    public void CanAgentHaveMoreAlliesOrEnemies_AgentHas44AlliesAnd45Enemies_ReturnsFalse()
    {
        var agentMock = new Mock<IAgent>();
        agentMock.Setup(x => x.ID).Returns(0);
        List<int> alliesIDs = new List<int>();
        List<int> enemiesIDs = new List<int>();

        for (int i = 2; i < 101; i++)
        {
            if (i <= 45)
                alliesIDs.Add(i);
            else
                enemiesIDs.Add(i);
        }
            
        agentMock.Setup(x => x.AlliesIDs).Returns(alliesIDs);
        agentMock.Setup(x => x.EnemiesIDs).Returns(enemiesIDs);

        bool decision = AlliesAndEnemiesManager.CanAgentHaveMoreAlliesOrEnemies(listOfAgents, agentMock.Object);

        Assert.IsFalse(decision);
    }

    [Test]
    public void DoesAgentHaveAllyOrEnemy_AgentHas3Allies_ReturnsTrue()
    {
        var agentMock = new Mock<IAgent>();
        agentMock.Setup(x => x.ID).Returns(0);
        agentMock.Setup(x => x.AlliesIDs).Returns(new List<int>() { 5, 2, 55 });

        bool result = AlliesAndEnemiesManager.DoesAgentHaveAllyOrEnemy(agentMock.Object, AlliesAndEnemiesManager.GroupType.Allies);

        Assert.IsTrue(result);
    }

    [Test]
    public void DoesAgentHaveAllyOrEnemy_AgentHas1Ally_ReturnsTrue()
    {
        var agentMock = new Mock<IAgent>();
        agentMock.Setup(x => x.ID).Returns(0);
        agentMock.Setup(x => x.AlliesIDs).Returns(new List<int>() { 5 });

        bool result = AlliesAndEnemiesManager.DoesAgentHaveAllyOrEnemy(agentMock.Object, AlliesAndEnemiesManager.GroupType.Allies);

        Assert.IsTrue(result);
    }

    [Test]
    public void DoesAgentHaveAllyOrEnemy_AgentHasNoAlly_ReturnsFalse()
    {
        var agentMock = new Mock<IAgent>();
        agentMock.Setup(x => x.ID).Returns(0);
        agentMock.Setup(x => x.AlliesIDs).Returns(new List<int>());

        bool result = AlliesAndEnemiesManager.DoesAgentHaveAllyOrEnemy(agentMock.Object, AlliesAndEnemiesManager.GroupType.Allies);

        Assert.IsFalse(result);
    }

    [Test]
    public void DoesAgentHaveAllyOrEnemy_AgentHas5Enemies_ReturnsTrue()
    {
        var agentMock = new Mock<IAgent>();
        agentMock.Setup(x => x.ID).Returns(0);
        agentMock.Setup(x => x.EnemiesIDs).Returns(new List<int>() { 5, 2, 55, 6, 30 });

        bool result = AlliesAndEnemiesManager.DoesAgentHaveAllyOrEnemy(agentMock.Object, AlliesAndEnemiesManager.GroupType.Enemies);

        Assert.IsTrue(result);
    }

    [Test]
    public void DoesAgentHaveAllyOrEnemy_AgentHas1Enemy_ReturnsTrue()
    {
        var agentMock = new Mock<IAgent>();
        agentMock.Setup(x => x.ID).Returns(0);
        agentMock.Setup(x => x.EnemiesIDs).Returns(new List<int>() { 97 });

        bool result = AlliesAndEnemiesManager.DoesAgentHaveAllyOrEnemy(agentMock.Object, AlliesAndEnemiesManager.GroupType.Enemies);

        Assert.IsTrue(result);
    }

    [Test]
    public void DoesAgentHaveAllyOrEnemy_AgentHasNoEnemy_ReturnsFalse()
    {
        var agentMock = new Mock<IAgent>();
        agentMock.Setup(x => x.ID).Returns(0);
        agentMock.Setup(x => x.EnemiesIDs).Returns(new List<int>());

        bool result = AlliesAndEnemiesManager.DoesAgentHaveAllyOrEnemy(agentMock.Object, AlliesAndEnemiesManager.GroupType.Enemies);

        Assert.IsFalse(result);
    }

    [Test]
    public void DoAgentsIDWereRepeatedInAlliesOrEnemiesGroup_ReturnsFalse()
    {
        List<AgentObject> agents = AgentData.GetAllAgentsOrReturnNull(dbConnection);

        foreach (AgentObject agent in agents)
        {
            List<int> allies = GroupStringManager.GetAlliesEnemiesClientsIDs(agent.Allies);
            List<int> enemies = GroupStringManager.GetAlliesEnemiesClientsIDs(agent.Enemies);
            List<Contract> listaAllies = new List<Contract>();
            List<Contract> listaEnemies = new List<Contract>();
            foreach (int id in allies)
            {
                if (listaAllies.Any(x => x.Value == id))
                    listaAllies.Find(x => x.Value == id).Length += 1;
                else
                    listaAllies.Add(new Contract(id, 1, 0, 0, 0));
            }

            foreach (int id in enemies)
            {
                if (listaEnemies.Any(x => x.Value == id))
                    listaEnemies.Find(x => x.Value == id).Length += 1;
                else
                    listaEnemies.Add(new Contract(id, 1, 0, 0, 0));
            }

            bool wereRepeated = false;

            if (listaAllies.Any(x => x.Length > 1))
                wereRepeated = true;

            if (listaEnemies.Any(x => x.Length > 1))
                wereRepeated = true;

            Assert.IsFalse(wereRepeated);
        }
    }

    [Test]
    public void DoesAnyAgentIDIsNotAllyAndEnemyAtTheSameTime_ReturnsFalse()
    {
        List<AgentObject> agents = AgentData.GetAllAgentsOrReturnNull(dbConnection);

        foreach (AgentObject agent in agents)
        {
            List<int> allies = GroupStringManager.GetAlliesEnemiesClientsIDs(agent.Allies);
            List<int> enemies = GroupStringManager.GetAlliesEnemiesClientsIDs(agent.Enemies);

            bool wereRepeated = false;

            foreach (int id in allies)
            {
                if (enemies.Any(x => x == id))
                    wereRepeated = true;
            }

            foreach (int id in enemies)
            {
                if (allies.Any(x => x == id))
                    wereRepeated = true;
            }

            Assert.IsFalse(wereRepeated);
        }
    }
}
