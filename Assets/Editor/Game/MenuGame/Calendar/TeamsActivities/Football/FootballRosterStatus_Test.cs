﻿using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using System;

public class FootballRosterStatus_Test {

    public DBConnection dbConnection;

    [SetUp]
    public void Setup()
    {
        dbConnection = new DBConnection();
    }

    [Test]
	public void CheckDemandList()
    {
        dbConnection = new DBConnection();
        FootballRosterStatus roster = new FootballRosterStatus(dbConnection, "Dallas");
        roster.SetDemandList();
        Assert.IsNotNull(roster.DemandList);
        dbConnection.CloseConnection();
	}

    [Test]
    public void CheckBuyingPlayer()
    {
        FootballRosterStatus roster = new FootballRosterStatus(dbConnection, "Dallas");
        FootballRosterStatus roster2 = new FootballRosterStatus(dbConnection, "Dallas");
        roster.SetDemandList();
        roster2.SetDemandList();
         
        roster2.PlayerWasBought("QB");

        Assert.AreNotSame(roster.DemandList,roster2.DemandList);

        dbConnection.CloseConnection();
    }

    
}
