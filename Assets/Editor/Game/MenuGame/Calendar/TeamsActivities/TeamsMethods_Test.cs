﻿using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using Mono.Data.Sqlite;

public class TeamsMethods_Test {

    private DBConnection dbConnection;

    [OneTimeSetUp]
    public void Init()
    {
        dbConnection = new DBConnection();
    }

    [OneTimeTearDown]
    public void Disconnect()
    {
        dbConnection.CloseConnection();
        dbConnection = null;
    }

    [Test]
    public void GivePlayerGuaranteedMoney_PlayerHasGuaranteedMoney_HisWealthWasCorrectlyIncreased()
    {
        dbConnection.StartTransaction();

        string query = "SELECT ID, Wealth, GotMoney, ContractGuaranteedMoney FROM players WHERE Contract > 0 AND ContractGuaranteedMoney > 0 AND Agent == -1 ORDER BY RANDOM() LIMIT 1";
        SqliteDataReader reader = dbConnection.DownloadQuery(query);

        reader.Read();
        int playerID = reader.GetInt32(0);
        int playerWealth = reader.GetInt32(1);
        int playerGotMoney = reader.GetInt32(2);
        int playerGuaranteedMoney = reader.GetInt32(3);

        reader.Close();
        reader = null;

        int expectedWealth = playerWealth + (playerGuaranteedMoney - playerGotMoney);
        if (playerGuaranteedMoney < playerGotMoney)
            expectedWealth = playerWealth;

        TeamsMethods.GivePlayerGuaranteedMoney(dbConnection, playerID);

        query = "SELECT Wealth FROM players WHERE ID = " + playerID + ";";
        reader = dbConnection.DownloadQuery(query);

        reader.Read();
        Assert.AreEqual(expectedWealth, reader.GetInt32(0));

        reader.Close();
        reader = null;
    }

    [Test]
	public void GivePlayerGuaranteedMoney_PlayerDoesNotHaveGuaranteedMoney_HisWealthWasNotIncreased() {
        dbConnection.StartTransaction();

        string query = "SELECT ID, Wealth FROM players WHERE Contract > 0 AND ContractGuaranteedMoney = 0 ORDER BY RANDOM() LIMIT 1";
        SqliteDataReader reader = dbConnection.DownloadQuery(query);

        reader.Read();
        int playerID = reader.GetInt32(0);
        int playerWealth = reader.GetInt32(1);

        reader.Close();
        reader = null;

        TeamsMethods.GivePlayerGuaranteedMoney(dbConnection, playerID);

        query = "SELECT Wealth FROM players WHERE ID = " + playerID + ";";
        reader = dbConnection.DownloadQuery(query);

        reader.Read();
        Assert.AreEqual(playerWealth, reader.GetInt32(0));

        reader.Close();
        reader = null;
    }

    [Test]
    public void GivePlayersGuaranteedMoney_PlayersHaveGuaranteedMoney_HisWealthWasCorrectlyIncreased()
    {
        dbConnection.StartTransaction();

        List<ClientObject> listOfFiredPlayers = new List<ClientObject>();

        string query = "SELECT ID FROM players WHERE Contract > 0 AND ContractGuaranteedMoney > 0 AND Agent == -1 ORDER BY RANDOM() LIMIT 5";
        SqliteDataReader reader = dbConnection.DownloadQuery(query);

        List<int> listOfIDsFiredPlayers = new List<int>();

        while (reader.Read())
            listOfIDsFiredPlayers.Add(reader.GetInt32(0));

        reader.Close();
        reader = null;

        foreach (int id in listOfIDsFiredPlayers)
            listOfFiredPlayers.Add(ClientData.GetAllDetails(dbConnection, id));

        List<int> listOfExpectedWealths = new List<int>();

        foreach (ClientObject client in listOfFiredPlayers)
        {
            if (client.Contract.GuaranteedMoney <= client.Contract.GotMoney)
                listOfExpectedWealths.Add(client.Wealth);
            else
                listOfExpectedWealths.Add(client.Wealth + (client.Contract.GuaranteedMoney - client.Contract.GotMoney));
        }
            
        TeamsMethods.GivePlayersGuaranteedMoney(dbConnection, listOfIDsFiredPlayers);

        List<int> listOfActualWealths = new List<int>();

        foreach(int id in listOfIDsFiredPlayers)
            listOfActualWealths.Add(ClientData.GetAllDetails(dbConnection, id).Wealth);

        Assert.AreEqual(listOfExpectedWealths[0], listOfActualWealths[0]);
        Assert.AreEqual(listOfExpectedWealths[1], listOfActualWealths[1]);
        Assert.AreEqual(listOfExpectedWealths[2], listOfActualWealths[2]);
        Assert.AreEqual(listOfExpectedWealths[3], listOfActualWealths[3]);
        Assert.AreEqual(listOfExpectedWealths[4], listOfActualWealths[4]);
    }
}
