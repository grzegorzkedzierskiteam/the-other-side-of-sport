﻿using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;

public class PensionersManager_Test {

    private DBConnection dbConnection;
    private PensionersManager manager;

    [OneTimeSetUp]
    public void Init()
    {
        dbConnection = new DBConnection();
        manager = new PensionersManager(dbConnection);
    }

    [OneTimeTearDown]
    public void Disconnect()
    {
        dbConnection.CloseConnection();
        dbConnection = null;
    }

    [Test]
	public void RetirePlayers_QueryWasSuccessfullyExecuted_DoesNotThrow() {
        dbConnection.StartTransaction();

        Assert.DoesNotThrow(() => manager.RetirePlayers());
	}

    [Test]
    public void DeletePlayers_QueryWasSuccessfullyExecuted_DoesNotThrow()
    {
        dbConnection.StartTransaction();

        Assert.DoesNotThrow(() => manager.DeletePlayers());
    }
}
