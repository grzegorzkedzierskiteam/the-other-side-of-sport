﻿using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using Moq;
using Mono.Data.Sqlite;

public class NegotiatingWithTeamEffects_Test {

    private DBConnection dbConnection;

    [OneTimeSetUp]
    public void Init()
    {
        dbConnection = new DBConnection();
    }

    [OneTimeTearDown]
    public void Disconnect()
    {
        dbConnection.CloseConnection();
        dbConnection = null;
    }

    [Test]
	public void GetPositionInDepthChartEffect_ClientIsQB_ReturnsValue() {

        string query = "SELECT ID FROM players LIMIT 1";
        SqliteDataReader reader = dbConnection.DownloadQuery(query);
        reader.Read();
        int id = reader.GetInt32(0);
        reader.Close();
        reader = null;

        var clientMock = new Mock<IClientObject>();
        clientMock.Setup(x => x.Position).Returns("QB");
        clientMock.Setup(x => x.ID).Returns(id);
        string teamName = "Dallas";

        int effect = NegotiatingWithTeamEffects.GetPositionInDepthChartEffect(dbConnection, teamName, clientMock.Object);

        Assert.AreNotEqual(0, effect);
	}

    [Test]
    [Sequential]
    public void GetPlayerAgeEffect(
        [Values(50, 42, 36, 32, 28, 22)] int age,
        [Values(-80, -60, -35, -19, 0, 6)] int expectedResult)
    {
        int result = NegotiatingWithTeamEffects.GetPlayerAgeEffect(age);

        Assert.AreEqual(expectedResult, result);
    }

    [Test]
    [Sequential]
    public void GetPlayerInjuryEffect(
        [Values(0, 12, 20)] int injuryTime,
        [Values(0, -15, -35)] int expectedResult)
    {
        int result = NegotiatingWithTeamEffects.GetPlayerInjuryEffect(injuryTime);

        Assert.AreEqual(expectedResult, result);
    }

    [Test]
    [Sequential]
    public void GetAgentSkillEffect(
       [Values(100, 60, 20, 5)] int respectability,
       [Values(100, 80, 40, 16)] int popularity,
       [Values(5, 1, 0, -5)] int expectedResult)
    {
        int result = NegotiatingWithTeamEffects.GetAgentSkillEffect(respectability, popularity);

        Assert.AreEqual(expectedResult, result);
    }
}
