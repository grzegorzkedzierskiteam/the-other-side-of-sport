﻿using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using Moq;

public class NegotiatingWithClientEffects_Test {

    [Test]
    public void GetCharacterEffect_CharactersAreTheSame_ReturnsNonZero()
    {
        var clientCharacter = new Mock<ICharacter>();
        var bidderCharacter = new Mock<ICharacter>();
        clientCharacter.Setup(x => x.NameOfCharacter).Returns("Born leader");
        bidderCharacter.Setup(x => x.NameOfCharacter).Returns("Born leader");

        Assert.AreNotEqual(0, NegotiatingWithClientEffects.GetCharacterEffect(clientCharacter.Object, bidderCharacter.Object));
    }

    [Test]
    public void GetCharacterEffect_CharactersAreNotTheSame_ReturnsZero()
    {
        var clientCharacter = new Mock<ICharacter>();
        var bidderCharacter = new Mock<ICharacter>();
        clientCharacter.Setup(x => x.NameOfCharacter).Returns("Born leader");
        bidderCharacter.Setup(x => x.NameOfCharacter).Returns("Phlegmatic");

        Assert.AreEqual(0, NegotiatingWithClientEffects.GetCharacterEffect(clientCharacter.Object, bidderCharacter.Object));
    }

    [Test]
    [Sequential]
    public void GetDifferenceBetweenAgentsEffect(
        [Values(67,52,44,32,30,15,0,-15,-23,-36,-44,-57,-90)] int expectedDifference,
        [Values(6,5,4,3,2,1,0,-1,-2,-3,-4,-5,-6)] int effect)
    {
        int actualEffect = NegotiatingWithClientEffects.GetDifferenceBetweenAgentsEffect(expectedDifference);
        Assert.AreEqual(effect, actualEffect);
    }

    [Test]
    public void GetDifferenceBetweenNewAgentAndCurrentAgentStats_TheirStatsAreTheSame_Returns0()
    {
        var bidder = new Mock<IBidder>();
        bidder.Setup(x => x.Popularity).Returns(50);
        bidder.Setup(x => x.Respectability).Returns(20);
        var currentAgent = new Mock<IBidder>();
        currentAgent.Setup(x => x.Popularity).Returns(50);
        currentAgent.Setup(x => x.Respectability).Returns(20);

        Assert.AreEqual(0, NegotiatingWithClientEffects.GetDifferenceBetweenNewAgentAndCurrentAgentStats(bidder.Object, currentAgent.Object));
    }

    [Test]
    public void GetDifferenceBetweenNewAgentAndCurrentAgentStats_BidderStatsAreGreater_ReturnsValueIsGreaterThan0()
    {
        var bidder = new Mock<IBidder>();
        bidder.Setup(x => x.Popularity).Returns(70);
        bidder.Setup(x => x.Respectability).Returns(20);
        var currentAgent = new Mock<IBidder>();
        currentAgent.Setup(x => x.Popularity).Returns(24);
        currentAgent.Setup(x => x.Respectability).Returns(12);

        Assert.Greater(NegotiatingWithClientEffects.GetDifferenceBetweenNewAgentAndCurrentAgentStats(bidder.Object, currentAgent.Object),0);
    }

    [Test]
    public void GetDifferenceBetweenNewAgentAndCurrentAgentStats_CurrentAgentStatsAreGreater_ReturnsValueIsLessThan0()
    {
        var bidder = new Mock<IBidder>();
        bidder.Setup(x => x.Popularity).Returns(10);
        bidder.Setup(x => x.Respectability).Returns(20);
        var currentAgent = new Mock<IBidder>();
        currentAgent.Setup(x => x.Popularity).Returns(64);
        currentAgent.Setup(x => x.Respectability).Returns(52);

        Assert.Less(NegotiatingWithClientEffects.GetDifferenceBetweenNewAgentAndCurrentAgentStats(bidder.Object, currentAgent.Object),0);
    }

    [Test]
    public void GetHavingOtherAgentEffect_ClientHasAgent_ReturnsMinus5()
    {
        Assert.AreEqual(-5, NegotiatingWithClientEffects.GetHavingOtherAgentEffect());
    }

    [Test]
    [Sequential]
	public void GetRatingOfWage(
        [Values(-22,1, 3, 4, 5, 8, 9, 13, 20)] int wage,
        [Values(9,9, 8, 6, 4, 3, 2, 1, 0)] int result)
    {
        Assert.AreEqual(result, NegotiatingWithClientEffects.GetRatingOfWage(wage));
	}

    [Test]
    [Sequential]
    public void GetRatingOfContractLength(
        [Values(10, 9, 7, 5, 4, 2, 1)] int contractLength,
        [Values(-8, -8, -5, 0, 1, 2, 3)] int result)
    {
        Assert.AreEqual(result, NegotiatingWithClientEffects.GetRatingOfContractMinLength(contractLength));
    }

    [Test]
    public void GetRetirementOrFreeAgentEffect_PlayerIsRetired_Returns5()
    {
        Assert.AreEqual(5, NegotiatingWithClientEffects.GetRetirementOrFreeAgentEffect("Retirement"));
    }

    [Test]
    public void GetRetirementOrFreeAgentEffect_PlayerIsFreeAgent_Returns2()
    {
        Assert.AreEqual(2, NegotiatingWithClientEffects.GetRetirementOrFreeAgentEffect("None"));
    }

    [Test]
    public void GetRetirementOrFreeAgentEffect_PlayerIsActiveAthleteWhoIsInProTeam_Returns0()
    {
        Assert.AreEqual(0, NegotiatingWithClientEffects.GetRetirementOrFreeAgentEffect("Dallas"));
    }

    [Test]
    [Sequential]
    public void GetRelationEffect(
        [Values(99, 95, 90, 83, 78, 72, 67, 62, 59, 55, 50, 48, 44, 39, 33, 28, 21, 19, 15, 9, 1)] int relation,
        [Values(12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 0, -3, -4, -5, -6, -7, -8, -9, -10, -11, -12)] int result)
    {
        Assert.AreEqual(result, NegotiatingWithClientEffects.GetRelationEffect(relation));
    }

    [Test]
    [Sequential]
    public void GetRespectabilityEffect(
        [Values(-22, 19, 34, 44, 54, 68, 75, 76, 83, 90, 95, 99)] int respectability,
        [Values(0, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10)] int result)
    {
        Assert.AreEqual(result, NegotiatingWithClientEffects.GetRespectabilityEffect(respectability));
    }

    [Test]
    [Sequential]
    public void GetPopularityEffectByBidder(
        [Values(-13, 40, 52, 62, 78, 90, 99)] int popularity,
        [Values(0, 0, 1, 2, 3, 4, 5)] int result)
    {
        Assert.AreEqual(result, NegotiatingWithClientEffects.GetPopularityEffectByBidder(popularity));
    }

    [Test]
    [Sequential]
    public void GetPopularityEffectByClient(
        [Values(99, 90, 83, 66, 42, 39, 22, 11, 1)] int popularity,
        [Values(-5, -4, -3, -2, -1, 0, 1, 2, 3)] int result)
    {
        Assert.AreEqual(result, NegotiatingWithClientEffects.GetPopularityEffectByClient(popularity));
    }

    [Test]
    [Sequential]
    public void GetOverallSkillEffect(
        [Values(99, 90, 75, 66, 52, 44, 37, 30, 11, 2)] int overallSKill,
        [Values(0, 1, 2, 3, 4, 5, 6, 7, 8, 9)] int result)
    {
        Assert.AreEqual(result, NegotiatingWithClientEffects.GetOverallSkillEffect(overallSKill));
    }
}
