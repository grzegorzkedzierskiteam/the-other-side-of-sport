﻿using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using Moq;

public class ContractMaker_Test {

    IContractMaker contractMaker;

    [OneTimeSetUp]
    public void Init()
    {
        contractMaker = new ContractMaker();
    }

    [Test]
    [Sequential]
	public void GetGuaranteedMoney(
        [Values(100,200,50,1000000000)] int value,
        [Values(4,20,5,100)] int percent,
        [Values(4,40,2,1000000000)] int expectedResult)
    {
        var guaranteedMoney = contractMaker.GetGuaranteedMoney(value, percent);

        Assert.AreEqual(expectedResult, guaranteedMoney);
	}

    [Test]
    public void GetNewGreaterContract_ContractIsHigherThan40Million_ReturnsContractValueNotHigherThan51Million()
    {
        var contractMock = new Mock<IContract>();
        contractMock.Setup(x => x.Value).Returns(50000000);
        contractMock.Setup(x => x.Length).Returns(1);

        var newContract = contractMaker.GetNewGreaterContract(contractMock.Object, 2018);

        Assert.LessOrEqual(newContract.Value, 51000000 * newContract.Length);
    }

    [Test]
    public void GetNewGreaterContract_ContractIsHigherThan30Million_ReturnsContractValueNotHigherThan33Million()
    {
        var contractMock = new Mock<IContract>();
        contractMock.Setup(x => x.Value).Returns(30000000);
        contractMock.Setup(x => x.Length).Returns(1);

        var newContract = contractMaker.GetNewGreaterContract(contractMock.Object, 2018);

        Assert.LessOrEqual(newContract.Value, 33000000 * newContract.Length);
    }

    [Test]
    public void GetNewGreaterContract_ContractIsHigherThan20Million_ReturnsContractValueNotHigherThan30Million()
    {
        var contractMock = new Mock<IContract>();
        contractMock.Setup(x => x.Value).Returns(20000000);
        contractMock.Setup(x => x.Length).Returns(1);

        var newContract = contractMaker.GetNewGreaterContract(contractMock.Object, 2018);

        Assert.LessOrEqual(newContract.Value, 30000000 * newContract.Length);
    }

    [Test]
    public void GetNewGreaterContract_ContractIsHigherThan10Million_ReturnsContractValueNotHigherThan18Million()
    {
        var contractMock = new Mock<IContract>();
        contractMock.Setup(x => x.Value).Returns(10000000);
        contractMock.Setup(x => x.Length).Returns(1);

        var newContract = contractMaker.GetNewGreaterContract(contractMock.Object, 2018);

        Assert.LessOrEqual(newContract.Value, 18000000 * newContract.Length);
    }

    [Test]
    public void GetNewGreaterContract_ContractIsHigherThan5Million_ReturnsContractValueNotHigherThan10Million()
    {
        var contractMock = new Mock<IContract>();
        contractMock.Setup(x => x.Value).Returns(5000000);
        contractMock.Setup(x => x.Length).Returns(1);

        var newContract = contractMaker.GetNewGreaterContract(contractMock.Object, 2018);

        Assert.LessOrEqual(newContract.Value, 10000000 * newContract.Length);
    }

    [Test]
    public void GetNewGreaterContract_ContractIsHigherThan1Million_ReturnsContractValueNotHigherThan3Million()
    {
        var contractMock = new Mock<IContract>();
        contractMock.Setup(x => x.Value).Returns(10000000);
        contractMock.Setup(x => x.Length).Returns(1);

        var newContract = contractMaker.GetNewGreaterContract(contractMock.Object, 2018);

        Assert.LessOrEqual(newContract.Value, 30000000 * newContract.Length);
    }

    [Test]
    public void GetNewGreaterContract_ContractIsLessThan1Million_ReturnsContractValueNotHigherThan4Million()
    {
        var contractMock = new Mock<IContract>();
        contractMock.Setup(x => x.Value).Returns(9000000);
        contractMock.Setup(x => x.Length).Returns(1);

        var newContract = contractMaker.GetNewGreaterContract(contractMock.Object, 2018);

        Assert.LessOrEqual(newContract.Value, 40000000 * newContract.Length);
    }

    [Test]
    public void GetNewLowerContract_ContractIsHigherThan30Million_ReturnsContractIsLower()
    {
        var contractMock = new Mock<IContract>();
        contractMock.Setup(x => x.Value).Returns(30000000);
        contractMock.Setup(x => x.Length).Returns(1);
        contractMock.Setup(x => x.YearOfSign).Returns(2017);

        var newContract = contractMaker.GetNewLowerContract(contractMock.Object, 2018);

        Assert.LessOrEqual(newContract.Value, 30000000 * newContract.Length);
    }

    [Test]
    public void GetNewLowerContract_ContractIsHigherThan20Million_ReturnsContractIsLower()
    {
        var contractMock = new Mock<IContract>();
        contractMock.Setup(x => x.Value).Returns(20000000);
        contractMock.Setup(x => x.Length).Returns(1);
        contractMock.Setup(x => x.YearOfSign).Returns(2017);

        var newContract = contractMaker.GetNewLowerContract(contractMock.Object, 2018);

        Assert.LessOrEqual(newContract.Value, 20000000 * newContract.Length);
    }

    [Test]
    public void GetNewLowerContract_ContractIsLowerThan10Million_ReturnsContractIsLower()
    {
        var contractMock = new Mock<IContract>();
        contractMock.Setup(x => x.Value).Returns(10000000);
        contractMock.Setup(x => x.Length).Returns(1);
        contractMock.Setup(x => x.YearOfSign).Returns(2017);

        var newContract = contractMaker.GetNewLowerContract(contractMock.Object, 2018);

        Assert.LessOrEqual(newContract.Value, 10000000 * newContract.Length);
    }
}
