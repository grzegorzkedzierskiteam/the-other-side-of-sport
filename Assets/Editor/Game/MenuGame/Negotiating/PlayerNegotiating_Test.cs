﻿using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using Moq;

public class PlayerNegotiating_Test {

    [Test]
    public void GetAndSetNegotiatingEffects_RelationOfClientIs100AndOfferIs2LengthAndWage1Popularity1OverallSkill1_ClientDoesNotHaveAgent_Over16()
    {
        var offerMock = new Mock<IOffer>();
        offerMock.Setup(x => x.ContractLength).Returns(2);
        offerMock.Setup(x => x.ContractValue).Returns(1);

        var playerAsBidderMock = new Mock<IBidder>();
        playerAsBidderMock.Setup(x => x.Popularity).Returns(100);
        playerAsBidderMock.Setup(x => x.Respectability).Returns(100);

        var clientMock = new Mock<IClientObject>();
        var clientCharacterMock = new Mock<ICharacter>();
        clientMock.Setup(x => x.Relation).Returns(100);
        clientMock.Setup(x => x.Character).Returns(clientCharacterMock.Object);
        clientMock.Setup(x => x.Popularity).Returns(1);
        clientMock.Setup(x => x.OverallSkill).Returns(1);

        PlayerNegotiatingWithClient negotiating = new PlayerNegotiatingWithClient(playerAsBidderMock.Object, clientMock.Object, offerMock.Object);

        Assert.Greater(negotiating.GetAndSetNegotiatingEffects(), 16);
    }

    [Test]
    public void GetAndSetNegotiatingEffects_RelationOfClientIs100AndOfferIs2LengthAndWage1Popularity1OverallSkill1_ClientHasAgent_Over16()
    {
        var offerMock = new Mock<IOffer>();
        offerMock.Setup(x => x.ContractLength).Returns(2);
        offerMock.Setup(x => x.ContractValue).Returns(1);

        var playerAsBidderMock = new Mock<IBidder>();
        playerAsBidderMock.Setup(x => x.Popularity).Returns(100);
        playerAsBidderMock.Setup(x => x.Respectability).Returns(100);

        var currentAgentMock = new Mock<IBidder>();
        playerAsBidderMock.Setup(x => x.Popularity).Returns(1);
        playerAsBidderMock.Setup(x => x.Respectability).Returns(1);

        var clientMock = new Mock<IClientObject>();
        var clientCharacterMock = new Mock<ICharacter>();
        clientMock.Setup(x => x.AgentID).Returns(4);
        clientMock.Setup(x => x.Relation).Returns(100);
        clientMock.Setup(x => x.Character).Returns(clientCharacterMock.Object);
        clientMock.Setup(x => x.Popularity).Returns(1);
        clientMock.Setup(x => x.OverallSkill).Returns(1);

        PlayerNegotiatingWithClient negotiating = new PlayerNegotiatingWithClient(playerAsBidderMock.Object, clientMock.Object, offerMock.Object,currentAgentMock.Object);

        Assert.Greater(negotiating.GetAndSetNegotiatingEffects(), 16);
    }
}
