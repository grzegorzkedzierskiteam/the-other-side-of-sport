﻿using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using Mono.Data.Sqlite;
using Messages;

namespace Messages_Tests
{
    public class MessageDeletingEffects_Test
    {
        private DBConnection dbConnection;
        private MenuController menuController;

        [OneTimeSetUp]
        public void Init()
        {
            dbConnection = new DBConnection();
            menuController = new MenuController();
        }

        [OneTimeTearDown]
        public void Disconnect()
        {
            dbConnection.CloseConnection();
            dbConnection = null;
        }

        [Test]
        public void DecreasePopularity_PlayerPopularityWasCorrectlyChanged()
        {
            dbConnection.StartTransaction();

            string query = "SELECT ID, Popularity FROM players WHERE Popularity > 50 LIMIT 1";
            SqliteDataReader reader = dbConnection.DownloadQuery(query);
            reader.Read();
            int id = reader.GetInt32(0);
            int popularity = reader.GetInt32(1);
            reader.Close();
            reader = null;

            MessageDeletingEffects.DecreasePopularity(dbConnection, id, PersonType.Athlete, 1, 10);

            query = "SELECT Popularity FROM players WHERE ID = " + id + ";";
            reader = dbConnection.DownloadQuery(query);
            reader.Read();
            int changedPopularity = reader.GetInt32(0);
            reader.Close();
            reader = null;

            Assert.Less(changedPopularity, popularity);
            Assert.Greater(changedPopularity, popularity - 10);
            Assert.AreNotSame(popularity, changedPopularity);
        }

        [Test]
        public void DeletePlayerFromTeam_PlayerWasDeletedFromTeam()
        {
            dbConnection.StartTransaction();

            string query = "SELECT ID,Team FROM players WHERE Team NOT IN ('None','Retirement') LIMIT 1;";
            SqliteDataReader reader = dbConnection.DownloadQuery(query);
            reader.Read();
            int id = reader.GetInt32(0);
            string team = reader.GetString(1);
            reader.Close();
            reader = null;

            MessageDeletingEffects.DeletePlayerFromTeam(dbConnection, id);

            query = "SELECT Team FROM players WHERE ID = " + id + ";";
            reader = dbConnection.DownloadQuery(query);
            reader.Read();
            string changedTeam = reader.GetString(0);
            reader.Close();
            reader = null;

            Assert.AreNotEqual(team, changedTeam);
            Assert.AreEqual("None", changedTeam);
        }
    }
}