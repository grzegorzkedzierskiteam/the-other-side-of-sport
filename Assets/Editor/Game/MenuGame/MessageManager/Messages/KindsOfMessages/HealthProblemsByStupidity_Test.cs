﻿using Messages;
using Mono.Data.Sqlite;
using NUnit.Framework;
using System.Collections.Generic;
using UnityEngine;

namespace Messages_Tests
{
    public class HealthProblemsByStupidity_Test
    {
        private DBConnection dbConnection;
        private MenuController menuController;
        private MessageManagerDB messageManagerDb;
        private IMessageManager messageManager;
        private IMessageEffects messageEffects;
        private IPlayerEnergy playerEnergy;
        private int participantID;
        private MyDateTime dateTime;

        [OneTimeSetUp]
        public void Init()
        {
            dbConnection = new DBConnection();
            menuController = new MenuController();
            dateTime = new MyDateTime(1, 2018);
            messageManagerDb = new MessageManagerDB(menuController);
            messageManagerDb.SetUpDate(dateTime);
            playerEnergy = new PlayerEnergy(menuController);

            messageEffects = new MessageEffects(menuController, messageManagerDb, playerEnergy);
            messageManager = new MessageManager(messageEffects);

            var query = "SELECT ID FROM players WHERE Team IN('None','Retirement') LIMIT 1;";
            var reader = dbConnection.DownloadQuery(query);
            reader.Read();
            participantID = reader.GetInt32(0);
            reader.Close();
        }

        [OneTimeTearDown]
        public void Disconnect()
        {
            dbConnection.CloseConnection();
            dbConnection = null;
        }

        [Test]
        public void Delete_PlayerIsNotInAnyTeam_CorrectlyExecuted()
        {
            dbConnection.StartTransaction();
            var query = "DELETE FROM messages;";
            dbConnection.ModifyQuery(query);

            query = "UPDATE agents SET Energy = 100 WHERE ID = 1;" +
                    "UPDATE players SET Wealth = 100000, Popularity = 100 WHERE ID = " + participantID + ";";
            dbConnection.ModifyQuery(query);

            var messageResponses = new List<MessageResponse>();

            var talk = new MessageResponse(TypeOfResponse.Talk, new List<ResponseCost>()
            {
                new ResponseCost(5, PropertyEnum.Energy),
                new ResponseCost(20, PropertyEnum.Relation)
            });

            messageResponses.Add(talk);

            var healthProblemsByStupidityMessage = new HealthProblemsByStupidity(0, "Test", 30, participantID, "Test",
                PersonType.Athlete, dateTime, messageResponses);

            messageManagerDb.AddMessageToDB(dbConnection, healthProblemsByStupidityMessage);

            var message = messageManagerDb.GetAllMessagesFromDB(dbConnection).Find(x => x.Description == "Test");
            //message.DeleteMessage(dbConnection);

            messageManager.DeleteMessage(dbConnection, message);

            query = "SELECT ID FROM messages WHERE Description = 'Test';";
            var reader = dbConnection.DownloadQuery(query);

            Assert.IsFalse(reader.HasRows);

            reader.Close();
            reader = null;

            query = "SELECT Popularity FROM players WHERE ID = " + participantID + ";";
            reader = dbConnection.DownloadQuery(query);

            reader.Read();
            Assert.Less(reader.GetInt32(0), 101);

            reader.Close();
            reader = null;
        }

        [Test]
        public void Respond_TalkMethod_CorrectlyExecuted()
        {
            dbConnection.StartTransaction();
            var query = "UPDATE agents SET Energy = 100 WHERE ID = 1;" +
                        "UPDATE players SET Relation = 50 WHERE ID = " + participantID + ";";

            dbConnection.ModifyQuery(query);

            var messageResponses = new List<MessageResponse>();

            var talk = new MessageResponse(TypeOfResponse.Talk, new List<ResponseCost>()
            {
                new ResponseCost(-5, PropertyEnum.Energy),
                new ResponseCost(20, PropertyEnum.Relation)
            });

            messageResponses.Add(talk);

            var healthProblemsByStupidityMessage = new HealthProblemsByStupidity(0, "Test", 30, participantID, "Test",
                PersonType.Athlete, dateTime, messageResponses);
            messageManagerDb.AddMessageToDB(dbConnection, healthProblemsByStupidityMessage);

            var message = messageManagerDb.GetAllMessagesFromDB(dbConnection).Find(x => x.Description == "Test");
            //message.Respond(dbConnection, TypeOfResponse.Talk);
            messageManager.Respond(dbConnection, message, TypeOfResponse.Talk);
            query = "SELECT ID FROM messages WHERE Description = 'Test';";
            var reader = dbConnection.DownloadQuery(query);

            Assert.IsFalse(reader.HasRows);

            reader.Close();
            reader = null;

            query = "SELECT Energy FROM agents WHERE ID = 1;";
            reader = dbConnection.DownloadQuery(query);

            reader.Read();
            Assert.AreEqual(95, reader.GetInt32(0));

            reader.Close();
            reader = null;

            query = "SELECT Relation FROM players WHERE ID = " + participantID + ";";
            reader = dbConnection.DownloadQuery(query);

            reader.Read();
            Assert.AreEqual(70, reader.GetInt32(0));

            reader.Close();
            reader = null;
        }
    }
}