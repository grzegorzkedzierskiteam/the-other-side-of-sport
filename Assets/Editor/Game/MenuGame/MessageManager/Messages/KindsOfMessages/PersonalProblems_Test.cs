﻿using Messages;
using Mono.Data.Sqlite;
using NUnit.Framework;
using UnityEngine;

namespace Messages_Tests
{
    public class PersonalProblems_Test
    {
        private DBConnection dbConnection;
        private MenuController menuController;
        private IMessageManagerDB messageManagerDb;
        private IMessageManager messageManager;
        private MessageGenerator messageGenerator;
        private int participantID;
        private MyDateTime dateTime;

        [SetUp]
        public void Init()
        {
            dbConnection = new DBConnection();
            menuController = new MenuController();
            dateTime = new MyDateTime(1, 2018);
            messageManagerDb = new MessageManagerDB(menuController);
            messageManagerDb.SetUpDate(dateTime);
            var messageEffects = new MessageEffects(menuController, messageManagerDb, new PlayerEnergy(menuController));
            messageManager = new MessageManager(messageEffects);
            messageGenerator = new MessageGenerator(dbConnection, new InjuriesController(dbConnection));

            var query = "SELECT ID FROM players WHERE Team IN('None','Retirement') LIMIT 1;";
            var reader = dbConnection.DownloadQuery(query);
            reader.Read();
            participantID = reader.GetInt32(0);
            reader.Close();
            reader = null;
        }

        [TearDown]
        public void Disconnect()
        {
            dbConnection.CloseConnection();
            dbConnection = null;
        }

        [Test]
        public void Delete_CorrectlyExecuted()
        {
            dbConnection.StartTransaction();
            var query = "DELETE FROM messages;";
            dbConnection.ModifyQuery(query);

            query = "UPDATE agents SET Energy = 100 WHERE ID = 1;" +
                    "UPDATE players SET Wealth = 100000, Popularity = 100 WHERE ID = " + participantID + ";";
            dbConnection.ModifyQuery(query);

            var client = ClientData.GetAllDetails(dbConnection, participantID);
            var personalProblem = messageGenerator.GetMessage(client, TypeOfMessage.PersonalProblems, dateTime);
            messageManagerDb.AddMessageToDB(dbConnection, personalProblem);

            var message = messageManagerDb.GetAllMessagesFromDB(dbConnection)
                .Find(x => x.TypeOfMessage == TypeOfMessage.PersonalProblems);
            //message.DeleteMessage(dbConnection);
            messageManager.DeleteMessage(dbConnection, message);

            query = "SELECT ID FROM messages WHERE Description = 'Test';";
            var reader = dbConnection.DownloadQuery(query);

            Assert.IsFalse(reader.HasRows);

            reader.Close();
            reader = null;

            query = "SELECT Relation, OverallSkill FROM players WHERE ID = " + participantID + ";";
            reader = dbConnection.DownloadQuery(query);

            reader.Read();
            Assert.LessOrEqual(reader.GetInt32(0), client.Relation);
            Assert.LessOrEqual(reader.GetInt32(1), client.OverallSkill);

            reader.Close();
            reader = null;
        }

        [Test]
        public void Respond_HelpMethod_CorrectlyExecuted()
        {
            dbConnection.StartTransaction();
            var query = "DELETE FROM messages";
            dbConnection.ModifyQuery(query);

            query = "UPDATE agents SET Energy = 100 WHERE ID = 1;" +
                        "UPDATE players SET Relation = 50 WHERE ID = " + participantID + ";";

            dbConnection.ModifyQuery(query);

            var client = ClientData.GetAllDetails(dbConnection, participantID);

            var personalProblem = messageGenerator.GetMessage(client, TypeOfMessage.PersonalProblems, dateTime);
            messageManagerDb.AddMessageToDB(dbConnection, personalProblem);

            var message = messageManagerDb.GetAllMessagesFromDB(dbConnection)
                .Find(x => x.TypeOfMessage == TypeOfMessage.PersonalProblems);
            //message.Respond(dbConnection, TypeOfResponse.Help);
            messageManager.Respond(dbConnection, message, TypeOfResponse.Help);
            Debug.Log(message.MessageResponses[0].ListOfCost[1].Value);
            query = "SELECT ID FROM messages WHERE Description = 'Test';";
            var reader = dbConnection.DownloadQuery(query);

            Assert.IsFalse(reader.HasRows);

            reader.Close();
            reader = null;

            query = "SELECT Energy FROM agents WHERE ID = 1;";
            reader = dbConnection.DownloadQuery(query);

            reader.Read();
            Assert.Less(reader.GetInt32(0), 100);

            reader.Close();
            reader = null;

            query = "SELECT Relation FROM players WHERE ID = " + participantID + ";";
            reader = dbConnection.DownloadQuery(query);

            reader.Read();
            Assert.Greater(reader.GetInt32(0), 50);

            reader.Close();
            reader = null;
        }
    }
}