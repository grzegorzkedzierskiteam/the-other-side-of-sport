﻿using System.Collections;
using System.Collections.Generic;
using Messages;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Messages_Tests
{
    public class HealthProblems_Test
    {
        private DBConnection dbConnection;
        private MenuController menuController;
        private MessageManagerDB messageManagerDb;
        private IMessageManager messageManager;
        private IMessageGenerator messageGenerator;
        private IMessageEffects messageEffects;
        private IPlayerEnergy playerEnergy;
        private int participantID;
        private MyDateTime dateTime;

        [SetUp]
        public void Init()
        {
            dbConnection = new DBConnection();
            menuController = new MenuController();
            dateTime = new MyDateTime(1, 2018);
            messageManagerDb = new MessageManagerDB(menuController);
            messageManagerDb.SetUpDate(dateTime);
            playerEnergy = new PlayerEnergy(menuController);
            messageGenerator = new MessageGenerator(dbConnection, new InjuriesController(dbConnection));

            messageEffects = new MessageEffects(menuController, messageManagerDb, playerEnergy);
            messageManager = new MessageManager(messageEffects);

            var query = "SELECT ID FROM players WHERE Team IN('None','Retirement') LIMIT 1;";
            var reader = dbConnection.DownloadQuery(query);
            reader.Read();
            participantID = reader.GetInt32(0);
            reader.Close();
        }

        [TearDown]
        public void Disconnect()
        {
            dbConnection.CloseConnection();
            dbConnection = null;
        }

        [Test]
        public void Delete_CorrectlyExecuted()
        {
            dbConnection.StartTransaction();
            var query = "DELETE FROM messages;";
            dbConnection.ModifyQuery(query);

            query = "UPDATE agents SET Energy = 100 WHERE ID = 1;" +
                    "UPDATE players SET Wealth = 100000, Popularity = 100 WHERE ID = " + participantID + ";";
            dbConnection.ModifyQuery(query);

            var client = ClientData.GetAllDetails(dbConnection, participantID);

            var healthProblems = messageGenerator.GetMessage(client, TypeOfMessage.HealthProblems, dateTime);

            messageManagerDb.AddMessageToDB(dbConnection, healthProblems);

            var message = messageManagerDb.GetAllMessagesFromDB(dbConnection)[0];

            messageManager.DeleteMessage(dbConnection, message);

            query = "SELECT ID FROM messages WHERE Description = 'Test';";
            var reader = dbConnection.DownloadQuery(query);

            Assert.IsFalse(reader.HasRows);

            reader.Close();
            reader = null;

            query = "SELECT Popularity, OverallSkill FROM players WHERE ID = " + participantID + ";";
            reader = dbConnection.DownloadQuery(query);

            reader.Read();
            Assert.Less(reader.GetInt32(0), 101);
            Assert.LessOrEqual(reader.GetInt32(1), client.OverallSkill);

            reader.Close();
            reader = null;

        }
    }
}
