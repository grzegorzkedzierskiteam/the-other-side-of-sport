﻿using System.Diagnostics;
using Messages;
using Mono.Data.Sqlite;
using NUnit.Framework;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace Messages_Tests
{
    public class AccusationOfDomesticViolence_Test
    {
        private DBConnection dbConnection;
        private MenuController menuController;
        private MessageManagerDB messageManagerDb;
        private MessageGenerator messageGenerator;
        private IMessageManager messageManager;
        private IMessageEffects messageEffects;
        private int participantID;
        private MyDateTime dateTime;

        [SetUp]
        public void Init()
        {
            dbConnection = new DBConnection();
            menuController = new MenuController();
            dateTime = new MyDateTime(1, 2018);
            messageManagerDb = new MessageManagerDB(menuController);
            messageManagerDb.SetUpDate(dateTime);
            messageGenerator = new MessageGenerator(dbConnection, new InjuriesController(dbConnection));

            messageEffects = new MessageEffects(menuController, messageManagerDb, new PlayerEnergy(menuController));
            messageManager = new MessageManager(messageEffects);

            var query = "SELECT ID FROM players WHERE Team IN('None','Retirement') LIMIT 1;";
            var reader = dbConnection.DownloadQuery(query);
            reader.Read();
            participantID = reader.GetInt32(0);
            reader.Close();
            reader = null;

            dbConnection.StartTransaction();
            query = "DELETE FROM messages; DELETE FROM messages_responses;";
            dbConnection.ModifyQuery(query);
        }

        [TearDown]
        public void Disconnect()
        {
            dbConnection.CloseConnection();
            dbConnection = null;
        }

        [Test]
        public void Respond_PaymentMethod_CorrectlyExecuted()
        {
            var query = "UPDATE agents SET Energy = 100 WHERE ID = 1;" +
                        "UPDATE players SET Wealth = 10000000, Popularity = 100, Relation = 100 WHERE ID = " +
                        participantID + ";";
            dbConnection.ModifyQuery(query);

            var client = ClientData.GetAllDetails(dbConnection, participantID);

            var accusationOfDomesticViolence =
                messageGenerator.GetMessage(client, TypeOfMessage.AccusationOfDomesticViolence, dateTime);
            messageManagerDb.AddMessageToDB(dbConnection, accusationOfDomesticViolence);

            var message = messageManagerDb.GetAllMessagesFromDB(dbConnection)
                .Find(x => x.TypeOfMessage == TypeOfMessage.AccusationOfDomesticViolence);
            //message.Respond(dbConnection, TypeOfResponse.Payment);
            messageManager.Respond(dbConnection, message, TypeOfResponse.Payment);

            query = "SELECT ID FROM messages WHERE TypeOfMessage = 'AccusationOfDomesticViolence';";
            var reader = dbConnection.DownloadQuery(query);

            Assert.IsFalse(reader.HasRows);

            reader.Close();
            reader = null;

            query = "SELECT Wealth, Relation FROM players WHERE ID = " + participantID + ";";
            reader = dbConnection.DownloadQuery(query);

            reader.Read();
            Assert.Less(reader.GetInt32(0), 100000000);
            Assert.Less(reader.GetInt32(1), 100);

            reader.Close();
            reader = null;

            query = "SELECT Energy FROM agents WHERE ID = 1;";
            reader = dbConnection.DownloadQuery(query);

            reader.Read();
            Assert.Less(reader.GetInt32(0), 100);

            reader.Close();
            reader = null;
        }

        [Test]
        public void Respond_TalkMethod_CorrectlyExecuted()
        {
            var query = "UPDATE agents SET Energy = 100 WHERE ID = 1;" +
                        "UPDATE players SET Relation = 50 WHERE ID = " + participantID + ";";

            dbConnection.ModifyQuery(query);

            var client = ClientData.GetAllDetails(dbConnection, participantID);

            var assaultMessage =
                messageGenerator.GetMessage(client, TypeOfMessage.AccusationOfDomesticViolence, dateTime);
            messageManagerDb.AddMessageToDB(dbConnection, assaultMessage);

            var message = messageManagerDb.GetAllMessagesFromDB(dbConnection)
                .Find(x => x.TypeOfMessage == TypeOfMessage.AccusationOfDomesticViolence);
            //message.Respond(dbConnection, TypeOfResponse.Talk);
            messageManager.Respond(dbConnection, message, TypeOfResponse.Talk);

            query = "SELECT ID FROM messages WHERE Description = 'Test';";
            var reader = dbConnection.DownloadQuery(query);

            Assert.IsFalse(reader.HasRows);

            reader.Close();
            reader = null;

            query = "SELECT Energy FROM agents WHERE ID = 1;";
            reader = dbConnection.DownloadQuery(query);

            reader.Read();
            Assert.Less(reader.GetInt32(0), 100);

            reader.Close();
            reader = null;

            query = "SELECT Relation FROM players WHERE ID = " + participantID + ";";
            reader = dbConnection.DownloadQuery(query);

            reader.Read();
            Assert.Greater(reader.GetInt32(0), 50);

            reader.Close();
            reader = null;
        }

        [Test]
        public void Delete_PlayerIsNotInAnyTeam_CorrectlyExecuted()
        {
            var query = "DELETE FROM messages;";
            dbConnection.ModifyQuery(query);

            query = "UPDATE agents SET Energy = 100 WHERE ID = 1;" +
                    "UPDATE players SET Wealth = 100000, Popularity = 100 WHERE ID = " + participantID + ";";
            dbConnection.ModifyQuery(query);

            var client = ClientData.GetAllDetails(dbConnection, participantID);

            var domesticViolence =
                messageGenerator.GetMessage(client, TypeOfMessage.AccusationOfDomesticViolence, dateTime);

            messageManagerDb.AddMessageToDB(dbConnection, domesticViolence);

            var message = messageManagerDb.GetAllMessagesFromDB(dbConnection)
                .Find(x => x.TypeOfMessage == TypeOfMessage.AccusationOfDomesticViolence);
            //message.DeleteMessage(dbConnection);
            messageManager.DeleteMessage(dbConnection, message);

            query = "SELECT COUNT(*) FROM messages;";
            var reader = dbConnection.DownloadQuery(query);
            reader.Read();

            Assert.Less(reader.GetInt32(0), 2);

            reader.Close();
            reader = null;
        }
    }
}