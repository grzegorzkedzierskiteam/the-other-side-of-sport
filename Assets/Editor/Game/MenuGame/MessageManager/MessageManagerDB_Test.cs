﻿using Mono.Data.Sqlite;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using UnityEngine;

namespace Messages_Tests
{
    public class MessageManagerDB_Test
    {
        private IDBConnection dbConnection;
        private IMessageManagerDB messageManagerDb;

        [OneTimeSetUp]
        public void Init()
        {
            dbConnection = new DBConnection();
            var menuController = new MenuController();
            var dateTime = new MyDateTime(1, 2018);
            messageManagerDb = new MessageManagerDB(menuController);
            messageManagerDb.SetUpDate(dateTime);
        }

        [OneTimeTearDown]
        public void Disconnect()
        {
            dbConnection.CloseConnection();
            dbConnection = null;
        }

        [Test]
        public void AddMessageToDB_InsertingCorrectMessage_WasInserted()
        {
            dbConnection.StartTransaction();

            var query = "DELETE FROM messages";
            dbConnection.ModifyQuery(query);

            var message = new Mock<IMessage>();
            message.Setup(x => x.ExpiringTime).Returns(999);
            message.Setup(x => x.DoesHaveEffect).Returns(true);
            message.Setup(x => x.Description).Returns("TEST");
            message.Setup(x => x.ParticipantID).Returns(2);
            message.Setup(x => x.ParticipantName).Returns("Test");
            message.Setup(x => x.ParticipantType).Returns(PersonType.Agent);
            message.Setup(x => x.TypeOfMessage).Returns(TypeOfMessage.Assault);
            message.Setup(x => x.MessageResponses).Returns(new List<MessageResponse>()
            {
                new MessageResponse(TypeOfResponse.Payment,
                    new List<ResponseCost>() {new ResponseCost(1, PropertyEnum.Energy)})
            });

            message.Setup(x => x.DateTime).Returns(new MyDateTime(1, 1999));

            messageManagerDb.AddMessageToDB(dbConnection, message.Object);

            query =
                "SELECT ID, Description, ExpiringTime, DoesHaveEffect, ParticipantID, ParticipantName, ParticipantType, TypeOfMessage, Week, Year FROM messages ORDER BY ID DESC";
            var reader = dbConnection.DownloadQuery(query);
            if (reader.HasRows)
            {
                reader.Read();

                Assert.IsNotNull(reader.GetInt32(0));
                Assert.IsNotNull(reader.GetString(1));
                Assert.IsNotNull(reader.GetInt32(2));
                Assert.IsNotNull(reader.GetInt32(3));
                Assert.IsNotNull(reader.GetInt32(4));
                Assert.IsNotNull(reader.GetString(5));
                Assert.IsNotNull(reader.GetString(6));
                Assert.IsNotNull(reader.GetString(7));
                Assert.IsNotNull(reader.GetInt32(8));
                Assert.IsNotNull(reader.GetInt32(9));
            }
            else
                Assert.Fail();

            reader.Close();
            reader = null;
        }

        [Test]
        public void GetAllMessagesFromDB_TwoMessagesExist_ReturnsListWith3Elements()
        {
            dbConnection.StartTransaction();

            var query = "DELETE FROM messages";
            dbConnection.ModifyQuery(query);

            for (var i = 0; i < 3; i++)
            {
                var message = new Mock<IMessage>();
                message.Setup(x => x.ExpiringTime).Returns(999);
                message.Setup(x => x.DoesHaveEffect).Returns(true);
                message.Setup(x => x.Description).Returns("TEST");
                message.Setup(x => x.ParticipantID).Returns(2);
                message.Setup(x => x.ParticipantName).Returns("Test");
                message.Setup(x => x.ParticipantType).Returns(PersonType.Agent);
                if (i % 2 == 0)
                    message.Setup(x => x.TypeOfMessage).Returns(TypeOfMessage.Assault);
                else
                    message.Setup(x => x.TypeOfMessage).Returns(TypeOfMessage.Notification);
                message.Setup(x => x.MessageResponses).Returns(new List<MessageResponse>()
                {
                    new MessageResponse(TypeOfResponse.Payment,
                        new List<ResponseCost>() {new ResponseCost(1, PropertyEnum.Energy)})
                });
                message.Setup(x => x.DateTime).Returns(new MyDateTime(1, 1999));

                messageManagerDb.AddMessageToDB(dbConnection, message.Object);
            }

            var messages = messageManagerDb.GetAllMessagesFromDB(dbConnection);

            foreach (var message in messages)
            {
                if (message.TypeOfMessage != TypeOfMessage.Notification)
                {
                    Assert.IsNotNull(message.MessageResponses);
                    Assert.IsNotNull(message.MessageResponses[0].TypeOfResponse);
                    Assert.IsNotNull(message.MessageResponses[0].ListOfCost);
                    Assert.IsNotNull(message.MessageResponses[0].ListOfCost[0].Value);
                    Assert.IsNotNull(message.MessageResponses[0].ListOfCost[0].Property);
                }

                Assert.IsNotNull(message.DateTime);
                Assert.IsNotNull(message.Description);
                Assert.IsNotNull(message.DoesHaveEffect);
                Assert.IsNotNull(message.ExpiringTime);
                Assert.IsNotNull(message.ID);
                Assert.IsNotNull(message.ParticipantID);
                Assert.IsNotNull(message.ParticipantName);
                Assert.IsNotNull(message.ParticipantType);
                Assert.IsNotNull(message.TypeOfMessage);
            }

            Assert.AreEqual(3, messages.Count);
        }
    }
}
