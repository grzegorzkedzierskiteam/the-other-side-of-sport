﻿using Messages;
using Mono.Data.Sqlite;
using NUnit.Framework;

namespace Messages_Tests
{
    public class MessageGenerator_Test
    {

        private IDBConnection dbConnection;
        private IMessageManagerDB messageManagerDb;
        private IMessageGenerator messageGenerator;
        private MyDateTime dateTime;

        [OneTimeSetUp]
        public void Init()
        {
            dbConnection = new DBConnection();
            var menuController = new MenuController();
            dateTime = new MyDateTime(1, 2018);
            messageManagerDb = new MessageManagerDB(menuController);
            messageManagerDb.SetUpDate(dateTime);
            var injuriesController = new InjuriesController(dbConnection);
            messageGenerator = new MessageGenerator(dbConnection, injuriesController);
        }

        [OneTimeTearDown]
        public void Disconnect()
        {
            dbConnection.CloseConnection();
            dbConnection = null;
        }

        [Test]
        public void GetMessage_Assault_ReturnsCorrectlyObject()
        {

            var query = "SELECT ID FROM players ORDER BY RANDOM() LIMIT 1;";
            var reader = dbConnection.DownloadQuery(query);
            reader.Read();
            var id = reader.GetInt32(0);
            reader.Close();
            reader = null;

            IPerson person = ClientData.GetAllDetails(dbConnection, id);

            var assaultMessage = messageGenerator.GetMessage(person, TypeOfMessage.Assault, dateTime);

            Assert.NotNull(assaultMessage.MessageResponses);
            Assert.NotNull(assaultMessage.MessageResponses[0].TypeOfResponse);
            Assert.NotNull(assaultMessage.MessageResponses[0].ListOfCost[0]);
            Assert.NotNull(assaultMessage.MessageResponses[0].ListOfCost[1]);
            Assert.NotNull(assaultMessage.MessageResponses[1].TypeOfResponse);
            Assert.NotNull(assaultMessage.MessageResponses[1].ListOfCost[0]);
            Assert.NotNull(assaultMessage.DateTime);
            Assert.NotNull(assaultMessage.Description);
            Assert.NotNull(assaultMessage.DoesHaveEffect);
            Assert.NotNull(assaultMessage.ExpiringTime);
            Assert.NotNull(assaultMessage.ID);
            Assert.NotNull(assaultMessage.ParticipantID);
            Assert.NotNull(assaultMessage.ParticipantName);
            Assert.NotNull(assaultMessage.ParticipantType);
            Assert.NotNull(assaultMessage.TypeOfMessage);
        }

        [Test]
        public void GetMessage_HealthProblemsByStupidity_ReturnsCorrectlyObject()
        {
            var query = "SELECT ID FROM players ORDER BY RANDOM() LIMIT 1;";
            var reader = dbConnection.DownloadQuery(query);
            reader.Read();
            var id = reader.GetInt32(0);
            reader.Close();
            reader = null;

            IPerson person = ClientData.GetAllDetails(dbConnection, id);

            var assaultMessage = messageGenerator.GetMessage(person, TypeOfMessage.HealthProblemsByStupidity, dateTime);

            Assert.NotNull(assaultMessage.MessageResponses);
            Assert.NotNull(assaultMessage.MessageResponses[0].TypeOfResponse);
            Assert.NotNull(assaultMessage.MessageResponses[0].ListOfCost[0]);
            Assert.NotNull(assaultMessage.DateTime);
            Assert.NotNull(assaultMessage.Description);
            Assert.NotNull(assaultMessage.DoesHaveEffect);
            Assert.NotNull(assaultMessage.ExpiringTime);
            Assert.NotNull(assaultMessage.ID);
            Assert.NotNull(assaultMessage.ParticipantID);
            Assert.NotNull(assaultMessage.ParticipantName);
            Assert.NotNull(assaultMessage.ParticipantType);
            Assert.NotNull(assaultMessage.TypeOfMessage);
        }
    }
}