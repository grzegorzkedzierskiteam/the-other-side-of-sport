﻿using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;

public class PlayerEnergy_Test {

    IDBConnection dBConnection;
    private IPlayerEnergy playerEnergy;

    [OneTimeSetUp]
    public void Init()
    {
        dBConnection = new DBConnection();
        playerEnergy = new PlayerEnergy(new MenuController());
    }

    [OneTimeTearDown]
    public void Disconect()
    {
        dBConnection.CloseConnection();
        dBConnection = null;
    }

	[Test]
	public void GetPlayerEnergy_ReturnsCorrectValue() {
        dBConnection.StartTransaction();
        string query = "UPDATE agents SET Energy = 100 WHERE ID = 1;";
        dBConnection.ModifyQuery(query);

        int result = playerEnergy.GetPlayerEnergy(dBConnection);

        Assert.AreEqual(100, result);
	}

    [Test]
    public void ChangePlayerEnergy_Energy47Value5_Returns52()
    {
        dBConnection.StartTransaction();
        string query = "UPDATE agents SET Energy = 47 WHERE ID = 1;";
        dBConnection.ModifyQuery(query);

        playerEnergy.ChangePlayerEnergy(dBConnection, 5);

        int result = playerEnergy.GetPlayerEnergy(dBConnection);

        Assert.AreEqual(52, result);
    }

    [Test]
    public void ChangePlayerEnergy_Energy42ValueMinus10_Returns32()
    {
        dBConnection.StartTransaction();
        string query = "UPDATE agents SET Energy = 42 WHERE ID = 1;";
        dBConnection.ModifyQuery(query);

        playerEnergy.ChangePlayerEnergy(dBConnection, -10);

        int result = playerEnergy.GetPlayerEnergy(dBConnection);

        Assert.AreEqual(32, result);
    }

    [Test]
    public void ChangePlayerEnergy_Energy2ValueMinus10_Returns0()
    {
        dBConnection.StartTransaction();
        string query = "UPDATE agents SET Energy = 2 WHERE ID = 1;";
        dBConnection.ModifyQuery(query);

        playerEnergy.ChangePlayerEnergy(dBConnection, -10);

        int result = playerEnergy.GetPlayerEnergy(dBConnection);

        Assert.AreEqual(0, result);
    }

    [Test]
    public void ChangePlayerEnergy_Energy96Value6_Returns100()
    {
        dBConnection.StartTransaction();
        string query = "UPDATE agents SET Energy = 96 WHERE ID = 1;";
        dBConnection.ModifyQuery(query);

        playerEnergy.ChangePlayerEnergy(dBConnection, 6);

        int result = playerEnergy.GetPlayerEnergy(dBConnection);

        Assert.AreEqual(100, result);
    }
}
