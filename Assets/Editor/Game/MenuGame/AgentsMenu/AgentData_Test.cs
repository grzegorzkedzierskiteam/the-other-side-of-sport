﻿using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;

public class AgentData_Test {

    private DBConnection dbConnection;

    [OneTimeSetUp]
    public void Init()
    {
        dbConnection = new DBConnection();
    }

    [OneTimeTearDown]
    public void Disconnect()
    {
        dbConnection.CloseConnection();
        dbConnection = null;
    }

    [Test]
	public void DB_GetAgentDetails_CheckWasDataDownloaded_WasDownloaded() {
        IAgentObject agentObject = AgentData.GetAllDetails(dbConnection, 2);

        Assert.NotNull(agentObject.Allies);
        Assert.NotNull(agentObject.Character);
        Assert.NotNull(agentObject.Clients);
        Assert.NotNull(agentObject.Enemies);
        Assert.NotNull(agentObject.Energy);
        Assert.NotNull(agentObject.FirstName);
        Assert.NotNull(agentObject.Gender);
        Assert.NotNull(agentObject.ID);
        Assert.NotNull(agentObject.IsFavorite);
        Assert.NotNull(agentObject.LastName);
        Assert.NotNull(agentObject.Popularity);
        Assert.NotNull(agentObject.Relation);
        Assert.NotNull(agentObject.Respectability);
        Assert.NotNull(agentObject.Type);
        Assert.NotNull(agentObject.Wealth);
    }

    [Test]
    public void DB_GetAgentDetails_CheckWasDataDownloaded_WasNotDownloaded()
    {
        IAgentObject agentObject = AgentData.GetAllDetails(dbConnection, 999999999);

        Assert.Null(agentObject);
    }

    [Test]
    public void DB_GetAgentAsBidder_CheckWasDataDownloaded_WasDownloaded()
    {
        IBidder bidder = AgentData.GetAgentAsBidder(dbConnection, 2);

        Assert.NotNull(bidder.Popularity);
        Assert.NotNull(bidder.Respectability);
    }

    [Test]
    public void DB_GetAgentAsBidder_CheckWasDataDownloaded_WasNotDownloaded()
    {
        IBidder bidder = AgentData.GetAgentAsBidder(dbConnection, 999999999);

        Assert.Null(bidder);
    }

    [Test]
    public void DB_GetAllAgents_CheckWereDownloaded100Agents_WasDownloaded()
    {
        List<AgentObject> agentsList = AgentData.GetAllAgentsOrReturnNull(dbConnection);

        Assert.AreEqual(100, agentsList.Count);
    }

    [Test]
    public void DB_GetAllClientsOfAgent_DoseNotThrow()
    {
        Assert.DoesNotThrow(() => AgentData.GetAllClientsOfAgent(dbConnection,1));
    }

    [Test]
    public void DB_UpdateStringGroupDB_UpdateClients_WasUpdated()
    {
        dbConnection.StartTransaction();
        string newGroup = "NEW+GROUP";

        AgentData.UpdateStringGroupDB(dbConnection, 1, newGroup, AgentGroupEnum.Clients);

        AgentObject agent = AgentData.GetAllDetails(dbConnection, 1);

        Assert.AreEqual(newGroup, agent.Clients);
    }

    [Test]
    public void DB_UpdateStringGroupDB_UpdateAllies_WasUpdated()
    {
        dbConnection.StartTransaction();
        string newGroup = "NEW+GROUP";

        AgentData.UpdateStringGroupDB(dbConnection, 1, newGroup, AgentGroupEnum.Allies);

        AgentObject agent = AgentData.GetAllDetails(dbConnection, 1);

        Assert.AreEqual(newGroup, agent.Allies);
    }

    [Test]
    public void DB_UpdateStringGroupDB_UpdateEnemies_WasUpdated()
    {
        dbConnection.StartTransaction();
        string newGroup = "NEW+GROUP";

        AgentData.UpdateStringGroupDB(dbConnection, 1, newGroup, AgentGroupEnum.Enemies);

        AgentObject agent = AgentData.GetAllDetails(dbConnection, 1);

        Assert.AreEqual(newGroup, agent.Enemies);
    }
}
