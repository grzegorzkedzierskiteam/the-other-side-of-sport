﻿using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;

public class PlayerFinanceManager_Test {

    private DBConnection dbConnection;
    private PlayerFinanceManager manager;

    [OneTimeSetUp]
    public void Init()
    {
        dbConnection = new DBConnection();
        IPlayerFinanceQueryMaker maker = new PlayerFinanceQueryMaker();
        manager = new PlayerFinanceManager(dbConnection, maker);
    }

    [OneTimeTearDown]
    public void Disconnect()
    {
        dbConnection.CloseConnection();
        dbConnection = null;
    }

    [Test]
    public void DB_AddIncomeOrExpense_CorrectlyAdded()
    {
        dbConnection.StartTransaction();
        IncomeExpenseObject income = new IncomeExpenseObject(0, 111111, 111111, 111111, 1, "", "");

        Assert.DoesNotThrow(() => manager.AddIncomeOrExpense(income));
    }

    [Test]
    public void DB_GetIncomeOrExpense_DoesNotThrow()
    {
        Assert.DoesNotThrow(() => manager.GetIncomeOrExpense(1));
    }

    [Test]
    public void DB_GetAllIncomesAndExpenses_DoesNotThrow()
    {
        Assert.DoesNotThrow(() => manager.GetAllIncomesAndExpenses());
    }

    [Test]
    public void DB_GetAllIncomes_DoesNotThrow()
    {
        Assert.DoesNotThrow(() => manager.GetAllIncomes());
    }

    [Test]
    public void DB_GetAllExpenses_DoesNotThrow()
    {
        Assert.DoesNotThrow(() => manager.GetAllExpenses());
    }
}
