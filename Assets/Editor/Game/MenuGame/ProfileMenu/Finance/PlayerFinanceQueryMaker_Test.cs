﻿using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;

public class PlayerFinanceQueryMaker_Test {

    PlayerFinanceQueryMaker maker;

    [SetUp]
    public void Init()
    {
        maker = new PlayerFinanceQueryMaker();
    }

    [Test]
	public void GetInsertQuery_ReturnsCorrectlyQuery() {
        IncomeExpenseObject income = new IncomeExpenseObject(0,9,9,0,5,"a","ta");
        string query = maker.GetInsertQuery(income);

        string expectedQuery = "INSERT INTO finance(Week,Year,Value,PersonID,PersonName,Info) VALUES(9,9,0,5,'a','ta');";

        Assert.AreEqual(expectedQuery, query);
    }

    [Test]
    public void GetAllExpensesQuery_ReturnsCorrectlyQuery()
    {
        string query = maker.GetAllExpensesQuery();

        string expectedQuery = "SELECT ID,Week,Year,Value,PersonID,PersonName,Info FROM finance WHERE Value < 0;";

        Assert.AreEqual(expectedQuery, query);
    }

    [Test]
    public void GetAllIncomesQuery_ReturnsCorrectlyQuery()
    {
        string query = maker.GetAllIncomesQuery();

        string expectedQuery = "SELECT ID,Week,Year,Value,PersonID,PersonName,Info FROM finance WHERE Value >= 0;";

        Assert.AreEqual(expectedQuery, query);
    }

    [Test]
    public void GetAllIncomesAndExpensesQuery_ReturnsCorrectlyQuery()
    {
        string query = maker.GetAllIncomesAndExpensesQuery();

        string expectedQuery = "SELECT ID,Week,Year,Value,PersonID,PersonName,Info FROM finance;";

        Assert.AreEqual(expectedQuery, query);
    }
    
    [Test]
    public void GetIncomeOrExpenseQuery_ReturnsCorrectlyQuery()
    {
        string query = maker.GetIncomeOrExpenseQuery(1);

        string expectedQuery = "SELECT ID,Week,Year,Value,PersonID,PersonName,Info FROM finance WHERE ID = 1;";

        Assert.AreEqual(expectedQuery, query);
    }
}
