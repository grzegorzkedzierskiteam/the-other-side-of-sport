﻿using Mono.Data.Sqlite;
using NUnit.Framework;
using System.Collections.Generic;

public class ClientData_Test
{

    private DBConnection dbConnection;
    private int randomPlayerID;

    [OneTimeSetUp]
    public void Init()
    {
        dbConnection = new DBConnection();

        string query = "SELECT ID FROM players ORDER BY ID DESC LIMIT 1";
        SqliteDataReader reader = dbConnection.DownloadQuery(query);
        reader.Read();
        randomPlayerID = reader.GetInt32(0);
        reader.Close();
        reader = null;
    }

    [OneTimeTearDown]
    public void Disconnect()
    {
        dbConnection.CloseConnection();
        dbConnection = null;
    }

    [Test]
    public void DB_GetClientDetails_CheckWasDataDownloaded_WasDownloaded()
    {
        string query = "SELECT ID FROM players LIMIT 1";
        SqliteDataReader reader = dbConnection.DownloadQuery(query);
        reader.Read();
        int id = reader.GetInt32(0);
        reader.Close();
        reader = null;

        IClientObject clientObject = ClientData.GetAllDetails(dbConnection, randomPlayerID);

        Assert.NotNull(clientObject.ID);
        Assert.NotNull(clientObject.FirstName);
        Assert.NotNull(clientObject.LastName);
        Assert.NotNull(clientObject.Age);
        Assert.NotNull(clientObject.Wealth);
        Assert.NotNull(clientObject.Dept);
        Assert.NotNull(clientObject.AgentID);
        Assert.NotNull(clientObject.Character);
        Assert.NotNull(clientObject.College);
        Assert.NotNull(clientObject.Contract.Value);
        Assert.NotNull(clientObject.Contract.GuaranteedMoney);
        Assert.NotNull(clientObject.Contract.Length);
        Assert.NotNull(clientObject.Contract.YearOfSign);
        Assert.NotNull(clientObject.Contract.GotMoney);
        Assert.NotNull(clientObject.ContractWithAgent);
        Assert.NotNull(clientObject.InjuryName);
        Assert.NotNull(clientObject.InjuryTime);
        Assert.NotNull(clientObject.IsFavorite);
        Assert.NotNull(clientObject.ReadyStatus);
        Assert.NotNull(clientObject.OverallSkill);
        Assert.NotNull(clientObject.Popularity);
        Assert.NotNull(clientObject.Position);
        Assert.NotNull(clientObject.Prospectiveness);
        Assert.NotNull(clientObject.Relation);
        Assert.NotNull(clientObject.Sport);
        Assert.NotNull(clientObject.Team);
        Assert.NotNull(clientObject.Type);
    }

    [Test]
    public void DB_GetClientDetails_CheckWasDataDownloaded_WasNotDownloaded()
    {
        IClientObject clientObject = ClientData.GetAllDetails(dbConnection, 999999999);

        Assert.Null(clientObject);
    }

    [Test]
    public void DB_GetClientContract_CheckWasDataDownloaded_WasDownloaded()
    {
        IContract contract = ClientData.GetClientContract(dbConnection, randomPlayerID);

        Assert.NotNull(contract.GotMoney);
        Assert.NotNull(contract.GuaranteedMoney);
        Assert.NotNull(contract.Length);
        Assert.NotNull(contract.Value);
        Assert.NotNull(contract.YearOfSign);
    }

    [Test]
    public void DB_GetClientContract_CheckWasDataDownloaded_WasNotDownloaded()
    {
        IContract contract = ClientData.GetClientContract(dbConnection, 999999999);

        Assert.Null(contract);
    }

    [Test]
    public void DB_GetSelectedPlayersDetails_CheckWasDataDownloaded_WasDownloaded()
    {
        string query = "SELECT ID FROM players ORDER BY RANDOM() LIMIT 4;";
        SqliteDataReader reader = dbConnection.DownloadQuery(query);
        List<int> clientsIDs = new List<int>();

        while (reader.Read())
            clientsIDs.Add(reader.GetInt32(0));

        reader.Close();
        reader = null;

        List<ClientObject> listOfSelectedAthletes = ClientData.GetSelectedPlayersDetails(dbConnection, clientsIDs);

        Assert.AreEqual(4, listOfSelectedAthletes.Count);

        foreach (ClientObject client in listOfSelectedAthletes)
        {
            Assert.NotNull(client.Age);
            Assert.NotNull(client.AgentID);
            Assert.NotNull(client.Character);
            Assert.NotNull(client.College);
            Assert.NotNull(client.Contract.GotMoney);
            Assert.NotNull(client.Contract.GuaranteedMoney);
            Assert.NotNull(client.Contract.Length);
            Assert.NotNull(client.Contract.Value);
            Assert.NotNull(client.Contract.YearOfSign);
            Assert.NotNull(client.ContractWithAgent);
            Assert.NotNull(client.Dept);
            Assert.NotNull(client.FirstName);
            Assert.NotNull(client.HasAgent);
            Assert.NotNull(client.ID);
            Assert.NotNull(client.InjuryName);
            Assert.NotNull(client.InjuryTime);
            Assert.NotNull(client.IsFavorite);
            Assert.NotNull(client.LastName);
            Assert.NotNull(client.OverallSkill);
            Assert.NotNull(client.Popularity);
            Assert.NotNull(client.Position);
            Assert.NotNull(client.Prospectiveness);
            Assert.NotNull(client.ReadyStatus);
            Assert.NotNull(client.Relation);
            Assert.NotNull(client.Sport);
            Assert.NotNull(client.Team);
            Assert.NotNull(client.Type);
            Assert.NotNull(client.Wealth);
        }
    }

    [Test]
    public void DB_GetAllAthletesDetails_CheckWasDataDownloaded_WasDownloaded()
    {
        List<ClientObject> listOfAllAthletes = ClientData.GetAllAthletesDetails(dbConnection);

        foreach (ClientObject client in listOfAllAthletes)
        {
            Assert.NotNull(client.Age);
            Assert.NotNull(client.AgentID);
            Assert.NotNull(client.Character);
            Assert.NotNull(client.College);
            Assert.NotNull(client.Contract.GotMoney);
            Assert.NotNull(client.Contract.GuaranteedMoney);
            Assert.NotNull(client.Contract.Length);
            Assert.NotNull(client.Contract.Value);
            Assert.NotNull(client.Contract.YearOfSign);
            Assert.NotNull(client.ContractWithAgent);
            Assert.NotNull(client.Dept);
            Assert.NotNull(client.FirstName);
            Assert.NotNull(client.HasAgent);
            Assert.NotNull(client.ID);
            Assert.NotNull(client.InjuryName);
            Assert.NotNull(client.InjuryTime);
            Assert.NotNull(client.IsFavorite);
            Assert.NotNull(client.LastName);
            Assert.NotNull(client.OverallSkill);
            Assert.NotNull(client.Popularity);
            Assert.NotNull(client.Position);
            Assert.NotNull(client.Prospectiveness);
            Assert.NotNull(client.ReadyStatus);
            Assert.NotNull(client.Relation);
            Assert.NotNull(client.Sport);
            Assert.NotNull(client.Team);
            Assert.NotNull(client.Type);
            Assert.NotNull(client.Wealth);
        }
    }

    [Test]
    public void DB_DeletePlayer_PlayerHasNoAgent_WasDeleted()
    {
        dbConnection.StartTransaction();

        string query = "SELECT ID FROM players WHERE Agent = -1 LIMIT 1;";
        SqliteDataReader reader = dbConnection.DownloadQuery(query);
        reader.Read();
        int clientID = reader.GetInt32(0);

        reader.Close();
        reader = null;

        ClientData.DeletePlayer(dbConnection, clientID);

        query = "SELECT ID FROM players WHERE ID = " + clientID + ";";
        reader = dbConnection.DownloadQuery(query);

        bool doesPlayerExist = reader.HasRows;
        reader.Close();
        reader = null;

        Assert.False(doesPlayerExist);
    }

    [Test]
    public void DB_DeletePlayer_PlayerHasAnyAgent_WasDeleted()
    {
        dbConnection.StartTransaction();

        string query = "SELECT ID, Agent FROM players WHERE Agent NOT IN(1,-1) LIMIT 1;";
        SqliteDataReader reader = dbConnection.DownloadQuery(query);
        reader.Read();
        int clientID = reader.GetInt32(0);
        int agentID = reader.GetInt32(1);

        reader.Close();
        reader = null;

        ClientData.DeletePlayer(dbConnection, clientID);

        query = "SELECT ID FROM players WHERE ID = " + clientID + ";";
        reader = dbConnection.DownloadQuery(query);

        bool doesPlayerExist = reader.HasRows;
        reader.Close();
        reader = null;

        query = "SELECT Clients FROM agents WHERE ID = " + agentID + ";";
        reader = dbConnection.DownloadQuery(query);
        reader.Read();

        string clientsStringGroup = reader.GetString(0);

        reader.Close();
        reader = null;

        Assert.False(GroupStringManager.IsPersonInGroup(clientsStringGroup, clientID.ToString()));
        Assert.False(doesPlayerExist);
    }

    [Test]
    public void DB_DeletePlayer_PlayerHasPlayerAsAgent_WasDeleted()
    {
        dbConnection.StartTransaction();

        string query = "SELECT ID FROM players WHERE Agent =-1 LIMIT 1;";
        SqliteDataReader reader = dbConnection.DownloadQuery(query);
        reader.Read();
        int clientID = reader.GetInt32(0);

        reader.Close();
        reader = null;

        query = "UPDATE players SET Agent = 1 WHERE ID = " + clientID + ";" +
                "UPDATE agents SET Clients = '+" + clientID + "' WHERE ID = 1;";

        dbConnection.ModifyQuery(query);

        ClientData.DeletePlayer(dbConnection, clientID);

        query = "SELECT ID FROM players WHERE ID = " + clientID + ";";
        reader = dbConnection.DownloadQuery(query);

        bool doesPlayerExist = reader.HasRows;
        reader.Close();
        reader = null;

        query = "SELECT Clients FROM agents WHERE ID = 1;";
        reader = dbConnection.DownloadQuery(query);
        reader.Read();

        string clientsStringGroup = reader.GetString(0);

        reader.Close();
        reader = null;

        Assert.False(GroupStringManager.IsPersonInGroup(clientsStringGroup, clientID.ToString()));
        Assert.False(doesPlayerExist);
    }
}
