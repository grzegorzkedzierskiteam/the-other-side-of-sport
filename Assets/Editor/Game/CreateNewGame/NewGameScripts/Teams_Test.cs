﻿using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
using Moq;

public class Teams_Test {

	[Test]
	public void GetFootballTeams_ReturnsArrayOf32Elements() {
        TeamDetails[] teams = Teams.GetFootballTeams();
        Assert.AreEqual(32, teams.Length);
    }

    [Test]
    public void GetNumberRandomTeams_Expect5RandomTeams_ReturnsArrayOf5Elements()
    {
        TeamDetails[] gotTeams = Teams.GetNumberRandomTeams(5, Teams.GetFootballTeams());
        Assert.AreEqual(5, gotTeams.Length);
    }

    [Test]
    public void GetNumberRandomTeams_Expect0RandomTeams_ReturnsEmptyArray()
    {
        TeamDetails[] gotTeams = Teams.GetNumberRandomTeams(0, Teams.GetFootballTeams());
        Assert.AreEqual(0, gotTeams.Length);
    }
}
