﻿using Mono.Data.Sqlite;
using NUnit.Framework;

public class Money_Test
{

    private IDBConnection dbConnection;

    [OneTimeSetUp]
    public void Init()
    {
        dbConnection = new DBConnection();
    }

    [OneTimeTearDown]
    public void Disconnect()
    {
        dbConnection.CloseConnection();
        dbConnection = null;
    }

    [Test]
    public void GetShortMoneyString_Minus4000000_ReturnsCorrectlyString()
    {
        int money = -4000000;

        string moneyString = Money.GetShortMoneyString(money);

        Assert.AreEqual(moneyString, "-$4M");
    }

    [Test]
    public void GetShortMoneyString_Minus10300_ReturnsCorrectlyString()
    {
        int money = -10300;

        string moneyString = Money.GetShortMoneyString(money);

        Assert.AreEqual(moneyString, "-$11k");
    }

    [Test]
    public void GetShortMoneyString_Minus7800_ReturnsCorrectlyString()
    {
        int money = -7800;

        string moneyString = Money.GetShortMoneyString(money);

        Assert.AreEqual(moneyString, "-$8k");
    }

    [Test]
    public void GetShortMoneyString_Minus5000_ReturnsCorrectlyString()
    {
        int money = -5000;

        string moneyString = Money.GetShortMoneyString(money);

        Assert.AreEqual(moneyString, "-$5k");
    }

    [Test]
    public void GetShortMoneyString_Minus500_ReturnsCorrectlyString()
    {
        int money = -500;

        string moneyString = Money.GetShortMoneyString(money);

        Assert.AreEqual(moneyString, "-$500");
    }

    [Test]
    public void GetShortMoneyString_500_ReturnsCorrectlyString()
    {
        int money = 500;

        string moneyString = Money.GetShortMoneyString(money);

        Assert.AreEqual(moneyString, "$500");
    }

    [Test]
    public void GetShortMoneyString_5000_ReturnsCorrectlyString()
    {
        int money = 5000;

        string moneyString = Money.GetShortMoneyString(money);

        Assert.AreEqual(moneyString, "$5k");
    }


    [Test]
    public void GetShortMoneyString_7800_ReturnsCorrectlyString()
    {
        int money = 7800;

        string moneyString = Money.GetShortMoneyString(money);

        Assert.AreEqual(moneyString, "$8k");
    }

    [Test]
    public void GetShortMoneyString_10300_ReturnsCorrectlyString()
    {
        int money = 10300;

        string moneyString = Money.GetShortMoneyString(money);

        Assert.AreEqual(moneyString, "$11k");
    }

    [Test]
    public void GetShortMoneyString_4000000_ReturnsCorrectlyString()
    {
        int money = 4000000;

        string moneyString = Money.GetShortMoneyString(money);

        Assert.AreEqual(moneyString, "$4M");
    }

    [Test]
    public void AthleteWastesMoney_PlayerHasEnoughMoney_WealthWasDecreased()
    {
        dbConnection.StartTransaction();

        string query = "SELECT ID FROM players WHERE Wealth > 1000 AND Dept = 0 LIMIT 1;";
        SqliteDataReader reader = dbConnection.DownloadQuery(query);
        reader.Read();

        int id = reader.GetInt32(0);

        reader.Close();
        reader = null;

        IClientObject athlete = ClientData.GetAllDetails(dbConnection, id);
        int wastedMoney = 100;
        int expectedWealth = athlete.Wealth - wastedMoney;

        Money.AthleteWastesMoney(dbConnection, wastedMoney, athlete);

        IClientObject updatedAthlete = ClientData.GetAllDetails(dbConnection, id);

        Assert.AreEqual(expectedWealth, athlete.Wealth);
        Assert.AreEqual(expectedWealth, updatedAthlete.Wealth);
        Assert.AreEqual(0, athlete.Dept);
        Assert.AreEqual(0, updatedAthlete.Dept);
    }

    [Test]
    public void AthleteWastesMoney_PlayerDoesNotHaveEnoughMoney_WealthWasDecreasedAndDeptWasIncreased()
    {
        dbConnection.StartTransaction();

        string query = "SELECT ID FROM players WHERE Wealth < 100000 AND Dept = 0 LIMIT 1;";
        SqliteDataReader reader = dbConnection.DownloadQuery(query);
        reader.Read();

        int id = reader.GetInt32(0);

        reader.Close();
        reader = null;

        IClientObject athlete = ClientData.GetAllDetails(dbConnection, id);
        int wastedMoney = 1000000;
        int expectedWealth = 0;
        int expectedDept = athlete.Dept + wastedMoney - athlete.Wealth;

        Money.AthleteWastesMoney(dbConnection, wastedMoney, athlete);

        IClientObject updatedAthlete = ClientData.GetAllDetails(dbConnection, id);

        Assert.AreEqual(expectedWealth, athlete.Wealth);
        Assert.AreEqual(expectedWealth, updatedAthlete.Wealth);
        Assert.AreEqual(expectedDept, athlete.Dept);
        Assert.AreEqual(expectedDept, updatedAthlete.Dept);
    }
}
