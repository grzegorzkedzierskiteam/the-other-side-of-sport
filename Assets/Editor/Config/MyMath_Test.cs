﻿using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;

public class MyMath_Test {

	[Test]
	public void GetIncreaseValueInTheRange_Max10Value5_Returns6() {
        Assert.AreEqual(6, MyMath.GetIncreasedValueInTheRange(10, 5));
	}

    [Test]
    public void GetIncreaseValueInTheRange_Max22Value21_Returns22()
    {
        Assert.AreEqual(22, MyMath.GetIncreasedValueInTheRange(22, 21));
    }

    [Test]
    public void GetIncreaseValueInTheRange_Max4Value4_Returns4()
    {
        Assert.AreEqual(4, MyMath.GetIncreasedValueInTheRange(4, 4));
    }

    [Test]
    public void GetDecreaseValueInTheRange_Min4Value8_Returns7()
    {
        Assert.AreEqual(7, MyMath.GetDecreasedValueInTheRange(3, 8));
    }

    [Test]
    public void GetDecreaseValueInTheRange_Min4Value5_Returns4()
    {
        Assert.AreEqual(4, MyMath.GetDecreasedValueInTheRange(4, 5));
    }

    [Test]
    public void GetDecreaseValueInTheRange_Min2Value2_Returns2()
    {
        Assert.AreEqual(2, MyMath.GetDecreasedValueInTheRange(2, 2));
    }
}
